#!/bin/bash

cd ~/ug/03_semestr/4_Adam_SystOperac/03_systemy/Lab2

rm -r Pulpit/*

mkdir Pulpit/test1 Pulpit/test2 Pulpit/kopia

cd Pulpit/test2

mkdir ../test1/test3

cd ../test1/test3

touch ../info1

echo "Wewnatrz info1">../info1

cd ../../kopia

touch ../test1/info2

echo "Wewnatrz info2">../test1/info2

cd ../test2

cp -r ../test1/* ../kopia/

cd ../kopia

rm ../test1/info*

cd ../test1

mv ../kopia/* .

cd ~

tree ~/ug/03_semestr/4_Adam_SystOperac/03_systemy/Lab2/Pulpit

cat ~/ug/03_semestr/4_Adam_SystOperac/03_systemy/Lab2/Pulpit/test1/info1

cat ~/ug/03_semestr/4_Adam_SystOperac/03_systemy/Lab2/Pulpit/test1/info2