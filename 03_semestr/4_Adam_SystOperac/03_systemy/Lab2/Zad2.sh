#!/bin/bash

cd ~/ug/03_semestr/4_Adam_SystOperac/03_systemy/Lab2

rm -r Pulpit/*

mkdir Pulpit/test_a

mkdir Pulpit/test_b

mkdir Pulpit/test_c

touch Pulpit/test_a/plik_a

touch Pulpit/test_b/plik_b

touch Pulpit/test_c/plik_c

echo "a">Pulpit/test_a/plik_a

echo "bb">Pulpit/test_b/plik_b

echo "ccc">Pulpit/test_c/plik_c

mkdir Pulpit/kopia

cp -r Pulpit/t* Pulpit/kopia

mkdir Pulpit/move1

mv Pulpit/t* Pulpit/move1/

tree

cat Pulpit/kopia/test_a/plik_a

cat Pulpit/kopia/test_b/plik_b

cat Pulpit/kopia/test_c/plik_c

cat Pulpit/move1/test_a/plik_a

cat Pulpit/move1/test_b/plik_b

cat Pulpit/move1/test_c/plik_c
