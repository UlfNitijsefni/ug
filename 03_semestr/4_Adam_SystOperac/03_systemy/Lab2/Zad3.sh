#!/bin/bash
cd ~/ug/03_semestr/4_Adam_SystOperac/03_systemy/Lab2

rm -r Pulpit

mkdir Pulpit

mkdir Pulpit/test

touch Pulpit/test/info1

echo "zawartosc info1">Pulpit/test/info1

touch Pulpit/test/info2

echo "zawartosc info2">Pulpit/test/info2

mkdir Pulpit/kopia

cd Pulpit/test

cp info2 ../kopia

cd ../kopia

mv info2 info3

echo "Test">info3

cat info3

cp info3 ../test

mv ../test/info3 ../test/info4

cd ~

tree ~/ug/03_semestr/4_Adam_SystOperac/03_systemy/Lab2/Pulpit

cat ~/ug/03_semestr/4_Adam_SystOperac/03_systemy/Lab2/Pulpit/test/info1

cat ~/ug/03_semestr/4_Adam_SystOperac/03_systemy/Lab2/Pulpit/test/info4