#include <stdio.h>
#include <stdlib.h>

void main()
{
    int x = 2;
    int y = 0;
    /*
    asm volatile(
        ".intel_syntax noprefix;"
        "mov eax,%1;"
        "add eax,eax;"
        "mov %0,eax;"
        ".att_syntax prefix;"
        : "=r" (y)
        : "r" (x)
        : "eax"
    );*/

    asm volatile(
        ".intel_syntax noprefix;"
        "mov eax,%1;"
        "add eax,eax;"
        "mov %0,eax;"
        ".att_syntax noprefix;"
        : "=r"(y)
        : "r"(x)
        : "eax");

    printf("Oryginalna liczba: %i\nPodwojona: %i\n\n", x, y);
}