#include <stdio.h>
#include <stdlib.h>

int main()
{
    int x = 2019;
    int y = 0;

    //Dzięki "volatile" nie będzie optymalizowany tekst w nawiasach
    asm volatile(
        ".intel_syntax noprefix;"
        "mov eax,%1;"
        "add eax,eax;"
        //"mov %0,eax;"
        ".att_syntax prefix;"
        : "=r"(y) //output
        : "r"(x)  //input
        : "eax"   //side effects (changed registers)
    );
    printf("Stare x = %i\nNowe y = %i\n", x, y);
    printf("A"
           "B\n\n");
    return 0;
}

/*

    : "=r" (y)  <= %0
    : "r" (x)   <= %1
    : "eax"


*/