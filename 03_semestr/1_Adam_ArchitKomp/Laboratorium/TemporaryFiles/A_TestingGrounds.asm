.686
.model flat

public _findRegex

.code
		_findRegex PROC
	;------------------------------------------
	start:
		mov eax, [esp + 8]
		mov eax, [esp + 4]
		mov ebx, eax
		xor ecx, ecx
		xor edx, edx

		jmp overit
	looptop:
		inc eax
		inc ecx
	overit:
		cmp [eax], BYTE PTR 0
		jnz looptop
		jmp foundLength
			
	foundLength:
		cmp ecx, BYTE PTR 5
			jl error
		mov eax, [esp + 8]
		jmp restartFunctionOnThisChar

	;------------------------------------------
	restartFunctionOnThisChar:
		cmp ebx, 0
			je error
		cmp ebx, 'a'
			je secondChar
		cmp ebx, 'b'
			je secondChar
		cmp ebx, 'd'
			je secondChar
		inc ebx
		jmp restartFunctionOnThisChar
		
	secondChar:
		inc ebx
		cmp ebx, 0
			je error
		cmp ebx, 'a'
			je restartFunctionOnThisChar
		cmp ebx, 'b'
			je restartFunctionOnThisChar
		cmp ebx, 'c'
			je restartFunctionOnThisChar
		inc ebx
		cmp ebx, 0
			je error

	waitForFirstC:
		inc ebx
		cmp ebx, 0
			je error
		cmp ebx, 'c'
			jne waitForFirstC
		inc ebx
		cmp ebx, 0
			je error
		jmp waitForSecondC

	waitForSecondC:
		mv ecx, 0	;ecx is now our iterator;Check if it doesn't interfere with rest of the code
		cmp'c', ebx
			je printing
		jmp restartFunctionOnThisChar	



	;------------------------------------------
	error:
		jmp returning

	printing:
		cmp ecx, eax   ;eax is the number of times we want to print
			jge returning	
		;print the string you found
		inc ecx
		jmp printing

	returning:
		ret

	; ================================
		mov esp, ebp
		pop ebp
		ret
		_findRegex ENDP
END


;===========================================================
		;mov dl,'A' ; print 'A'
		;mov ah, 'A'
		;mov ah, 06
		;int 21h

		;mov eax, [esp + 4]
		;;mov eax, %1
		;mov eax, 1
		;;mov ebx, eax
		;xor ecx, ecx