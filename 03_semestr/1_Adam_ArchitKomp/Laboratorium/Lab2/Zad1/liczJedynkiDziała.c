#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    printf("Podaj liczbe: ");
    unsigned int x;
    scanf("%i", &x);
    unsigned int y;

    asm volatile(

        ".intel_syntax noprefix;"

        "mov eax, %1;"

        "mov ebx, 0;"  // ebx = długość aktualnego ciągu
        "mov ecx, 32;" //ecx = iterator
        "mov edx, 0;"  //edx = max

        "highLoop:"     // Sprawdzanie bitów
        "sub ecx, 1;" // Dekrementacja iteratora
        "shr eax, 1;"
        "jc doAdd;"
        "jmp comparision;"

        // ------------------szukanie jedynki
        "doAdd:"
        "add ebx, 1;"
        "jmp incrLoop;"

        // ---------------- - znajdujemy zero
        "comparision:" // 0 => czy aktualny ciąg jest dłuższy niż poprzedni
        "cmp ebx, edx;"
        "jl doZero;"
        "mov edx, ebx;"

        "doZero:"
        "mov ebx, 0;"
        // ----------------

        "incrLoop:"
        "cmp ecx, 0;" // Czy pętla ma dalej działać
        "jnz highLoop;"

        "mov %0, edx;"

        ".att_syntax prefix;"
        : "=r"(y)
        : "r"(x)
        : "eax", "ebx", "ecx", "edx");

    printf("Wartosc x = %i, wartosc y = %i\n", x, y);
    return 0;
}