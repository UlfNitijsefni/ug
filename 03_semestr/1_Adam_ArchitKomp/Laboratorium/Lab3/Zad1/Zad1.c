#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    unsigned int x = 0xFF0FFF0F;
    //unsigned int x = 0x00000000;
    //unsigned int x = 0xFFFFFFFF;
    int y;

    asm volatile(

        ".intel_syntax noprefix;"

        "mov eax, %1;"

        "mov ebx, 0;"  // ebx = długość aktualnego ciągu
        "mov ecx, 33;" //ecx = iterator
        "mov edx, 0;"  //edx = aktualna max długość

        "doLoop:"     // sprawdzamy bity danych wejściowych
        "sub ecx, 1;" // iterator = iterator - 1
        "shr eax, 1;"
        "jc foundOne;"
        "jnc foundZero;"

        // ------------------znajdujemy jedynkę
        "foundOne:"
        "add ebx, 1;"
        "jmp doIncrement;"

        // ---------------- - znajdujemy zero
        "foundZero:" // jeżeli znajdziemy 0 to sprawdzamy czy aktualny ciąg > max
        "cmp ebx, edx;"
        "jl doZero;"
        "mov edx, ebx;"

        "doZero:"
        "mov ebx, 0;"
        // ----------------

        "doIncrement:"
        "cmp ecx, 0;" // sprawdzamy czy pętla ma się jeszcze wykonywać
        "jnz doLoop;"

        "mov %0, edx;"

        ".att_syntax prefix;"
        : "=r"(y)
        : "r"(x)
        : "eax", "ebx", "ecx", "edx");

    printf("Wartosc x = %u, największa długość = %i\n", x, y);
    return 0;
}