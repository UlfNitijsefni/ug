#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    //printf("Podaj liczbe: ");
    unsigned int x = 0x00000000;
    //scanf("%i", &x);
    int y;

    asm volatile(

        ".intel_syntax noprefix;"

        "mov eax, %1;"

        "mov ebx, 0;"  // ebx to długość aktualnego ciągu jedynek
        "mov ecx, 33;" //ecx to iterator pętli sprawdzającej
        "mov edx, 0;"  //edx to aktualna największa znaleziona wartość

        "doLoop:"     // sprawdzamy po koleji bity danych wejściowych
        "sub ecx, 1;" // zmniejszamy iterator o jeden
        "shr eax, 1;"
        "jc doAdd;"
        "jmp doCompare;"

        // ------------------znajdujemy jedynkę
        "doAdd:"
        "add ebx, 1;"
        "jmp doIncrement;"

        // ---------------- - znajdujemy zero
        "doCompare:" // jeżeli znajdziemy 0 to sprawdzamy czy aktualny ciąg jest dłuższy od poprzedniego najdłuższego
        "cmp ebx, edx;"
        "jl doZero;"
        "mov edx, ebx;"

        "doZero:"
        "mov ebx, 0;"
        // ----------------

        "doIncrement:"
        "cmp ecx, 0;" // sprawdzamy czy pętla ma się jeszcze wykonywaC
        "jnz doLoop;"

        "mov %0, edx;"

        ".att_syntax prefix;"
        : "=r"(y)
        : "r"(x)
        : "eax", "ebx", "ecx", "edx");

    printf("Wartosc x = %u, wartosc y = %i\n", x, y);
    return 0;
}