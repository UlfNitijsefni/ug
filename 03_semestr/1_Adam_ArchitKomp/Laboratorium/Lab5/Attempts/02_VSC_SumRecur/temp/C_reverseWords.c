// #include <stdio.h>
// #include <stdlib.h>

// int main(int argc, char **argv)
// {
//     int n = 10;

//     asm volatile(
//         ".intel_syntax noprefix;"

//         "mov eax, %0;"

//         "_fibonacci PROC;"

//         "push    ebp;"
//         "mov ebp, esp;"
//         //;================================

//         "mov ecx, DWORD PTR 0;"
//         "mov edx, eax;"
//         "mov eax, DWORD PTR 1;"
//         "mov ebx, DWORD PTR 1;"
//         "push DWORD PTR 1;"

//         "call fibonacci_recur;"

//         "sub esp, 4;"

//         //;================================
//         "mov esp, ebp;"
//         "pop ebp;"
//         "RET;"

//         "_fibonacci ENDP;"

//         //;================================
//         "fibonacci_recur PROC;"

//         "inc ecx;"
//         "cmp ecx, edx;"
//         "jg returning;"

//         "mov eax, [esp + 4];"

//         "cmp ecx, DWORD PTR 3;"
//         "jge biggerThanThree;"

//         "push ebx;"
//         "call fibonacci_recur;"
//         "jmp ESPback;"

//         "biggerThanThree:;"
//         "add eax, ebx;"
//         "push ebx;"
//         "mov ebx, eax;"
//         "call fibonacci_recur;"

//         "ESPback:;"
//         "add esp, 4;"

//         "returning:;"
//         "RET;"

//         "fibonacci_recur ENDP;"
//         //;================================

//         ".att_syntax prefix;"
//         :
//         : "r"(n)
//         : "eax", "ebx", "ecx", "edx");

//     printf("\nWynik: \n%d\n\n", n);
//     return 0;
// }

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    int n = 10;

    asm volatile(
        ".intel_syntax noprefix;"

        "mov eax, %0;"

        "_fibonacci PROC;"

        "push    ebp;"
        "mov ebp, esp;"
        //;================================

        "mov ecx, DWORD PTR 0;"
        "mov edx, eax;"
        "mov eax, DWORD PTR 1;"
        "mov ebx, DWORD PTR 1;"
        "push DWORD PTR 1;"

        "call fibonacci_recur;"

        "sub esp, 4;"

        //;================================
        "mov esp, ebp;"
        "pop ebp;"
        "RET;"

        "_fibonacci ENDP;"

        //;================================
        "fibonacci_recur PROC;"

        "inc ecx;"
        "cmp ecx, edx;"
        "jg returning;"

        "mov eax, [esp + 4];"

        "cmp ecx, DWORD PTR 3;"
        "jge biggerThanThree;"

        "push ebx;"
        "call fibonacci_recur;"
        "jmp ESPback;"

        "biggerThanThree:;"
        "add eax, ebx;"
        "push ebx;"
        "mov ebx, eax;"
        "call fibonacci_recur;"

        "ESPback:;"
        "add esp, 4;"

        "returning:;"
        "RET;"

        "fibonacci_recur ENDP;"
        //;================================

        ".att_syntax prefix;"
        :
        : "r"(n)
        : "eax", "ebx", "ecx", "edx");

    printf("\nWynik: \n%d\n\n", n);
    return 0;
}