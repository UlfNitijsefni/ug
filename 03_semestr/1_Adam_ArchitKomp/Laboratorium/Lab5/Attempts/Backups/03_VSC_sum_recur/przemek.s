.intel_syntax noprefix

.global main

.data
    N: .int 2
    messg: .asciz "Suma szeregu to %d\n"

.text

main:
    //suma szeregu od 0 do N, rekurencyjnie
    mov eax, [N]
    push eax
    call suma
    add esp, 4
    call drukuj

suma:
    cmp eax, 0
    je koniec
    dec eax
    push eax
    call suma
    add esp, 4
    add eax, [esp+4]
    koniec:
    ret

drukuj:
    push eax
    push offset messg
    call printf
    add esp, 8
    ret


