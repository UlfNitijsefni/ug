.intel_syntax noprefix

.global main

.data
    N: .int 8
    msg: .asciz "n-ty wyraz: %d\n"

.text

main:
	mov edx, [N]
	mov ecx, DWORD PTR 0

	mov eax, DWORD PTR 1
	mov ebx, DWORD PTR 1
	push DWORD PTR 1

	call fibonacci_recur

	add esp, 4
	call drukuj


fibonacci_recur:
	inc ecx
	cmp ecx, edx
	jg returning

	mov eax, [esp + 4]

	cmp ecx, DWORD PTR 3
	jge biggerThanThree

	push ebx
	call fibonacci_recur
	jmp ESPback

	biggerThanThree:
		add eax, ebx
		push ebx
		mov ebx, eax
		call fibonacci_recur

	ESPback:
		add esp, 4

	returning:
		ret


drukuj:
    push eax
    push offset msg
    call printf
    add esp, 8
    ret
