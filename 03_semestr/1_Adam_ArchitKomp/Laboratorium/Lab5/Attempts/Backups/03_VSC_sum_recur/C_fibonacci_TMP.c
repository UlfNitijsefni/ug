#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    int n;
    int fib_no = 2;
    printf("Wpisz numer wyrazu ciagu Fibonacciego:\n");
    scanf("%d", &fib_no);
    int nn = fib_no;

    asm(
        ".intel_syntax noprefix;"

        "mov edx, %0;"
        "mov ecx, DWORD PTR 0;"

        "mov eax, DWORD PTR 1;"
        "mov ebx, DWORD PTR 1;"
        "push DWORD PTR 1;"

        "call fibonacci_recur;"

        "add esp, 4;"
        "call drukuj;"


        "fibonacci_recur:"
            "inc ecx;"
            "cmp ecx,edx;"
            "jg returning;"

            "mov eax,[esp + 4];"

            "cmp ecx, DWORD PTR 2;"
            "jle next_fib;"

            "add eax, ebx;"

            "next_fib:"
                "push ebx;"
                "mov ebx, eax;"
                "call fibonacci_recur;"
                "add esp, 4;"

            "returning:"
                "ret;"

        "drukuj: "
            "ret;"

        "mov %0, eax;"

          ".att_syntax prefix;"
        : "=r"(n)
        : "r"(nn)
        : "eax", "ebx", "ecx", "edx");

    printf("\nWynik: \n%d\n\n", n);
    return 0;
}