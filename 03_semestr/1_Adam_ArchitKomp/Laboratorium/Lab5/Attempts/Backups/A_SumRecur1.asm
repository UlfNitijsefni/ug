.686
.model flat

public _fibonacci

; ===========================================================================================================================
.code
_fibonacci PROC

		push    ebp			; umieszczenie wskaznika stosu sprzed wywolania ponizszego kodu w ebp
		mov ebp, esp		; przesuniecie wartosci w esp do ebp (zamaze to co w ebp)
		;================================

		mov ecx, eax
		call fibonacci_recur

		;================================
		mov esp, ebp			; wczytanie do ebp zapamietanej na poczatku wartosci
		pop ebp					; zdjecie ze stosu wskaznika - to adres wywolania ktore nastapi po zakonczeniu tutejszej funkcji
		ret						; ?? nie wiem po co to :)
_fibonacci ENDP

; ===========================================================================================================================
fibonacci_recur PROC

		cmp ecx, DWORD PTR 1
		je ifOne

		cmp ecx, DWORD PTR 2
		je ifTwo
		dec ecx
		call fibonacci_recur

		;mov ecx, eax
		;dec ecx
		;call _fibonacci

		ifOne:
			push DWORD PTR 1
			pop eax
			;mv ecx, ebx
			jmp endOfFunction

		ifTwo:
			push DWORD PTR 1
			;pop eax
			dec ecx
			call fibonacci_recur
			;jmp endOfFunction
			jmp forTwoEndOfFunction

		;mov ebx, DWORD PTR 1
		;push [ebx]
		;push [ebx]
		;pop [eax]
		;mov [ebx], [eax]
		;pop [eax]
		;add [eax], [ebx]
		;push [ebx]
		;push [eax]

		lastEndOfFunction:
		add esp, 12
		jmp returning

		forTwoEndOfFunction:
		add esp, 8
		jmp returning

		endOfFunction:
		;mov eax, 0
		;add esp, 8

		returning:
		RET

fibonacci_recur ENDP

;================================


END
            