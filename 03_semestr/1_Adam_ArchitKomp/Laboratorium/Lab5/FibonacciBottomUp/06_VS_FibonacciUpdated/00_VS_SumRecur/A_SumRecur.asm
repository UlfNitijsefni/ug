.686
.model flat

public _fibonacci

;WERSJA Z REKURENCJ� NA POCZ�TKU
; ===========================================================================================================================
.code

_fibonacci PROC

		push    ebp
		mov ebp, esp1`
		;================================

		mov ecx, DWORD PTR 0	;
		mov edx, eax			;iterator
		mov eax, DWORD PTR 0	;posredni wynik i na koncu ostateczny
		mov ebx, DWORD PTR 1
		;push DWORD PTR 1

		call fibonacci_recur
		;================================
		mov esp, ebp
		pop ebp
		RET

_fibonacci ENDP

; ===========================================================================================================================
fibonacci_recur PROC

	cmp edx, 2
	jle lessThanThree
	jmp biggerThanTwo

	lessThanThree:
		mov eax, 1
		jmp returning

	biggerThanTwo:
		dec edx
		call fibonacii_recur
		push eax

		dec edx
		call fibonacci_recur

		add eax, [esp]
		add esp, 4

	returning:
		RET

fibonacci_recur ENDP
;================================


END