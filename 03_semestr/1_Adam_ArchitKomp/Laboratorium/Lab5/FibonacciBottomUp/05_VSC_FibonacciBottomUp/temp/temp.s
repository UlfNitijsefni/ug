.686
.model flat

public _fibonacci_element

.code
	_fibonacci_element PROC

		; ===================================================================

		mov ecx, eax
		call giveElem_rec
		ret

		; ===================================================================

		giveElem_rec:

			push ecx

			cmp ecx, 2
			jg sumElems

			mov eax, 1
			jmp printAnswer

		sumElems:
			dec ecx
			call giveElem_rec
			push eax

			dec ecx
			call giveElem_rec
			add eax, [esp]
			add esp, 4


		printAnswer:
			pop ecx
			ret

		; ===================================================================
       
	_fibonacci_element ENDP

END