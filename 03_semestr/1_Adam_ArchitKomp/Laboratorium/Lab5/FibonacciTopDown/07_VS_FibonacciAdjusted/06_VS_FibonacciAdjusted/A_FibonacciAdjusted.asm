.686
.model flat

public _fibonacci

;WERSJA Z REKURENCJ� NA POCZ�TKU
; ===========================================================================================================================
.code

_fibonacci PROC

		push    ebp
		mov ebp, esp
		;================================

		mov ecx, eax			;iterator
		mov eax, DWORD PTR 0	;posredni wynik i na koncu ostateczny

		call fibonacci_recur
		;================================
		mov esp, ebp
		pop ebp
		RET

_fibonacci ENDP

; ===========================================================================================================================
fibonacci_recur PROC

	push ecx

	cmp ecx, 2
	jg biggerThanTwo

	lessThanThree:
		mov eax, 1
		jmp returning

	biggerThanTwo:
		dec ecx
		call fibonacci_recur
		push eax

		dec ecx
		call fibonacci_recur

		add eax, [esp]
		add esp, 4

	returning:
		pop ecx
		RET

fibonacci_recur ENDP
;================================


END