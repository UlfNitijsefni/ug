.intel_syntax noprefix

.global main

.data
    N: .int 8
    msg: .asciz "n-ty wyraz: %d\n\n"

.text

main:

    mov ecx, [N]
    mov eax, DWORD PTR 0

    call fibonacci_recur
    call drukuj

fibonacci_recur:

	push ecx

	cmp ecx, 2
	jg biggerThanTwo

	lessThanThree:
		mov eax, 1
		jmp returning

	biggerThanTwo:
		dec ecx
		call fibonacci_recur
		push eax

		dec ecx
		call fibonacci_recur

		add eax, [esp]
		add esp, 4

	returning:
		pop ecx
		RET


drukuj:
    push eax
    push offset msg
    call printf
    add esp, 8
    ret
