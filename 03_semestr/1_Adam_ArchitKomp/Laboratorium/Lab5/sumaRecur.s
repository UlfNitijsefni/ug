.intel_syntax noprefix
.global main
.text

//-----------------------------------------------
main:
 mov eax,7

//-----------------------------------------------

 push eax;
 call sumRecur;
 add esp,4;

//-----------------------------------------------

 push eax
 push offset msg
 call printf
 add esp,8

 ret

//-----------------------------------------------
sumRecur:
    mov eax,[esp+4];
    cmp eax, 0x00000000
    je returning
        dec eax
        push eax
        call sumRecur
        add esp,4
        add eax,[esp+4]

returning:
    ret
 //-----------------------------------------------

.data
msg:
.asciz "Wynik=%i\n"
