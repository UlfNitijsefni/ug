#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    //printf("Podaj liczbe: ");
    char name[] = "ABC";
    //unsigned int x = 0x00000000;
    //unsigned int x = 0xFFFFFFFF;
    //scanf("%i", &x);
    int y;
    char x;

	String toChange;

    asm volatile(

        ".intel_syntax noprefix;"

        "lea eax, name;"
        "mov ebx, 'A';"
        "mov [eax], bl;"

        ".att_syntax prefix;"
        : "=r"(y)
        : "r"(name)
        : "eax", "ebx");

    printf("Wynik: %s", name);
    return 0;
}