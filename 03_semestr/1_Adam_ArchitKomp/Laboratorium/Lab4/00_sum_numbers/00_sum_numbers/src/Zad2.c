#include <stdio.h>
#include <stdlib.h>

int main() {
    int x = 0xFFF00000;;
    int y;


//Dzięki "volatile" nie będzie optymalizowany tekst w nawiasach
asm volatile (
    ".intel_syntax noprefix;"
    // "mov eax,%1;"
    // "add eax,eax;"
    // "add eax,eax;"
    // "mov ebx,eax;"
    // "add eax,eax;"
    // "add eax,eax;"
    // "add eax,ebx;"
    // "shl eax;"
    // "mov %0,eax;"
/*
    "mov eax,%1;"
    "mov ebx, 0;"
    "sub eax,0;"
    "jz koniec;"    //Skacze do etykiety koniec gdy na eax znajduje się 1
    "petla:"        //Etykieta - może być dowolna
    "add ebx,1;"
    "shr eax;"
    "jnc petla;"
    "koniec:"
    "mov %0, ebx;"
*/

//LICZENIE JEDYNEK W LICZBIE, 0xFFF00000 DAJE 32 ZERA
    "mov eax,%1;"
    "xor ebx,ebx;"
    "mov ecx, 33;"
    "petla:"
    "shl eax;"
    "jnc skok;"
    "inc ebx;"
    "skok:"
    "and eax, eax;"
    //"dec ecx;" //dec i and działa
    "jnz petla;"
    "mov %0,ebx;"

    ".att_syntax prefix;"
    : "=r" (y) //output
    : "r" (x)  //input
    : "eax", "ebx"    //side effects (changed registers)
);
    printf("x = %i\ny = %i\n\n", x, y);
    //printf("A" "B\n\n");
    return 0;

    //Można zerować bity używając komendy "xor ebx,ebx;" <= zawsze są te same bity, więc wpisuje 0


    //"and eax,eax;" - Może być używane żeby ustawiać flagę bez zmiany bitów. Tak samo "or eac,eac;"
}

/*

    : "=r" (y)  <= %0
    : "r" (x)   <= %1
    : "eax"


*/