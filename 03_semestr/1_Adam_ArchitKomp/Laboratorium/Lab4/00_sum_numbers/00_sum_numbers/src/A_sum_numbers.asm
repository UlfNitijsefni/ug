.686
.model flat

public _sum_numbers

.code
		_sum_numbers PROC

		; zapamiętanie licznika programu ?
		; =======================================================================================
		push    ebp			; zapamietanie na stosie wartosci EBP sprzed wykonania tej funkcji
		mov ebp, esp		; wczytanie do EBP adresu wierzcholka stosu
		; =======================================================================================

		mov eax, [ebp + 8]		; wczytanie do EAX wartości aNumber umieszczonej na stosie przez kod C
		;mov ebx, [ebp + 8]		; wczytanie do EAX wartości bNumber umieszczonej na stosie przez kod C
		add eax, 0



		
		; wynik działania zostanie pobrany przez kod w C z EAX

		; przywrócenie licznika programu ?
		; =======================================================================================
		mov esp, ebp		; przywrocenie wartosci EBP sprzed wywolania tej funkcji
		pop ebp				; zdjecie ze stosu EBP - ta wartosc jest juz niepotrzebna, bo zostala wczytana do ESP
		ret					; nie wiem po co to ale wiem, ze musi byc - Google wiedza :)
		; =======================================================================================

		_sum_numbers ENDP
END