.686
.model flat

public _zamien_kolejnosc


.code
		_zamien_kolejnosc PROC


         xor ecx, ecx
         xor edx, edx

         doFindZero:
            inc ebx
            cmp [ebx], BYTE PTR 0
            jne doFindZero
            dec ebx

         doLoop:
            mov cl, [eax] 
            mov dl, [ebx] 
            mov [ebx], cl 
            mov [eax], dl 
            inc eax
            cmp eax, ebx
            je koniec
            dec ebx
            cmp eax, ebx
            jmp resetForWords//je koniec
            jmp doLoop

#            resetForWords:
#            mov eax, %0
#            mov ebx, %0
#            xor ecx, ecx
#            xor edx, edx

#            doFindSpace:
#            inc ebx
#            cmp [ebx], BYTE PTR 0
#            jne doFindSpace
#            dec ebx


        koniec:

        .att_syntax prefix

        :
        : r(inputString)
        : eax, ebx, ecx, edx)
END