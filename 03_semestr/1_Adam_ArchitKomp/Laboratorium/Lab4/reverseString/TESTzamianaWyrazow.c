#include <stdio.h>

int main()
{
    char inputString[] = "Ala ma kota";

    printf("Zdanie przed zmianą: %s\n", inputString);

    asm volatile(
        ".intel_syntax noprefix;"
         "mov eax, %0;"
         "mov ebx, %0;"
         "xor ecx, ecx;"
         "xor edx, edx;"

         "doFindZero:"
            "inc ebx;"
            "cmp [ebx], BYTE PTR 0;"
            "jne doFindZero;"
            "dec ebx;"

         "doLoop:"
            "mov cl, [eax];" 
            "mov dl, [ebx];" 
            "mov [ebx], cl;" 
            "mov [eax], dl;" 
            "inc eax;"
            "cmp eax, ebx;"
            "je koniec;"
            "dec ebx;"
            "cmp eax, ebx;"
            "jmp resetForWords;"//"je koniec;"
            "jmp doLoop;"

        "resetForWords:"
            "mov eax, %0;"
            "mov ebx, %0;"
            "xor ecx, ecx;"
            "xor edx, edx;"

        "doFindSpace:"
            "inc ebx;"
            "cmp [ebx], BYTE PTR 0;"
            "jne doFindSpace;"
            "dec ebx;"


        "koniec:"

        ".att_syntax prefix;"

        :
        : "r"(inputString)
        : "eax", "ebx", "ecx", "edx");

    printf("Zdani po zmianie: %s\n", inputString);
    return 0;
}
/*


            "cmp [ebx], " ";"
*/