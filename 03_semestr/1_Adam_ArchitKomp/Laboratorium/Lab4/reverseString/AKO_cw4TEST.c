#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	//unsigned int x = 0xFF0FFF0F;
    //unsigned int x = 0x00000000;
    //unsigned int x = 0xFFFFFFFF;
    int y;

    asm volatile(

		".intel_syntax noprefix;"

   
        ".att_syntax prefix;"
        : "=r"(y)
        : "r"(x)
        : "eax", "ebx", "ecx", "edx");

    printf("Wartosc x = %u, największa długość = %i\n", x, y);
    return 0;
}