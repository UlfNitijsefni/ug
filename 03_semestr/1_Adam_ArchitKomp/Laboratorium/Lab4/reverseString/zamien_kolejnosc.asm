.686
.model flat

public _zamien_kolejnosc


.code
		_zamien_kolejnosc PROC

			push		ebp
			mov		ebp, esp
			mov		al, [ebp + 8]			; wpisany z klawiatury znak
			mov		bl, 'a'
			cmp		al, bl
			je		male_z
			mov		bl, 'A'
			cmp		al, bl
			je		duze_z
			dec		al
			jmp		koniec

		male_z:
			mov		al, 'z'
			jmp		koniec

		duze_z:
			mov		al, 'Z'
			jmp		koniec

			koniec:

			pop		ebp
			ret
		_zamien_kolejnosc ENDP
END


