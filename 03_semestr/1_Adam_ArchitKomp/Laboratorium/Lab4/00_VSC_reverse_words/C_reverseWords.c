#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    char toChange[] = " Ala ma  kota";
	// char toChange[] = "  ";
	// char toChange[] = "";

    asm volatile(
        ".intel_syntax noprefix;"

            "mov eax, %0;"
            "mov ecx, %0;"
            "mov edx, %0;"

            
			"cmp [edx], BYTE PTR 0;"
			"jne findStringEnd;"
			"jmp koniec;"

		"findStringEnd:"
			"inc edx;"
			"cmp [edx], BYTE PTR 0;"
			"jne findStringEnd;"
			"dec edx;"

		"reverseWord1:"
			"mov bl, [ecx] ;"
			"mov bh, [edx] ;"
			"mov [edx], bl;"
			"mov [ecx], bh;"
			"inc ecx;"
			"dec edx;"
			"cmp edx, ecx;"
			"jg reverseWord1;"

			//"; --------------------------------------------;"
		"startNextWord:"
			"mov ecx, eax;"
			"mov edx, eax;"
			"jmp findNextSpace;"

		"findNextSpace:"
			"cmp [edx], BYTE PTR 0;"
			"je lastReverse;"
			"inc edx;"
			"cmp [edx], BYTE PTR 0;"
			"je lastReverse;"
			"cmp [edx], BYTE PTR ' ';"
			"jne findNextSpace;"

			//"; record space position;"
			"mov eax, edx;"
			"inc eax;"
			"dec edx;"

		"reverseWord:"
			"mov bl, [ecx] ;"
			"mov bh, [edx] ;"
			"mov [edx], bl;"
			"mov [ecx], bh;"
			"inc ecx;"
			"dec edx;"
			"cmp edx, ecx;"
			"jg reverseWord;"
			"jmp startNextWord;"

		"lastReverse:"
			"dec edx;"
			"jmp lastSwapLetters;"

		"lastSwapLetters:"
			"mov bl, [ecx] ;"
			"mov bh, [edx] ;"
			"mov [edx], bl;"
			"mov [ecx], bh;"
			"inc ecx;"
			"dec edx;"
			"cmp edx, ecx;"
			"jg lastSwapLetters;"

			"koniec:"


        ".att_syntax prefix;"
        :
        : "r"(toChange)
        : "eax", "ebx", "ecx", "edx");

    printf("\nWynik: \n%s\n\n", toChange);
    return 0;
}