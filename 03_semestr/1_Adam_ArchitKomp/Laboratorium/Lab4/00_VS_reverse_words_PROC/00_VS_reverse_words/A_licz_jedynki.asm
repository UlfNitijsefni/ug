.686
.model flat

public _count_ones

.code
		_count_ones PROC

		push    ebp			; umieszczenie wskaznika stosu sprzed wywolania ponizszego kodu w ebp
		mov ebp, esp			; przesuniecie wartosci w esp do ebp (zamaze to co w ebp)
		sub esp, 12			; cofniecie wskaznika stosu na pootrzeby tutuejszej funkcji
		; ================================

		push eax			; ZAMIAST: mov eax, %1		; // eax dana liczba do sprawdzenia

		mov ebx, 0			; ebx to d�ugo�� aktualnego ci�gu jedynek
        mov ecx, 32			; ecx to iterator p�tli sprawdzaj�cej
		mov edx, 0			; edx to aktualna najwi�ksza znaleziona warto��


        doLoop:			; // sprawdzamy po koleji bity danych wej�ciowych
        sub ecx, 1		; // zmniejszamy iterator o jeden
        shr eax, 1
		jc doAdd
		jmp doCompare

		;------------------znajdujemy jedynk�
		doAdd:
        add ebx, 1
		jmp doIncrement	

		; -----------------znajdujemy zero
		doCompare:		; je�eli znajdziemy 0 to sprawdzamy czy aktualny ci�g jest d�u�szy od poprzedniego najd�u�szego
		cmp ebx, edx
		jl doZero
		mov edx, ebx

		doZero:
		mov ebx, 0
		;----------------

		doIncrement:
        cmp ecx, 0		; sprawdzamy czy p�tla ma si� jeszcze wykonywaC
        jnz doLoop


		mov		eax, edx		; ZAMIAST mov %0, ebx

		; ================================
		mov esp, ebp			; wczytanie do ebp zapamietanej na poczatku wartosci
		pop ebp				; zdjecie ze stosu wskaznika - to adres wywolania ktore nastapi po zakonczeniu tutejszej funkcji
		ret					; ?? nie wiem po co to :)
		_count_ones ENDP
END