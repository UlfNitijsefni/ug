#include <stdio.h>
#include <stdlib.h>

void zamien_kolejnosc(char*);

int main(int argc, char **argv)
{
	char toChange[] = "STEFAN ZAPIL WIECZOREM";

	zamien_kolejnosc(toChange);

	printf("\nWynik: \n%s\n\n", toChange);
	return 0;
}