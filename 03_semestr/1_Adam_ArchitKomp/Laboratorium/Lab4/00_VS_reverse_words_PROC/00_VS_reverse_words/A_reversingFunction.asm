.686
.model flat

public _zamien_kolejnosc

.code
		_zamien_kolejnosc PROC

		push    ebp			; umieszczenie wskaznika stosu sprzed wywolania ponizszego kodu w ebp
		mov ebp, esp		; przesuniecie wartosci w esp do ebp (zamaze to co w ebp)
		;sub esp, 12			; cofniecie wskaznika stosu na pootrzeby tutuejszej funkcji
		;================================

			mov ecx, eax
			mov edx, eax

			cmp [edx], BYTE PTR 0
			jne findStringEnd
			jmp koniec

		findStringEnd:
			inc edx
			cmp [edx], BYTE PTR 0
			jne findStringEnd
			dec edx
			call swapLetters

		; --------------------------------------------

		startNextWord:
			;cmp [edx], BYTE PTR 0
			;je lastReverse
			mov ecx, eax
			mov edx, eax
			jmp findNextSpace

	    findNextSpace:
			cmp [edx], BYTE PTR 0
			je lastReverse
			inc edx
			cmp [edx], BYTE PTR 0
			je lastReverse
			cmp [edx], BYTE PTR ' '
			jne findNextSpace

			; record space position
			mov eax, edx
			inc eax
			dec edx

			; reverse word
			call swapLetters
			jmp startNextWord
				
		lastReverse:
			dec edx
			jmp lastSwapLetters

		lastSwapLetters:
			call swapLetters

		koniec:


		;================================
		mov esp, ebp			; wczytanie do ebp zapamietanej na poczatku wartosci
		pop ebp					; zdjecie ze stosu wskaznika - to adres wywolania ktore nastapi po zakonczeniu tutejszej funkcji
		ret						; ?? nie wiem po co to :)
		
		_zamien_kolejnosc ENDP


; ===========================================================================================================================
swapLetters PROC
		reverseWord:
			mov bl, [ecx] 
			mov bh, [edx] 
			mov [edx], bl
			mov [ecx], bh	
			inc ecx
			dec edx
			cmp edx, ecx
			jg reverseWord
		RET
swapLetters ENDP
; ===========================================================================================================================

END
            