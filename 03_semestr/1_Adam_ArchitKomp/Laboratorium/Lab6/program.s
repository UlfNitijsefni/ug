.intel_syntax noprefix

.global _start

.text


//============================================
_start:
    mov eax, [esp+8]
    mov ecx, [esp+12]
    jmp atoi


//--------------------------------------------
basicRepeating:
//edx - iterator
//ecx - adress of end of string
//ebx - doneOnce
//eax - adress of current letter
    cmp eax, ecx
        je regexEqEnd
    cmp [eax], BYTE PTR 'r'
        je returnToOne
    cmp [eax], BYTE PTR 'q'
        je returnToOne
    cmp [eax], BYTE PTR 'p'
        je returnToOne
    add eax, 1
    jmp basicRepeating


//--------------------------------------------
gotFirstChar:
    add eax, 1
    cmp eax, ecx
        je  popAtEnd
    cmp ebx, 0
        je firstRepeat
    cmp [eax], BYTE PTR 'q'
        je backToMainLoopOnFirstLetter
    cmp [eax], BYTE PTR 'r'
        je foundR
    jmp gotFirstChar


//--------------------------------------------
foundR:
    add eax, 1
    jmp end

//--------------------------------------------
end:
    pop ebx
    cmp ebx, ecx
        je exit
    push edx
    mov edx, eax
    mov ecx, ebx
    sub edx, ecx
    mov eax, 4
    jmp printing




//--------------------------------------------
regexEqEnd:
push    ecx
jmp     end


//--------------------------------------------
 popAtEnd:
pop     eax
push    ecx
jmp     end

returnToOne:
push    eax
mov     ebx, 0
jmp     gotFirstChar

firstRepeat:
cmp     [eax], BYTE PTR 'q'
je      backToMainLoopOnFirstLetter
cmp     [eax], BYTE PTR 'r'
je      backToMainLoopOnFirstLetter
mov     ebx, 1
jmp     gotFirstChar

backToMainLoopOnFirstLetter:
pop     eax
add     eax, 1
mov     ebx, 0
jmp     basicRepeating

//onC2:
//add     eax, 1
//jmp     end

exit:
mov     eax, 1
mov     ebx, 0
int     0x80

printing:
mov     eax, 4
push    ecx
push    edx
mov     ebx, 1
int     0x80
pop     edx
pop     ecx
pop     ebx
dec     bl
push    ebx
cmp     [esp], BYTE PTR 0
jne     printing
add     esp, 4
jmp     exit

atoi:
mov     edx, ecx
mov     dl, [edx]
sub     dl, 48
jmp     basicRepeating
