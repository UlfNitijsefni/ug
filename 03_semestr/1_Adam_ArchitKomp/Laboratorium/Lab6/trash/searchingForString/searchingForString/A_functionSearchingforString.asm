.686
.model flat

public _count_ones

.code
		_count_ones PROC

	start:	
		mv [eax], %1
		mv [ebx], [eax]
		mv [ecx], 0
	loop:
		cmp [eax, 0]
			je foundLength
		inc eax
		inc ecx
		jmp loop
	foundLength:
		cmp[ecx], 5
			jl error
		mv [eax], %2


	restartFunctionOnThisChar:
		cmp [ebx], 0
			je error
		cmp 'a', [ebx]
			je secondChar
		cmp 'b', [ebx]
			je secondChar
		cmp 'd', [ebx]
			je secondChar
		inc ebx
		jmp restartFunctionOnThisChar
	secondChar:
		inc ebx
		cmp [ebx], 0
			je error
		cmp 'a', [ebx]
			je restartFunctionOnThisChar
		cmp 'b', [ebx]
			je restartFunctionOnThisChar
		cmp 'c', [ebx]
			je restartFunctionOnThisChar
		inc ebx
		cmp [ebx], 0
			je error
	waitForFirstC:
		inc ebx
		cmp [ebx], 0
			je error
		cmp 'c', [ebx]
			jne waitForFirstC
		inc ebx
		cmp [ebx], 0
			je error
	waitForSecondC:
		mv [ecx], 0  ;ecx is now our iterator#Check if it doesn't interfere with rest of the code
		cmp 'c', [ebx]
			je printing
		jmp restartFunctionOnThisChar	

	error:
		;somehow write about the fact that the string ended without finding a match
		jmp returning

	printing:
		cmp [ecx], [eax]  ;eax is the number of times we want to print
			jge returning	
		;print the string you found
		inc ecx
		jmp printing

	returning:
		return

	; ================================
		mov esp, ebp			; wczytanie do ebp zapamietanej na poczatku wartosci
		pop ebp				; zdjecie ze stosu wskaznika - to adres wywolania ktore nastapi po zakonczeniu tutejszej funkcji
		ret					; ?? nie wiem po co to :)
		_count_ones ENDP
END