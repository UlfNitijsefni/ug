.686
.model flat

extern __write : PROC

public _findRegex

.code
		_findRegex PROC
	;------------------------------------------
	start:
		mov eax, [esp + 4] ;zawiera string
		xor ecx, ecx
		xor edx, edx

		jmp overit
	looptop:
		inc eax
		inc ecx
	overit:
		cmp [eax], BYTE PTR 0
		jnz looptop

	foundLength:
		cmp ecx, BYTE PTR 4
			jl error
		mov eax, [esp + 4]

	;------------------------------------------
	findFirstChar:
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je checkSecondChar
		cmp cl, 'b'
			je checkSecondChar
		cmp cl, 'd'
			je checkSecondChar
		inc eax
		jmp findFirstChar

	checkSecondChar:
		mov edx, eax 		;zapisujemy adres pierwszego znaku naszego stringa
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
		cmp cl, 'c'
			je findFirstChar
		

	plusSatisfied:
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
		jmp waitForC


	waitForC:
		cmp cl, 0
			je error
		cmp cl, 'c'
			je waitForSecondC
		jmp plusSatisfied

	waitForSecondC:
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
		cmp cl, 'c'
			jne waitForC		

		inc eax
		mov ecx, eax
		xor eax, eax
		xor ebx, ebx
		mov ebx, [esp+8]

	preparePrint:
		mov BYTE PTR [ecx], ' '
		add ecx, 1
		sub ecx, edx
		push ecx
		push edx
		push 1

	print:

		call __write
		dec ebx
		cmp ebx, 0
		jne print
		add esp, 12
	;------------------------------------------
	error:
		jmp returning

	returning:
		ret
	; ================================
		mov esp, ebp
		pop ebp
		ret
		_findRegex ENDP
END
