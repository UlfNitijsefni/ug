.intel_syntax noprefix

.global main

.data
	N: .int 6
	mssg: .asciz "regur99"

.text
//===================================================================

	main:
		mov eax, offset mssg
		xor ecx, ecx
		xor edx, edx

		jmp overit
	looptop:
		inc eax
		inc ecx
	overit:
		cmp [eax], BYTE PTR 0
		jnz looptop

	foundLength:
		cmp ecx, BYTE PTR 4
			jl error
		mov eax, [esp + 4]

	;------------------------------------------
	findFirstChar:
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je checkSecondChar
		cmp cl, 'b'
			je checkSecondChar
		cmp cl, 'd'
			je checkSecondChar
		inc eax
		jmp findFirstChar

	checkSecondChar:
		mov edx, eax
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
		cmp cl, 'c'
			je findFirstChar
		

	plusSatisfied:
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
		jmp waitForC


	waitForC:
		cmp cl, 0
			je error
		cmp cl, 'c'
			je waitForSecondC
		jmp plusSatisfied

	waitForSecondC:
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
		cmp cl, 'c'
			jne waitForC		

		inc eax
		mov ecx, eax
		xor eax, eax
		xor ebx, ebx
		mov ebx, [esp+8]

	preparePrint:
		mov BYTE PTR [ecx], ' '
		add ecx, 1
		sub ecx, edx
		push ecx
		push edx
		push 1

	print:

		call __write
		dec ebx
		cmp ebx, 0
		jne print
		add esp, 12
//------------------------------------------
	error:
		jmp returning

	returning:
		ret

//===================================================================

	printAnswer:

		push eax
		push offset mssg
		call printf
		add esp, 8
		ret