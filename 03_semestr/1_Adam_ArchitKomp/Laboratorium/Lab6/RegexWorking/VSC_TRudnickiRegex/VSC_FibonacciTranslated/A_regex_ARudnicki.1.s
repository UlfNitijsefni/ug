.intel_syntax noprefix

.global main

.data


.text
//===================================================================

	main:
        call loadTextAddr

		xor ecx, ecx
		xor edx, edx

		jmp overit
	looptop:
		inc eax
		inc ecx
	overit:
		cmp [eax], BYTE PTR 0
		jnz looptop

	foundLength:
		cmp ecx, BYTE PTR 4
			jl error

        call loadTextAddr

	//------------------------------------------
	findFirstChar:
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je checkSecondChar
		cmp cl, 'b'
			je checkSecondChar
		cmp cl, 'd'
			je checkSecondChar
		inc eax
		jmp findFirstChar

	checkSecondChar:
		mov edx, eax
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
		cmp cl, 'c'
			je findFirstChar
		

	plusSatisfied:
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
		jmp waitForC


	waitForC:
		cmp cl, 0
			je error
		cmp cl, 'c'
			je waitForSecondC
		jmp plusSatisfied

	waitForSecondC:
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
		cmp cl, 'c'
			jne waitForC		

        // przed drukowaniem
		inc eax
		mov ecx, eax
		xor eax, eax
		xor ebx, ebx
		//call loadPrintN

	preparePrint:
		mov BYTE PTR [ecx], ' '
		add ecx, 1
		sub ecx, edx

        //Przygotowanie do drukowania, nie umieszczać w pętli!
        mov eax, edx
        
        mov edx, ecx        
        mov ecx, eax

        xor di, di
        mov di, WORD PTR 7

	print:
        cmp di, WORD PTR 0
            je afterPrinting
        call _drukuj
        dec di
        jmp print

//------------------------------------------

    afterPrinting:
        //pop edx
        jmp returning

	error:
		jmp returning

	returning:
		ret

//===================================================================
    _drukuj:
        mov ebx, 1
        mov eax, 4
        int 0x80
    ret

//===================================================================
    loadTextAddr:
        mov eax, esp
        add eax, DWORD PTR 8
    ret


//===================================================================
    loadPrintN:
        mov WORD PTR di, [esp + 12]
        //mov ebx, [esp + 12]
        //add ebx, DWORD PTR 8
    ret