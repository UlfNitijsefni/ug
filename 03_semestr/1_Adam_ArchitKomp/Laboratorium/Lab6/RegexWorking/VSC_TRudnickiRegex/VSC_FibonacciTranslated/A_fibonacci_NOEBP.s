.intel_syntax noprefix

.global main

.data
	N: .int 6
	mssg: .asciz "Szukany wyraz ciągu Fibonacciego jest równy %d\n\n"

.text
		// ===================================================================

	main:

		mov ecx, [N]
		call giveElem_rec
		call printAnswer


		// ===================================================================

		giveElem_rec:

			push ecx

			cmp ecx, 2
			jg sumElems

			mov eax, 1
			jmp returnAnswer

		sumElems:
			dec ecx
			call giveElem_rec
			push eax

			dec ecx
			call giveElem_rec
			add eax, [esp]
			add esp, 4


		returnAnswer:
			pop ecx
			ret

		// ===================================================================

	printAnswer:

		push eax
		push offset mssg
		call printf
		add esp, 8
		ret