.intel_syntax noprefix

.global main

.data
	N: .int 7
	mssg: .asciz "---bexxxcc==="
    frm: .asciz "\n%s\n"

.text
//===================================================================

	main:
		//mov eax, offset mssg
        mov eax, [esp + 8]
        xor eax, eax
		xor ecx, ecx
		xor edx, edx

		jmp overit
	looptop:
		inc eax
		inc ecx
	overit:
		cmp [eax], BYTE PTR 0
		jnz looptop

	foundLength:
		cmp ecx, BYTE PTR 4
			jl error
		//mov eax, [di + 4]
        mov eax, offset mssg

	//------------------------------------------
	findFirstChar:
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je checkSecondChar
		cmp cl, 'b'
			je checkSecondChar
		cmp cl, 'd'
			je checkSecondChar
		inc eax
		jmp findFirstChar

	checkSecondChar:
		mov edx, eax
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
		cmp cl, 'c'
			je findFirstChar
		

	plusSatisfied:
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
		jmp waitForC


	waitForC:
		cmp cl, 0
			je error
		cmp cl, 'c'
			je waitForSecondC
		jmp plusSatisfied

	waitForSecondC:
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
		cmp cl, 'c'
			jne waitForC		

		inc eax
		mov ecx, eax
		xor eax, eax
		xor ebx, ebx
		mov ebx, [N]

	preparePrint:
		mov BYTE PTR [ecx], ' '
		add ecx, 1
		sub ecx, edx

        //Przygotowanie do drukowania, nie umieszczać w pętli!
        mov eax, edx
        
        mov edx, ecx        
        mov ecx, eax

        push di
        xor di, di
        //mov di, [offset N]
        mov di, WORD PTR 7
	print:
        cmp di, WORD PTR 0
            je afterPrinting
        call _drukuj
        dec di
        jmp print

//------------------------------------------

    afterPrinting:
        pop di
        jmp returning

	error:
		jmp returning

	returning:
		ret

//===================================================================
    _drukuj:
        mov ebx, 1
        mov eax, 4
        int 0x80
    ret
