.intel_syntax noprefix

.global main

.data
	N: .int 3
	mssg: .asciz "0000rqr999"

.text
//===================================================================

	main:
		mov eax, offset mssg
		xor ecx, ecx
		xor edx, edx

		jmp overit
	looptop:
		inc eax
		inc ecx
	overit:
		cmp [eax], BYTE PTR 0
		jnz looptop

	foundLength:
		cmp ecx, BYTE PTR 4
			jl error
		mov eax, offset mssg

	//------------------------------------------
	findFirstChar:
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'p'
			je checkSecondChar
		cmp cl, 'q'
			je checkSecondChar
		cmp cl, 'r'
			je checkSecondChar
		inc eax
		jmp findFirstChar

	checkSecondChar:
		mov edx, eax
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'q'
			je findFirstChar
		cmp cl, 'r'
			je findFirstChar

	waitForR:
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'r'
			jne waitForR
		inc eax
		mov ecx, eax
		xor eax, eax
		xor ebx, ebx
		mov ebx, [N]

	preparePrint:
		mov BYTE PTR [ecx], ' '
		add ecx, 1
		sub ecx, edx
		push ecx
		push edx
		push 1

	print:

		call __write
		dec ebx
		cmp ebx, 0
		jne print
		add esp, 12
//------------------------------------------
	error:
		jmp returning

	returning:
		ret
