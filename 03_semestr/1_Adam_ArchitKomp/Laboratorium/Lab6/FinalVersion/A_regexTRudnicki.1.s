.intel_syntax noprefix

.global main

.data
	N: .int 3
	//mssg: .ascii "0000rwa99r9"
    mssg: .ascii "abcc b accabxx xycc pqr"

.text
//===================================================================

	main:
		mov eax, offset mssg
		xor ecx, ecx
		xor edx, edx

		jmp overit
	looptop:
		inc eax
		inc ecx
	overit:
		cmp [eax], BYTE PTR 0
		jnz looptop

	foundLength:
		cmp ecx, BYTE PTR 5
			jl error
		mov eax, offset mssg

	//------------------------------------------
	findFirstChar:
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je checkSecondChar
		cmp cl, 'b'
			je checkSecondChar
		cmp cl, 'd'
			je checkSecondChar
		inc eax
		jmp findFirstChar

	checkSecondChar:
		mov edx, eax
		inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
        cmp cl, 'c'
			//je findFirstChar
            jne checkSecondChar

    findSecondC:
        inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'c'
			jne waitForFirstC    

    waitForFirstC:
		cmp cl, 0
			je error
		cmp cl, 'a'
			je findFirstChar
		cmp cl, 'b'
			je findFirstChar
        cmp cl, 'c'
            jmp checkSecondChar

	//waitForFirstC:
	//	inc eax
	//	mov cl, [eax]
	//	cmp cl, 0
	//		je error
	//	cmp cl, 'c'
	//		jne waitForFirstC


    waitForSecondsC:
        inc eax
		mov cl, [eax]
		cmp cl, 0
			je error
		cmp cl, 'c'
			jne waitForFirstC
		inc eax
		mov ecx, eax
		xor eax, eax
		xor ebx, ebx
		mov ebx, [N]


	preparePrint:
		mov BYTE PTR [ecx], ' '
		add ecx, 1
		sub ecx, edx
		push ecx
		push edx
		push 1

	print:
		call __write
		dec ebx
		cmp ebx, 0
		jne print
		add esp, 12
//------------------------------------------
	error:
		jmp returning

	returning:
		ret