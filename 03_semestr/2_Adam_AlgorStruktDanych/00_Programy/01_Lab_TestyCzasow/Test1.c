#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/////////////////////////////////////////////
//   PROCEDURY POMOCNICZE                  //
/////////////////////////////////////////////

//Locks memory and fills it with the array of 0's and 1's in random order.
void MakeArray(int n, int ***M)
{
    int x, y;
    (*M) = (int **)malloc(n * sizeof(int *));
    for (x = 0; x < n; x++)
    {
        (*M)[x] = (int *)malloc(n * sizeof(int));
    }
    for (x = 0; x < n; x++)
    {
        for (y = 0; y < n; y++)
        {
            (*M)[x][y] = rand() % 2;
        }
    }
}

//ALGORYTM 1============================
int SimpleAlg(int n, int **M)
{
    int x, y, xTop, yTop, xBot, yBot;
    int max = 0;
    int localMax = 0;

    for (x = 0; x < n; x++)
        for (y = 0; y < n; y++)
            for (xTop = n; xTop > x - 1; xTop--)
                for (yTop = n; yTop > y - 1; yTop--)
                {
                    localMax = 0;
                    for (xBot = x; xBot < xTop; xBot++)
                        for (yBot = y; yBot < yBot; yBot++)
                        {
                            localMax += M[x][y];
                        }
                    if ((localMax == (xTop - x + 1) * (yTop - y + 1)) && (localMax > max))
                        max = localMax;
                }
    return max;
}

/////////////////////////////////////////////
//   ALGORYTM PIERWSZY                     //
/////////////////////////////////////////////
int ALGO_NAIWNY(int n, int **M)
{
    int x1, y1, x2, y2, x, y;
    int max = 0;
    int local_max = 0;

    for (x1 = 0; x1 < n; x1++)
        for (y1 = 0; y1 < n; y1++)
            for (x2 = n - 1; x2 > x1 - 1; x2--)
                for (y2 = n - 1; y2 > y1 - 1; y2--)
                {
                    local_max = 0;
                    for (x = x1; x < x2 + 1; x++)
                        for (y = y1; y < y2 + 1; y++)
                            local_max += M[x][y];
                    if ((local_max == (x2 - x1 + 1) * (y2 - y1 + 1)) && (local_max > max))
                        max = local_max;
                }
    return max;
}

/////////////////////////////////////////////
//   ALGORYTM PIERWSZY v.2                 //
/////////////////////////////////////////////
int ALGO_NAIWNY_TEST(int n, int **M)
{
    int x1, y1, x2, y2, x, y;
    int max = 0;
    int local_max = 0;

    for (x1 = 0; x1 < n; x1++)
        for (y1 = 0; y1 < n; y1++)
            for (x2 = n - 1; x2 > x1 - 1; x2--)
                for (y2 = n - 1; y2 > y1 - 1; y2--)
                {
                    local_max = 0;
                    for (x = x1; x < x2 + 1; x++)
                        for (y = y1; y < y2 + 1; y++)
                            local_max += M[x][y];
                    if ((local_max == (x2 - x1 + 1) * (y2 - y1 + 1)) && (local_max > max))
                        max = local_max;
                }
    return max;
}

//ARRAY PRINTING========================
void ArrPrint(int n, int **M)
{
    int x, y;
    for (x = 0; x < n; x++)
    {
        for (y = 0; y < n; y++)
        {
            printf(" %d", M[x][y]);
        }
        printf("\n");
    }
}

//ARRAY REMOVING========================
void ArrRemove(int n, int **M)
{
    int x, y;
    for (x = 0; x < n; x++)
    {
        free(M[x]);
    }
    free(M);
}

void main()
{
    int n = 10;
    int **Macierz;
    MakeArray(n, &Macierz);
    ArrPrint(n, Macierz);

    printf("\nSimple algorythm: %d \n", SimpleAlg(n, Macierz));
    printf("\nTest algorythm: %d \n", ALGO_NAIWNY(n, Macierz));
    printf("\nRemade Test algorythm: %d \n", ALGO_NAIWNY_TEST(n, Macierz));
    ArrRemove(n, Macierz);
    printf("\n");
}