#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MLD 1000000000.0
/////////////////////////////////////////////
//   PROCEDURY POMOCNICZE                  //
/////////////////////////////////////////////
void utworz_MACIERZ(int n, int ***M)
{
    // alokuj� pami�� na tablic� rozmiaru nxn
    // i wpisuje losowe warto�ci 0/1 w macierzy
    int i, j;
    (*M) = (int **)malloc(n * sizeof(int *));
    for (i = 0; i < n; i++)
    {
        (*M)[i] = (int *)malloc(n * sizeof(int));
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            (*M)[i][j] = rand() % 2; //ORYGINALNE WPISZYWANIE 0 I 1
            //(*M)[i][j] = 0; //WPROWADZONE WPISYWANIE JEDNEJ WARTOSCI WSZEDZIE
        }
    }
}
/////////////////////////////////////////////
/*void utworz_MACIERZ1D(int n, double **M)
{
    for (int i = 0; i < n; i++)
        M[i] = malloc(sizeof(double *));
    //(*M) = (int *)malloc(n * sizeof(int *));
}*/
/////////////////////////////////////////////
void utworz_MACIERZ_x(int n, int ***M, int x)
{
    // alokuj� pami�� na tablic� rozmiaru nxn
    // i wpisuje do macierzy wsz�dzie warto�ci x
    int i, j;
    (*M) = (int **)malloc(n * sizeof(int *));
    for (i = 0; i < n; i++)
    {
        (*M)[i] = (int *)malloc(n * sizeof(int));
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            (*M)[i][j] = x;
        }
    }
}
/////////////////////////////////////////////
void wypisz_MACIERZ(int n, int **M)
{
    // wypisuje warto�ci macierzy
    int i, j;

    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
            printf(" %d", M[i][j]);
        printf("\n");
    }
}
/////////////////////////////////////////////
void zwolnij_MACIERZ(int n, int **M)
{
    // zwalania pami�� zarezerwowan� dla macierzy
    int i;
    for (i = 0; i < n; i++)
    {
        free(M[i]);
    }
    free(M);
}
/////////////////////////////////////////////
void zwolnij_MACIERZ1D(int n, double *M)
{
    free(M);
}
/////////////////////////////////////////////
//   ALGORYTM PIERWSZY                     //
/////////////////////////////////////////////
int ALGO_NAIWNY(int n, int **M)
{
    int x1, y1, x2, y2, x, y;
    int max = 0;
    int local_max = 0;

    for (x1 = 0; x1 < n; x1++)
        for (y1 = 0; y1 < n; y1++)
            for (x2 = n - 1; x2 > x1 - 1; x2--)
                for (y2 = n - 1; y2 > y1 - 1; y2--)
                {
                    local_max = 0;
                    for (x = x1; x < x2 + 1; x++)
                        for (y = y1; y < y2 + 1; y++)
                            local_max += M[x][y];
                    if ((local_max == (x2 - x1 + 1) * (y2 - y1 + 1)) && (local_max > max))
                        max = local_max;
                }
    return max;
}
/////////////////////////////////////////////
//   ALGORYTM DRUGI                        //
/////////////////////////////////////////////
int REKURENCJA(int **M, int x1, int y1, int x2, int y2)
{
    if ((x2 == x1) && (y2 == y1))
        return M[x1][y1];
    else if ((x2 - x1) > (y2 - y1))
        return REKURENCJA(M, x1, y1, (int)(x1 + x2) / 2, y2) * REKURENCJA(M, (int)(x1 + x2 + 1) / 2, y1, x2, y2);
    else
        return REKURENCJA(M, x1, y1, x2, (int)(y1 + y2) / 2) * REKURENCJA(M, x1, (int)(y1 + y2 + 1) / 2, x2, y2);
}
/////////////////////////////////////////////
int ALGO_REKURENCYJNY(int n, int **M)
{
    int x1, y1, x2, y2;
    int max = 0;
    int local_max;

    for (x1 = 0; x1 < n; x1++)
        for (y1 = 0; y1 < n; y1++)
            for (x2 = x1; x2 < n; x2++)
                for (y2 = y1; y2 < n; y2++)
                {
                    local_max = REKURENCJA(M, x1, y1, x2, y2) * (x2 - x1 + 1) * (y2 - y1 + 1);
                    if (local_max > max)
                        max = local_max;
                }
    return max;
}
/////////////////////////////////////////////
//   ALGORYTM TRZECI                       //
/////////////////////////////////////////////
int ALGO_DYNAMICZNY(int n, int **M)
{
    int x1, x2, y;
    int max = 0;
    int iloczyn;
    int **MM;

    utworz_MACIERZ_x(n, &MM, 0);

    for (y = 0; y < n; y++)
        for (x1 = 0; x1 < n; x1++)
        {
            iloczyn = 1;
            for (x2 = x1; x2 < n; x2++)
            {
                iloczyn *= M[x2][y];
                MM[x1][x2] = iloczyn * (x2 - x1 + 1 + MM[x1][x2]);
                if (MM[x1][x2] > max)
                    max = MM[x1][x2];
            }
        }
    return max;
}
/////////////////////////////////////////////
//   ALGORYTM CZWARTY                      //
/////////////////////////////////////////////
int ALGO_CZULY(int n, int **M)
{
    int x1, y1, x2, y2, ymax;
    int max = 0;
    int local_max = 0;

    for (x1 = 0; x1 < n; x1++)
        for (y1 = 0; y1 < n; y1++)
        {
            local_max = 0;
            x2 = x1;
            ymax = n;
            while ((x2 < n) && (M[x2][y1] == 1))
            {
                y2 = y1;
                while ((y2 < ymax) && (M[x2][y2] == 1))
                {
                    y2++;
                }
                ymax = y2;
                local_max = (x2 - x1 + 1) * (ymax - y1);
                if (local_max > max)
                    max = local_max;
                x2++;
            }
        }
    return max;
}

/////////////////////////////////////////////
//  ALGORYTM WŁASNY                        //
/////////////////////////////////////////////

int ALGO_WLASNY_NAIWNY(int n, int **M)
{
    int x, y, x1, y1, x2, y2;
    int curr_max = 0;
    int max = 0;

    for (x = 0; x < n; x++)
        for (y = 0; y < n; y++)
            for (x1 = n - 1; x1 > x - 1; x1--)
                for (y1 = n - 1; y1 > y; y1--)
                {
                    curr_max = 0;
                    for (x2 = x; x2 < x1 + 1; x2++)
                        for (y2 = y; y2 < y1 + 1; y2++)
                            curr_max += M[x2][y2];
                    if ((curr_max == (x1 - x + 1) * (y1 - y + 1)) && (curr_max > max))
                        max = curr_max;
                }
    return max;
}

/////////////////////////////////////////////
//  ALGORYTM WŁASNY 2                      //
/////////////////////////////////////////////

int ALGO_WLASNY_CZULY(int n, int **M)
{
    int x, y, max, localmax, x1, y1, ymax;
    max = 0;
    localmax = 0;

    for (x = 0; x < n; x++)
        for (y = 0; y < n; y++)
        {
            localmax = 0;
            x1 = x;
            ymax = n;
            while ((x1 < n) && M[x1][y] == 1) //W przypadku jedynek i zer nie trzeba dawać == 1 na końcu, wystarczy M[x1][y])
            {
                y1 = y;
                while ((y1 < ymax) && M[x1][y1] == 1)
                {
                    y1++;
                }
                ymax = y1;
                localmax = (x1 - x + 1) * (ymax - y);
                if (localmax > max)
                    max = localmax;
                x1++;
            }
        }
    return max;
}

/////////////////////////////////////////////
//  DO TESTOW                              //
/////////////////////////////////////////////

void TESTY_ZALOZEN(int n, int **M, int (*alg)(int, int **M), int which)
{
    struct timespec tp0, tp1;
    double sredniaWspol = 0.0;
    //double ostatniCzas = double[20];

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tp0);
    for (int x = 0; x < 20; x++)
    {
        alg(n, M);
    }
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tp1);
    double Tn = (tp1.tv_sec + tp1.tv_nsec / MLD) - (tp0.tv_sec + tp0.tv_nsec / MLD);

    float sredniaCzasow = Tn * 1000 / 20;

    sredniaCzasow = sredniaCzasow / n;

    switch (which)
    {
    case 1:
        printf("Sredni czas 20 naiwnych dla n = %d : %f. Wspolczynnik: %f\n", n, sredniaCzasow, sredniaCzasow * 10000000 / (n * n * n * n * n * n));
        break;
    case 2:
        printf("Sredni czas 20 rekurencji dla n = %d : %f. Wspolczynnik: %f\n", n, sredniaCzasow, sredniaCzasow * 10000000 / (n * n * n * n * n * n));
        break;
    case 3:
        printf("Sredni czas 20 dynamicznych dla n = %d : %f. Wspolczynnik: %f\n", n, sredniaCzasow, sredniaCzasow * 10000000 / (n * n * n));
        break;
    case 4:
<<<<<<< HEAD
        printf("Sredni czas 20 czułych dla: %f. Wspolczynnik: %f\n", sredniaCzasow, sredniaCzasow * 10000000 / (n * n * n * n));
=======
        printf("Sredni czas 20 czułych dla n = %d : %f. Wspolczynnik: %f\n", n, sredniaCzasow, sredniaCzasow * 10000000 / (n * n * n * n)); //lub n*n dla samych 0
>>>>>>> 1f413dfed47efc59dd390104e9a646bc41e2108a
        break;
    }
    sredniaCzasow = 0;
}

/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////
int main()
{
    int i = 20; //wymiar macierzy/
    int **Macierz;
    srand(time(NULL));

    for (int n = i; n < 30; n++)
    {
<<<<<<< HEAD
=======

>>>>>>> 1f413dfed47efc59dd390104e9a646bc41e2108a
        utworz_MACIERZ(n, &Macierz);
        //utworz_MACIERZ_x(n,&Macierz,0);
        //utworz_MACIERZ_x(n,&Macierz,1);

        //wypisz_MACIERZ(n, Macierz);
<<<<<<< HEAD
        printf("Testy dla n = %d :\n", n);
=======

>>>>>>> 1f413dfed47efc59dd390104e9a646bc41e2108a
        TESTY_ZALOZEN(n, Macierz, ALGO_WLASNY_NAIWNY, 1);
        TESTY_ZALOZEN(n, Macierz, ALGO_REKURENCYJNY, 2);
        TESTY_ZALOZEN(n, Macierz, ALGO_DYNAMICZNY, 3);
        TESTY_ZALOZEN(n, Macierz, ALGO_WLASNY_CZULY, 4);
<<<<<<< HEAD
        printf("//////////////////////////////////////////////////////////////////////////////////////////\n");
=======
>>>>>>> 1f413dfed47efc59dd390104e9a646bc41e2108a

        zwolnij_MACIERZ(n, Macierz);
    }

    return 1;
}
