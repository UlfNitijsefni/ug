#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//--------------------------------------------------------------------
int findLength(FILE *srcFile);
void readFile(FILE *srcFile, int *dataTab, int firstIndex, int lastIndex);
void printArr(int n, int *dataTab);
void changeParent(int *dataTab, int idxParent, int n);
void buildHeap(int *dataTab, int n);
void heapSort(int *dataTab, int n);

//--------------------------------------------------------------------
//--------------------------------------------------------------------
int main()
{
    int newInt;
    int n = 0;
    int firstIndex;
    int lastIndex;
    int fileLength;

    //--------------------------------------------------------------------
    FILE *srcFile;
    srcFile = fopen("przyklad.txt", "r");

    fscanf(srcFile, "%i", &firstIndex);
    firstIndex = firstIndex - 1;
    fscanf(srcFile, "%i", &lastIndex);
    lastIndex = lastIndex - 1;
    fileLength = findLength(srcFile);
    n = lastIndex - firstIndex + 1;

    fclose(srcFile);

    int dataTab[lastIndex - firstIndex];

    srcFile = fopen("przyklad.txt", "r");
    readFile(srcFile, dataTab, firstIndex, lastIndex);

    //buildHeap(dataTab, n);

//    for(int i = 0; i < n/2; i++)
        heapSort(dataTab, n);

    printArr(n - 1, dataTab);
    fclose(srcFile);

    printf("length: %d\n\n", n);
}

//============================================================================================
int findLength(FILE *srcFile)
{
    int newInt;
    int i = 0;
    while (fscanf(srcFile, "%i", &newInt) != EOF)
        i++;
    return i;
}

//--------------------------------------------------------------------------------------------
void printArr(int n, int *dataTab)
{
    printf("\n\n");
    for (int i = 0; i < n + 1; i++)
        printf("%d ", dataTab[i]);
    printf("\n\n");
}

//--------------------------------------------------------------------------------------------
void readFile(FILE *srcFile, int *dataTab, int firstIndex, int lastIndex)
{
    int temporary;
    int i;
    for (i = 0; i < firstIndex + 2; i++)
    {
        fscanf(srcFile, "%i", &temporary);
    }
    for (i = 0; i < lastIndex - firstIndex + 1; i++)
    {
        fscanf(srcFile, "%i", &dataTab[i]);
    }
}

//--------------------------------------------------------------------------------------------
void changeParent(int *dataTab, int idxParent, int n)
{
    int idxLeft = idxParent * 2;
    int idxRight = idxParent * 2 + 1;
    int biggerSon;
    int temporary;

    if (dataTab[idxLeft] > dataTab[idxParent] && idxLeft <= n - 1)
        biggerSon = idxLeft;
    else
        biggerSon = idxParent;

    if (dataTab[idxRight] > dataTab[biggerSon] && idxRight <= n - 1)
        biggerSon = idxRight;

    if (biggerSon != idxParent)
    {
        temporary = dataTab[biggerSon];
        dataTab[biggerSon] = dataTab[idxParent];
        dataTab[idxParent] = temporary;

        changeParent(dataTab, biggerSon, n);
    }
}

//--------------------------------------------------------------------------------------------
void buildHeap(int *dataTab, int n)
{
    for (int i = n / 2; i >= 0; i--)
    {
        changeParent(dataTab, i, n);
    }
}

//--------------------------------------------------------------------------------------------
void heapSort(int *dataTab, int n)
{
    int temp;
    printf("\n\n");
    buildHeap(dataTab, n);
    for (int i = n - 1; i >= 0; i--)
    {
        temp = dataTab[i];
        dataTab[i] = dataTab[0];
        dataTab[0] = temp;
        n = n - 1;
        changeParent(dataTab, 1, n);
        printf(" %d", dataTab[i]);
    }
    printf("\n\n");
}