#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//--------------------------------------------------------------------
int findLength(FILE *srcFile);
void **readFile(FILE *srcFile, int *dataTab, int firstIndex, int lastIndex);
void printArr(int arrLength, int *dataTab);
void heapify(int *dataTable, int startingIndex, int dataSize);
void printTab(FILE *srcFile, int *dataToPrint, int n);
void buildHeap(int *, int);
void heapSort(int *A, int tableLength);

//============================================================================================
int main()
{
    int newInt;
    int n = 0;
    int firstIndex;
    int lastIndex;
    int arrLength;

    //--------------------------------------------------------------------
    FILE *srcFile;
    FILE *resultFile;
    srcFile = fopen("przyklad.txt", "r");

    fscanf(srcFile, "%i", &firstIndex);
    fscanf(srcFile, "%i", &lastIndex);
    firstIndex -= 1;
    lastIndex -= 1;
    arrLength = findLength(srcFile);
    n = lastIndex - firstIndex + 1;

    fclose(srcFile);

    int receivedInts[lastIndex - firstIndex];
    srcFile = fopen("przyklad.txt", "r");
    readFile(srcFile, receivedInts, firstIndex, lastIndex);
    //--------------------------------------------------------------------

    buildHeap(receivedInts, n);
    // printTab(receivedInts, n);

    heapSort(receivedInts, n);
    fclose(srcFile);

    resultFile = fopen("result.txt", "w");
    printTab(resultFile, receivedInts, n);
    fclose(resultFile);

    return 0;
}

//--------------------------------------------------------------------------------------------
//void printTab(FILE *srcFile, int *tabToPrint, int n)
void printTab(FILE *srcFile, int *tabToPrint, int n)
{
    for (int j = 0; j < n; j++)
        fprintf(srcFile, "%i\n", tabToPrint[j]);
    printf("\n\n");
}

//--------------------------------------------------------------------------------------------
int findLength(FILE *srcFile)
{
    int newInt;
    int i = 0;
    while (fscanf(srcFile, "%i", &newInt) != EOF)
        i++;
    return i;
}

//--------------------------------------------------------------------------------------------
void printArr(int arrLength, int *dataTab)
{
    printf("\n\n");
    for (int i = 0; i < arrLength; i++)
        printf("%d\n", dataTab[i]);
    printf("\n\n");
}

//--------------------------------------------------------------------------------------------
void **readFile(FILE *srcFile, int *receivedInts, int firstIndex, int lastIndex)
{
    int temporary;
    int i;
    for (i = 0; i < firstIndex + 2; i++)
    {
        fscanf(srcFile, "%i", &temporary);
    }
    for (i = 0; i < lastIndex - firstIndex + 1; i++)
    {
        fscanf(srcFile, "%i", &receivedInts[i]);
    }
}

//--------------------------------------------------------------------------------------------
void heapify(int *A, int currentIndex, int heapSize)
{
    int l = (2 * currentIndex) + 1;
    int r = (2 * currentIndex) + 2;
    int largest;
    if (l < heapSize && A[l] < A[currentIndex])
        largest = l;
    else
        largest = currentIndex;

    if (r < heapSize && A[r] < A[largest])
        largest = r;

    if (largest != currentIndex)
    {
        int tempValue = A[currentIndex];
        A[currentIndex] = A[largest];
        A[largest] = tempValue;
        heapify(A, largest, heapSize);
    }
}

//--------------------------------------------------------------------------------------------
void heapSort(int *A, int tableLength)
{
    buildHeap(A, tableLength);
    int temp;
    for (int i = tableLength - 1; i >= 0; i--)
    {
        temp = A[i];
        A[i] = A[0];
        A[0] = temp;
        tableLength -= 1;
        heapify(A, 0, tableLength);
    }
}

//--------------------------------------------------------------------------------------------
void buildHeap(int *A, int n)
{
    for (int i = n / 2; i >= 0; i--)
    {
        heapify(A, i, n);
    }
}
