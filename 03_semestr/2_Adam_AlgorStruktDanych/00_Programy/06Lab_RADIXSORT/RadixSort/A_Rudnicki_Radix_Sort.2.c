#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define STRING_LENGTH_MAX 7
#define MN 10
#define MAS_STRINGS 1000
#define MAX_STRING_LENGTH 100

void countingSort(int, int);
void radixSort(int, int);
void swapArrays();
void testRadix(char[][STRING_LENGTH_MAX], int, int);
int getLastIndex(char *);
char **createEmptyArrString(int, int);
int *createEmptyArrInt(int numberOfStrings);
char **copyArr(char arrA[][STRING_LENGTH_MAX], int numberOfStrings, int numberOfChars);
int *findSizes(int numberOfStrings);
void freeTwoDimensions(char **tab, int numberOfStrings);
void printTab(char **tab, int numberOfStrings, char *msg);
char **readFile(char **);
int toLowerCase(char letter);

char **arrA;
char **arrB;
int *lenArrA;
int *lenArrB;

//==============================================================================================
void main()
{
    int numberOfStrings = 7;
    int numberOfChars = STRING_LENGTH_MAX;

    char tab_1[][STRING_LENGTH_MAX] = {"01", "13", "12", "10", "03", "20", "22"};
    testRadix(tab_1, numberOfStrings, numberOfChars);

    char tab_2[][STRING_LENGTH_MAX] = {"aaacccb", "ccccddd", "dddaaba", "dddaaza", "beeeeaa", "cffffff", "FUAFAFa"};
    testRadix(tab_2, numberOfStrings, numberOfChars);

    char tab_3[][STRING_LENGTH_MAX] = {"ab", "Ab", "A", "cd", "Da", "CD", "da"};
    testRadix(tab_3, numberOfStrings, numberOfChars);

    char tab_4[][STRING_LENGTH_MAX] = {"2", "23", "2345", "234", "2345678", "2", "234"};
    testRadix(tab_4, numberOfStrings, numberOfChars);

    char tab_5[][STRING_LENGTH_MAX] = {"6432", "123", "67", "09557", "023567", "957", "1567"};
    testRadix(tab_5, numberOfStrings, numberOfChars);

    char tab_6[][STRING_LENGTH_MAX] = {"567", "167", "867", "567", "347", "4567", "1645567"};
    testRadix(tab_6, numberOfStrings, numberOfChars);
}

//--------------------------------------------------------------------------------------
void testRadix(char tab[][STRING_LENGTH_MAX], int numberOfStrings, int numberOfChars)
{
    arrA = copyArr(tab, numberOfStrings, numberOfChars);
    arrB = createEmptyArrString(numberOfStrings, numberOfChars);
    lenArrA = findSizes(numberOfStrings);
    lenArrB = createEmptyArrInt(numberOfStrings);

    printTab(arrA, numberOfStrings, "Persort:");
    radixSort(numberOfStrings, STRING_LENGTH_MAX - 1);
    printTab(arrA, numberOfStrings, "Postsort:");
    printf("\n");

    freeTwoDimensions(arrA, numberOfStrings);
    free(arrB);
    free(lenArrA);
    free(lenArrB);
}

//--------------------------------------------------------------------------------------
void radixSort(int numberOfStrings, int currIndex)
{
    if (currIndex >= 0)
    {
        countingSort(numberOfStrings, currIndex);

        swapArrays();

        radixSort(numberOfStrings, currIndex - 1);
    }
}

//--------------------------------------------------------------------------------------
void swapArrays()
{
    //Zamienia miejscami tablic wejscia i wyjscia
    char **tempWords;
    tempWords = arrA;
    arrA = arrB;
    arrB = tempWords;

    //Zamienia miejscami tablic dlugosci wejscia i wyjscia
    char *tempLengths;
    tempLengths = lenArrA;
    lenArrA = lenArrB;
    lenArrB = tempLengths;
}

//--------------------------------------------------------------------------------------
void countingSort(int numberOfStrings, int id)
{
    char tempArray[123], letter, idxB, *currWord;
    int maxIndex, letterIntLower;

    for (int i = 0; i < 123; i++)
        tempArray[i] = 0;

    for (int i = 0; i < numberOfStrings; i++)
    {
        letter = arrA[i][id];
        letterIntLower = toLowerCase(letter);
        tempArray[letterIntLower]++;
    }

    for (int i = 1; i < 123; i++)
        tempArray[i] += tempArray[i - 1];

    for (int j = numberOfStrings - 1; j >= 0; j--)
    {
        currWord = arrA[j];
        maxIndex = lenArrA[j];

        letter = maxIndex < id ? '0' : currWord[id];
        letterIntLower = toLowerCase(letter);

        idxB = tempArray[letterIntLower] - 1;

        arrB[idxB] = currWord;
        lenArrB[idxB] = maxIndex;
        tempArray[letterIntLower]--;
    }
}

//--------------------------------------------------------------------------------------
int getLastIndex(char *word)
{
    char nextChar = 'z';
    int n;
    for (n = 0; nextChar > 0; n++)
        nextChar = word[n];
    return n - 2;
}

//--------------------------------------------------------------------------------------
char **copyArr(char arrA[][STRING_LENGTH_MAX], int numberOfStrings, int numberOfChars)
{
    char **tab = createEmptyArrString(numberOfStrings, numberOfChars);
    for (int i = 0; i < numberOfStrings; i++)
        for (int j = 0; j < numberOfChars; j++)
        {
            tab[i][j] = arrA[i][j];
            tab[i][j + 1] = 0;
        }
    return tab;
}

//--------------------------------------------------------------------------------------
int *findSizes(int numberOfStrings)
{
    int *lengthsTab = createEmptyArrInt(numberOfStrings);
    for (int i = 0; i < numberOfStrings; i++)
        lengthsTab[i] = getLastIndex(arrA[i]);
    return lengthsTab;
}

//--------------------------------------------------------------------------------------
void printTab(char **tab, int numberOfStrings, char *msg)
{
    printf("%s\t", msg);
    for (int i = 0; i < numberOfStrings; i++)
    {
        printf("%s ", tab[i]);
    }
    printf("\n");
}

//--------------------------------------------------------------------------------------
char **createEmptyArrString(int numberOfStrings, int numberOfChars)
{
    char **output = malloc(sizeof(char *) * numberOfStrings);
    for (int i = 0; i < numberOfStrings; i++)
        output[i] = malloc(sizeof(char) * numberOfChars + 1);
    return output;
}

//--------------------------------------------------------------------------------------
int *createEmptyArrInt(int numberOfStrings)
{
    return malloc(sizeof(int) * numberOfStrings);
}

//--------------------------------------------------------------------------------------
void freeTwoDimensions(char **tab, int numberOfStrings)
{
    for (int i = 0; i < numberOfStrings; i++)
        free(tab[i]);
    free(tab);
}

//--------------------------------------------------------------------------------------
int toLowerCase(char letter)
{
    // Musi byc rzutowane na int bo inaczej indeks nie dziala w pelni poprawnie w tablicy c.
    return (int)(letter >= 'A' && letter <= 'Z' ? letter + 32 : letter);
}