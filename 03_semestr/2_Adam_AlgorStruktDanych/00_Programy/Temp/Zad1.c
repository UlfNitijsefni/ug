/*#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//--------------------------------------------------------------------
int findLength(FILE *srcFile);
void readFile(FILE *srcFile, int *dataTab, int firstIndex, int lastIndex);
void printArr(int n, int *dataTab);
void changeParent(int *dataTab, int idxParent, int n);
void buildHeap(int *dataTab, int n);
void heapSort(int *dataTab, int n);

//--------------------------------------------------------------------
//--------------------------------------------------------------------
int main()
{
    int newInt;
    int n = 0;
    int firstIndex;
    int lastIndex;
    int fileLength;

    //--------------------------------------------------------------------
    FILE *srcFile;
    srcFile = fopen("przyklad.txt", "r");

    fscanf(srcFile, "%i", &firstIndex);
    firstIndex = firstIndex - 1;
    fscanf(srcFile, "%i", &lastIndex);
    lastIndex = lastIndex - 1;
    fileLength = findLength(srcFile);
    n = lastIndex - firstIndex + 1;

    fclose(srcFile);

    int dataTab[lastIndex - firstIndex];

    srcFile = fopen("przyklad.txt", "r");
    readFile(srcFile, dataTab, firstIndex, lastIndex);

    //buildHeap(dataTab, n);

    heapSort(dataTab, n);

    printArr(n - 1, dataTab);
    fclose(srcFile);

    printf("length: %d\n\n", n);
}

//============================================================================================
int findLength(FILE *srcFile)
{
    int newInt;
    int i = 0;
    while (fscanf(srcFile, "%i", &newInt) != EOF)
        i++;
    return i;
}

//--------------------------------------------------------------------------------------------
void printArr(int n, int *dataTab)
{
    printf("\n\n");
    for (int i = 0; i < n + 1; i++)
        printf("%d ", dataTab[i]);
    printf("\n\n");
}

//--------------------------------------------------------------------------------------------
void readFile(FILE *srcFile, int *dataTab, int firstIndex, int lastIndex)
{
    int temporary;
    int i;
    for (i = 0; i < firstIndex + 2; i++)
    {
        fscanf(srcFile, "%i", &temporary);
    }
    for (i = 0; i < lastIndex - firstIndex + 1; i++)
    {
        fscanf(srcFile, "%i", &dataTab[i]);
    }
}

//--------------------------------------------------------------------------------------------
void changeParent(int *dataTab, int idxParent, int n)
{
    int idxLeft = idxParent * 2;
    int idxRight = idxParent * 2 + 1;
    int biggerSon;
    int temporary;

    if (dataTab[idxLeft] > dataTab[idxParent] && idxLeft <= n - 1)
        biggerSon = idxLeft;
    else
        biggerSon = idxParent;

    if (dataTab[idxRight] > dataTab[biggerSon] && idxRight <= n - 1)
        biggerSon = idxRight;

    if (biggerSon != idxParent)
    {
        temporary = dataTab[biggerSon];
        dataTab[biggerSon] = dataTab[idxParent];
        dataTab[idxParent] = temporary;

        changeParent(dataTab, biggerSon, n);
    }
}

//--------------------------------------------------------------------------------------------
void buildHeap(int *dataTab, int n)
{
    for (int i = n / 2; i >= 0; i--)
    {
        changeParent(dataTab, i, n);
    }
}

//--------------------------------------------------------------------------------------------
void heapSort(int *dataTab, int n)
{
    int temp;
    printf("\n\n");
    buildHeap(dataTab, n);
    for (int i = n - 1; i >= 0; i--)
    {
        temp = dataTab[i];
        dataTab[i] = dataTab[0];
        dataTab[0] = temp;
        n = n - 1;
        changeParent(dataTab, 1, n);
        printf(" %d", dataTab[i]);
    }
    printf("\n\n");
}*/

#include <stdio.h>

//void Heapify(int *, int, int);
void BuildHeap(int *A, int length) {
    	for( int i = (length / 2); i > 0; i--)
		Heapify(A, i, length);
}

void HeapSort(int *A, int length) {
	int tmp;
	BuildHeap(A, length);
	for(int i = length; i > 1; i--) {
		tmp = A[i];
		A[i] = A[1];
		A[1] = tmp;
		length --;
		Heapify(A, 1, length);

	}

}
void Heapify( int *A, int i, int heapSize) {
	int tmp, largest;
	int l = 2 * i; // i -aktualny element
	int r = (2 * i) + 1;
	if(l <= heapSize && A[l] > A[i])
		largest = l;
	else
		largest = i;
	if(r <= heapSize && A[r] > A[largest])
		largest = r;
	if(largest != i) {
		tmp = A[largest];
		A[largest] = A[i];
		A[i] = tmp;
		Heapify(A, largest, heapSize);
	}

}
int main(int argc, char *argv[]) {

	FILE *plik1, *plik2;

//int A[11]; //{0, 23, 17, 14, 6, 13, 10, 1, 5, 7, 12};
	int length = 11;

	plik1 = fopen(argv[1], "r");
	int getLength(FILE *srcFile)
{
    int newInt;
    int i = 0;
    while (fscanf(srcFile, "%i", &newInt) != EOF)
        i++;
    return i;
}
length=getLength(plik1);
int A[length];
	for( int i = 1; i < length; i++)
		fscanf(plik1, "%d", &A[i]);
	fclose(plik1);
	HeapSort(A, length);
	plik2 = fopen(argv[2], "w");
	for( int i = 2; i <= length; i++)
		fprintf(plik2, "%d\n", A[i]);
	fclose(plik2);

	/* for(int i = 1; i < length; i++)
		printf("%d\t", A[i]);

	printf("\n");
		*/
	return 0;

}