#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#define MLD 1000000000.0

int Partition(int[], int, int);
void QuickSort(int*, int, int);
void printArr(int*, int);
void Testing(int[], int);

//------------------------------------------------------------------------------------
/*
void main()
{
	int testArr1[] = { 0 };
	int testArr2[] = { 0,2,3,4,5,6,7 };
	int testArr3[] = { 5,4,3,2,1 };
	int testArr4[] = { 0,1,2,421,4,124,1243 };
	int testArr5[] = { -5,4,-3,-2,1 };



	Testing(testArr1, 1);
	Testing(testArr2, 7);
	Testing(testArr3, 5);
	Testing(testArr4, 7);
	Testing(testArr5, 5);




	struct timespec tp0, tp1;
	double sredniaWspol = 0.0;


	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tp0);
	for (int x = 0; x < 20; x++)
	{

	}
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tp1);
	double Tn = (tp1.tv_sec + tp1.tv_nsec / MLD) - (tp0.tv_sec + tp0.tv_nsec / MLD);

	float sredniaCzasow = Tn * 1000 / 20;
	//sredniaCzasow = sredniaCzasow / n;

}
*/
int main() {
	struct timespec tp0, tp1;
	double Tn, Fn, x;
	long int n; // liczba test�w

	for (n = 500; n < 30000; n = n + 1000) {

		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tp0);

		// przyk�adowe obliczenia
		x = procedura1(n);
		// x=procedura2(n);
		// procedura3(n);

		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tp1);

		// zgadywana funkcja czasu
		//       Fn=5*n;
		//     Fn=20000*n;
		//     Fn=n*n*n;
		//     Fn=n*log(n);
		//     Fn=n*n*sqrt(n);
		//     Fn=n*n;
		Fn = n * n / 1000;

		Tn = (tp1.tv_sec + tp1.tv_nsec / MLD) - (tp0.tv_sec + tp0.tv_nsec / MLD);
		printf("n: %5ld \tczas: %3.10lf \twspolczynnik: %3.5lf\n", n, Tn, Fn / Tn);

//====================================================================================
void Testing(int providedArray[], int n) {
	if (n == 0)
		return;

	printArr(providedArray, n);
	printf("\n");

	QuickSort(providedArray, 0, n - 1);

	printArr(providedArray, n);
	printf("\n\n");
}

//====================================================================================
void printArr(int *toPrint, int size)
{
	for (int i = 0; i < size; i++)
	{
		printf("%d | ", toPrint[i]);
	}
}

//------------------------------------------------------------------------------------
int Partition(int givenArr[], int firstIndex, int lastIndex)
{

	int forSwaps = 0;
	int i = firstIndex - 1;

	for (int j = firstIndex; j <= lastIndex; j++)
	{
		if (givenArr[j] <= givenArr[lastIndex])
		{
			i++;
			forSwaps = givenArr[j];
			givenArr[j] = givenArr[i];
			givenArr[i] = forSwaps;
		}
	}
	if (i < lastIndex)
		return i;
	else
		return i - 1;
}

//------------------------------------------------------------------------------------
void QuickSort(int *providedArray, int firstIndex, int lastIndex) {
	if (firstIndex < lastIndex) {
		int dividePoint = Partition(providedArray, firstIndex, lastIndex);
		QuickSort(providedArray, firstIndex, dividePoint);
		QuickSort(providedArray, dividePoint + 1, lastIndex);
	}
}