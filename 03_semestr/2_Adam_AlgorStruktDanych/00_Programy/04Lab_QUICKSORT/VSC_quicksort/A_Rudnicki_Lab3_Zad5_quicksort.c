#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define MLD 1000000000.0

void quicksort(int *A, int first, int last);
int partition(int *A, int first, int last);
void swap(int *A, int first, int last);
void printArr(int *A, int length);
void quicksortCHANGED(int *A, int first, int last, int minLength);
void bubbleSort(int first, int last, int *table);
void copyArr(int preSortPT[], int size, int table[]);
void testQuickBubble(int tabToSort[], int tabLength, int testTab[], int idxLast, int minLength);
void createArray(int tabLength, int random, int testTab[]);
char *getPessimism(int order);

int nQuick;
int nBubble;

//============================================================================================
int main()
{
    //srand(time(NULL));

    int order = -1;

    int tabLength = 500;
    int testTab[500];
    int tabToSort[500];

    //int tabLength = 1000;
    //int testTab[1000];
    //int tabToSort[1000];

    //int tabLength = 2500;
    //int testTab[2500];
    //int tabToSort[2500];

    createArray(tabLength, order, tabToSort);
    // printArr(tabToSort, 500);
    // return 0;

    int idxLast = tabLength - 1;

    copyArr(tabToSort, tabLength, testTab);

    printf("\nTable length: %i, table order: %s \n\n", tabLength, getPessimism(order));

    testQuickBubble(tabToSort, tabLength, testTab, idxLast, 0);
    testQuickBubble(tabToSort, tabLength, testTab, idxLast, tabLength / 16);
    testQuickBubble(tabToSort, tabLength, testTab, idxLast, tabLength / 8);
    testQuickBubble(tabToSort, tabLength, testTab, idxLast, tabLength / 4);
    testQuickBubble(tabToSort, tabLength, testTab, idxLast, tabLength / 2);
    testQuickBubble(tabToSort, tabLength, testTab, idxLast, tabLength * 2);

    //printArr(tabToSort, tabLentgh);
    //printf("\n");
    //printArr(testTab, tabLentgh);
    return 0;
}

//--------------------------------------------------------------------------------------------
void testQuickBubble(int tabToSort[], int tabLength, int testTab[], int idxLast, int minLength)
{

    nQuick = 0;
    nBubble = 0;
    //CLOCK_PROCESS_CPUTIME_ID
    struct timespec tp0, tp1;
    clock_gettime(2, &tp0);

    // // Windows ==================
    // clock_t startTime, stopTime;
    // startTime = clock();
    // // ===========================

    for (int x = 0; x < 100; x++)
    {
        copyArr(tabToSort, tabLength, testTab);
        quicksortCHANGED(testTab, 0, idxLast, minLength);
    }

    clock_gettime(2, &tp1);
    double Tn = 1000 * ((tp1.tv_sec + tp1.tv_nsec / MLD) - (tp0.tv_sec + tp0.tv_nsec / MLD));

    // // Windows ==================
    // stopTime = clock();
    // double Tn = (stopTime - startTime) * 1000.0 / CLOCKS_PER_SEC;
    // // ===========================

    printf("Time: %i ms - bubble-treshold: %i - n Quick: %i / n Bubble: %i\n\n", (int)Tn, minLength, nQuick, nBubble);
}

//--------------------------------------------------------------------------------------------
int partition(int *A, int first, int last)
{

    nQuick++;
    int i = first - 1;

    for (int j = first; j <= last; j++)
    {
        if (A[j] <= A[last])
        {
            i++;
            if (i != j)
                swap(A, i, j);
        }
    }

    if (i < last)
        return i;
    else
        return i - 1;
}

//--------------------------------------------------------------------------------------------
void quicksortCHANGED(int *A, int first, int last, int minLength)
{

    if (first < last)
    {

        int q = partition(A, first, last);

        if (q - first > minLength)
            quicksortCHANGED(A, first, q, minLength);
        else
            bubbleSort(first, last, A);

        if (last - q + 1 > minLength)
            quicksortCHANGED(A, q + 1, last, minLength);
        else
            bubbleSort(first, last, A);
    }
}

//--------------------------------------------------------------------------------------------
void quicksort(int *A, int first, int last)
{
    if (first < last)
    {
        int pivot = partition(A, first, last);
        quicksort(A, first, pivot);
        quicksort(A, pivot + 1, last);
    }
}

//--------------------------------------------------------------------------------------------
void swap(int *A, int first, int last)
{
    int temp;
    temp = A[first];
    A[first] = A[last];
    A[last] = temp;
}

//--------------------------------------------------------------------------------------------
void printArr(int *A, int length)
{
    for (int i = 0; i < length; i++)
    {
        printf(" %i", A[i]);
    }
    printf("\n\n");
}

//--------------------------------------------------------------------------
void bubbleSort(int first, int last, int *table)
{

    //int temp;
    nBubble++;

    for (int j = first; j < last; j++)
        for (int i = first; i < last - 1; i++)
            if (table[i] > table[i + 1])
                swap(table, i, j);
}

//--------------------------------------------------------------------------
void copyArr(int preSortPT[], int size, int table[])
{
    for (int i = 0; i < size; i++)
        table[i] = preSortPT[i];
}

//--------------------------------------------------------------------------
void createArray(int tabLength, int sorted, int testTab[])
{
    if (sorted == -1)
        for (int i = 0; i < tabLength; i++)
            testTab[i] = tabLength - i;

    else if (sorted == 1)
        for (int i = 0; i < tabLength; i++)
            testTab[i] = i + 1;

    else
        for (int i = 0; i < tabLength; i++)
            testTab[i] = rand() % 100;
}

//--------------------------------------------------------------------------
char *getPessimism(int order)
{
    switch (order)
    {
    case 1:
        return "NICE DATA - ascending";
    case 0:
        return "MEDIUM NICE - random";
    case -1:
        return "OBNOXIOUS - descending";
    }
}