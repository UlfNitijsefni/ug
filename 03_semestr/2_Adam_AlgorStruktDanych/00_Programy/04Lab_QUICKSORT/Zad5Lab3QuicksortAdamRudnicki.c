#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define MLD 1000000000.0

void quicksort(int *A, int first, int last);
int partition(int *A, int first, int last);
void swap(int *A, int first, int last);
void printArr(int *A, int length);
void quicksortCHANGED(int *A, int first, int last, int minLength);
void bubbleSort(int first, int last, int table[]);
void copyArr(int preSortPT[], int size, int table[]);
void testQuickBubble(int tabToSort[], int tabLength, int testTab[], int idxLast, int minLength);
void createArray(int tabLength, int random, int testTab[]);

//============================================================================================
int main()
{
    srand(time(NULL));

    // int tabLentgh = 500;
    // int testTab[500];
    // int tabToSort[500];

    // int tabLentgh = 1000;
    // int testTab[1000];
    // int tabToSort[1000];

    int tabLentgh = 2500;
    int testTab[2500];
    int tabToSort[2500];
    createArray(tabLentgh, -1, tabToSort);

    int idxLast = tabLentgh - 1;

    copyArr(tabToSort, tabLentgh, testTab);


    testQuickBubble(tabToSort, tabLentgh, testTab, idxLast, 0);
    testQuickBubble(tabToSort, tabLentgh, testTab, idxLast, 4);
    // testQuickBubble(tabToSort, tabLentgh, testTab, idxLast, 16);
    // testQuickBubble(tabToSort, tabLentgh, testTab, idxLast, 64);
    // testQuickBubble(tabToSort, tabLentgh, testTab, idxLast, 256);
    testQuickBubble(tabToSort, tabLentgh, testTab, idxLast, 5000);

    return 0;
}

//--------------------------------------------------------------------------------------------
void testQuickBubble(int tabToSort[], int tabLength, int testTab[], int idxLast, int minLength)
{

    //CLOCK_PROCESS_CPUTIME_ID
    struct timespec tp0, tp1;
    clock_gettime(2, &tp0);

    for (int x = 0; x < 5; x++)
    {
        copyArr(tabToSort, tabLength, testTab);
        quicksortCHANGED(testTab, 0, idxLast, minLength);
    }

    clock_gettime(2, &tp1);
    double Tn = 1000*((tp1.tv_sec + tp1.tv_nsec / MLD) - (tp0.tv_sec + tp0.tv_nsec / MLD));
    printf("Time: %F ms - bubble-treshold: %i\n\n", Tn, minLength);
}

//--------------------------------------------------------------------------------------------
int partition(int *A, int first, int last)
{
    int i = first - 1;

    for (int j = first; j <= last; j++)
    {
        if (A[j] <= A[last])
        {
            i++;
            if (i != j)
                swap(A, i, j);
        }
    }
    if (i < last)
        return i;
    else
        return i - 1;
}

//--------------------------------------------------------------------------------------------
void quicksortCHANGED(int *A, int first, int last, int minLength)
{
    if (first < last)
    {
        if(last - first > minLength){
            int q = partition(A, first, last);

            if (q - first > minLength)
                quicksortCHANGED(A, first, q, minLength);
            else
                bubbleSort(first, last, A);

            if (last - q + 1 > minLength)
                quicksortCHANGED(A, q + 1, last, minLength);
            else
                bubbleSort(first, last, A);
        }
    }
    else bubbleSort(first, last, A);
}

//--------------------------------------------------------------------------------------------
void quicksort(int *A, int first, int last)
{
    if (first < last)
    {
        int pivot = partition(A, first, last);
        quicksort(A, first, pivot);
        quicksort(A, pivot + 1, last);
    }
}

//--------------------------------------------------------------------------------------------
void swap(int *A, int first, int last)
{
    int temp;
    temp = A[first];
    A[first] = A[last];
    A[last] = temp;
}

//--------------------------------------------------------------------------------------------
void printArr(int *A, int length)
{
    for (int i = 0; i < length; i++)
    {
        printf(" %i", A[i]);
    }
    printf("\n\n");
}

//--------------------------------------------------------------------------
void bubbleSort(int first, int last, int table[])
{

    int temp;

    for (int j = first; j < last; j++)
        for (int i = first; i < last; i++)
            if (table[i] > table[i + 1])
                if (i != j)
                    swap(table, i, j);
}

//--------------------------------------------------------------------------
void copyArr(int preSortPT[], int size, int table[])
{
    for (int i = 0; i < size; i++)
        table[i] = preSortPT[i];
}

//--------------------------------------------------------------------------
void createArray(int tabLength, int sorted, int testTab[])
{
    if (sorted == -1)
        for (int i = 0; i < tabLength; i++)
            testTab[i] = tabLength - i;

    else if (sorted == 1)
        for (int i = 0; i < tabLength; i++)
            testTab[i] = i + 1;

    else
        for (int i = 0; i < tabLength; i++)
            testTab[i] = rand() % 100;
}
