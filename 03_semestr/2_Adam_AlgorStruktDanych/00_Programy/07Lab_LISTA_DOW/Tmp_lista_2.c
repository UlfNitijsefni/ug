#include <stdio.h>
#include <stdlib.h>

typedef struct wordNode
{
    char *inputWord;
    struct wordNode *next;
} word_node;

word_node *createNode(word_node *, char *);
void drukuj(word_node *listHead);
void wstaw(word_node *toInsert, word_node **listHead);
word_node *szukaj(char *toFind, word_node *listHead);

//======================================================================

void main()
{
    word_node *head = createNode(NULL, "Początek listy");
    word_node *tail = head;
    for (int i = 0; i < 3; i++)
    {
        tail = createNode(tail, "kolejny element");
    }
    //printf("%s\n", head->inputWord);
    //printf("%s\n", tail->inputWord);
    drukuj(head);

    //word_node *toInsert = createNode(NULL, "New beginnings");
    //wstaw(toInsert, &head);

    word_node foundElem = *szukaj("Początek listy", head);
    printf("\n\n");
    drukuj(head);
}

//======================================================================

word_node *createNode(word_node *prevNode, char *nodeWord)
{
    word_node *temp = malloc(sizeof(word_node));

    temp->inputWord = nodeWord;
    temp->next = NULL;

    if (prevNode != NULL)
        prevNode->next = temp;
    return temp;
}

//-----------------------------------------------------------------------

void drukuj(word_node *listHead)
{
    word_node *temp = listHead;

    printf("%s\n", temp->inputWord);
    while (temp->next != NULL)
    {
        temp = temp->next;
        printf("%s\n", temp->inputWord);
    }
}

//----------------------------------------------------------------------

void wstaw(word_node *toInsert, word_node **listHead)
{
    toInsert->next = *listHead;
    *(listHead) = toInsert;
}

//----------------------------------------------------------------------

word_node *szukaj(char *toFind, word_node *listHead)
{
    word_node *temp = listHead;
    temp->inputWord = "szukam";
    while (temp->next != NULL)
    {
        if (temp->inputWord != toFind)
            temp = temp->next;
    }

    if (temp->inputWord == "szukam")
    {
        printf("Nie znaleziono elementu.\n");
        return NULL;
    }
    else
        return temp;
}