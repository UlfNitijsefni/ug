#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define STRING_LENGTH_MAX 45
#define MN 10
#define MAS_STRINGS 1000
#define MAX_STRING_LENGTH 100

typedef struct singleWord
{
    char *znaki;
    struct singleWord *next;
} myWord;

typedef struct myList
{
    struct singleWord *head;
    struct singleWord *tail;
} sentryList;

myWord *createWord(char *znaki);
void deleteWord(myWord *head);

myWord *USUN(char *toRemove, myWord *head);
myWord *WSTAW(char *toAdd, myWord *head);
myWord *SZUKAJ(char *toFind, myWord *head);
myWord *KASUJ(sentryList *head);
myWord *BEZPOWTORZEN(myWord *provided);
myWord *SCAL(myWord **A, myWord **B);
void DRUKUJ(myWord *head);

void printList(myWord *head, char *message);
int stringCompare(char *A, char *B);
void printLabel(char *msg);
myWord *createTestList(int n);
void copyText(char *src, char *dst);
void insertSentry(myWord *head, char *providedWord);
myWord *SZUKAJWartownik(sentryList *providedList, char *toFind);
sentryList *createListHead(myWord *head);
sentryList *freeSentry(sentryList *toDelete);
//==============================================================================================
void main()
{
    myWord *testList = createWord("pierwszy");
    sentryList *testSentry = createListHead(testList);

    // .......................................
    // printLabel("Test WSTAW");
    // printList(testList, "przed");
    // testList = WSTAW("drugi", testList);
    // testList = WSTAW("trzeci", testList);
    // printList(testList, "po");

    // .......................................
    // printLabel("Test USUN");
    // printList(testList, "przed");
    // char *toRemove = "drugi";
    // testList = USUN(toRemove, testList);
    // printList(testList, "po");

    // .......................................
    printLabel("Test KASUJ");
    printList(testList, "przed");
    testSentry = KASUJ(testSentry);
    printList(testList, "po");

    // .......................................
    // printLabel("Test BEZPOWTORZEN");
    // myWord *testList2 = createTestList(20);
    // myWord *testList3 = createTestList(4);
    // myWord *noRepeats = BEZPOWTORZEN(testList2);
    // printList(testList2, "Lista zrodlowa");
    // printList(noRepeats, "Bez powtorzen");

    // .......................................
    // printLabel("Test SCAL");
    // printList(testList2, "lista A przed");
    // printList(testList3, "lista B przed");
    // myWord *testList4 = createWord("");
    // testList4 = SCAL(&testList2, &testList3);
    // printList(testList4, "A,B scalone");
    // printList(testList2, "lista A po");
    // printList(testList3, "lista B po");

    // .......................................
    // myWord *testList7 = createTestList(2);
    // printLabel("Test SZUKAJ z wartownikiem");
    // printList(testList7, "Oryginalna lista:");
    // char *toFind = "1";
    // myWord *result = SZUKAJWartownik(testSentry, toFind);
    // DRUKUJ(result);

    printf("\n\n");
}

// LIST NODE - CREATE, DELETE
//======================================================================================
myWord *createWord(char *noweZnaki)
{
    myWord *new_myWord = malloc(sizeof(myWord));

    new_myWord->znaki = malloc(sizeof(char) * STRING_LENGTH_MAX);
    copyText(noweZnaki, new_myWord->znaki);

    new_myWord->next = NULL;

    return new_myWord;
}

//--------------------------------------------------------------------------------------
void deleteWord(myWord *toDelete)
{
    free(toDelete->znaki);
    free(toDelete);
}

// LIST ACTIONS
//======================================================================================
myWord *WSTAW(char *toAdd, myWord *head)
{
    myWord *new_myWord = createWord(toAdd);
    new_myWord->next = head;
    return new_myWord;
}

//--------------------------------------------------------------------------------------
myWord *SZUKAJ(char *toFind, myWord *head)
{
    if (head == NULL)
        return NULL;
    else if (stringCompare(toFind, head->znaki))
        if (head->next != NULL)
            return head;
        else
            return SZUKAJ(toFind, head->next);
}

//--------------------------------------------------------------------------------------
myWord *USUN(char *toRemove, myWord *head)
{
    myWord *firstHead = head;
    myWord *temp;
    myWord *nextHead = firstHead;

    if (!head)
        return NULL;

    if (stringCompare(head->znaki, toRemove))
        return head->next;

    for (int i = 0; 1; i++)
    {
        if (!nextHead->next)
            return firstHead;
        if (stringCompare(toRemove, nextHead->next->znaki))
        {
            temp = nextHead->next;
            nextHead->next = nextHead->next->next;
            deleteWord(temp);
            return firstHead;
        }
    }
}

//--------------------------------------------------------------------------------------
myWord *KASUJ(sentryList *list)
{
    myWord *head = list->head;
    if (head == NULL)
    {
        free(list->head);
        // free(list->tail);
        free(list);
        return NULL;
    }

    myWord *temp = head;
    while (head->next != NULL)
    {
        head = head->next;
        deleteWord(temp);
        temp = head;
    }
    deleteWord(temp);
    free(list->head);
    free(list->tail);
    free(list);
    return NULL;
}

//--------------------------------------------------------------------------------------
myWord *BEZPOWTORZEN(myWord *provided)
{
    if (provided == NULL)
        return NULL;

    myWord *newHead = createWord(provided->znaki);
    myWord *iterator = provided->next;

    while (iterator != NULL)
    {
        if (SZUKAJ(iterator->znaki, newHead) == NULL && iterator->znaki != NULL)
            newHead = WSTAW(iterator->znaki, newHead);
        iterator = iterator->next;
    }
    return newHead;
}

//--------------------------------------------------------------------------------------
myWord *SCAL(myWord **A, myWord **B)
{
    myWord *newHead = *A;
    myWord *temp = *A;

    while (temp->next != NULL)
    {
        temp = temp->next;
    }
    temp->next = *B;

    *A = NULL;
    *B = NULL;
    return newHead;
}

//--------------------------------------------------------------------------------------
void DRUKUJ(myWord *head)
{
    if (head == NULL)
        printf("Element nie istnieje.\n");
    else
        printf("%s | ", head->znaki);
}

// UTILS
//======================================================================================
int stringCompare(char *A, char *B)
{
    int isEqual = 1;
    for (int i = 0; 1; i++)
    {
        if (A[i] == 0 && B[i] == 0)
            return 1;
        else if (A[i] == B[i])
        {
        }
        else
            return 0;
    }
}

//--------------------------------------------------------------------------------------
void printList(myWord *head, char *message)
{
    printf("%s:\t", message);
    if (head == NULL)
    {
        printf("(lista jest pusta)\n");
        return;
    }

    int i = 1;
    myWord *temp = head;
    while (i)
    {
        if (temp != NULL)
        {
            DRUKUJ(temp);
            temp = temp->next;
        }
        else
        {
            printf("\n");
            i = 0;
        }
    }
}

//--------------------------------------------------------------------------------------
void printLabel(char *msg)
{
    printf("\n-----------------------------\n%s\n", msg);
}

//--------------------------------------------------------------------------------------
myWord *createTestList(int n)
{
    myWord *newHead = createWord("1");
    for (int i = 0; i < n; i++)
    {
        int z = (i % 10) + 1;
        char str[2];
        sprintf(str, "%d", z);
        newHead = WSTAW(str, newHead);
    }

    return newHead;
}

//--------------------------------------------------------------------------------------
void copyText(char *src, char *dst)
{
    int i = -1;
    do
    {
        i++;
        dst[i] = src[i];
    } while (src[i] != 0);
}

//--------------------------------------------------------------------------------------
void insertSentry(myWord *tail, char *providedWord)
{
    tail->next = createWord(providedWord);
}

//--------------------------------------------------------------------------------------
myWord *SZUKAJWartownik(sentryList *providedList, char *toFind)
{
    myWord *head = providedList->head;
    while (1)
    {
        if (stringCompare(toFind, head->znaki))
            if (head->next != NULL)
                return head;
            else
                return NULL;
        head = head->next;
    }
}

//--------------------------------------------------------------------------------------
sentryList *createTestListSentry(int n)
{
    myWord *newHead = createWord("1");
    newHead->next = createWord(NULL);
    myWord *temp = newHead;
    for (int i = 0; i < n; i++)
    {
        int z = (i % 10) + 1;
        char str[2];
        sprintf(str, "%d", z);
        newHead = WSTAW(str, newHead);
    }
}

//--------------------------------------------------------------------------------------
sentryList *createListHead(myWord *head)
{
    sentryList *new_myList = malloc(sizeof(sentryList));

    new_myList->head = malloc(sizeof(myWord));
    new_myList->tail = malloc(sizeof(myWord));

    new_myList->head = head;

    while (new_myList->tail->next != NULL)
    {
        new_myList->tail = new_myList->tail->next;
    }

    return new_myList;
}

//--------------------------------------------------------------------------------------
sentryList *freeSentryList(sentryList *toDelete)
{
    myWord *firstHead = toDelete->head;
    myWord *temp;
    myWord *nextHead = firstHead;

    for (int i = 0; 1; i++)
    {
        if (!nextHead->next)
            return firstHead;
        if (stringCompare(toRemove, nextHead->next->znaki))
        {
            temp = nextHead->next;
            nextHead->next = nextHead->next->next;
            deleteWord(temp);
            return firstHead;
        }
    }
}