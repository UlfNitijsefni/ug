#include <stdio.h>
// #include <string.h>
#include <stdlib.h>
#include <math.h>

// void readFile();
void testingForSizeX(int size, unsigned int *hashArray);
void countHashes(int arrSize, unsigned int *hashArray);
void printArr(unsigned int *hashArray, int arrSize);
void fillWithZeros(int *hashArray, int length);
int hashString(char *word, int arrSize, int wordLength);
double getAvgPerNonZero(unsigned int *hashArray, int arrSize);
int getMaxValue(unsigned int *hashArray, int arrSize);
int countZeros(unsigned int *hashArray, int arrSize);
// int hashString(char *word, int arrSize);
//==============================================================================================
void main()
{
    int testArr2[1031];
    int testArr1[1613];
    int testArr3[1949];
    int testArr4[1024];
    int testArr5[1536];
    int testArr6[2048];

    printf("\nTesty korzystne:\n");
    testingForSizeX(1031, testArr2);
    testingForSizeX(1613, testArr1);
    testingForSizeX(1949, testArr3);

    printf("\nTesty niekorzystne:\n");
    testingForSizeX(1024, testArr4);
    testingForSizeX(1536, testArr5);
    testingForSizeX(2048, testArr6);
}

//----------------------------------------------------------------------------------------------
void testingForSizeX(int size, unsigned int *hashArray)
{
    printf("\n//==//==//==//==//==//==//==//==//\n");
    int arrSize = size;
    double average = 0.0;

    int zeroes = 0;
    int max = 0;

    fillWithZeros(hashArray, arrSize);
    countHashes(arrSize, hashArray);
    // (hashArray, arrSize);

    average = getAvgPerNonZero(hashArray, arrSize);
    max = getMaxValue(hashArray, arrSize);
    zeroes = countZeros(hashArray, arrSize);

    printf("Hash table length: %d\n", arrSize);
    printf("Number input elements: %d\n", 3744);
    printf("Maximum number of collisions: %d\n", max);
    printf("Average number of elements in non-empty indexes: %f\n", average);
    printf("Number of empty indexes: %d\n", zeroes);
}

//----------------------------------------------------------------------------------------------
void countHashes(int arrSize, unsigned int *hashArray)
{

    FILE *file;
    file = fopen("3700.txt", "r");
    int temp = 0;
    char word[50];

    while (fscanf(file, "%s", word) != EOF)
    {
        temp = hashString(word, arrSize, 49);

        if (temp >= 0)
            hashArray[temp]++;
        else
        {
            printf("Błąd funkcji hashującej - zwrócony indeks jest ujemny.\n");
        }
    }
    fclose;
}

//----------------------------------------------------------------------------------------------
void printArr(unsigned int *hashArray, int arrSize)
{
    for (int i = 0; i < arrSize; i++)
        printf("%d\n", hashArray[i]);
}

//----------------------------------------------------------------------------------------------
void fillWithZeros(int *hashArray, int length)
{
    for (int i = 0; i < length; i++)
    {
        hashArray[i] = 0;
    }
}

//----------------------------------------------------------------------------------------------
double getAvgPerNonZero(unsigned int *hashArray, int arrSize)
{
    int currSum = 0;
    int nonZeroCounter = 0;

    for (int i = 0; i < arrSize; i++)
    {
        if (hashArray[i] != 0)
        {
            nonZeroCounter++;
            currSum += hashArray[i];
        }
    }

    return currSum / (double)nonZeroCounter;
}

//----------------------------------------------------------------------------------------------
int getMaxValue(unsigned int *hashArray, int arrSize)
{
    int max = 0;
    for (int i = 0; i < arrSize; i++)
    {
        if (hashArray[i] > max)
            max = hashArray[i];
    }
    return max;
}

//----------------------------------------------------------------------------------------------
int countZeros(unsigned int *hashArray, int arrSize)
{
    int zeroes = 0;
    for (int i = 0; i < arrSize; i++)
    {
        if (hashArray[i] == 0)
            zeroes++;
    }
    return zeroes;
}

//----------------------------------------------------------------------------------------------
int hashString(char *word, int arrSize, int wordLength)
{
    int temp = 1;
    int i = 0;
    // while (word[i + 1] != 0)
    while (word[i] != 0)
    {
        temp = ((int)((sqrt(5) - 1) / 2 * i) + temp * (word[i] + i)) % arrSize;
        // temp = (int)((sqrt(5) - 1) / 2 * word[i] + word[i + 1]);
        i++;
    }

    return temp;
}