#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#define MLD 1000000000.0
#define MAX_LEN_SURNAME 70

typedef struct
{
    int orderOfInput;
    int quantity;
    int ifDeleted;
    char *surname;
} oneSurname;

void getStrings(oneSurname *tab, int tabSize, double maxFill, char *fileToRead);
int insertKey(oneSurname *tab, char *str, int tabSize, int orderOfInput);
void printTab(oneSurname *tab, int tabSize);
int getStrLength(char *toHash);
int makeHash(char *str, int tabSize);

void deleteElem(oneSurname *dstTab, int hash);
int makeSquareHash(char *str, int tabSize, int adder);
int findElem(oneSurname *dstTab, char *str, int tabSize, int orderOfInput);
void clearTab(oneSurname *tab, int tabSize, double maxFill, char *fileToRead);
int countDeletions(oneSurname *tab, int tabSize);
char *inputTab;
oneSurname *prepareTab(int tabSize);
removeTab(oneSurname *tab, int tabSize);

void testHashTab(int tabSize, int srcSize, char* tabType, char *fileToRead);

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void main()
{
    printf("\n========================\nTablica korzystne:\n\n");
    testHashTab(47, 100, "KORZYSTNA", "names_20-100.txt");
    testHashTab(1250, 100, "NIEKORZYSTNA", "names_20-100.txt");

    printf("\n\n");
    testHashTab(239, 100, "KORZYSTNA", "names_20-100.txt");
    testHashTab(200, 100, "NIEKORZYSTNA", "names_20-100.txt");

    printf("\n\n");
    testHashTab(1171, 6000, "KORZYSTNA", "names_A_20-6000.txt");
    testHashTab(1125, 6000, "NIEKORZYSTNA", "names_A_20-6000.txt");

    printf("\n\n");
    testHashTab(1171, 10000, "KORZYSTNA", "names_B_20-10000.txt");
    testHashTab(2048, 10000, "NIEKORZYSTNA", "names_B_20-10000.txt");

    printf("\n\n");

    exit(0);
}

void testHashTab(int tabSize, int srcSize, char* tabType, char *fileToRead)
{
    clock_t start, end;
    start = clock();

    oneSurname *tab = prepareTab(tabSize);
    getStrings(tab, tabSize, 0.8, fileToRead);
    clearTab(tab, tabSize, 0.8, fileToRead);
    getStrings(tab, tabSize, 0.4, fileToRead);
    int nDeleted = countDeletions(tab, tabSize);
    removeTab(tab, tabSize);

    end = clock();

    double totaltTime = (double)(end - start) / CLOCKS_PER_SEC;
    printf("%d\t\trozmiar tablicy haszowanej (%s)\n", tabSize, tabType);
    printf("%d\t\trozmiar danych\n", srcSize);
    printf("%d\t\tliczba pustych komórek po usuniętych kluczach\n", nDeleted);
    printf("%lf\tczas wykonania testu\n\n", totaltTime);
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void getStrings(oneSurname *tab, int tabSize, double maxFill, char *fileToRead)
{
    int elemCounter = 0;
    char str[MAX_LEN_SURNAME];

    FILE *file;
    file = fopen(fileToRead, "r");

    int notFull = 1;

    while (fscanf(file, "%s", str) != EOF && notFull && elemCounter < tabSize * maxFill)
    {
        elemCounter++;
        notFull = insertKey(tab, str, tabSize, elemCounter);
    }
    fclose(file);
}

//--------------
int insertKey(oneSurname *dstTab, char *str, int tabSize, int orderOfInput)
{
    int countQuantity = 1;
    int i;
    int hash;

    for (i = 0; i < tabSize; i++)
    {
        hash = makeSquareHash(str, tabSize, i);
        if (strcmp(dstTab[hash].surname, "") == 0)
        {
            copyString(dstTab[hash].surname, str);
            dstTab[hash].quantity = countQuantity;
            dstTab[hash].orderOfInput = orderOfInput;
            break;
        }
        if (strcmp(dstTab[hash].surname, str) == 0)
        {
            dstTab[hash].quantity++;
            countQuantity++;
        }
    }

    return i == tabSize ? 0 : 1;
}

//-------------
int findElem(oneSurname *dstTab, char *str, int tabSize, int orderOfInput)
{
    int hash = 1;
    for (int i = 0; i < tabSize; i++)
    {
        hash = makeSquareHash(str, tabSize, i);
        if (strcmp(dstTab[hash].surname, str) == 0 && dstTab[hash].orderOfInput == orderOfInput)
        {
            return hash;
        }
    }
    return -1;
}

//-------------
void deleteElem(oneSurname *dstTab, int hash)
{
    dstTab[hash].quantity = 0;
    dstTab[hash].orderOfInput = 0;
    strcpy(dstTab[hash].surname, "");
    dstTab[hash].ifDeleted = 1;
}

//-------------
void clearTab(oneSurname *tab, int tabSize, double maxFill, char *fileToRead)
{
    int elemCounter = 0;
    int hashToKill = 0;
    char str[MAX_LEN_SURNAME];

    FILE *file;
    file = fopen(fileToRead, "r");

    while (fscanf(file, "%s", str) != EOF && elemCounter < tabSize * maxFill)
    {
        elemCounter++;
        if (elemCounter % 2 == 0)
        {
            hashToKill = findElem(tab, str, tabSize, elemCounter);
            if (hashToKill == -1)
                continue;
            deleteElem(tab, hashToKill);
        }
    }
    fclose(file);
}

//-------------
int makeSquareHash(char *str, int tabSize, int adder)
{
    long hash = (makeHash(str, tabSize) + (13 * adder) + (11 * adder * adder)) % tabSize;
    return (int)hash;
}

int makeHash(char *toHash, int tabSize)
{
    int toHashSize = getStrLength(toHash);

    int currHash = 1;
    for (int i = 0; i < toHashSize; i++)
    {
        currHash = ((int)((sqrt(5) - 1) / 2 * i) + currHash * (toHash[i] + i)) % tabSize;
    }
    if (currHash >= 0)
        return currHash;
    else
    {
        printf("ERROR\n");
        exit;
    }
}

//-------------
int countDeletions(oneSurname *tab, int tabSize)
{
    int deletCount = 0;
    for (int i = 0; i < tabSize; i++)
    {
        if (tab[i].ifDeleted == 1 && strcmp(tab[i].surname, ""))
            deletCount++;
    }
    return deletCount;
}

// UTILS
//--------------------------------------------------------------------------------------------------------
void printTab(oneSurname *tab, int tabSize)
{
    for (int i = 0; i < tabSize; i++)
    {
        if (strcmp(tab[i].surname, ""))
            printf("%3d %s \n", tab[i].orderOfInput, tab[i].surname);
        else
            printf("---------------|\n");
    }
}

//------------
int getStrLength(char *toHash)
{
    int counter = 0;
    while (toHash[counter] != '\0')
        counter++;
    return counter;
}

//--------------
oneSurname *prepareTab(int tabSize)
{
    oneSurname *dataTab = malloc(tabSize * sizeof(oneSurname));
    for (int i = 0; i < tabSize; i++)
    {
        dataTab[i].surname = malloc(MAX_LEN_SURNAME * sizeof(char));
        strcpy(dataTab[i].surname, "");
        dataTab[i].orderOfInput = -1;
        dataTab[i].ifDeleted = 0;
    }
    return dataTab;
}

//--------------
removeTab(oneSurname *tab, int tabSize)
{
    for (int i = 0; i < tabSize; i++)
        free(tab[i].surname);
    free(tab);
}

//-----------
void copyString(char *toFill, char *toInsert)
{
    int length = getStrLength(toInsert);
    for (int i = 0; i <= length; i++)
        toFill[i] = toInsert[i];
}