#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define maxCharCount 254
#define maxStrings 10

// void radixSort(char *A[], int n);
void bubbleSort(int first, int last, char *table);
// void fillWithChars(char *A[], int size);
void printArr(char *A, int n);
void readWord(char *A[], int n);
void copyArr(int preSortPT[], int size, int table[]);
void createArray(int tabLength, int sorted, char testTab[]);
void swap(char *A, int first, int last);

//============================================================================================
void main()
{
    char testTab[maxStrings];
    // char tabToSort[] = {'a', 'c', 'e', 'b', 'd', 'f', 'g', 'i', 'h', 'j'};
    char tabToSort[] = {'a', 'c', 'b'};
    // char tabToSort[maxStrings];

    // bubbleSort(0, 2, tabToSort);

    //fillWithChars(tabToSort, maxStrings);
    // printArr(tabToSort, maxStrings);
    printArr(tabToSort, 3);

    //countingSort(tabToSort, maxStrings);
}

//--------------------------------------------------------------------------------------------
// void radixSort(char *A[], int n)
// {
// }

//--------------------------------------------------------------------------
void bubbleSort(int first, int last, char *table)
{
    //int temp;
    //nBubble++;

    for (int j = first; j < last; j++)
        for (int i = first; i < last - 1; i++)
            if (table[i] > table[i + 1])
                swap(table, i, j);
}
//--------------------------------------------------------------------------------------------
// void fillWithChars(char *A[], int size)
// {
//     for (int i = 0; i < size; i++)
//     {
//         A[i] = i;
//     }
// }

//--------------------------------------------------------------------------------------------
// char *A[MN],*B[MN],*T[MN];
void printArr(char *A, int n)
{
    for (int i = 0; i < n; i++)
    {
        // printf("%c %s",*(A[i]+1),A[i]);
        printf(" %c", A[i]);
    }
    printf("\n\n");
}

//--------------------------------------------------------------------------------------------
void readWord(char *A[], int n)
{
    char slowo[maxCharCount];
    int i = 0;
    for (i = 0; i < n; i++)
    {
        scanf("%s", slowo);
        A[i] = (char *)malloc(sizeof(char) * maxCharCount);
        // strcpy(A[i],slowo); ???
    }
}

//--------------------------------------------------------------------------
void copyArr(int preSortPT[], int size, int table[])
{
    for (int i = 0; i < size; i++)
        table[i] = preSortPT[i];
}

//--------------------------------------------------------------------------
void createArray(int tabLength, int sorted, char testTab[])
{
    if (sorted == -1)
        for (int i = 0; i < tabLength; i++)
            testTab[i] = tabLength - i;

    else if (sorted == 1)
        for (int i = 0; i < tabLength; i++)
            testTab[i] = i + 1;

    else
        for (int i = 0; i < tabLength; i++)
            testTab[i] = rand() % 100;
}

//--------------------------------------------------------------------------
void swap(char *A, int first, int last)
{
    char temp = A[first];
    A[first] = A[last];
    A[last] = temp;
}