#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define STRING_LENGTH_MAX 45
#define MN 10
#define MAS_STRINGS 1000
#define MAX_STRING_LENGTH 100

struct list
{
    char *znaki;
    struct list *next;
};

typedef struct list slowo;

slowo *createWord(char *znaki);
void deleteWord(slowo *head);

slowo *USUN(char *toRemove, slowo *head);
slowo *WSTAW(char *toAdd, slowo *head);
slowo *SZUKAJ(char *toFind, slowo *head);
slowo *KASUJ(slowo *head);
slowo *BEZPOWTORZEN(slowo *provided);
slowo *SCAL(slowo **A, slowo **B);
void DRUKU(slowo *head);

void printList(slowo *head, char *message);
int stringCompare(char *A, char *B);
void printLabel(char *msg);
slowo *createTestList(int n);
void copyText(char *src, char *dst);

//==============================================================================================
void main()
{
    slowo *testList = createWord("pierwszy");

    // .......................................
    printLabel("Test WSTAW");
    printList(testList, "przed");
    testList = WSTAW("drugi", testList);
    testList = WSTAW("trzeci", testList);
    printList(testList, "po");

    // .......................................
    printLabel("Test USUN");
    printList(testList, "przed");
    char *toRemove = "drugi";
    testList = USUN(toRemove, testList);
    printList(testList, "po");

    // .......................................
    printLabel("Test KASUJ");
    printList(testList, "przed");
    testList = KASUJ(testList);
    printList(testList, "po");

    // .......................................
    printLabel("Test BEZPOWTORZEN");
    slowo *testList2 = createTestList(20);
    slowo *testList3 = createTestList(4);
    slowo *noRepeats = BEZPOWTORZEN(testList2);
    printList(testList2, "Lista zrodlowa");
    printList(noRepeats, "Bez powtorzen");

    // .......................................
    printLabel("Test SCAL");
    printList(testList2, "lista A przed");
    printList(testList3, "lista B przed");
    slowo *testList4 = createWord("");
    testList4 = SCAL(&testList2, &testList3);
    printList(testList4, "A,B scalone");
    printList(testList2, "lista A po");
    printList(testList3, "lista B po");

    printf("\n\n");
}

// LIST NODE - CREATE, DELETE
//======================================================================================
slowo *createWord(char *noweZnaki)
{
    slowo *new_slowo = malloc(sizeof(slowo));

    new_slowo->znaki = malloc(sizeof(char) * STRING_LENGTH_MAX);
    copyText(noweZnaki, new_slowo->znaki);

    new_slowo->next = NULL;

    return new_slowo;
}

//--------------------------------------------------------------------------------------
void deleteWord(slowo *toDelete)
{
    free(toDelete->znaki);
    free(toDelete);
}

// LIST ACTIONS
//======================================================================================
slowo *WSTAW(char *toAdd, slowo *head)
{
    slowo *new_slowo = createWord(toAdd);
    new_slowo->next = head;
    return new_slowo;
}

//--------------------------------------------------------------------------------------
slowo *SZUKAJ(char *toFind, slowo *head)
{
    if (head == NULL)
        return NULL;
    else if (stringCompare(toFind, head->znaki))
        return head;
    else
        return SZUKAJ(toFind, head->next);
}

//--------------------------------------------------------------------------------------
slowo *USUN(char *toRemove, slowo *head)
{
    slowo *firstHead = head;
    slowo *temp;
    slowo *nextHead = firstHead;

    if (!head)
        return NULL;

    if (stringCompare(head->znaki, toRemove))
        return head->next;

    for (int i = 0; 1; i++)
    {
        if (!nextHead->next)
            return firstHead;
        if (stringCompare(toRemove, nextHead->next->znaki))
        {
            temp = nextHead->next;
            nextHead->next = nextHead->next->next;
            deleteWord(temp);
            return firstHead;
        }
    }
}

//--------------------------------------------------------------------------------------
slowo *KASUJ(slowo *head)
{
    if (head == NULL)
        return NULL;

    slowo *temp = head;
    while (head->next != NULL)
    {
        head = head->next;
        deleteWord(temp);
        temp = head;
    }
    deleteWord(temp);
    return NULL;
}

//--------------------------------------------------------------------------------------
slowo *BEZPOWTORZEN(slowo *provided)
{
    if (provided == NULL)
        return NULL;

    slowo *newHead = createWord(provided->znaki);
    slowo *iterator = provided->next;

    while (iterator != NULL)
    {
        if (SZUKAJ(iterator->znaki, newHead) == NULL && iterator->znaki != NULL)
            newHead = WSTAW(iterator->znaki, newHead);
        iterator = iterator->next;
    }
    return newHead;
}

//--------------------------------------------------------------------------------------
slowo *SCAL(slowo **A, slowo **B)
{
    slowo *newHead = *A;
    slowo *temp = *A;

    while (temp->next != NULL)
    {
        temp = temp->next;
    }
    temp->next = *B;

    *A = NULL;
    *B = NULL;
    return newHead;
}

//--------------------------------------------------------------------------------------
void DRUKU(slowo *head)
{
    printf("%s | ", head->znaki);
}

// UTILS
//======================================================================================
int stringCompare(char *A, char *B)
{
    int isEqual = 1;
    for (int i = 0; 1; i++)
    {
        if (A[i] == 0 && B[i] == 0)
            return 1;
        else if (A[i] == B[i])
        {
        }
        else
            return 0;
    }
}

//--------------------------------------------------------------------------------------
void printList(slowo *head, char *message)
{
    printf("%s:\t", message);
    if (head == NULL)
    {
        printf("(lista jest pusta)\n");
        return;
    }

    int i = 1;
    slowo *temp = head;
    while (i)
    {
        if (temp != NULL)
        {
            DRUKU(temp);
            temp = temp->next;
        }
        else
        {
            printf("\n");
            i = 0;
        }
    }
}

//--------------------------------------------------------------------------------------
void printLabel(char *msg)
{
    printf("\n-----------------------------\n%s\n", msg);
}

//--------------------------------------------------------------------------------------
slowo *createTestList(int n)
{
    slowo *newHead = createWord("1");
    for (int i = 0; i < n; i++)
    {
        int z = (i % 10) + 1;
        char str[2];
        sprintf(str, "%d", z);
        newHead = WSTAW(str, newHead);
    }
    return newHead;
}

//--------------------------------------------------------------------------------------
void copyText(char *src, char *dst)
{
    int i = -1;
    do
    {
        i++;
        dst[i] = src[i];
    } while (src[i] != 0);
}