#include <stdio.h>
#include <stdlib.h>
#define MAX 20
typedef struct element
{
    struct element *next;
    char *slowo;
} el_listy;

el_listy *p; //pierwszy element listy

void kopiujTekst(char *in, char *out)
{
    int i = -1;
    do
    {
        i++;
        out[i] = in[i];

    } while (in[i] != 0);
}

el_listy *tworz(char *nowe_znaki)
{
    el_listy *nowe_Slowo = malloc(sizeof(el_listy));
    nowe_Slowo->slowo = malloc(sizeof(char) * MAX);
    kopiujTekst(nowe_znaki, nowe_Slowo->slowo);
    nowe_Slowo->next = NULL;
    return nowe_Slowo;
}

int czyRowne(char *A, char *B)
{

    for (int i = 0; 1; i++)
    {
        if (A[i] == 0 && B[i] == 0)
            return 1;
        else if (A[i] == B[i])
        {
        }
        else
            return 0;
    }
}

el_listy *WSTAW(el_listy *head, char *v)
{
    // p - nowe slowo
    el_listy *p = tworz(v);
    p->next = head;
    return p;
}

el_listy *SZUKAJ(char *a, el_listy *head)
{
    //a- do znalezienia
    if (head == NULL)
        return NULL;
    else if (czyRowne(a, head->slowo))
        return head;
    else
        return SZUKAJ(a, head->next);
}

void DRUKUJ(el_listy *head)
{
    /*jakby nie widzi tego*/
    if (head == NULL)
    {
        printf("(lista jest pusta)\n");
        return;
    }

    int i = 1;
    el_listy *temp = head;
    while (i)
    {
        if (temp != NULL)
        {
            printf("%s\n", temp->slowo);
            temp = temp->next;
        }
        else
        {
            printf("\n");
            i = 0;
        }
    }
}

el_listy *USUN(char *usun, el_listy *head)
{
    el_listy *pierwsze = head;
    el_listy *temp;
    el_listy *nastepny = pierwsze;

    if (!head)
        return NULL;

    if (czyRowne(head->slowo, usun))
        return head->next;

    for (int i = 0; 1; i++)
    {
        if (!pierwsze->next)
            return pierwsze;
        if (czyRowne(usun, nastepny->next->slowo))
        {
            temp = nastepny->next;
            nastepny->next = nastepny->next->next;
            free(temp->slowo);
            free(temp);
            return pierwsze;
        }
    }
}

el_listy *KASUJ(el_listy *lista)
{
    // if (lista == NULL)
    //     return NULL;

    el_listy *wsk = lista;
    el_listy *temp = lista;

    while (wsk->next != NULL) //przesuwamy wsk aż znajdziemy ostatni element
    {
        wsk = wsk->next;
        free(temp->slowo);
        free(temp);
        // wsk = lista;
    }
    free(wsk->slowo);
    free(wsk);
    return NULL;
}

el_listy *BEZPOWTORZEN(el_listy *pierwsza)
{
    if (pierwsza == NULL)
        return NULL;

    el_listy *poczatek = tworz(pierwsza->slowo);
    el_listy *iterator = pierwsza->next;

    while (iterator != NULL)
    {
        if (SZUKAJ(iterator->slowo, poczatek) == NULL && iterator->slowo != NULL)
            poczatek = WSTAW(poczatek, iterator->slowo);
        iterator = iterator->next;
    }
    return poczatek;
}
el_listy *SCAL(el_listy **A, el_listy **B)
{
    el_listy *poczatek = *A;
    el_listy *temp = *A;

    while (temp->next != NULL)
    {
        temp = temp->next;
    }
    temp->next = *B;

    *A = NULL;
    *B = NULL;
    return poczatek;
}

void main()
{
    printf("Na poczatku: \n");
    el_listy *lista = tworz("jeden");
    DRUKUJ(lista);
    printf("Po wstawieniu: \n");
    lista = WSTAW(lista, "mama");
    DRUKUJ(lista);
    printf("Po wstawieniu: \n");
    lista = WSTAW(lista, "kot");
    DRUKUJ(lista);
    char *slowoDoUsuniecia = "mama";
    printf("Po usunieciu slowa '%s': \n", slowoDoUsuniecia);
    lista = USUN(slowoDoUsuniecia, lista);
    DRUKUJ(lista);
    KASUJ(lista);
    printf("Po skasowaniu listy: \n");

    /*nie dziala opcja z kasowaniem, program zawiesza sie jakby wtedy*/
    //lista=KASUJ(lista);

    DRUKUJ(lista);

    el_listy *lista2 = tworz("abc1");
    lista2 = WSTAW(lista2, "abc2");
    lista2 = WSTAW(lista2, "abc3");
    lista2 = WSTAW(lista2, "abc1");
    lista2 = WSTAW(lista2, "abc3");
    lista2 = WSTAW(lista2, "abc4");
    printf("lista2: \n");
    DRUKUJ(lista2);
    el_listy *bezPowtorzen = BEZPOWTORZEN(lista2);
    printf("lista2 bez powtorzen: \n");
    DRUKUJ(bezPowtorzen);
    el_listy *lista3 = tworz("abcd");
    lista3 = WSTAW(lista3, "abcde");
    lista3 = WSTAW(lista3, "abc2");
    printf("lista3: \n");
    DRUKUJ(lista3);
    printf("scalone 2 listy: \n");
    el_listy *lista4 = SCAL(&bezPowtorzen, &lista3);
    DRUKUJ(lista4);
}

/*void main(){
	unsigned int calculateFibonacciASM( int index );
}

asm_volatile(
  ".globl _calculateFibonacciASM"
  "_calculateFibonacciASM:"
  "mov ecx, [ esp + 4 ] "
  "mov eax,0"
  "mov esi,1"
  "mov edi,0"
  "loop:"
  "test ecx,ecx\n"
  "jbe loopEnd\n"
  "sub ecx,1\n"
  "mov edi,eax\n"
  "add eax,esi\n"
  "mov esi,edi\n"
  "jmp loop\n"
  "loopEnd:\n"
  "ret"
);
*/
/*.intel_syntax noprefix
.global main
.text
main:
 //podanie wartosci
    mov ecx,100
//***********

//wywolanie funkcji
petla:
   mov eax, ecx
   push eax ;//wrzucenie liczby na stos
   call f ;//wywolanie funkcji
    add esp, 4; //czyszczenie stosu
//******************

//wypisanie wyniki
    push eax
    push offset msg
    call printf
    add esp, 8
    mov eax, 0
//**************
    ret

f:
    mov eax, [esp+4];//wczytywanie zmiennej n[eax] do stosu
    cmp eax, 0x00000000
    je zero

    mov eax,ecx
    sub ecx,1
    jmp petla
    ret

zero:
    xor eax,eax
    ret



.data
msg: .asciz "Wynik=%i\n"
.byte 0

*/
