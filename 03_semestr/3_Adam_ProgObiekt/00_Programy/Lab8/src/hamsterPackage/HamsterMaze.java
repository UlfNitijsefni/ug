package hamsterPackage;
import java.util.*;

//-------------------------------------------------------------------------------------------
class Hamster {
  private int hamsterNumber;
  private String hamsterName;
  Hamster(int i, String name) {
	  hamsterNumber = i;
	  hamsterName = name;
  }
  public String hamsterString() {
    return "This is Hamster #" + hamsterNumber + "And this is his name: " + hamsterName;
  }
  public int hamsterNumber() {
	    return hamsterNumber;
  }
  public String hamsterName() {
	    return hamsterName;
  }
}

//-------------------------------------------------------------------------------------------
class Printer {
    static void printAll(List <Hamster> allHamsters) {
  	  for (int i = 0 ;i < allHamsters.size(); i++) {
		  System.out.println("Hamsters number:" + allHamsters.get(i).hamsterNumber());
		  System.out.println("His name: " + allHamsters.get(i).hamsterName());
	  }
    }
}

//-------------------------------------------------------------------------------------------
public class HamsterMaze {
  public static void main(String[] args) {
      List<Hamster> v = new ArrayList<Hamster>();
      for(int i = 0; i < 3; i++)
	  v.add(new Hamster(i, ((Integer)(100 - i)).toString()));
      
      Printer.printAll(v);
  }
  public Map<String, Hamster> mappingHamsters(List<Hamster> provided) {
	  Map<String, Hamster> map = new HashMap<String, Hamster>();
	  for(int i = 0; i < provided.size(); i++) {
		  map.put(provided.get(i).hamsterName(), provided.get(i));
	  }
	  return map;
  }
}
/*
import java.util.*;
class Komparator implements Comparator<Hamster>{

	@Override
	public int compare(Hamster o1, Hamster o2) {
		
		return o1.hamsterNumber().compareTo(o2.hamsterNumber());
	}
	
}
class Hamster implements Comparable<Hamster>{
          private int hamsterNumber;
          public String name;
          Random r = new Random();
          Hamster() { hamsterNumber =r.nextInt(10) ; }
          Hamster(int i) { hamsterNumber = i; }
          public String hamsterNumber() {
            return "This is Hamster #" + hamsterNumber;
          }
          
        
		@Override
		public int compareTo(Hamster o) {
		    // return 0;
			 int result = Integer.compare(this.hamsterNumber,o.hamsterNumber); 
			 return result;
			
		}


        }

        class Printer {
            static void printAll(List <Hamster> hl) {
                for(int i=0; i<hl.size(); i++)
                System.out.println(hl.get(i).hamsterNumber());



            }
            static void usun(Iterator<Hamster> ite,List<Hamster>h) {
            	while(ite.hasNext()) {
                    ite.remove();
                }
            }
        }
public class HamsterMaze {
        public static void main(String[] args) {

              List<Hamster> v = new ArrayList<Hamster>();

//              for(int i = 0; i < 3; i++)
//              {
//                  v.add(new Hamster(i));
//              }


              for(int i = 0; i < 20; i++)
              {
                  v.add(new Hamster());
              }
            
            //Printer.printAll(v); zamiast tego iterator:
               Collections.sort(v);
               Iterator<Hamster> it=v.iterator();
               
               while(it.hasNext()) {

                   Hamster t = it.next();
                   System.out.print(t.hamsterNumber()+", ");
               }
               Printer.usun(it,v);
               System.out.println("");
               System.out.println("*************************************************************************");
               List<Hamster> v2 = new ArrayList<Hamster>();
               for(int i = 0; i < 10; i++)
               {
                v2.add(new Hamster());
               }     
              Collections.sort(v2, new Komparator());
              Iterator<Hamster> it2=v2.iterator();
              
        
              while(it2.hasNext()) {

                  Hamster t2 = it2.next();
                  System.out.print(t2.hamsterNumber()+", ");
              }
              
            
              Map<String,Hamster> mapa= new TreeMap<String,Hamster>();
              for(int i = 0; i < 3; i++)
              {
                  mapa.put(new Hamster(i).name=("imie"+i),new Hamster(i));
              }
              System.out.println("");
              System.out.println("klucze:");
              System.out.println(mapa.keySet());

              for(String ha : mapa.keySet()) {
                 System.out.println( mapa.get(ha).hamsterNumber());
              }
        }
}


*/