/**
 @version 1.30 2000-05-02
 @author Cay Horstmann
 */

 import javax.swing.*;
 import java.awt.*;

public class TestHelloWorld
{
    public static void main(String[] args)
    {
        RamkaNieWitajSwiecie ramka = new RamkaNieWitajSwiecie();
        ramka.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ramka.setVisible(true);
        //ramka.show();
    }
}

/**
 Ramka , zawierajaca panel wiadomosci
 */
class RamkaNieWitajSwiecie extends JFrame
{
    Toolkit zestaw = Toolkit.getDefaultToolkit();
    Dimension rozmiarEkranu = zestaw.getScreenSize();
    int wysEkranu = rozmiarEkranu.height;
    int szerEkranu = rozmiarEkranu.width;

    public RamkaNieWitajSwiecie()
    {
        setTitle("Testowe okno");
        setSize(SZEROKOSC, WYSOKOSC);

        // dolacz panel do ramki

        PanelNieWitajSwiecie panel = new PanelNieWitajSwiecie();
        Container powZawartosci = getContentPane();
        powZawartosci.add(panel);
    }

    public static final int SZEROKOSC = 400;
    public static final int WYSOKOSC = 400;

    public static final int WIADOMOSC_X = 500;
    public static final int WIADOMOSC_Y = 500;
}

/**
 Panel, wyswietlajacy wiadomosc
 */
//class PanelNieWitajSwiecie extends JPanel
//{
//    public void paintComponent(Graphics g)
//    {
//        super.paintComponent(g);
//
//        g.drawString("To nie jest program 'Witaj, Swiecie'",
//                WIADOMOSC_X, WIADOMOSC_Y);
//    }
//

//}
