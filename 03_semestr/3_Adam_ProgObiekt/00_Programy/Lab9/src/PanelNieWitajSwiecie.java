/**
 @version 1.30 2000-05-02
 @author Cay Horstmann
 */

import javax.swing.*;
import java.awt.*;

/**
 Panel, wyswietlajacy wiadomosc
 */
class PanelNieWitajSwiecie extends JPanel
{
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        g.drawString("To nie jest program 'Witaj, Swiecie'",
                WIADOMOSC_X, WIADOMOSC_Y);
    }

    public static final int WIADOMOSC_X = 500;
    public static final int WIADOMOSC_Y = 500;
}
