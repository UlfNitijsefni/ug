package zwierzeta;

public class Czworonog extends Zwierze{
	
//	protected String ryk;
	
	public Czworonog() {
		super();
	}
	
	public Czworonog(String ryk) {
		super(ryk);
	}
	
	protected void ryki() {		
		this.sredniRyk();
		this.glosnyRyk();
	}
}
