package zwierzeta;

public class Zwierze {
	
	protected String ryk;
	
	
	public Zwierze(String ryk) {
		this.ryk = ryk;
		cichyRyk();
	}
	
	
	private void cichyRyk() {
		printOnScreen(ryk);
	}
	
	protected void sredniRyk() {
		printOnScreen(ryk + " " + ryk);
	}
	
	public void glosnyRyk() {
		printOnScreen(ryk + " " + ryk + " " + ryk);
	}
	
	public Zwierze() {
		cichyRyk();
	}
	
	protected void printOnScreen(String toPrint) {
		String myName = this.getClass().getName().toString().split("\\.")[1];
		System.out.println("Ryczy " + myName + ": " + toPrint);
	}
	
}