package Test;

import Domain.Pawn;
import Domain.Vector2;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PawnTest {
    Pawn pawn;

    @Before
    public void init() {
        pawn = new Pawn();
        pawn.setPosition(new Vector2(0,0));
    }

    @Test
    public void testPawnShouldMoveOnePositionForward() {
        Assert.assertTrue(pawn.isValidMove(new Vector2(0,1)));
    }

    @Test
    public void testPawnShouldMoveSeveralPositionForward() {
        Assert.assertFalse(pawn.isValidMove(new Vector2(0,5)));
    }

    @Test
    public void testPawnShouldNotMoveOnePositionLeft() {
        Assert.assertFalse(pawn.isValidMove(new Vector2(1,0)));

    }
}
