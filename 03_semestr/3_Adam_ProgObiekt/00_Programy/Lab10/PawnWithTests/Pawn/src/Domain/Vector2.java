package Domain;

public class Vector2 {
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    private int x;
    private int y;

    public SetVector2(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
