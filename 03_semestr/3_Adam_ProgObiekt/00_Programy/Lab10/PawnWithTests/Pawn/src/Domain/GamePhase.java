package Domain;

public enum GamePhase {
    Menu, Game, End, Lost
}
