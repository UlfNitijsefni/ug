package Domain;

public class Pawn extends ChessPiece {
    public Pawn() { }

    @Override
    public boolean isValidMove(Vector2 pos) {
        if(getPosition().getX() == pos.getX()) {
            return true;
        }
        return false;
    }

    public void PlacePosition() {

    }
}
