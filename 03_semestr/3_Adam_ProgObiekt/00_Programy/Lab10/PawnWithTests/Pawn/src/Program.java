import Controllers.GameController;

public class Program {
    public static void main(String[] args) {
        GameController gameController = new GameController(10);
        gameController.gameLoop();
    }
}
