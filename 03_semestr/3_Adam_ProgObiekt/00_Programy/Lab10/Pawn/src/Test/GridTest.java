package Test;

import Domain.Grid;
import Domain.Vector2;
import org.junit.Assert;
import org.junit.Test;

public class GridTest {

    @Test
    public void testGridShouldBeNotNull() {
        Grid grid;
        grid = new Grid(new Vector2(2,3));
        Assert.assertNotNull(grid);
    }

    @Test
    public void testGridSizeShouldNotBeNull(){
        Grid grid = new Grid(new Vector2(2,2));
        Assert.assertNotNull(grid.getSize());
    }

    @Test
    public void testGirdSizeShouldHaveProperValue() {
        Vector2 size = new Vector2(2,2);
        Grid grid = new Grid(size);
        Assert.assertEquals(size, grid.getSize());
    }

}
