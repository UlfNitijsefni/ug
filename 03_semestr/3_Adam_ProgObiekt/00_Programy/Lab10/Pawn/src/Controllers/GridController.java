package Controllers;

import Domain.*;

public class GridController {
    private Grid grid;
    int moveCounter;

    public GridController(int sizeX ,int sizeY) {
        grid = new Grid(new Vector2(sizeX, sizeY));
        moveCounter = 0;
    }

    public void generateGrid() {
        for(int x = 0; x < grid.getSize().getX();x++) {
            for (int y = 0; y < grid.getSize().getY(); y++) {
                Vector2 position = new Vector2(x,y);
                SquareColor colorForCell = getColorForPosition(position);
                grid.getBoard()[x][y] = new Square(colorForCell,position);
            }
        }
    }

    public void clearBoard() {
        for (int x =0; x <grid.getSize().getX();x++) {
            for(int y = 0; y < grid.getSize().getY(); y++) {
                grid.getSquareAt(x,y).setPieceOnSquare(null);
            }
        }
    }

    public int getMoveCounter() {
        return moveCounter;
    }

    public boolean placePieceOnBoard(ChessPiece piece, Vector2 position) {
        if(position == null) {
            position = getSpawnDefaultPosition();
        }

        grid.getSquareAt(position).setPieceOnSquare(piece);
        piece.setPosition(position);
        return true;
    }

    public int getGridSizeY() {
        return grid.getSize().getY();
    }

    public Vector2 getSpawnDefaultPosition() {
        int x = grid.getSize().getX() / 2;
        int y = grid.getSize().getY() / 2;
        return new Vector2(x,y);
    }

    public boolean movePiece(ChessPiece piece, Vector2 newPosition) {
        if(piece.isValidMove(newPosition)){
            grid.getSquareAt(piece.getPosition()).setPieceOnSquare(null);
            grid.getSquareAt(newPosition).setPieceOnSquare(piece);
            piece.setPosition(newPosition);

            if(grid.getSquareAt(newPosition).getSquareColor() == SquareColor.Black){
                moveCounter++;
            }
            return true;
        }
        return false;
    }

    private SquareColor getColorForPosition(Vector2 position) {
        if(position.getX() % 2 == 0){
            if(position.getY() % 2 == 0){
                return SquareColor.White;
            }else{
                return SquareColor.Black;
            }
        }else{
            if(position.getY() % 2 == 0){
                return SquareColor.Black;
            }else{
                return SquareColor.White;
            }
        }
    }
}
