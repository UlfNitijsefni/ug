package Controllers;

import Domain.GamePhase;
import Domain.Pawn;
import Domain.Vector2;

import java.util.Scanner;

public class GameController {
    GridController gridController;
    GamePhase gamePhase;
    Pawn pawn;
    int moveLimit;

    public GameController(int moveLimit)
    {
        this.moveLimit = moveLimit;
        initGame();
    }

    public void gameLoop() {
        while(true) {
            switch(gamePhase){
                case Menu:
                    printMenu();
                    break;
                case Game:
                    if(checkWinCondition()){
                        changeGamePhase(GamePhase.End);
                    }
                    if(checkLoseCondition()){
                        changeGamePhase(GamePhase.Lost);
                    }
                    printGameMenu();
                    break;
                case End:
                    printEndMessage();
                    break;
                case Lost:
                    printLostMessage();
                    break;
            }
        }
    }

    private void printLostMessage(){
        System.out.println("Przegrałeś! Wpisz 'r', żeby zacząć od początku");
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        if(s.equals("r")) {
            initGame();
        }
    }

    private void printEndMessage() {
        System.out.println("Gratulacje! Wpisz 'r', żeby zacząć od początku");
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        if(s.equals("r")) {
            initGame();
        }
    }

    private boolean checkLoseCondition() {
        if(gridController.getMoveCounter() >= moveLimit) {
            return true;
        }
        return false;
    }

    private boolean checkWinCondition() {
        if(pawn.getPosition().getY() == gridController.getGridSizeY() - 1){
            return true;
        }
        return false;
    }

    private void initGame(){
        gridController = new GridController(10,10);
        gridController.generateGrid();
        gamePhase = GamePhase.Menu;
        pawn = new Pawn();
        gridController.placePieceOnBoard(pawn, null);
    }

    private void changeGamePhase(GamePhase phase) {
        gamePhase = phase;
    }

    private void printGameMenu() {
        System.out.println("Jesteś na pozycji: X: " + pawn.getPosition().getX() + " Y: " + pawn.getPosition().getY());
        System.out.println("Wpisz 'w', aby poruszyć się naprzód. Wpisz 'r', aby zresetować stan gry.");
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        if(s.equals("w")) {
            Vector2 newPosition = new Vector2(pawn.getPosition().getX(), pawn.getPosition().getY() + 1);
            gridController.movePiece(pawn,newPosition);
        } else if(s.equals("r")) {
            initGame();
        }
        return;
    }

    private void printMenu() {
        System.out.println("Naciśnij, aby rozpocząć");
        Scanner in = new Scanner(System.in);
        in.nextLine();
        changeGamePhase(GamePhase.Game);
    }


}
