package Domain;

public class Grid {
    private Vector2 size;
    private int cellsCount;
    private Square[][] board;

    public Grid(Vector2 size) {
        this.size = size;
        board = new Square[size.getX()][size.getY()];
        cellsCount = size.getX() * size.getY();
    }

    public Vector2 getSize() {
        return size;
    }

    public void setSize(Vector2 size) {
        this.size = size;
    }

    public int getCellsCount() {
        return cellsCount;
    }

    public void setCellsCount(int cellsCount) {
        this.cellsCount = cellsCount;
    }

    public Square[][] getBoard() {
        return board;
    }

    public void setBoard(Square[][] board) {
        this.board = board;
    }

    public Square getSquareAt(Vector2 position) {
        return board[position.getX()][position.getY()];
    }

    public Square getSquareAt(int x, int y) {
        return board[x][y];
    }

}
