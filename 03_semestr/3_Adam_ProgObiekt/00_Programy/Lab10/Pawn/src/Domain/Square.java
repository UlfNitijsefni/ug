package Domain;

public class Square {
    private SquareColor squareColor;
    private Vector2 position;

    public SquareColor getSquareColor() {
        return squareColor;
    }

    public void setSquareColor(SquareColor squareColor) {
        this.squareColor = squareColor;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public ChessPiece getPieceOnSquare() {
        return pieceOnSquare;
    }

    public void setPieceOnSquare(ChessPiece pieceOnSquare) {
        this.pieceOnSquare = pieceOnSquare;
    }

    private ChessPiece pieceOnSquare;

    public Square(SquareColor color, Vector2 position) {
        squareColor = color;
        this.position = position;
    }
}
