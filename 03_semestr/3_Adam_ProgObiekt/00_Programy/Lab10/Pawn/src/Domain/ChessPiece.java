package Domain;

public abstract class ChessPiece {
    private Vector2 position;

    public ChessPiece() { }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public abstract boolean isValidMove(Vector2 pos);
}
