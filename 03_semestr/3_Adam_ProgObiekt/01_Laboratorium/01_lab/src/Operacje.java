class KlasaA {
    int wartoscA = 10;
    int wartoscB = 5;
}

class Kalkulator {
    public int dodaj(int a, int b) {
        return a + b;
    }
}

public class Operacje {
    public static void main(String[] args) {

        /*

        // Błąd! Trzeba stworzyć obiekt klasy
        int a = KlasaA.wartoscA;

        // Również błąd, musimy stworzyć obiekt i na nim operować
        wartoscA = 5;
        */

        //Tworzenie instancji klasy 'KlasaA'
        KlasaA klasa = new KlasaA();

        // Tworzymy instancję klasy Kalkulator
        Kalkulator kalkulator = new Kalkulator();

        // pobieramy wartości zadeklarowane w klasie 'KlasaA' i przekazujemy jako parametry do funkcji
        int razem = kalkulator.dodaj(klasa.wartoscA, klasa.wartoscB);


        System.out.println(razem);
    }
}