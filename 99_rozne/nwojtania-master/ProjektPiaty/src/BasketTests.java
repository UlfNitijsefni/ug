import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

public class BasketTests {
		@Test
		public void testAssertEquals(){
		Basket b = new Basket();
		Product pr = new Product("mleko",2.5);
		Product pr2 = new Product("mleczko",4.9);
		assertTrue(b.addProduct(pr));
		assertFalse(b.addProduct(pr));
		assertEquals(1,b.getProductsCount());
		assertTrue(b.removeProduct(pr.getId())); 
		//System.out.println(b.getProduct(pr.getId()));
		assertNull(b.getProduct(pr.getId()));
		assertNotNull(pr);
		b.addProduct(pr);
		 assertTrue(b.updateProduct(1, pr2));
		 assertEquals(5.39,b.calculateBruttoBasketValue());
		 assertEquals(4.9,b.calculateBasketNettoValue());
		 
		 
		 
		 }
	
}

