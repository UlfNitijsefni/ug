import java.util.ArrayList;
import java.util.List;

public class Basket {
    private List<Product> products;

    public boolean addProduct(Product product) {
        if(products.contains(product)) {
            return false;
        }

        products.add(product);
        return true;
    }

    public boolean removeProduct(int productId) {
        products.remove(productId-1);
        return true;
    }

    public Product getProduct(int productId) {
        for (Product product: products) {
            if(product.getId() == productId){
                return product;
            }
        }
        return null;
    }

    public boolean updateProduct(int productId, Product updatedProduct) {
        if(updatedProduct == null) {
            return false;
        }

        Product oldProduct = getProduct(productId);
        oldProduct.setName(updatedProduct.getName());
        oldProduct.setNettoPrice(updatedProduct.getNettoPrice());
        return true;
    }

    public int getProductsCount() {
        return products.size();
    }

    public double calculateBasketNettoValue() {
        double value = 0;
        for(Product product: products) {
            value += product.getNettoPrice();
        }
        return value;
    }

    public double calculateBruttoBasketValue() {
        double value = 0;
        for(Product product: products) {
            value += product.getBruttoPrice();
        }
        return value;
        
    }


    public Basket() {
        products = new ArrayList<Product>();
    }


}

/*import java.util.ArrayList;
import java.util.List;

public class Basket {
    private List<Product> products;

    public boolean addProduct(Product product) {
        if(products.contains(product)) {
            return false;
        }

        products.add(product);
        return true;
    }

    public boolean removeProduct(int productId) {
        products.remove(productId);
        return true;
    }

    public Product getProduct(int productId) {
        for (Product product: products) {
            if(product.getId() == productId){
                return product;
            }
        }
        return null;
    }

    public boolean updateProduct(int productId, Product updatedProduct) {
        if(updatedProduct == null) {
            return false;
        }

        Product oldProduct = getProduct(productId);
        oldProduct.setName(updatedProduct.getName());
        oldProduct.setNettoPrice(updatedProduct.getNettoPrice());
        return true;
    }

    public int getProductsCount() {
        return products.size();
    }

    public double calculateBasketNettoValue() {
        double value = 0;
        for(Product product: products) {
            value += product.getNettoPrice();
        }
        return value;
    }

    public double calculateBruttoBasketValue() {
        double value = 0;
        for(Product product: products) {
            value += product.getBruttoPrice();
        }
        return value;
    }


    public Basket() {
        products = new ArrayList<Product>();
    }


}*/
