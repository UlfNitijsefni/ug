import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;

public class ProductTests {
	
		@Test
		public void testAssertEquals(){
		Product p = new Product("mleko",2.5);
		p.setNettoPrice(10.98);
		p.setName("mleczko");
		assertEquals(10.98,p.getNettoPrice());
		assertEquals("mleczko",p.getName());
		assertEquals(1,p.getId());
		assertEquals(12.07,p.getBruttoPrice());
		 
 }
}
