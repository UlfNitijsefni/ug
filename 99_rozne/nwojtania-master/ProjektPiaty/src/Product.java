 public class Product {
    private static int idIncremental = 1;
    private int id;
    private String name;
    private double nettoPrice;
    private int vat;

    public Product(String name, double nettoPrice) {
        
    	this.id = idIncremental;
    	idIncremental++;
        this.name = name;
        this.nettoPrice = nettoPrice;
        vat = 23;
    }

    public Product(String name, double nettoPrice, int vat) {
        idIncremental++;
        this.id = idIncremental;
        this.name = name;
        this.nettoPrice = nettoPrice;
        this.vat = vat;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getNettoPrice() {
        return nettoPrice;
    }

    public void setNettoPrice(double nettoPrice) {
        this.nettoPrice = nettoPrice;
    }

    public double getBruttoPrice() {
    	
        int l= (int)((nettoPrice + (nettoPrice * 0.1))*100);
        return (double)l/100;
    }
}

  
/*public class Product {
    private static int idIncremental = 1;
    private int id;
    private String name;
    private double nettoPrice;
    private int vat;

    public Product(String name, double nettoPrice) {
        idIncremental++;
        this.id = idIncremental;
        this.name = name;
        this.nettoPrice = nettoPrice;
        vat = 23;
    }

    public Product(String name, double nettoPrice, int vat) {
        idIncremental++;
        this.id = idIncremental;
        this.name = name;
        this.nettoPrice = nettoPrice;
        this.vat = vat;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getNettoPrice() {
        return nettoPrice;
    }

    public void setNettoPrice(double nettoPrice) {
        this.nettoPrice = nettoPrice;
    }

    public double getBruttoPrice() {
        return nettoPrice + (nettoPrice * 0.1);
    }
}*/
