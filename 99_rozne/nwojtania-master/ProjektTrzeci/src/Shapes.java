//: c07:Shapes.java
// From 'Thinking in Java, 2nd ed.' by Bruce Eckel
// www.BruceEckel.com. See copyright notice in CopyRight.txt.
// Polymorphism in Java.

abstract class Shape { 
  abstract void draw();
  abstract void erase();
  abstract public void wyswietlKomunikat(); 
}

class Circle extends Shape {
  void draw() { 
    System.out.println("Circle.draw()"); 
  }
  void erase() { 
    System.out.println("Circle.erase()"); 
  }
public void wyswietlKomunikat() {
	  System.out.println( "Circle.komunikat");
  }
}

class Square extends Shape {
  void draw() { 
    System.out.println("Square.draw()"); 
  }
  void erase() { 
    System.out.println("Square.erase()"); 
  }
public void wyswietlKomunikat() {
	  System.out.println( "Square.komunikat");
  }
}

class Triangle extends Shape {
  void draw() { 
    System.out.println("Triangle.draw()"); 
  }
  void erase() { 
    System.out.println("Triangle.erase()");
  }
public void wyswietlKomunikat() {
	  System.out.println( "Triangle.komunikat");
  }
}

public class Shapes {
  public static Shape randShape() {
    switch((int)(Math.random() * 3)) {
      default:
      case 0: return new Circle();
      case 1: return new Square();
      case 2: return new Triangle();
    }
  }
  public static void main(String[] args) {
    Shape[] s = new Shape[9];
    // Fill up the array with shapes:
    for(int i = 0; i < s.length; i++)
      s[i] = randShape();//rzutowanie w gore
    // Make polymorphic method calls:
    for(int i = 0; i < s.length; i++)
    {  s[i].draw();
      s[i].wyswietlKomunikat();
    }
  }
} ///:~
