import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class Student {
	  public String imię;
      public String nazwisko;
      public int numerIndeksu;
      public String data;

      ArrayList<Integer>oceny=new ArrayList<Integer>();
      public Student() {

      }
      public Student(String i, String n, int nI, String d) {
              imię=i;
              nazwisko=n;
              numerIndeksu=nI;
              data=d;
      }
      public void WypiszDane() {
              System.out.println("imie:" +imię+" nazwisko: "+nazwisko+" nr indeksu: "+numerIndeksu+" data rozpoczecia studiow: "+data);
      }
      public void IloscDniJakoStudent() throws ParseException {
    	  Date currentDate = new Date();
    	  SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
		Date date1 = myFormat.parse(data);
  	    long diff = currentDate.getTime() - date1.getTime();
  	    System.out.println("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
    	 
      }
     
      public void DodajOcene(int ocena) {
              oceny.add(ocena);
      }
      public void ObliczSrednia() {
              double srednia = 0.0;

              for (Integer d : oceny)
                  srednia += d;
              srednia /= oceny.size();
              System.out.println("srednia ocen: "+srednia);
      }

}

