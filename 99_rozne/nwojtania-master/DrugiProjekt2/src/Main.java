import java.text.ParseException;


public class Main {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		 Student s1 = new Student();
         s1.imię="Darek";
         s1.nazwisko="Kowalski";
         s1.numerIndeksu=123456;
         s1.data="11 05 2018";
         s1.WypiszDane();
         s1.IloscDniJakoStudent();
         s1.DodajOcene(5);
         s1.DodajOcene(3);
         s1.ObliczSrednia();
        
         Student s2 = new Student("Jan","Nowak",3333333,"15 10 2019");
         s2.WypiszDane();
         s2.IloscDniJakoStudent();
         s2.DodajOcene(1);
         s2.DodajOcene(6);
         s2.ObliczSrednia();
	}

}

