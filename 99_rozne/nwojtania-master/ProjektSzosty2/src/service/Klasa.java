package service;
import java.util.List;

import domain.IShape;

public class Klasa implements IShapeManager{
	public List<IShape> shapes;
	@Override
	public void addShape(IShape shape) {
		// TODO Auto-generated method stub
		shapes.add(shape);
	}

	@Override
	public void printShapesArea() {
		// TODO Auto-generated method stub
		for(IShape s: shapes) {
			System.out.print(s);
			System.out.println(s.calculateArea());
			
		}
	}

	@Override
	public void printShapesPerimeter() {
		// TODO Auto-generated method stub
		for(IShape s: shapes) {
			System.out.print(s);
			System.out.println(s.calculatePerimeter());
		}
			
	}

	
}
