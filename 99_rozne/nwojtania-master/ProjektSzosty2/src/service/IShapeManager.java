package service;

import domain.IShape;

public interface IShapeManager {
public void addShape(IShape shape);
public void printShapesArea();
public void printShapesPerimeter();
}
