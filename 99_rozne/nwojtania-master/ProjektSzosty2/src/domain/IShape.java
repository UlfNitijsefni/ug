package domain;

public interface IShape {
public double calculateArea();
public double calculatePerimeter();
}
