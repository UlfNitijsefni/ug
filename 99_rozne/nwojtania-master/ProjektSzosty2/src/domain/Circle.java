package domain;

public class Circle implements IShape {
public int r;
	@Override
	public double calculateArea() {
		// TODO Auto-generated method stub
		return 3.14*r*r;
	}

	@Override
	public double calculatePerimeter() {
		// TODO Auto-generated method stub
		return 2*3.14*r;
	}

}
