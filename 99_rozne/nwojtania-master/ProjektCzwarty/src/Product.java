import java.util.Date;
import java.util.UUID;


public class Product implements Comparable<Product> {
	//private int id;
	String uniqueID = UUID.randomUUID().toString();
	private double price;
	private ProductType type;
	private Manufacturer manufacturer;
	private Date productionDate;
	
	public Product( double price, ProductType type, Manufacturer manufacturer, Date productionDate) {
		
		this.manufacturer=manufacturer;
		this.price=price;
		this.productionDate=productionDate;
		this.type=type;
	}
	public Product() {
		
	}
//	public Product(int id) {
//		this.id=id;
//	}
//
//	public int getId() {
//		return id;
//	}
//
//	public void setId(int id) {
//		this.id = id;
//	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public ProductType getType() {
		return type;
	}

	public void setType(ProductType type) {
		this.type = type;
	}

	public Manufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Date getProductionDate() {
		return productionDate;
	}

	public void setProductionDate(Date productionDate) {
		this.productionDate = productionDate;
	}
	@Override
	public String toString() {
		return " " + getManufacturer().name+" ," + type + ",cena=" + price+"  /";
	}

	@Override
	public int compareTo(Product p) {
		return this.getManufacturer().compareTo(p.getManufacturer());
	}	
	
	}



/*import java.util.Date;

public class Product {
	private int id;
	private double price;
	private ProductType type;
	private Manufacturer manufacturer;
	private Date productionDate;
	
	public Product(int id, double price, ProductType type, Manufacturer manufacturer, Date productionDate) {
		this.id=id;
		this.manufacturer=manufacturer;
		this.price=price;
		this.productionDate=productionDate;
		this.type=type;
	}
	public Product(int id) {
		this.id=id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public ProductType getType() {
		return type;
	}

	public void setType(ProductType type) {
		this.type = type;
	}

	public Manufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Date getProductionDate() {
		return productionDate;
	}

	public void setProductionDate(Date productionDate) {
		this.productionDate = productionDate;
	}
	
}*/
