import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;


public class Warehouse {
 Date lastUpdate;
 ArrayList<Product> products = new ArrayList<Product>();
 static int licznik=0;
 
 public static Warehouse Tworzwarunek() {
	 if(licznik==0) {
		 licznik++;	 
		 return new Warehouse();
	 }
	 return null;
 }
	 
 
 public void addProduct(Product newProduct) {
	 products.add(newProduct);
	
 }
 
 public void deleteProduct(int id) {
	 products.remove(id);
 }
 
 public void modifyProduct(int id, Product product) {
	 products.set(id,product);
 }
 public Product getProduct(int id){
	 
	 return products.get(id); 
 }
 
public int getProductsCount() {
	return products.size();
}
public void printProductsOfType(ProductType type) {
	for(int i=0; i<products.size();i++) {
	if(products.get(i).getType()==type)
		System.out.println(products.get(i));
	}
	
}
public void printManufacturerProducts(String manufacturerName) {
	for(int i=0; i<products.size();i++) {
		if(products.get(i).getManufacturer().name==manufacturerName)
			System.out.println(products.get(i));
		}
}
public ArrayList<Product> getProductsBelowPrice(double price) {
	ArrayList<Product> pr = new ArrayList<Product>();
	for(int i=0; i<products.size();i++) {
	if(products.get(i).getPrice()<price*1.23) {
	pr.add(products.get(i));
	}
	}
	return pr;
}

public void printMostPopularManufacturer1() {
	Collections.sort(products);
	for (int i=0; i < products.size(); i++) { 
		System.out.println(products.get(i));			
	 }
	int licznikMax=1;
	int licznik=1;
	String najcz = products.get(0).getManufacturer().name;
	for (int i = 0; i < products.size()-1; i++) {

		if (products.get(i).getManufacturer().name.equals(products.get(i+1).getManufacturer().name))  {
			licznik++;
		} 
		else {			
			if (licznik > licznikMax) {
				najcz = products.get(i).getManufacturer().name;
				licznikMax = licznik;
				licznik = 1; 
			}	 
		}
		
    	if (i==products.size()-2) {
			if (licznik > licznikMax) {
				najcz = products.get(i+1).getManufacturer().name;
				licznikMax = licznik;
			}
		}	
	}

	
	System.out.println( najcz+" "+licznikMax+"razy") ;	
}
	
	/*	System.out.println(nameOfManufacturers);
	String name=null;
	for(int i=0; i<nameOfManufacturers.size();i++)
	{
		name=nameOfManufacturers.
		for(int j=0; j<products.size();j++)
		{
			
		}
			
	}*/


/*public void printMostPopularManufacturer() {
	int counter=0;
	int counter2=0;
	String nazwa=null;
	for(int i=0; i<products.size();i++) {
		nazwa=products.get(i).getManufacturer().name;
		if(nazwa==products.get(i).getManufacturer().name)
		counter++;
		else
		nazwa=products.get(i+1).getManufacturer().name;
		if(nazwa==products.get(i).getManufacturer().name)
		counter2++;
		if(counter<=counter2)
		counter=0;
		else counter2=0;
				
	}
	if(counter<counter2)
		System.out.println(nazwa+counter2);
	else
	System.out.println(nazwa+counter);
}*/
public ArrayList<Product> getProductsMadeBefore(Date date) {
	ArrayList<Product> pr = new ArrayList<Product>();
	for(int i=0; i<products.size();i++) {
		if(products.get(i).getProductionDate().before(date)) {
		pr.add(products.get(i));
		}
		}
	return pr;
}

}

/*import java.util.ArrayList;
import java.util.Date;

public class Warehouse {
 Date lastUpdate;
 ArrayList<Product> products = new ArrayList<Product>();
 
 public void addProduct(Product newProduct) {
	 products.add(newProduct);
 }
 
 public void deleteProduct(int id) {
	 products.remove(id);
 }
 
 public void modifyProduct(int id, Product product) {
	 products.set(id,product);
 }
 public Product getProduct(int id){
	 
	 return products.get(id); 
 }
 
public int getProductsCount() {
	return products.size();
}
public void printProductsOfType(ProductType type) {
	for(int i=0; i<products.size();i++) {
	if(products.get(i).getType()==type)
		System.out.println(products.get(i));
	}
	
}
public void printManufacturerProducts(String manufacturerName) {
	for(int i=0; i<products.size();i++) {
		if(products.get(i).getManufacturer().name==manufacturerName)
			System.out.println(products.get(i));
		}
}
public ArrayList<Product> getProductsBelowPrice(double price) {
	ArrayList<Product> pr = new ArrayList<Product>();
	for(int i=0; i<products.size();i++) {
	if(products.get(i).getPrice()<price) {
	pr.add(products.get(i));
	}
	}
	return pr;
}

public void printMostPopularManufacturer() {
	
}
public ArrayList getProductsMadeBefore(Date date) {
	return products;
}

}*/
