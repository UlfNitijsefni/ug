.intel_syntax noprefix

.global _start

.text

_start:
mov     eax, [esp+8]
mov     ecx, [esp+12]
jmp     atoi

mainLoop:
//eax - *currentLetter
//ebx - doneOnce
//ecx - *endOfString
//edx - count
cmp     eax, ecx
je      regexEqEnd
cmp     [eax], BYTE PTR 'a'
je      gotoFirst
cmp     [eax], BYTE PTR 'b'
je      gotoFirst
cmp     [eax], BYTE PTR 'd'
je      gotoFirst
add     eax, 1
jmp     mainLoop

foundFirstLetter:
add     eax, 1
cmp     eax, ecx
je      regexEqEndStackNotEmpty
cmp     ebx, 0
je      notDoneOnce
cmp     [eax], BYTE PTR 'a'
je      ifInFoundFirstLetterGoBackToMainLoop
cmp     [eax], BYTE PTR 'b'
je      ifInFoundFirstLetterGoBackToMainLoop
cmp     [eax], BYTE PTR 'c'
je      foundC
jmp     foundFirstLetter

foundC:
add     eax, 1
cmp     [eax], BYTE PTR 'c'
je      foundSecondC
cmp     eax, ecx
je      regexEqEndStackNotEmpty
pop     eax
add     eax, 1
mov     ebx, 0
jmp     mainLoop

end:
pop     ebx
cmp     ebx, ecx
je      exit
push    edx
mov     edx, eax
mov     ecx, ebx
sub     edx, ecx
mov     eax, 4
jmp     printRegex



regexEqEnd:
push    ecx
jmp     end

regexEqEndStackNotEmpty:
pop     eax
push    ecx
jmp     end

gotoFirst:
push    eax
mov     ebx, 0
jmp     foundFirstLetter

notDoneOnce:
cmp     [eax], BYTE PTR 'a'
je      ifInFoundFirstLetterGoBackToMainLoop
cmp     [eax], BYTE PTR 'c'
je      ifInFoundFirstLetterGoBackToMainLoop
cmp     [eax], BYTE PTR 'b'
je      ifInFoundFirstLetterGoBackToMainLoop
mov     ebx, 1
jmp     foundFirstLetter

ifInFoundFirstLetterGoBackToMainLoop:
pop     eax
add     eax, 1
mov     ebx, 0
jmp     mainLoop

foundSecondC:
add     eax, 1
jmp     end

exit:
mov     eax, 1
mov     ebx, 0
int     0x80

printRegex:
mov     eax, 4
push    ecx
push    edx
mov     ebx, 1
int     0x80
pop     edx
pop     ecx
pop     ebx
dec     bl
push    ebx
cmp     [esp], BYTE PTR 0
jne     printRegex
add     esp, 4
jmp     exit

atoi:
mov     edx, ecx
mov     dl, [edx]
sub     dl, 48
jmp     mainLoop
