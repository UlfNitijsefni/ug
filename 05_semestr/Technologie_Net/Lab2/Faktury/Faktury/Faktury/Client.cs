﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faktury
{
    class Client
    {
        String Company;
        String Adress;
        int NIPNumber;
        int bankAcc;

        public Client(String ClientCompany, String ClientAdress, int ClientNIP_Number)
        {
            Company = ClientCompany;
            Adress = ClientAdress;
            NIPNumber = ClientNIP_Number;
            bankAcc = 0;
        }

        public Client(String ClientCompany, String ClientAdress, int ClientNIP_Number, int Companybank_acc)
        {
            Company = ClientCompany;
            Adress = ClientAdress;
            NIPNumber = ClientNIP_Number;
            bankAcc = Companybank_acc;
        }

//-----------------------------------------------------------------------

        public String getCompany()
        {
            return this.Company;
        }

        public String getAdress()
        {
            return this.Adress;
        }

        public int getNIP()
        {
            return this.NIPNumber;
        }

        public int getBankAccount()
        {
            return this.bankAcc;
        }

//-----------------------------------------------------------------------

        public void setCompany(String newCompanyName)
        {
            this.Company = newCompanyName;
        }

        public void setAdress(String newAdress)
        {
            this.Adress = newAdress;
        }

        public void setNIP(int newNIP)
        {
            this.NIPNumber = newNIP;
        }

        public void setBankAccount(int newBankAccount)
        {
            this.bankAcc = newBankAccount;
        }
    }
}
