﻿
using System;
using Employees;

class MainClass
{
    static void Main(string[] args)
    {
        // Display the number of command line arguments.
        Console.WriteLine(args.Length);
        Address testAdress1 = new Address("TestStreet", 1, 2, "Warszawa");
        Address testAdress2 = new Address("TestStreet", 1, 2, "Gdansk");
        Employee testEmployee1 = new OfficeEmployee(0, "Test1", "TestSurname1", 20, 12, testAdress1, 0, 80);
        Employee testEmployee2 = new OfficeEmployee(1, "Test2", "TestSurname2", 30, 11, testAdress1, 1, 90);
        Employee testEmployee3 = new TraderEmployee(2, "Test3", "TestSurname3", 40, 9, testAdress2,
            TraderEmployee._Efficiency.Srednia, 50);

        Register register = new Register();

        register.addEmployee(testEmployee1);

        Employee[] toAdd = {testEmployee2, testEmployee3 } ; 
        register.addMultiEmployee(toAdd);
        
        register.writeAllSortedLists();

        register.printEmployeesFromCity("Warszawa");
        register.printEmployeesFromCity("Krakow");
        
        register.printEmployeesWithValue();
        
        register.removeEmp(1);
        register.removeEmp(1);
        
        register.printEmployeesWithValue();
    }
}