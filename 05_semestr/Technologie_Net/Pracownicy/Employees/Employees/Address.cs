using System;

namespace Employees
{
    public class Address
    {
        private string Street;
        private int Building_Number;
        private int Room_Number;
        private string City;

        public Address(string street, int buildingNumber, int roomNumber, string city)
        {
            Street = street ?? throw new ArgumentNullException(nameof(street));
            Building_Number = buildingNumber;
            Room_Number = roomNumber;
            City = city ?? throw new ArgumentNullException(nameof(city));
        }

        public string Street1
        {
            get => Street;
            set => Street = value;
        }

        public int BuildingNumber
        {
            get => Building_Number;
            set => Building_Number = value;
        }

        public int RoomNumber
        {
            get => Room_Number;
            set => Room_Number = value;
        }

        public string City1
        {
            get => City;
            set => City = value;
        }
    }
}
