using Employees;

namespace Employees
{
    public abstract class Employee
    {
        public uint Id;
        public string Name;
        public string LastName;
        public uint Age;
        public uint Experience;
        public Address WorkAdress;

        protected Employee(uint id, string name, string lastName, uint age, uint experience, Address workAdress)
        {
            Id = id;
            Name = name;
            LastName = lastName;
            Age = age;
            Experience = experience;
            WorkAdress = workAdress;
        }

        
        public uint Id1
        {
            get => Id;
            set => Id = value;
        }

        public string Name1
        {
            get => Name;
            set => Name = value;
        }

        public string LastName1
        {
            get => LastName;
            set => LastName = value;
        }

        public uint Age1
        {
            get => Age;
            set => Age = value;
        }

        public uint Experience1
        {
            get => Experience;
            set => Experience = value;
        }

        public Address WorkAdress1
        {
            get => WorkAdress;
            set => WorkAdress = value;
        }

        public abstract uint CompanyValue();
        
        public override string ToString() => $"{this.GetType().Name}: '{Name} {LastName}'";
    }
}
