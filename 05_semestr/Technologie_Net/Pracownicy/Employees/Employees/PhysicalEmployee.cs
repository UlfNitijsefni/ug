using System;
using Employees;

namespace EmployeeRegister
{
    public class PhysicalEmployee : Employee
    {
        private uint _strength;

        public uint Strength
        {
            get => _strength;
            set
            {
                if (value < 1 | value > 100)
                    throw new ArgumentException("Strength can only be between 1 and 100");
                _strength = value;
            }
        }
        public PhysicalEmployee(uint id, string name, string lastName,
            uint age, uint yearsOfExperience, Address workAddress, uint strength) 
            : base(id, name, lastName, age, yearsOfExperience, workAddress)
        {
            Strength = strength;
        }

        public override uint CompanyValue() => Experience * Strength / Age;
    }
}