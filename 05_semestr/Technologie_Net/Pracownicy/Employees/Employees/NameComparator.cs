using System;
using System.Collections.Generic;

namespace Employees

{
    public class NameComparator : IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        {
            if (x == null && y != null) return -1;
            if (x != null && y == null) return 1;
            if (x == null) return 0;
            return string.Compare(x.LastName1, y.LastName1, StringComparison.CurrentCulture);
        }
    }
}