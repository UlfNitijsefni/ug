using System.Collections.Generic;

namespace Employees

{
    public class ExperienceComparator : IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        {
            if (x == null && y != null) return -1;
            if (x != null && y == null) return 1;
            if (x == null) return 0;
            return (int) (y.Experience - x.Experience);
        }
    }
}