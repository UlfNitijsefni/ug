using System;

namespace Employees

{

    public class TraderEmployee : Employee
    {
        public enum _Efficiency { Niska, Srednia, Wysoka }

        private uint _commission;
        public _Efficiency _efficiency;


        public _Efficiency Efficiency
        {
            get => _efficiency;
            set => _efficiency = value;
        }

        public uint CommissionPercentage
        {
            get => _commission;
            set
            {
                if (value > 100)
                    throw new ArgumentException("Commission is a percent value.");
                else if(value <= 0)
                    throw new ArgumentException("Commision must be bigger than 0%");
                _commission = value;
            }
        }
        public TraderEmployee(uint id, string name, string lastName, uint age, uint yearsOfExperience, 
            Address workAddress, _Efficiency efficiency, uint commission)
            : base(id, name, lastName, age, yearsOfExperience, workAddress)
        {
            _efficiency = efficiency;
            CommissionPercentage = commission;
        }

        public override uint CompanyValue()
        {
            switch (_efficiency)
            {
                case (_Efficiency.Niska):
                    return (60 * this.Experience1);
                case (_Efficiency.Srednia):
                    return (90 * this.Experience1);
                case (_Efficiency.Wysoka):
                    return (120 * this.Experience1);
                default:
                    return 0;
            }
        }
    }
}