using System;
using System.Diagnostics.Eventing.Reader;
using System.Runtime.Remoting.Messaging;
using Employees;

namespace Employees
{
    
    public class OfficeEmployee : Employee
    {
        private uint _iq;

        public uint OfficeId { get; private set; }
        public uint Iq
        {
            get => _iq;
            set
            {
                if (value < 70 | value > 150){
                    throw new ArgumentException("IQ can only be between 70 and 150");
                }
                _iq = value;
            }
        }
        public OfficeEmployee(uint id, string name, string lastName, uint age,
            uint yearsOfExperience, Address workAddress, uint officeId, uint iq) 
            : base(id, name, lastName, age, yearsOfExperience, workAddress)
        {
            OfficeId = officeId;
            Iq = iq;
        }

        public override uint CompanyValue()
        {
            return Experience1 * _iq;
        }
    }
}