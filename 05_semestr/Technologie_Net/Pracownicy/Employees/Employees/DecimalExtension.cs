using System;

namespace EmployeeRegister
{
    public static class DecimalExtension
    {
        public static bool AlmostEquals(this decimal num1, decimal num2, int precision)
        {
            return Math.Round(num1, precision, MidpointRounding.ToEven) ==
                   Math.Round(num2, precision, MidpointRounding.ToEven);
        }
    }
}