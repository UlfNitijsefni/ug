using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using EmployeeRegister;

namespace Employees

{
    public class Register
    {
        private List<Employee> employees = new List<Employee>();

        public List<Employee> getEmployeesList()
        {
            return this.employees;
        }

    public void addEmployee(Employee newEmp)
        {
            employees.Add(newEmp);
        }
        
        
        public void addMultiEmployee(Employee[] newEmps)
        {
            foreach (var newEmp in newEmps)
            {
                employees.Add(newEmp);
            }
        }
        
        
        public void removeEmp(int ID)
        {
            if (employees.Remove(employees.Find(emp => emp.Id1 == ID)))
            {
                Console.WriteLine("\nEmployee with ID " + ID + " was successfully deleted");
                return;
            }
            Console.WriteLine("\nNo employee with ID " + ID + " was found");
        }
        
        
        public List<Employee> getExperienceSortedEmployees()
        {
            List<Employee> list = employees;
            list.Sort(new ExperienceComparator());
            return list;
        }

        public List<Employee> getAgeSortedEmployees()
        {
            List<Employee> list = employees;
            list.Sort(new AgeComparator());
            return list;
        }

        public List<Employee> getLastNameSortedEmployees()
        {
            List<Employee> list = employees;
            list.Sort(new NameComparator());
            return list;
        }
        
        
        public void printEmployeesFromCity(string cityName)
        {
            List<Employee> fromCity = new List<Employee>();
            foreach (var employee in employees)
            {
                if (employee.WorkAdress1.City1 == cityName)
                {
                    fromCity.Add(employee);
                }
            }

            Console.Write("\n");
            if (fromCity.Count == 0)
            {
                Console.WriteLine("There were no employees found in city of " + cityName);
            }
            else
            {
                Console.WriteLine("Employees from city " + cityName);
                foreach (var emp in fromCity)
                {
                    Console.WriteLine(emp + " City: " + emp.WorkAdress.City1);
                }
            }
        }
        
        
        public void printEmployeesWithValue()
        {
            List<(Employee, uint)> efficiencies = new List<(Employee, uint)>();

            foreach (var employee in employees)
            {
                efficiencies.Add((employee, employee.CompanyValue()));
            }

            Console.WriteLine("\nEmployees with their values");
            foreach (var emp in efficiencies)
            {
                Console.WriteLine(emp.Item1 + " Value for corp: " + emp.Item2);
            }
            
            // return efficiencies;
        }


        public void writeAllSortedLists()
        {
            List<Employee> experienceList = new List<Employee>();
            List<Employee> ageList = new List<Employee>();
            List<Employee> lastNameList = new List<Employee>();
            
            
            Console.WriteLine("\nSorted by experience, descending");
            experienceList = getExperienceSortedEmployees();
            foreach (var item in experienceList)
                Console.WriteLine(item + " Experience: " + item.Experience);
            
            Console.WriteLine("\nSorted by Age, ascending");
            ageList = getAgeSortedEmployees();
            foreach (var item in ageList)
                Console.WriteLine(item + " Age: " + item.Age);
            
            
            Console.WriteLine("\nSorted by last name, alphabetically");
            lastNameList = getLastNameSortedEmployees();
            foreach (var item in lastNameList)
                Console.WriteLine(item);
        }
    }    
}