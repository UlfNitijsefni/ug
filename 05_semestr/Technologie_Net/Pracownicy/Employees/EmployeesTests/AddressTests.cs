﻿using Employees;
using NUnit.Framework;

namespace EmployeesTests
{
    public class AddressTests
    {
        private Address testAdress;

        [SetUp]
        public void Setup()
        {
            testAdress = new Address("TestStreet", 1, 3, "TestCity");
        }

        [Test]
        public void streetTest()
        {
            Assert.That(testAdress.Street1, Is.EqualTo("TestStreet"));
        }
        
        [Test]
        public void buildingNumberTest()
        {
            Assert.That(testAdress.BuildingNumber, Is.EqualTo(1));
        }
        
        [Test]
        public void roomNumberTest()
        {
            Assert.That(testAdress.RoomNumber, Is.EqualTo(3));
        }
        
        [Test]
        public void cityTest()
        {
            Assert.That(testAdress.City1, Is.EqualTo("TestCity"));
        }

        [Test]
        public void changeStreetTest()
        {
            testAdress.Street1 = ("TestStreet2");
            Assert.That(testAdress.Street1, Is.EqualTo("TestStreet2"));
        }
        
        [Test]
        public void changeBuildingNumberTest()
        {
            testAdress.BuildingNumber = (2);
            Assert.That(testAdress.BuildingNumber, Is.EqualTo(2));
        }
        
        [Test]
        public void changeRoomTest()
        {
            testAdress.RoomNumber = (4);
            Assert.That(testAdress.RoomNumber, Is.EqualTo(4));
        }
        
        [Test]
        public void changeCityTest()
        {
            testAdress.Street1 = ("TestStreet2");
            Assert.That(testAdress.Street1, Is.EqualTo("TestStreet2"));
        }
    }
}