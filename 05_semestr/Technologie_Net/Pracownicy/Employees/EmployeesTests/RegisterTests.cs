﻿using System;
using System.Collections.Generic;
using Employees;
using NUnit.Framework;

namespace EmployeesTests
{
    public class RegisterTests
    {
        private Register register;
        [SetUp]
        public void Setup()
        {
            register = new Register();
        }

        [Test]
        public void AddOneTest()
        {
            int originalLength = register.getEmployeesList().Count;
            register.addEmployee(null);
            int newLength = register.getEmployeesList().Count;
            
            Assert.That(originalLength + 1, Is.EqualTo(newLength));
        }

        [Test]
        public void DeleteOneTest()
        {
            Address testAdress1 = new Address("TestStreet", 1, 2, "Warszawa");
            Employee testEmployee1 = new OfficeEmployee(0, "Test1", "TestSurname1", 20, 12, testAdress1, 0, 80);

            register.addEmployee(testEmployee1);
            register.removeEmp(0);
            
            Assert.That(0, Is.EqualTo(register.getEmployeesList().Count));
        }

        [Test]
        public void AddMultipleTest()
        {
            Address testAdress1 = new Address("TestStreet", 1, 2, "Warszawa");
            Address testAdress2 = new Address("TestStreet", 1, 2, "Krakow");
            Employee testEmployee1 = new OfficeEmployee(0, "Test1", "TestSurname1", 20, 12, testAdress1, 0, 80);
            Employee testEmployee2 = new OfficeEmployee(0, "Test2", "TestSurname2", 20, 12, testAdress2, 0, 80);
            Employee testEmployee3 = null;
            
            register.addMultiEmployee(new Employee[] { testEmployee1, testEmployee2, testEmployee3});
            List<String> testList = new List<String>();
            testList.Add("a");
            testList.Add("b");
            testList.Add(null);
            Assert.That(testList.Count, Is.EqualTo(register.getEmployeesList().Count));
        }

    }
}