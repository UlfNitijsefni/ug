﻿using Employees;
using NUnit.Framework;

namespace EmployeesTests
{
    public class ComparatorsTest
    {
        Employee testEmployee1;
        Employee testEmployee2;
        Address testAdress1 = new Address("TestStreet1", 0, 0, "TestCity1");
        Address testAdress2 = new Address("TestStreet2", 1, 1, "TestCity2");
        AgeComparator ageComparator = new AgeComparator();
        ExperienceComparator experienceComparator = new ExperienceComparator();
        NameComparator nameComparator = new NameComparator();

        // [TestInitialize]
        // public void TestInitialize()
        // {
        //  Wykomentowane ponieważ z jakiegoś powodu nie było rozpoznawane TestInitialize   
        // }


        //=========================================================
        //AGE COMPARATOR
        [Test]
        public void TwoNullEmployeesAgeCompareTest()
        {
            testEmployee1 = testEmployee2 = null;
            Assert.That(ageComparator.Compare(testEmployee1, testEmployee2), Is.EqualTo(0));
        }

        [Test]
        public void EmployeeOneNullAgeCompareTest()
        {
            testEmployee1 = null;
            testEmployee2 = new OfficeEmployee(1, "TestName2", "TestLastName2", 70, 7, testAdress2, 0, 78);
            Assert.That(ageComparator.Compare(testEmployee1, testEmployee2), Is.EqualTo(-1));
        }

        [Test]
        public void EmployeeTwoNullAgeCompareTest()
        {
            testEmployee1 = new OfficeEmployee(0, "TestName1", "TestLastName1", 40, 3, testAdress1, 1, 90);
            testEmployee2 = null;
            Assert.That(ageComparator.Compare(testEmployee1, testEmployee2), Is.EqualTo(1));
        }

        [Test]
        public void BothEmployeesNotNullAgeCompareTest()
        {
            testEmployee2 = new OfficeEmployee(1, "TestName2", "TestLastName2", 70, 7, testAdress2, 0, 78);
            testEmployee1 = new OfficeEmployee(0, "TestName1", "TestLastName1", 40, 3, testAdress1, 1, 90);
            Assert.That(ageComparator.Compare(testEmployee1, testEmployee2), Is.EqualTo(-30));
        }

        //=========================================================
        //EXPERIENCE COMPARATOR

        [Test]
        public void TwoNullEmployeesExperienceCompareTest()
        {
            testEmployee1 = testEmployee2 = null;
            Assert.That(experienceComparator.Compare(testEmployee1, testEmployee2), Is.EqualTo(0));
        }

        [Test]
        public void EmployeeOneNullExperienceCompareTest()
        {
            testEmployee1 = null;
            testEmployee2 = new OfficeEmployee(1, "TestName2", "TestLastName2", 70, 7, testAdress2, 0, 78);
            Assert.That(experienceComparator.Compare(testEmployee1, testEmployee2), Is.EqualTo(-1));
        }

        [Test]
        public void EmployeeTwoNullExperienceCompareTest()
        {
            testEmployee1 = new OfficeEmployee(0, "TestName1", "TestLastName1", 40, 3, testAdress1, 1, 90);
            testEmployee2 = null;
            Assert.That(experienceComparator.Compare(testEmployee1, testEmployee2), Is.EqualTo(1));
        }

        [Test]
        public void BothEmployeesNotNullExperienceCompareTest()
        {
            testEmployee2 = new OfficeEmployee(1, "TestName2", "TestLastName2", 70, 7, testAdress2, 0, 78);
            testEmployee1 = new OfficeEmployee(0, "TestName1", "TestLastName1", 40, 3, testAdress1, 1, 90);
            Assert.That(experienceComparator.Compare(testEmployee1, testEmployee2), Is.EqualTo(4));
        }

        //=========================================================
        //NAME COMPARATOR

        [Test]
        public void TwoNullEmployeesNameCompareTest()
        {
            testEmployee1 = testEmployee2 = null;
            Assert.That(nameComparator.Compare(testEmployee1, testEmployee2), Is.EqualTo(0));
        }

        [Test]
        public void EmployeeOneNullNameCompareTest()
        {
            testEmployee1 = null;
            testEmployee2 = new OfficeEmployee(1, "TestName2", "TestLastName2", 70, 7, testAdress2, 0, 78);
            Assert.That(nameComparator.Compare(testEmployee1, testEmployee2), Is.EqualTo(-1));
        }

        [Test]
        public void EmployeeTwoNullNameCompareTest()
        {
            testEmployee1 = new OfficeEmployee(0, "TestName1", "TestLastName1", 40, 3, testAdress1, 1, 90);
            testEmployee2 = null;
            Assert.That(nameComparator.Compare(testEmployee1, testEmployee2), Is.EqualTo(1));
        }
    }
}