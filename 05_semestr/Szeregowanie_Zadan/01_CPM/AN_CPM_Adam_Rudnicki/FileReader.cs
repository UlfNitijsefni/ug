﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AN_CPM_Adam_Rudnicki
{
    public class FileReader
    {
        private String filePath = @"..\..\..\TestInput.txt";
            // @"D:\Dane\UG\ug\05_semestr\Szeregowanie_Zadan\Alg1\AN_CPM_Adam_Rudnicki\AN_CPM_Adam_Rudnicki\TestInput.txt";
        public FileReader()
        {
        }

        public List<Tuple<int, int, List<int>>> getDataFromFile()
        {
            List<string[]> lineElements = new List<string[]>();
            List<string> indexes = new List<string>();      // TODO remove this line - unused
            List<Tuple<int, int, List<int>>> translatedElements = new List<Tuple<int, int, List<int>>>();
            string[] text = System.IO.File.ReadAllLines(filePath);

            foreach (var line in text)
                lineElements.Add(Regex.Split(line, @"\-"));

            foreach (var element in lineElements)
            {
                int index = Int32.Parse(element[0]);
                int cost = Int32.Parse(element[1]);
                List<string> stringedChildren = new List<string>();

                foreach (var childIndex in Regex.Split(element[2], @"\,"))
                {
                    stringedChildren.Add(childIndex);
                }

                List<int> children = new List<int>();
                foreach (var child in stringedChildren)
                {
                    //If ensures that elements with no children don't try to parse empty strings
                    if (child.Length > 0)
                        children.Add(Int32.Parse(child));
                }

                //This way children indexes are sorted in ascending order
                children.Sort();
                Tuple<int, int, List<int>> tempTuple = new Tuple<int, int, List<int>>(index, cost, children);
                translatedElements.Add(tempTuple);
            }

            // //Should sort list of node data by index, ascending
            // sortListOfNodeDataByIndexes(translatedElements);
            return translatedElements;
        }


        public void printData(List<Tuple<int, int, List<int>>> data)
        {
            foreach (var position in data)
            {
                int i = 0;
                Console.WriteLine("Index: " + position.Item1 + " Cost: " + position.Item2);
                Console.WriteLine("Children:");
                foreach (var parent in position.Item3)
                    try
                    {
                        Console.WriteLine("Child " + i + ": " + parent);
                        i++;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Wystapil error przy drukowaniu dzieci. Najprawdopodbniej brak rodzica");
                        break;
                    }

                Console.WriteLine("");
            }
        }

        // private List<Tuple<int, int, List<int>>> sortListOfNodeDataByIndexes(List<Tuple<int, int, List<int>>> data)
        // {
        //     List<Tuple<int, int, List<int>>> newData = new List<Tuple<int, int, List<int>>>();
        //
        //     for (int i = 0; i < data.Count; i++)
        //         {
        //             int minIndex = data.Count - 1;
        //             for (int j = 0; j < data.Count; j++)
        //             {
        //                 if (minIndex > data.ElementAt(j).Item1)
        //                 {
        //                     minIndex = data.ElementAt(j).Item1;
        //                 }
        //             }
        //             newData.Add(data.ElementAt(minIndex));
        //         }
        //
        //     return newData;
        // }
    }
}