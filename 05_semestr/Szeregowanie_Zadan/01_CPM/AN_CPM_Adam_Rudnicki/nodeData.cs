﻿using System.Collections.Generic;

namespace AN_CPM_Adam_Rudnicki
{
        // TODO usunąć nodeData - nie jest nigdzie użyta
    public class nodeData
    {
        private int index;
        private int cost;
        private List<int> indexes;

        public nodeData(int newIndex, int newCost, List<int> newIndexes)
        {
            this.index = newIndex;
            this.cost = newCost;
            this.indexes = newIndexes;
        }

        public int getIndex()
        {
            return this.index;
        }

        public void setIndex(int newIndex)
        {
            this.index = newIndex;
        }

        public int getCost()
        {
            return this.cost;
        }

        public void setCost(int newCost)
        {
            this.cost = newCost;
        }

        public List<int> getIndexes()
        {
            return this.indexes;
        }

        public void setIndexes(List<int> newIndexes)
        {
            this.indexes = newIndexes;
        }
    }
}