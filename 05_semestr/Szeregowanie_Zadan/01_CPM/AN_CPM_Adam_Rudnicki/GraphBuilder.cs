﻿using System;
using System.Collections.Generic;

namespace AN_CPM_Adam_Rudnicki
{
    public class GraphBuilder
    {
        public GraphBuilder()
        {
        }

        public List<Node> buildGraphFromData(List<Tuple<int, int, List<int>>> data)
        {
            List<Node> graph = new List<Node>();

            foreach (var dataset in data)
            {
                Node tempNode = new Node(dataset.Item1, dataset.Item2, dataset.Item3);

                graph.Add(tempNode);
            }
            return graph;
        }
    }
}