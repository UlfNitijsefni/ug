﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace AN_CPM_Adam_Rudnicki
{


    
    public class GraphCrawler
    {
        public int CriticalCost { get; set; }


        
        // recursive computation of earliest times
        public void CalcFastestTimes(List<Node> graph)
        {
            foreach (Node node in graph)
                if (node.parents.Count == 0)
                    SetChildrenFastestTimes(node);
        }

        private void SetChildrenFastestTimes(Node parent)
        {
            int parentFinishTime = parent.fastestTime + parent.cost;
            foreach (Node child in parent.children)
            {
                if (child.fastestTime < parentFinishTime)
                    child.fastestTime = parentFinishTime;

                SetChildrenFastestTimes(child);
            }
        }


        // critical path
        public List<List<Node>> CalcCriticalPaths(List<Node> graph)
        {
            List<List<Node>> criticalPathsList = new List<List<Node>>();

            foreach (Node node in graph)
                if (node.children.Count == 0)
                {
                    var myPath = new List<Node>();
                    RecordCriticalPath(node, ref myPath, ref criticalPathsList);
                }

            return ExtractCriticalPaths(criticalPathsList);
        }

        private void RecordCriticalPath(Node child, ref List<Node> myPath, ref List<List<Node>> fastestPathsList)
        {
            myPath.Insert(0, child);

            if (child.parents.Count == 0)
                fastestPathsList.Add(myPath);

            else
            {
                if (child.parents[0].GetFastestFinish() == child.fastestTime)
                    RecordCriticalPath(child.parents[0], ref myPath, ref fastestPathsList);

                foreach (Node parent in child.parents.Skip(1).ToList())
                    if (parent.GetFastestFinish() == child.fastestTime)
                    {
                        var nextPath = new List<Node>(myPath);
                        RecordCriticalPath(parent, ref nextPath, ref fastestPathsList);
                    }
            }
        }

        private List<List<Node>> ExtractCriticalPaths(List<List<Node>> fastestPathsList)
        {
            int n = fastestPathsList.Count;
            var costsList = new List<int>(n);

            for (int i = 0; i < n; i++)
            {
                costsList.Insert(i,0);
                foreach (Node node in fastestPathsList[i])
                    costsList[i] += node.cost;
            }

            CriticalCost = costsList.Max();
            var criticalPathsList = new List<List<Node>>();

            for (int i = 0; i < n; i++)
                if (costsList[i] == CriticalCost)
                    criticalPathsList.Add(fastestPathsList[i]);

            return criticalPathsList;
        }
    }
}