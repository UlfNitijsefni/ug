﻿using System;
using System.Collections.Generic;

namespace AN_CPM_Adam_Rudnicki
{
    public class GraphOperations
    {
        public List<Node> getCriticalPath(Node pathEnd, List<Node> pathToDate)
        {
            //TODO: This is wrong
            Node longestParent = null;
            if (pathEnd != null)
            {
                List<Node> parents = pathEnd.getParents();
                if (parents != null && parents.Count > 0)
                {
                    longestParent = parents[0];

                    foreach (var parent in parents)
                    {
                        // if (parent.getFastestTime() + parent.getCost() >
                        //     longestParent.getFastestTime() + longestParent.getCost())
                        //     longestParent = parent;
                        if (parent.getCost() > longestParent.getCost())
                            longestParent = parent;
                    }
                }

                pathToDate.Add(pathEnd);
                return getCriticalPath(longestParent, pathToDate);
            }
            else
            {
                pathToDate.Sort((x, y) => x.getIndex().CompareTo(y.getIndex()));
                return pathToDate;
            }
        }

        public void setFastestTimes(List<Node> graph)
        {
            //Sets fastest possible start time
            foreach (var node in graph)
            {
                int fastTime = computeEarliestPossibleTime(node.getParents());
                // node.setFastestTime(node.getCost() + fastTime);
                node.setFastestTime(fastTime);
            }
        }
        
        private int computeEarliestPossibleTime(List<Node> parents)
        {
            if (parents != null && parents.Count > 0)  //TODO remove all checks parents != null / children != null
            {
                int cost = parents[0].getFastestTime() + parents[0].getCost();
                foreach (var node in parents)
                    // cost = Math.Min(node.getFastestTime() + node.getCost(), cost);
                    cost = Math.Max(node.getFastestTime() + node.getCost(), cost);
                return cost;
            }
            else
                return 0;
        }

        public void setLastStartTimes(List<Node> graph, List<Node> criticalPath, int critPathTime)
        {
            //i decreasing so the algorithm goes from leafs to root, unlike how it would with foreach or normal incrementation
            for(int i = graph.Count - 1; i >= 0; i--)
            {
                Node node = graph[i];
                
                int tempTime = critPathTime;
                
                if (node.getChildren().Count > 0)
                {
                    foreach (var child in node.getChildren())
                    {
                        tempTime = Math.Min(tempTime, child.getLastPossibleTime());
                    }
                }
                node.setLastPossibleTime(tempTime - node.getCost());
            }
        }

        public void fillChidlrenOnGraph(List<Node> graph)
        {
            for (int i = graph.Count - 1; i >= 0; i--)
            {
                graph[i].fillChildrenList(graph);
            }
        }

        public int getLongestTaskTimeInGraph(List<Node> graph)
        {
            var time = 0;
            foreach (var node in graph)
            {
                time = Math.Max(node.getCost(), time);
            }

            return time;
        }

        public List<List<Node>> removeShorterCritPaths(List<List<Node>> critPaths)
        {
            int longestPathTime = 0;
            List<List<Node>> newCritPaths = new List<List<Node>>();
            foreach (var path in critPaths)
            {
                longestPathTime = Math.Max(path[path.Count - 1].getFastestTime(), longestPathTime);
            }

            foreach (var path in critPaths)
            {
                if (path[path.Count - 1].getFastestTime() == longestPathTime)
                {
                    newCritPaths.Add(path);
                }
            }

            return newCritPaths;
        }
    }
}