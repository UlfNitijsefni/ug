﻿using System;
using System.Collections.Generic;

namespace AN_CPM_Adam_Rudnicki
{
    public class Node
    {
        private int index;
        public int cost { get; }
        public int fastestTime { get; set; }
        public int lastPossibleTime { get; set; }
        public List<Node> parents { get; }  // TODO made public
        public List<Node> children { get; }  // TODO made public
        public List<Node> criticalParents { get; set; } = new List<Node>();  // TODO new list

        private List<int> childIndexes;

        public Node(int newIndex, int newCost, List<int> newChildIndexes)
        {
            this.index = newIndex;
            this.cost = newCost;
            this.parents = new List<Node>();
            this.children = new List<Node>();
            this.childIndexes = newChildIndexes;
        }

        // TODO new method
        public int GetFastestFinish()
        {
            return fastestTime + cost;
        }


        public int getIndex()
        {
            return this.index;
        }


        public List<Node> getParents()
        {
            return this.parents;
        }

        public List<Node> getChildren()
        {
            return this.children;
        }

        public void setFastestTime(int newTime)
        {
            this.fastestTime = newTime;
        }

        public int getFastestTime()
        {
            return this.fastestTime;
        }

        public void setLastPossibleTime(int newTime)
        {
            // TODO usunąć cały if po naprawieniu programu
            if (newTime < fastestTime)
            {
                Console.WriteLine(
                    "Proba wpisania najpozniejszego czasu przed najwczesniejszym w indeksie " + this.index);
            }

            // else
            this.lastPossibleTime = newTime;
        }

        public int getLastPossibleTime()
        {
            return this.lastPossibleTime;
        }

        public int getCost()
        {
            return this.cost;
        }


        public void addChild(Node newChid)
        {
            if (newChid == null)
            {
                throw new ArgumentNullException();
            }
            else if (newChid.index <= index)
            {
                throw new Exception(
                    "In addChild: Indexation incorrect - attempted insertion of lower index after higher");
            }
            else
            {
                children.Add(newChid);
            }
        }

        // TODO usunąć, niewykorzystana
        public void removeChid(Node toDelete)
        {
            children.Remove(toDelete);
        }

        public void addParent(Node newParent)
        {
            // TODO usunąć wszystkie niepotrzebne "this" 
            this.parents.Add(newParent);
        }

        public void fillChildrenList(List<Node> nodes)
        {
            foreach (var i in childIndexes)
            {
                // TODO correction
                // addChild(nodes[index - 1]);
                // nodes[index - 1].addParent(this);
                Node node = findNode(nodes, i);
                children.Add(node);
                node.parents.Add(this);
            }
        }

        private Node findNode(List<Node> nodes, int i)
        {
            foreach (Node node in nodes)
                if (node.index == i)
                    return node;
            throw new Exception("Could not find node with index: " + i);
        }
    }
}