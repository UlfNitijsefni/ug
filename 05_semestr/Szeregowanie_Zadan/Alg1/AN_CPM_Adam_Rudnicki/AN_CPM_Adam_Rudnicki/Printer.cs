﻿using System;
using System.Collections.Generic;

namespace AN_CPM_Adam_Rudnicki
{
    public class Printer
    {
        public Printer() {}

        public void printEarliestCompletions(List<Node> graph)
        {
            int i = 0;
            foreach (var node in graph)
            {
                Console.WriteLine("Earliest possible start time for node " + (i + 1) + ": "+ node.getFastestTime());
                i++;
            }
            Console.WriteLine("\n");
        }

        public void printLastStartTimes(List<Node> graph)
        {
            foreach (var node in graph)
            {
                Console.WriteLine("Last possible time to activate node " + node.getIndex() + ": "+ node.getLastPossibleTime() + ", czas trwania: " + node.getCost());
            }
            Console.WriteLine("\n");
        }

        public void printCriticalPath(List<Node> path, int pathIndex)
        {
            Console.WriteLine("Path " + pathIndex);
            int i = 1;
            int totalCost = 0;
            foreach (var node in path)
            {
                Console.WriteLine(
                    "Critical paths' node " + i + 
                    ", index: " + node.getIndex() + 
                    ", fastestStart: " + node.fastestTime +
                    ", cost: " + node.cost
                    );
                i++;
                totalCost += node.cost;
            }
            Console.WriteLine("Total cost: " + totalCost);
            Console.WriteLine("");
        }
    }
}