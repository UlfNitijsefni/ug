﻿using System;
using System.Collections.Generic;

namespace AN_CPM_Adam_Rudnicki
{
    class Program
    {
        static void Main(string[] args)
        {
            FileReader fileReader = new FileReader();
            GraphBuilder graphBuilder = new GraphBuilder();
            Printer printer = new Printer();
            GraphOperations graphOperations = new GraphOperations();

            List<Tuple<int, int, List<int>>> data = fileReader.getDataFromFile();
            // fileReader.printData(data);  // TODO commented out

            List<Node> graph = graphBuilder.buildGraphFromData(data);
            if (graph.Count == 0)
            {
                Console.WriteLine("Empty graph");
                return;
            }

            graphOperations.fillChidlrenOnGraph(graph);

            GraphCrawler graphCrawler = new GraphCrawler();
            graphCrawler.CalcFastestTimes(graph);
            List<List<Node>> critPaths = graphCrawler.CalcCriticalPaths(graph);

            
            graphOperations.setLastStartTimes(graph, critPaths[0],
                critPaths[0][critPaths[0].Count - 1].getFastestTime() + critPaths[0][critPaths[0].Count - 1].getCost());


            Console.WriteLine("\nEARLIEST START TIMES:\n");
            printer.printEarliestCompletions(graph);

            Console.WriteLine("\nLAST POSSIBLE COMPLETION TIMES FOR PATH 1:\n");
            // printer.printLastStartTimes(critPaths[0]);
            printer.printLastStartTimes(graph);

            int i = 1;
            foreach (var criticalPath in critPaths)
            {
                printer.printCriticalPath(criticalPath, i);
                i++;
            }

            Console.WriteLine("Program finished");
        }
    }
}