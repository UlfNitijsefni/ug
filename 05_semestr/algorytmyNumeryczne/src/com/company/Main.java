package com.company;

public class Main {

    public static void main(String[] args) {

            //8x^3 + 7x^2 - 4x -8

            int max = 5;
            int min = -4;
            int generatedNumber = 10;

            int[][] results = new int[2][5];

            for(int i = 0; i < 5; i ++){
                generatedNumber = (int) ((Math.random() * ((max - min) + 1)) + min);
                results[0][i] = generatedNumber;
                results[1][i] = computeAnswer(generatedNumber);
            }

            System.out.println(" Tabelka w formacie\n|warto�� x|wynik po podstawieniu\n");
            for(int i = 0; i < 5; i++) {
                for (int j = 0; j < 2; j++) {
                    System.out.print("| " + results[j][i]);
                }
                System.out.print("\n");
            }
            System.out.println("\n Adam Rudnicki");

        }


        //=============================================================================
        public static int computeAnswer(int x){
        int result = 0;
        int computedValue = 1;


        for(int i = 0; i < 3; i++){
            switch (i){
                case 0:
                    result+=(-8 * computedValue);
                    break;

                case 1:
                    result+=(-4 * computedValue);
                    break;

                case 2:
                    result+=(7 * computedValue);
                    break;

                case 3:
                    result+=(8 * computedValue);
                    break;

            }

            computedValue = computedValue*x;
        }

        return result;
        }
    }
