#!/bin/bash

if [[ -f $1 ]]
then
    echo Podana sciezka pokazuje na plik

elif [[ -d $1 ]]
then
    echo Podana sciezka pokazuje na folder

else
    echo Podana sciezka nie pokazuje niczego    

fi

exit 0
