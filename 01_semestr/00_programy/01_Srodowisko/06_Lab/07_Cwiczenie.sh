#!/bin/bash

if [ $# -eq 0 ]
then
    echo Brakuje argumentu

elif [ `echo $1 | grep .txt` != "" ]
then
    emacs $1

else
    ./$1

fi  
   
exit 0
