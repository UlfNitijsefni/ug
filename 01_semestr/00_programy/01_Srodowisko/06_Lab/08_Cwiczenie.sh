#!/bin/bash

echo Wybierz jakie chcesz przeprowadzic dzialanie
echo "1) wyjscie"
echo "2) dodawanie"
echo "3) odejmowanie"
echo "4) mnozenie"
echo "5) dzielenie"

read n
wynik=0
case $n in
    1)
	exit 0
	;;
    2)
	wynik=$(( $1 + $2 ))
	;;
    3)
	wynik=$(( $1-$2 ))
	;;
    4)
	wynik=$(( $1*$2 ))
	;;
    5)
	if (( $2 == 0 ))
	then
	    echo !!! Dzielenie przez 0 jest niemozliwe !!!
	    exit 0
	    
	else
	    wynik=$(( $1/$2 ))
	fi
	;;
esac

echo $wynik
		
exit 0
