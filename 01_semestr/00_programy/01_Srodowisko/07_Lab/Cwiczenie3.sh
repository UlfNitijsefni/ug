#!/bin/bash

max=${@:1:1}
min=$max

for (( i=1; $i < $#; i++ ))
do
    
    if (( $max < ${@:$i+1:1} ))
    then max=${@:$i+1:1}
    fi

    if (( $min > ${@:$i+1:1} ))
    then min=${@:$i+1:1}
    fi

done

echo Najmniejsza z podanych wartosci wynosi $min, a najwieksza $max.

exit 0
