#!/bin/bash

slowo=$1
dlugosc=${#slowo}
dlugoscMala=$(( $dlugosc/2 ))


#==============================================================================
function pierwszaLitera {
    ARG1=$( echo $1 | head -c 1 )
    echo Pierwsza litera podanego wyrazu to $ARG1
}


#------------------------------------------------------------------------------
function ostatniaLitera {
    ARG2=$( echo $1 | tail -c 2 )
    echo Ostatnia litera podanego wyrazu to $ARG2
}


#------------------------------------------------------------------------------
function doSrodka {
    ARG1=$( echo $1 | head -c $(( $dlugosc - $dlugoscMala )) )
    echo Pierwsza polowa podanego wyrazu to $ARG1
}


#------------------------------------------------------------------------------
function doKonca {
    ARG1=$( echo $1 | tail -c $(( 1 + $dlugoscMala )) )
    echo Druga polowa podanego wyrazu to $ARG1
}


#------------------------------------------------------------------------------
pierwszaLitera $1;
ostatniaLitera $1;
doSrodka $1 dlugosc dlugoscMala;
doKonca $1 dlugoscMala;

exit 0
