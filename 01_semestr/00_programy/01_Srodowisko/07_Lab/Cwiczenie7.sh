#!/bin/bash

function odwroconyNapis {
    
    TX=$1
    NOWY=""
    
    for (( i=${#TX}; $i >= 0; i-- ))
    do
	NOWY=$NOWY"${TX:$i:1}"
    done

    echo $NOWY
}

odwroconyNapis $@


exit 0
