#!/bin/bash



#==============================================================================
function sprawdzDodatnie {
    if [[ $1 -gt 0 ]] && [[ $2 -gt 0 ]] && [[ $3 -gt 0 ]]
    then
	dodatnie=1
    else
	dodatnie=0
    fi
}


#-----------------------------------------------------------------------------
function sprawdzTrojkat {
    sprawdzDodatnie $@
    if [[ $dodatnie = 1 ]]
    then
	if [[ $(( $1 + $2 )) -gt $3 ]] \
	       && [[ $(( $1 + $3 )) -gt $3 ]] \
	       && [[ $(( $2 + $3 )) -gt $1 ]]
	then
	    echo 1
	    
	else
	    echo 0
	fi
    else
	echo Nie mozna stworzyc trojkata z bokow niedodatnich!
    fi
}


#-----------------------------------------------------------------------------

sprawdzTrojkat $@ 

exit 0
