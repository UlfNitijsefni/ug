#!/bin/bash

ARGS_PLUS=`echo $@ | sed 's/\ /\ \+\ /g'`

TOTAL=$(( `echo $ARGS_PLUS` ))

echo $(( $TOTAL / $# ))
