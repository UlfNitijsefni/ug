#!/bin/bash

{
    data=$(date +%Y_%m_%d)
    mkdir $data
    
    Pliki=$(find -daystart -ctime 0)
    cp $Pliki $data
}

exit 0
