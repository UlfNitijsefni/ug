#include<stdio.h>

int averageOfArgs(int *argumentsPT, int size);


//==========================================================================
void main(){
  
  int arguments[] = {0, 12, 13, 54, 2, 5, 456};
  int *argumentsPT;
  
  int size = sizeof arguments / sizeof arguments[0];
  
  argumentsPT = arguments;
  
  int average = averageOfArgs(argumentsPT, size);
  printf("\nŚrednia wszystkich argumentów tablicy wynosi %d\n", average);
  
}


//--------------------------------------------------------------------------
int averageOfArgs(int *argumentsPT, int size){
  
  int sum = 0;
  int average;
  
  for (int i = 0; i < size; i++) {
    sum += *(argumentsPT + i);
  }
  average = sum/size;
  return average;
}
