#include<stdio.h>
#include<stdlib.h>


/* void checkSize(int size); */
void fillEmpty(char zArray[12][12], int size);
void addLetters(char zArray[12][12], int size);
void printArr(char zArray[12][12], int size);


//===========================================================================
void main (){
  
  int size = 12;
  /* printf("Wpisz pozadany rozmiar litery:\n"); */
  /* scanf("%d", &size); */
  char zArray[size][size];
  
  /* checkSize(size); */
  fillEmpty(zArray, size);
  addLetters(zArray, size);
  printArr(zArray, size);
}


//---------------------------------------------------------------------------
/* void checkSize(int size){ */

/*   if (size < 3 || size > 30){ */
/*     printf("\nPodales liczbe znajdujaca sie poza dozwolonym zakresem.\n\n"); */
/*     exit(0); */
/*   } */
/* } */


//---------------------------------------------------------------------------
void fillEmpty(char zArray[12][12], int size){
  
  for (int i = 0; i < size; i++){
    for (int j = 0; j < size; j++){
      
      zArray[i][j] = ' ';
      
    }
  }
}


//---------------------------------------------------------------------------
void addLetters(char zArray[12][12], int size){
  
  for(int i = 0; i < size; i++){
    zArray[i][0]='Z';
    zArray[i][size-1]='Z';  
  }
  
  for(int i = 1; i < size-1; i++)
    zArray[size-i-1][i] = 'Z';
}


//---------------------------------------------------------------------------
void printArr(char zArray[12][12], int size) {
  
  for(int i = 0; i < size; i++){
    for (int j = 0; j < size; j++)
      printf("%c", zArray[j][i]);	 
    printf("\n");    
  }
}
