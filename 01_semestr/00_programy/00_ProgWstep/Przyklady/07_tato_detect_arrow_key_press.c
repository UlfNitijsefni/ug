# include <stdio.h>
# include <stdlib.h>


// -------------------------------------------------------------------------
void raw_terminal();
void cooked_terminal();
void check_arrow();


// =========================================================================
void main () {
  /* raw_terminal(); */
  //------------------------------------------------------------------------

  check_arrow();

  //------------------------------------------------------------------------
  /* cooked_terminal(); */
}

// -------------------------------------------------------------------------
void raw_terminal() {
  system("/bin/stty raw");
  system("clear");
}

// -------------------------------------------------------------------------
void cooked_terminal() {
  system("/bin/stty cooked");
}

// -------------------------------------------------------------------------
void check_arrow() {

  char znak;
  znak = getchar();
  printf("%u\n\n", (unsigned int)znak);
  
  /* printf("%c", znak); */

  switch(znak) {
  case 119:
    printf("You pressed W.\n");
    break;
    
  case 115:
    printf("You pressed S.\n");
    break;
    
  case 97:
    printf("You pressed A.\n");
    break;
    
  case 100:
    printf("You pressed D.\n");
    break;
    
  default:
    printf("You pressed something that  is not WSAD.\n");
  }
}

