#include <stdio.h>


void reverseArray(int scannedArgs[], int *size);
void readArg(int scannedArgs[], int *size);
void printArray(int scannedArgs[], int *size);

//=============================================================================
void main(){
  
  int *size, arrayLength, scSize;
  
  printf("Podaj ilosc argumentow na ktorych bedzie pracowal program:\n");
  scanf("%d", &scSize);
  size = &scSize;
  int scannedArgs[*size];
  
  readArg(scannedArgs, size);
  reverseArray(scannedArgs, size);
  printArray(scannedArgs, size);
  
}


//-----------------------------------------------------------------------------
void reverseArray(int scannedArgs[], int *size){
  
  int temp;
  
  for (int i = 0; i <= *size/2; i++) {    
    int temp = scannedArgs[*size - i];
    scannedArgs[*size - i] = scannedArgs[i];
    scannedArgs[i] = temp;
  }  
}


//-----------------------------------------------------------------------------
void readArg(int scannedArgs[], int *size) {
    
  for(int i = 0; i < *size; i++){
    printf("Podaj argument ktory ma sie znalezc pod indeksem %d\n", i);
    scanf("%d", &scannedArgs[i]);
  }

  printf("Tablica przed odwroceniem kolejnosci argumentow:\n{ ");
  
  for(int i = 0; i < *size; i++){
    
    printf("%d ", scannedArgs[i]);
    
  }
  printf("}\n");
}


//-----------------------------------------------------------------------------
void printArray(int scannedArgs[], int *size) {
  
  printf("Tablica po odwroceniu kolejnosci argumentow:\n{ ");
  for (int i = 1; i <= *size; i++) {
    printf("%d ", scannedArgs[i]);
  }
  printf("}\n");
}
