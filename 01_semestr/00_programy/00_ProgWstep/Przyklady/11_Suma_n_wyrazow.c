#include <stdio.h>

int liczCiag(int n);
int wyrazCiagu(int i);
int caseFor(int n);
int caseWhile(int n);
int caseDoWhile(int n);


//=============================================================================
void main(){
  int n, suma;

  printf("Wpisz liczbe:\n");
  scanf("%d", &n);

  suma = liczCiag(n);
  printf("suma: %d\n", suma);
}


//-----------------------------------------------------------------------------
int liczCiag(int n){
  
  int rodzaj;
  printf("Wybierz rodzaj petli: for (1), while (2) lub  doWhile (3) i wpisz wybrana:\n");
  scanf("%d", &rodzaj);
  
  switch(rodzaj){

  case(1):
    return caseFor(n);
    
  case(2):
    return caseWhile(n);

  case(3):
    return caseDoWhile(n);
  }

}

//-----------------------------------------------------------------------------
int wyrazCiagu(int i){
  
  int z=(i/3)*100;

  switch(i%3){
    case(0):
      return z+21;
    case(1):
      return z+52;
    case(2):
      return z+13;
  }
}


//-----------------------------------------------------------------------------
int caseFor(int n){
  int suma = 0;
   for (int i=0; i<n; i++){
      suma += wyrazCiagu(i);
    }
   return suma;
}


//-----------------------------------------------------------------------------
int caseWhile(int n){
    int suma = 0;
    int i = 0;
    while (i<n){
      suma += wyrazCiagu(i);
      i++;
    }
    return suma;
}


//-----------------------------------------------------------------------------
int caseDoWhile(int n){
 int suma = 0;
    
   if(n == 0) return 0;

   else{
      int i = 0;
      do {
        suma += wyrazCiagu(i);
	i++;
      } while (i<n);
      return suma;
   }
}
