#include<stdio.h>


void scanArgs(double *args);
int findSecondMax(double *args);


//======================================================================
void main(){
  
  double args[10];
  
  printf("\nPodawaj po kolei kolejne wartosci" \
          " ktore maja sie znalezc w tablicy:\n\n");

  scanArgs(args);
  
  int wynik = findSecondMax(args);

  if (wynik > 0)
    printf("\nDruga najwieksza wartosc pojawila sie %d razy\n", wynik);
  else
    printf("\nWszystkie liczby sa takie same\n");
}



//----------------------------------------------------------------------
void scanArgs(double *args){
  for(int i = 0; i < 10; i++){
    printf("Podaj liczbe %d:  \n", i+1);
    scanf("%lf", &args[i]);
  }
}


//----------------------------------------------------------------------
int findSecondMax(double *args){

  double max = args[0];
  double secondMax;
  
  /* if (args[0] < args[1]) */
  /*   secondMax = args[1]; */
  
  int ile = 0;
  int jest = 0;
  
  for (int i = 0; i < 10; i++){
    if(args[i] > max){
      max = args[i];
      secondMax = max;
      jest = 1;
    }
    else if (args[i] < max)
      if(jest == 0){
	secondMax = args[i];
	jest = 1;
      }
      else if(args[i] > secondMax){
	secondMax = args[i];
	jest = 1;
      }
  }

  if(jest > 0)
    for (int i = 0; i < 10; i++)
      if(args[i] == secondMax)
	ile++;

  return ile;
}
