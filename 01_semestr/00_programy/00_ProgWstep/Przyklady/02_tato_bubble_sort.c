#include<stdio.h>

void getNumbers(int reSortPT[], int size);
void sortArr(int preSortPT[], int size, int* postSortPT[]);
void copyArr(int preSortPT[], int size, int* postSortPT[]);
void printPostSort(int* postSort[], int size);


//==========================================================================
void main (){

  int size;
  printf("Podaj jak dluga ma być tablica ktora chcesz posortowac:\n");
  scanf("%d", &size);
  
  int preSort[size];
  int* postSort[size];

  getNumbers(preSort, size);
  copyArr(preSort, size, postSort);
  sortArr(preSort, size, postSort);
  printPostSort(postSort, size);
}


//--------------------------------------------------------------------------
void sortArr(int preSortPT[], int size, int* postSortPT[]) {

  int *temp;
  
  for (int j = 0; j < size; j++){
    for (int i = 0; i < size-1; i ++){
      if(**(postSortPT + i) < **(postSortPT + i + 1)){
	temp = postSortPT[i];
	postSortPT[i] = postSortPT[i+1];
	postSortPT[i+1] = temp;
      }
    }
  }
}



//--------------------------------------------------------------------------
void getNumbers(int *preSortPT, int size) {
  
  for (int i = 0; i < size; i++){
    
    printf("Podaj wartosc ktora ma sie znalezc w komorce o indeksie %d:\n", i);
    scanf("%d", (preSortPT + i));
  } 
}


//--------------------------------------------------------------------------
void copyArr(int preSortPT[], int size, int* postSortPT[]) {
  for (int i = 0; i < size; i++)
    postSortPT[i] = &(preSortPT[i]);
}


//--------------------------------------------------------------------------
void printPostSort(int* postSort[], int size){
  
  printf("Tablica postSort po wyświetleniu wartości" \
	 "do których są wskaźniki w odpowiednich komórkach:\n|");
    
  for (int i = 0; i < size; i++)
    printf(" %d |", *(postSort[i]));

  printf("\n\n");
}
