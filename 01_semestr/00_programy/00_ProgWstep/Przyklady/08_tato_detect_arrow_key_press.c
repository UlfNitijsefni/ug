# include <stdio.h>
# include <stdlib.h>


// -------------------------------------------------------------------------
void raw_terminal();
void cooked_terminal();
void check_arrow_x5();

unsigned int i;

// =========================================================================
void main () {
  /* raw_terminal(); */
  //------------------------------------------------------------------------

    for (i=0; i<5; i++);
       check_arrow_x5();

  //------------------------------------------------------------------------
  /* cooked_terminal(); */
}

// -------------------------------------------------------------------------
void raw_terminal() {
  system("/bin/stty raw");
  system("clear");
}

// -------------------------------------------------------------------------
void cooked_terminal() {
  system("/bin/stty cooked");
}

// -------------------------------------------------------------------------
void check_arrow_x5() {

  char znak;
  znak = getchar();
  /* printf("%u\n\n", (unsigned int)znak); */  

  switch(znak) {
  case 119:
    printf("You pressed W.\n");
    break;
    
  case 115:
    printf("You pressed S.\n");
    break;
    
  case 97:
    printf("You pressed A.\n");
    break;
    
  case 100:
    printf("You pressed D.\n");
    break;
    
  default:
    printf("You pressed something that  is not WSAD.\n");
  }
}

