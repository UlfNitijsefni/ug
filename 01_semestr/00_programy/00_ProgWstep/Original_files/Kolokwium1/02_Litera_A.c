#include<stdio.h>

int printSpaces(int z, int spacjePrzed, int spacjeWewnatrz);
void drawA(const int n, int z);


// ============================================================================
void main(){

  int n, z;
  
  printf("Wpisz liczbe:\n");
  scanf("%d", &n);

  z=n;
  drawA(n, z);
}


// ---------------------------------------------------------------------------
void drawA(const int n, int z){

  int spacjeWewnatrz = 1;
  int spacjePrzed = n;

  if (n == 0){
    printf("Wysokosc jest zerowa.\n");
    return;
  }
  
  printSpaces(n, spacjePrzed, spacjeWewnatrz);
  printf(" A\n");
  
  
  // TOP LEGS
  for(int c = 0; c<n/2 - 1; c++){

    // margin
    spacjePrzed = printSpaces(spacjePrzed, spacjePrzed, spacjeWewnatrz);

    // left leg
    printf("A");

    // empty inside
    for(int j = 0; j < spacjeWewnatrz; j++)
      printf(" ");

    // right leg
    spacjeWewnatrz += 2;
    printf("A\n");
  }



  
    // HORIZONTAL BAR
  if(n > 1){
    // margin
    spacjePrzed = printSpaces(spacjePrzed/2, spacjePrzed, spacjeWewnatrz);

    // bar
    if(n%2 == 0){
      for (int i = 0; i < n; i++) printf("A");
        printf("A\n");
    }
  
    else{
      for(int i = 0; i < n; i++)
	printf("A");
      printf("\n");
    }

    spacjeWewnatrz += 2;
  }



    //BOTTOM LEGS
    if(n%2 == 1){
      for(int i = 0; i < n/2; i++) {
      
        //left leg
        spacjePrzed = printSpaces(spacjePrzed/2, spacjePrzed, spacjeWewnatrz);
        printf("A");

        //space in the middle
        for(int j = 0; j < spacjeWewnatrz; j++)
	  printf(" ");

        //right leg
        spacjeWewnatrz += 2;
        printf("A\n");
      }
    }
    else{
      for(int i = 1; i < n/2; i++) {
      
        //left leg
        spacjePrzed = printSpaces(spacjePrzed/2, spacjePrzed, spacjeWewnatrz);
        printf("A");

        //space in the middle
        for(int j = 0; j < spacjeWewnatrz; j++)
  	  printf(" ");

        //right leg
        spacjeWewnatrz += 2;
        printf("A\n");
      }
    
  }
}


// ---------------------------------------------------------------------------
int  printSpaces(int z, int spacjePrzed, int spacjeWewnatrz){
  int  k;
  
  for(k = 0; k<(spacjePrzed - 1); k++){
    printf(" ");
  }

  spacjePrzed = spacjePrzed - 1; 
  return spacjePrzed;
}

