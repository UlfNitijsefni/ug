#include<stdio.h>

void sortArray(int *variablesPT, int size);
void printArray(int *variablesPT, int size);


//===========================================================================
void main(){
  
  int variables[] = {12, 124, 6, 56, 13, 58};
  int *variablesPT = variables;
  int size = sizeof variables / sizeof variables[0];
  
  sortArray(variablesPT, size);
  printArray(variablesPT, size);
}


//---------------------------------------------------------------------------
void sortArray(int *variablesPT, int size) {
  
  int forReplace = 0;
  for (int j = 0; j < size; j++) {
    for (int i = 0; i < size - 1; i++){
      if(*(variablesPT + i) > *(variablesPT + i + 1)){
	forReplace = *(variablesPT + i);
	*(variablesPT + i) = *(variablesPT + i + 1);
	*(variablesPT + i + 1) = forReplace;
      }
    }
  }
}



//---------------------------------------------------------------------------
void printArray(int *variablesPT, int size) {
  
  printf("\nTablica variables przedstawia się następująco:\n|");
  
  for (int i = 0; i < size; i ++){
    printf(" %d |", *(variablesPT + i));
  }
  printf("\n\n");
}
