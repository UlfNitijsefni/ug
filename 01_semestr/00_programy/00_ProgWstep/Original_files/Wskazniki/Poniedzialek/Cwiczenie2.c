#include<stdio.h>

int sumOfArgs(int *argumentsPT, int size);


//==========================================================================
void main(){
  
  int arguments[] = {0, 12, 13, 54, 2, 5, 456};
  int *argumentsPT;
  
  int size = sizeof arguments / sizeof arguments[0];
  
  argumentsPT = arguments;
  
  int sum = sumOfArgs(argumentsPT, size);
  printf("\nSuma wszystkich argumentów tablicy wynosi %d\n", sum);
  
}


//--------------------------------------------------------------------------
int sumOfArgs(int *argumentsPT, int size){
  
  int sum = 0;
  
  for (int i = 0; i < size; i++) {
    sum += *(argumentsPT + i);
  }
  return sum;
}
