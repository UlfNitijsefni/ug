#include<stdio.h>


int wczytajDoPT(int *wartosciPT, int dlugosc);


//=============================================================================
void main(){
  
  int wartosc1, wartosc2, wartosc3;
  wartosc1=20;
  wartosc2=15;
  wartosc3=6;

  int wartosci[] = {20, 15, 6, 2, 4};
  int* wartosciPT=wartosci;
  int dlugosc = sizeof wartosci / sizeof wartosci[0];
  
  int wynik = wczytajDoPT(wartosciPT, dlugosc);
  
  printf("\n\nIloczyn liczb w tabeli wynosi: %d\n\n", wynik);
}


//-----------------------------------------------------------------------------
int wczytajDoPT(int *wartosciPT, int dlugosc){
  
  int wynik = 1;
  
  for(int i = 0; i < dlugosc; i++){
    wynik = wynik * *(wartosciPT + i);
  }
  return wynik;
}
