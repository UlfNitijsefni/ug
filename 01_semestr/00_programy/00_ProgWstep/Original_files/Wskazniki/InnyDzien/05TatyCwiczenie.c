#include<stdio.h>


void wczytajDoPT(int *wartosc1PT, int *wartosc2PT);
//=============================================================================
void main(){
  
  int wartosc1, wartosc2;
  wartosc1=20;
  wartosc2=15;
  
  int* wartosc1PT, *wartosc2PT;
  wartosc1PT=&wartosc1;
  wartosc2PT=&wartosc2;
  
  wczytajDoPT(wartosc1PT, wartosc2PT);
  
  printf("\nZmienne wartosc1 i wartosc2 wynosza po zmianie %d i %d.\n", *wartosc1PT, *wartosc2PT);
}


//-----------------------------------------------------------------------------
void wczytajDoPT(int *wartosc1PT, int *wartosc2PT){
  
  printf("\nPodaj, w formacie 'X X', wartosci jakie chcesz zeby mialy wartosc1 i wartosc 2:\n");
  scanf("%d %d", wartosc1PT, wartosc2PT);
  
}
