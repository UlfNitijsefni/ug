#include <stdio.h>

void scanArgs(int *arrPT, int *arrSize);
void moveArgs(int *arrPT, int *arrSize);
void printArray(int *arrPT, int *arrSize);
//=============================================================================
void main() {
  int *arrSize, *arrPT, scannedSize;
  
  printf("Podaj ilosc elementow ktore maja byc wczytane do tablicy:");
  scanf("%d", &scannedSize);
  arrSize = &scannedSize;
  
  int scannedArg[*arrSize];
  arrPT = &scannedArg[0];
  
  scanArgs(arrPT, arrSize);
  printArray(arrPT, arrSize);

  moveArgs(arrPT, arrSize);
  printArray(arrPT,  arrSize);
}


//-----------------------------------------------------------------------------
void scanArgs(int *arrPT, int *arrSize) {
  for (int i = 0; i < *arrSize; i++) {
    printf("Podaj liczbe ktora ma sie znalez w komorce %d : ", i);
    scanf("%d", &(*(arrPT + i)));
  }
}


//-----------------------------------------------------------------------------
void moveArgs(int *arrPT, int *arrSize){
  
  int wyrazStracony = *(arrPT + *arrSize - 1);
  for(int i = *arrSize - 1; i >= 0; i--){
    if (i == 0)
      *(arrPT) = wyrazStracony;
    else
      *(arrPT + i) = *(arrPT + i - 1);
  }
}


//-----------------------------------------------------------------------------
void printArray(int *arrPT, int *arrSize) {
  printf("Tablica przed przesunieciem:\n{ ");
  for (int i = 0; i < *arrSize; i++) {
    printf("%d ", *(arrPT + i));
  }
  printf("}\n");
}
