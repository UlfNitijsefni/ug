#include<stdio.h>


int wymnozDane(int *wartoscPt, int wartosc);

//=============================================================================
void main(){

  int liczbaDoWskaznika = 2;
  int wartosc = 90;
  int *wartoscPT = &liczbaDoWskaznika;

  int wynik = wymnozDane(wartoscPT, wartosc);
  
  printf("\nIloraz podaych wartosci wynosi %d\n\n", wynik);
}


//-----------------------------------------------------------------------------
int wymnozDane(int *wartoscPt, int wartosc) {
    
  return *wartoscPt * wartosc;
  
}
