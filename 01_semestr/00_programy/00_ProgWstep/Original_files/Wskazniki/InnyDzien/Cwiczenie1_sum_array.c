#include <stdio.h>


void sum(int array[], int *size, int *sumaWskaz);
void print(int array[], int* size);



//============================================================================
void main (){


  int *dlugoscWskaz, *sumaWskaz, dlugosc, doWskaznika;

  sumaWskaz = &doWskaznika;
  *sumaWskaz = 0;
  int Tab1[] = { 8, 4, 5, 12 ,7, 214};
  dlugosc = sizeof Tab1 / sizeof Tab1[0];
  dlugoscWskaz = &dlugosc;
  
  printf ("rozmiar tablicy wynosi: %d\n", *dlugoscWskaz);
  print(Tab1, dlugoscWskaz);
  
  sum(Tab1, dlugoscWskaz, sumaWskaz);
  printf("suma wynosi %d\n", *sumaWskaz);
  
}


//----------------------------------------------------------------------------
void sum(int array[], int *size, int *sumaWskaz) {
  
  int suma=0;
  for (int i = 0; i < *size; i++) {
    suma += array[i];
  }
  *sumaWskaz = suma;
}


//----------------------------------------------------------------------------
void print(int array[], int *size) {
  
  printf("{");
  for (int i = 0; i < *size; i++) {
    printf("%d", array[i]);
    if (i < *size - 1) printf(",");
  }
  printf("}\n");
}
