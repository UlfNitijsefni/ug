#include<stdio.h>


int wymnozTablice(int *wartoscPT);

//=============================================================================
void main(){

  int wartosci[] = {90, 12, 4};
  int *wartosciPT = wartosci;

  int wynik = wymnozTablice(wartosciPT);
  
  printf("\nIloraz podaych wartosci wynosi %d\n\n", wynik);
}


//-----------------------------------------------------------------------------
int wymnozTablice(int *wartosciPT) {

  return *wartosciPT * *(wartosciPT + 1) * *(wartosciPT + 2);
  
}

