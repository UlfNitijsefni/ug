#include <stdio.h>


int main(int argc, char *args){
  
  FILE *wsk_plik;
  char dane[3][10];
  
  if (argc != 2){
    
    printf("\nZle podane argumenty; wymagany jest tylko i wylacznie plik na ktorym przeprowadzane beda operacje\n\n");
      return -1;
    
  }
  if ((wsk_plik = fopen(args[1], "r")) != NULL){
    
    fscanf (wsk_plik, "%s, %s, %s", dane[0], dane[1], dane[2]);
    fclose(wsk_plik);
    printf("%s, %s, %s", dane[0], dane[1], dane[2]);
    
  }
  return 0;
}
