#include <stdio.h>

int main(int argc, char *args[]){
  
  FILE *wsk_plik;
  if (argc !=2){
      printf("Uzycie: %s nazwa_pliku\n", args[0]);
      return -1;
    }
  if ((wsk_plik = fopen(args[1], "w")) != NULL){
    printf("Otwarto plik\n");
    fprintf(wsk_plik, "Testowe zdanie\n", 5);
    fclose(wsk_plik);
  }
  return 0;
}
