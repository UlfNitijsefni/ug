#include <stdio.h>

void printN(unsigned int n);
void printSpaces(int x);


//=============================================================================
void main(){

  unsigned int n;
  
  printf("Wpisz rozmiar jaki chcesz zeby mialo M: \n");
  scanf("%u", &n);

  if(n == 1)printf("M\n");
  else if(n > 1) printN(n);
  else printf("Twoja rozmiar jest zbyt maly.\n");
}


//-----------------------------------------------------------------------------
void printN(unsigned int n){
  int spacjeSrodek=0;
  int spacjePocz=n;
  for(int i = 0; i < n; i++){
    printSpaces(spacjePocz);
    for(int j = 0; j < spacjeSrodek; j++) printf(" ");
    spacjeSrodek+=2;
    
    printf("M");
    printSpaces(2*spacjePocz);
    for(int j = 2; j < spacjeSrodek; j++) printf(" ");
    printf("M\n");
    spacjePocz=spacjePocz-1;
  }
}


//-----------------------------------------------------------------------------
void printSpaces(int x){
  int spacjePocz = x;
  for(int j = 0; j < spacjePocz; j++) printf(" ");
  printf("M");
}
