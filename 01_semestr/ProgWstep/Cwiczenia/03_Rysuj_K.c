#include<stdio.h>

void rysujK(int n);
void rysujGore(int n);
void rysujDol(int n);

//===============================================================================
void main(){

  int n;

  printf("Podaj porzadana wysokosc litery:\n");
  scanf("%d", &n);

  int g=n/3;
  int d=2*g;

  if(n < 4){
    printf("Nie da sie uzyskac pozadanych proporcji przy danej wysokosci.\n");
      }
  else
    rysujK(n);
}


//-------------------------------------------------------------------------------
void rysujK(int n){

  int g = n/3;
  int d = n - g;

  if(n == 1)
    printf("K\n");
  
  else if (n == 0)
    printf("Wysokosc jest za mala");
		     
  else{
    rysujGore(g);
    rysujDol(d);
  }
}


//-------------------------------------------------------------------------------
void rysujGore(int n){

  int spacje=2*n - 1;
  
  for (int i = 0; i < n; i++){

    printf("K");
    
    for(int j = 0; j < spacje; j++) printf(" ");
    
    printf("K\n");
    spacje-=2;
  }
}


//-------------------------------------------------------------------------------
void rysujDol(int n){

  int spacje=0;
  
  for (int i = 0; i < n; i++){
    printf("K");
    
    for(int j = spacje; j > 0; j--) printf(" ");
    printf("K\n");
    spacje+=2;
  }
}
