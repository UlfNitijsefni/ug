#include <stdio.h>


void pobierzWskaz(double*, double);


//===============================================================================
void main(){

  double n = 1.111;
  printf("%f\n", n);
  
  double* Wskaz = &n;

  pobierzWskaz(Wskaz, 5.555);
  printf("%f\n", n);

  pobierzWskaz(&n, 8.888);
  printf("%f\n", n);
}


//-------------------------------------------------------------------------------
void pobierzWskaz(double* Wskaz, double Podana){
  
  *Wskaz = Podana;
  
}
