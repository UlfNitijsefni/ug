#include<stdio.h>

int liczNWD(unsigned int n, unsigned int z);
int wczytajLiczbe();
int dzielnik(int n, int z, int i, int NWDwew);


//=============================================================================
void main(){

  unsigned int n, z;
  
  n=wczytajLiczbe();
  z=wczytajLiczbe();

  int NWD=liczNWD(n, z);
  
  printf("\n\n%d\n", NWD);
}


//-----------------------------------------------------------------------------
int liczNWD(unsigned int n, unsigned int z){

  int i = 1;
  int NWDwew = 1;
  if(n==z){
    return n;
  }
  else{
    if(n < z){
      return dzielnik(n, z, i, NWDwew);
    }
    else{
      return dzielnik(z, n, i, NWDwew);
    }
  }
}


//-----------------------------------------------------------------------------
int wczytajLiczbe(){
  int n;
  
  printf("Podaj liczbe:");
  scanf("%u", &n);
  return n;
}


//-----------------------------------------------------------------------------
int dzielnik(int n, int z, int i, int NWDwew){  

  if(i > n){
    return NWDwew;
  }
  else{
    if(n%i==0 && z%i==0){
      NWDwew=i;
      return dzielnik(n, z, ++i, NWDwew);
    }
    return dzielnik(n, z, ++i, NWDwew);
  }
}
