#include<stdio.h>

void wczytajLiczby(int*);
void drugaTablica(int*, int*);
void wypiszTablice(int*, int*);
void sortTablica2(int*);

//=============================================================================
void main(){
  
  int liczby[5];
  int liczby2[5];
  wczytajLiczby(liczby);
  drugaTablica(liczby, liczby2);
  sortTablica2(liczby2);
  wypiszTablice(liczby, liczby2);
}



//-----------------------------------------------------------------------------
void wczytajLiczby(int* liczby){
  
  for(int i = 0; i < 5; i++){
    printf("Podaj liczbe:");
    scanf("%d", &liczby[i]);
  }
}


//-----------------------------------------------------------------------------
void drugaTablica(int* liczby, int* liczby2){
  for(int i = 0; i < 5; i++){
    liczby2[i]=liczby[i];
  }
}


//-----------------------------------------------------------------------------
void wypiszTablice(int* liczby, int* liczby2){
  
  for (int i = 0; i<5; i++){
    printf("Wyraz %d w tablicy I ma przypisana wartosc %d\n", i, liczby[i]);
  }
  
  printf("\n\n");
  
  for (int i = 0; i<5; i++){
    printf("Wyraz %d w tablicy II ma przypisana wartosc %d\n", i, liczby2[i]);
  }  
}


//-----------------------------------------------------------------------------
void sortTablica2(int* liczby2){
  
  int k = liczby2[0];
  printf("\nk ma wartosc %d\n\n", k);
  int flaga;
  
  for (int j = 0; j < 5; j++){
    for (int i = 0; i < 4; i++){
      if(liczby2[i] < liczby2[i+1]){
	k = liczby2[i];
	liczby2[i] = liczby2[i+1];
	liczby2[i+1] = k;
	flaga++;
      }    
    }
  }
}
