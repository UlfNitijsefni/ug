#include<stdio.h>

void skanujTablice(int*, int*);
void wypiszTablice(int*);

//===============================================================================
void main(){

  int n;
  int tablica1[5];
  int tablica2[10];

  skanujTablice(tablica1, tablica2);
}



//-----------------------------------------------------------------------------


void skanujTablice(int* tablica1, int* tablica2){
  for(int i = 0; i < 5; i++){
    
    printf("\nWpisz liczbe ktora ma sie znajdowac na %d miejscu tabeli:\n", i+1);
    
    scanf("%d", &tablica1[i]);

    tablica2[i]=tablica1[i];
    tablica2[i+5]=tablica1[i];
  }
  wypiszTablice(tablica2);
}



//-----------------------------------------------------------------------------
void wypiszTablice(int* tablica2){

for (int i = 0; i < 10; i++)
  printf("Wartosc pod indeksem %d w tablicy 2 wynosi %d", i, tablica2[i]);
}
