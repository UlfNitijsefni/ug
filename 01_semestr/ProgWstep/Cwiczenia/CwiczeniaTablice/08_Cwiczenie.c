#include<stdio.h>

void skanujTablice(int*);
void skrajnosci(int*);


//===============================================================================
void main(){

  int tablica1[5];

  skanujTablice(tablica1);
  skrajnosci(tablica1);
}


//-------------------------------------------------------------------------------
void skanujTablice(int* tablica1){ 
  for (int i = 0; i<5; i++){

    printf("Podaj wartosc ktora ma sie znalezc pod indeksem %d:  ", i);
    scanf("%d", &tablica1[i]);
  }
}


//-------------------------------------------------------------------------------
void skrajnosci(int* tablica1){
  
  int max=tablica1[0];
  int min=tablica1[0];

  for(int i = 0; i<4; i++){
    if(tablica1[i] > tablica1[i+1])
      min = tablica1[i+1];

    if(tablica1[i] < tablica1[i+1])
      max = tablica1[i+1];
  }
  printf("Najwieksza wartosc w tablicy to %d, a najmniejsza; %d\n", max, min);
}
