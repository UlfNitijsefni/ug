#include <stdio.h>
#include <stdlib.h>

int getInt();

//==========================================================================
void main(){

  char* ABC;
  int Index = getInt();
  
  ABC = malloc(Index*sizeof(char));
  
  for (int i = 0; i < Index; i++){
    ABC[i] = 'a'+i;
  }
  printf("\n%c\n\n", ABC[Index-1]);
}


//--------------------------------------------------------------------------
int getInt(){
  
  int Index;
  
  printf("Podaj liczbe od 1 do 20:\n");
  scanf("%d", &Index);
  return Index;
}
