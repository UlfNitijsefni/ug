#include <stdio.h>
#include <stdlib.h>

void scanLetter(char* Wyraz);

//=========================================================================
void main(){
  
  char* Wyraz = malloc(11*sizeof(char));
  scanLetter(Wyraz);
  printf("%s\n\n", Wyraz);

  free (Wyraz);
  printf("\nZwolniono pamiec\n\n");
}


//-------------------------------------------------------------------------
void scanLetter(char* Wyraz){
  
  printf("Wpisz wyraz o dlugosci nie przekraczajacej 10 znakow:\n");
  scanf("%s", Wyraz);
  
}
