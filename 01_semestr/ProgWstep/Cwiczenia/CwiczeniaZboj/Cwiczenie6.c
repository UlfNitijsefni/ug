#include <stdio.h>
#include <stdlib.h>



int main (int argc, char *args[]){
  
  FILE *plik;
  
  if (argc != 4){
    printf("Podales zla ilosc argumentow.\n");
    return 1;
  }
  
  if ((plik = fopen(args[1], "w")) != NULL)
    fprintf(plik, "%d", atoi(args[2])*atoi(args[3]));
  
}
