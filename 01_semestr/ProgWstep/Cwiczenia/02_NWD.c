#include<stdio.h>

int liczNWD(unsigned int n, unsigned int z);
int wczytajLiczbe();
int dzielnik(int n, int z);


//=============================================================================
void main(){

  unsigned int n, z;
  
  n=wczytajLiczbe();
  z=wczytajLiczbe();

  int NWD=liczNWD(n, z);
  
  printf("\n\n%d\n", NWD);
}


//-----------------------------------------------------------------------------
int liczNWD(unsigned int n, unsigned int z){

  if(n==z){
    return n;
  }
  else{
    if(n < z){
      return dzielnik(n, z);
    }
    else{
      return dzielnik(z, n);
    }
  }
}


//-----------------------------------------------------------------------------
int wczytajLiczbe(){
  int n;
  
  printf("Podaj liczbe:");
  scanf("%u", &n);
  return n;
}


//-----------------------------------------------------------------------------
int dzielnik(int n, int z){  
  int NWDwew;

  for(int i=1; i<=n; i++){
    if(n%i==0 && z%i==0){
      NWDwew = i;
    }
  }
  return NWDwew;
}
