package apswing2;

/*******************************************************************
* pole typu checkbox - przykład 2
*******************************************************************/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class p08 extends JFrame implements ActionListener{

    JButton zapisz;
    JPanel panel_lewy,panel_prawy;
    int tab[]={0,0};

    public p08(){ 
        setSize(800, 600);
        setLocation(100, 100);
        setResizable(false);
        
        JPanel naglowek=new JPanel();                
        naglowek.setPreferredSize(new Dimension(800, 50));         

        panel_lewy=new JPanel();     
        panel_lewy.setPreferredSize(new Dimension(200, 450));
        BoxLayout boxlayout = new BoxLayout(panel_lewy, BoxLayout.Y_AXIS); 
        panel_lewy.setLayout(boxlayout); //ustawienie manadżera rozkładu        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); 

        JLabel info=new JLabel("Wybierz opcję:");
        panel_lewy.add(info);   
        
        JCheckBox o1=new JCheckBox("opcja 1");
        o1.setSelected(false); //wyłączenie "zaznaczenia" checkbox-a
        panel_lewy.add(o1); 
        o1.addItemListener((ItemEvent e) -> {
            if (e.getStateChange() == 1) {
                tab[0]=1;
            } else {
                tab[0]=0;
            }
        });        
        
        JCheckBox o2=new JCheckBox("opcja 2");
        o2.setSelected(false); 
        panel_lewy.add(o2); 
        o2.addItemListener((ItemEvent e) -> {
            if (e.getStateChange() == 1) {
                tab[1]=1;
            } else {
                tab[1]=0;
            }
        });  
        
        zapisz=new JButton("Sprawdź");        
        panel_lewy.add(zapisz);        
        zapisz.addActionListener(this);
        
        panel_prawy=new JPanel(new FlowLayout(FlowLayout.LEFT));  
        BoxLayout boxlayout1 = new BoxLayout(panel_prawy, BoxLayout.Y_AXIS); 
        panel_prawy.setLayout(boxlayout1); 

        JPanel stopka=new JPanel();      
        
        JSplitPane podzial3=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel_lewy, panel_prawy);        
        podzial3.setDividerSize(0);
        
        JSplitPane podzial2=new JSplitPane(JSplitPane.VERTICAL_SPLIT,naglowek,podzial3);       
        podzial2.setDividerSize(0);
              
        JSplitPane podzial1=new JSplitPane(JSplitPane.VERTICAL_SPLIT,podzial2,stopka);
        podzial1.setDividerSize(0);
        
        add(podzial1);           
    }   
    
    public static void main(String[] args) {
        p08 okno = new p08();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();
        if (zrodlo==zapisz)
        {
            panel_prawy.removeAll();            
            if (tab[0]==1){
                panel_prawy.removeAll();
                panel_prawy.add(Box.createRigidArea(new Dimension(30,10)));
                JLabel wynik1=new JLabel();
                wynik1.setBounds(0, 0, 300, 100);
                wynik1.setText("checkbox 1 - on");
                panel_prawy.add(wynik1); 
                if (tab[1]==1){
                    panel_prawy.add(Box.createRigidArea(new Dimension(30,10)));
                    JLabel wynik2=new JLabel();
                    wynik2.setBounds(0, 0, 300, 100);
                    wynik2.setText("checkbox 2 - on");
                    panel_prawy.add(wynik2);  
                }
            }
            else if (tab[1]==1){
                    panel_prawy.removeAll();
                    panel_prawy.add(Box.createRigidArea(new Dimension(30,10)));
                    JLabel wynik1=new JLabel();
                    wynik1.setBounds(0, 0, 300, 100);
                    wynik1.setText("checkbox 2 - on");
                    panel_prawy.add(wynik1); 
                    if (tab[0]==1){
                        panel_prawy.add(Box.createRigidArea(new Dimension(30,10)));
                        JLabel wynik2=new JLabel();
                        wynik2.setBounds(0, 0, 300, 100);
                        wynik2.setText("checkbox 1 - on");
                        panel_prawy.add(wynik2);  
                    }                        
            }      
            panel_prawy.doLayout();
        }
    }
    
}
