package apswing2;

/*******************************************************************
* Layout aplikacji - brak widocznych granic sekcji
* BoxLayout - "pionowy" menedżer rozkładu dla dwóch przycisków
*******************************************************************/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class p02 extends JFrame implements ActionListener{

    JButton p1,p2;
    JPanel panel_lewy,panel_prawy;
    JLabel wynik1,stopka_lab;

    public p02(){ 
        setSize(800, 600);
        setLocation(100, 100);
        setResizable(false);

        panel_lewy=new JPanel();
        panel_lewy.setPreferredSize(new Dimension(200, 400));
        
        BoxLayout boxlayout = new BoxLayout(panel_lewy, BoxLayout.Y_AXIS); //BoxLayout - menedżer rozkładu
        panel_lewy.setLayout(boxlayout); //ustawienie manadżera rozkładu
        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); //margines dla przycisku 1 - margines lewy, margines górny
        p1=new JButton("przycisk 1");
        panel_lewy.add(p1);
        p1.addActionListener(this);
        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); //margines dla przycisku 2 - margines lewy, margines górny
        p2=new JButton("przycisk 2");
        panel_lewy.add(p2);
        p2.addActionListener(this);      
        
        panel_prawy=new JPanel(new FlowLayout(FlowLayout.LEFT));   
        
        JPanel stopka=new JPanel();           
        stopka_lab=new JLabel("stopka");
        stopka.add(stopka_lab);

        JSplitPane podzial2=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel_lewy, panel_prawy);        
        //usunięcie granicy między panelem lewym a prawym
        podzial2.setDividerSize(0);
              
        JSplitPane podzial1=new JSplitPane(JSplitPane.VERTICAL_SPLIT,podzial2,stopka);
        podzial1.setDividerSize(0);
        
        add(podzial1);
    }   

    public static void main(String[] args) {
        p02 okno = new p02();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();       
        if (zrodlo==p1)
        {
            panel_prawy.removeAll();
            wynik1=new JLabel("Wciśnięto przycisk 1");
            panel_prawy.add(wynik1);            
            panel_prawy.doLayout();
        }
        else if (zrodlo==p2)
        {
            panel_prawy.removeAll(); 
            wynik1=new JLabel("Wciśnięto przycisk 2");
            panel_prawy.add(wynik1);            
            panel_prawy.doLayout();
        }        
    }
    
}
