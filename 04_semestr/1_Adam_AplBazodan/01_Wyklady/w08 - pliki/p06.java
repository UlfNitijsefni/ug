package apswing2;

/*******************************************************************
* wczytanie tabeli - przykład 3
* nazwa tabeli podawana jest z poziomu formularza
* brak sprawdzenia istnienia tabeli o podanej nazwie
* nazwy kolumn tabeli wprowadzana są ręcznie
*******************************************************************/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

public class p06 extends JFrame implements ActionListener{

    JButton p1,p2,zapisz;
    JPanel panel_lewy,panel_prawy,wczytanie_tabeli;
    JLabel wynik1;
    JTextField podaj_nazwa_tabeli;
    String nazwa_tabeli;
    
    Connection con;
    java.util.List<String[]> lista = new ArrayList<String[]>();
    ResultSet wynik_sql_2;
    int licznik=1;

    public p06(){ 
        setSize(800, 600);
        setLocation(100, 100);
        setResizable(false);
        
        JPanel naglowek=new JPanel();                
        naglowek.setPreferredSize(new Dimension(800, 50));  

        panel_lewy=new JPanel();     
        panel_lewy.setPreferredSize(new Dimension(200, 450));
        BoxLayout boxlayout = new BoxLayout(panel_lewy, BoxLayout.Y_AXIS); //BoxLayout - menedżer rozkładu
        panel_lewy.setLayout(boxlayout); //ustawienie manadżera rozkładu        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); //margines dla przycisku 1 - margines lewy, margines górny
        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); //margines dla przycisku 1 - margines lewy, margines górny
        p1=new JButton("wczytaj");
        panel_lewy.add(p1);
        p1.addActionListener(this);
        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); //margines dla przycisku 2 - margines lewy, margines górny
        p2=new JButton("wyświetl");
        panel_lewy.add(p2);
        p2.addActionListener(this);                
        
        panel_prawy=new JPanel(new FlowLayout(FlowLayout.LEFT));  
        BoxLayout boxlayout1 = new BoxLayout(panel_prawy, BoxLayout.Y_AXIS); //BoxLayout - menedżer rozkładu
        panel_prawy.setLayout(boxlayout1); //ustawienie manadżera rozkładu         

        JPanel stopka=new JPanel();  
        
        JSplitPane podzial3=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel_lewy, panel_prawy);        
        podzial3.setDividerSize(0);
        
        JSplitPane podzial2=new JSplitPane(JSplitPane.VERTICAL_SPLIT,naglowek,podzial3);       
        podzial2.setDividerSize(0);
              
        JSplitPane podzial1=new JSplitPane(JSplitPane.VERTICAL_SPLIT,podzial2,stopka);
        podzial1.setDividerSize(0);
        
        add(podzial1);           
    }   

    public static void main(String[] args) {
        p06 okno = new p06();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();       
        if (zrodlo==p1){
            panel_prawy.removeAll();
            
            wczytanie_tabeli=new JPanel();
            
            JLabel info_nazwa_tabeli=new JLabel("Podaj nazwę tabeli:");
            info_nazwa_tabeli.setBounds(10,10,300,30);
            wczytanie_tabeli.add(info_nazwa_tabeli);        
                        
            podaj_nazwa_tabeli=new JTextField("");
            podaj_nazwa_tabeli.setBounds(10,40,200,30);
            wczytanie_tabeli.add(podaj_nazwa_tabeli); 
            
            zapisz=new JButton("Zapisz");
            zapisz.setBounds(10,80,80,30); 
            wczytanie_tabeli.add(zapisz);         
            zapisz.addActionListener(this);  
        
            panel_prawy.add(wczytanie_tabeli);        
            panel_prawy.doLayout(); 
        }
        else if (zrodlo==zapisz){          
            nazwa_tabeli=podaj_nazwa_tabeli.getText();
            JOptionPane.showMessageDialog(this,"Zapisano","INFORMACJA",JOptionPane.INFORMATION_MESSAGE);            
        }
        else if (zrodlo==p2)
        {
            panel_prawy.removeAll();
            
            try{
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                con = DriverManager.getConnection("jdbc:sqlserver://"+
                    "localhost:1433;databaseName=bd_java;"+
                    "user=sa;password=zaq1@WSX;");

                Statement zapytanie1 = con.createStatement();
                //String sql1="select count(*) from osoby";
                String sql1="select count(*) from "+nazwa_tabeli;
                ResultSet wynik_zapytania1 = zapytanie1.executeQuery(sql1);     
                wynik_zapytania1.next();
                int ile_rek = wynik_zapytania1.getInt(1);      

                Statement zapytanie = con.createStatement(); 
                String sql="select * from osoby";
                ResultSet wynik = zapytanie.executeQuery(sql);          
                ResultSetMetaData wynik_kol = wynik.getMetaData();
                int ile_kolumn = wynik_kol.getColumnCount(); 

                while(wynik.next()) 
                {   
                    String[] tab=new String[ile_kolumn]; //do zapisu wszystkich wartości każdego rekordu

                    for (int i=0;i<ile_kolumn;i++){
                        tab[i]=wynik.getString(i+1); 
                    }
                    lista.add(tab);               
                    licznik++;
                    System.out.println();                 
                }      

                String array[][]=new String[lista.size()][];
                for (int i=0;i<array.length;i++){
                    String[] row=lista.get(i);
                    array[i]=row;
                }         

                String[] columns={"id","imie","nazwisko","waga"};  
                JTable jt1=new JTable(array,columns);         

                JScrollPane scrollpane = new JScrollPane(jt1);
                scrollpane.setPreferredSize(new Dimension(300,500));

                panel_prawy.add(scrollpane); 
                panel_prawy.doLayout();         

                zapytanie1.close();
                zapytanie.close();

                con.close();
            }
            catch(SQLException error_polaczenie) {
                System.out.println("Błąd połączenia z bazą danych");}
            catch(ClassNotFoundException error_sterownik) {
                           System.out.println("Brak sterownika");} 
        }      
    }
    
}
