package apswing2;

/*******************************************************************
* pole typu radio - przykład 1
*******************************************************************/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class p09 extends JFrame implements ActionListener{

    JPanel panel_lewy,panel_prawy;

    public p09(){ 
        setSize(800, 600);
        setLocation(100, 100);
        setResizable(false);
        
        JPanel naglowek=new JPanel();                
        naglowek.setPreferredSize(new Dimension(800, 50));         

        panel_lewy=new JPanel();     
        panel_lewy.setPreferredSize(new Dimension(200, 450));
        BoxLayout boxlayout = new BoxLayout(panel_lewy, BoxLayout.Y_AXIS); 
        panel_lewy.setLayout(boxlayout); //ustawienie manadżera rozkładu        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); 

        JLabel info=new JLabel("Wybierz opcję:");
        panel_lewy.add(info);   
        
        JRadioButton o1 = new JRadioButton("opcja 1");
        o1.setSelected(false); //wyłączenie "zaznaczenia" checkbox-a
        JRadioButton o2 = new JRadioButton("opcja 2");
        o2.setSelected(false); //wyłączenie "zaznaczenia" checkbox-a
        
        /*
        optionLinux.setForeground(Color.BLUE);
        o1.setBackground(Color.YELLOW);
        o1.setFont(new java.awt.Font("Tahoma", Font.BOLD, 16));
        o1.setToolTipText("podpowiedź ...");
        */
         
        //UWAGA! Aby przyciski radio nie działały taj jak checkbox-y 
        //należy umieścić jej w grupie!
        ButtonGroup grupa=new ButtonGroup();
        grupa.add(o1);
        grupa.add(o2);
        //Do panelu dodajemy nie grupę, tylko przyciski "radio"!
        panel_lewy.add(o1);
        panel_lewy.add(o2);
                
        o1.addItemListener((ItemEvent e) -> {
            if (e.getStateChange() == 1) {
                panel_prawy.removeAll();
                panel_prawy.add(Box.createRigidArea(new Dimension(30,10)));
                JLabel wynik=new JLabel();
                wynik.setBounds(0, 0, 300, 100);
                wynik.setText("radio 1 - on");
                panel_prawy.add(wynik);            
                panel_prawy.doLayout();
            }
        });
        
        
        o2.addItemListener((ItemEvent e) -> {
            if (e.getStateChange() == 1) {
                panel_prawy.removeAll();
                panel_prawy.add(Box.createRigidArea(new Dimension(30,10)));
                JLabel wynik=new JLabel();
                wynik.setBounds(0, 0, 300, 100);
                wynik.setText("radio 2 - on");
                panel_prawy.add(wynik);            
                panel_prawy.doLayout();
            }
        });                      
                
        panel_prawy=new JPanel(new FlowLayout(FlowLayout.LEFT));  
        BoxLayout boxlayout1 = new BoxLayout(panel_prawy, BoxLayout.Y_AXIS); //BoxLayout - menedżer rozkładu
        panel_prawy.setLayout(boxlayout1); 

        JPanel stopka=new JPanel();      
        
        JSplitPane podzial3=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel_lewy, panel_prawy);        
        podzial3.setDividerSize(0);
        
        JSplitPane podzial2=new JSplitPane(JSplitPane.VERTICAL_SPLIT,naglowek,podzial3);       
        podzial2.setDividerSize(0);
              
        JSplitPane podzial1=new JSplitPane(JSplitPane.VERTICAL_SPLIT,podzial2,stopka);
        podzial1.setDividerSize(0);
        
        add(podzial1);           
    }   
    
    public static void main(String[] args) {
        p09 okno = new p09();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
    }
    
}




