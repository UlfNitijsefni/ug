package apswing2;

/*******************************************************************
* Layout aplikacji:
* 1) ustawienie koloru wybranych paneli
* 2) usunięcie domyślnych obramowań wszystkich paneli
*******************************************************************/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class p11 extends JFrame implements ActionListener{

    JButton p1,p2;
    JPanel panel_lewy,panel_prawy;
    JLabel wynik1;

    public p11(){ 
        setSize(800, 600);
        setLocation(100, 100);
        setResizable(false);
        
        JPanel naglowek=new JPanel();    
        naglowek.setBackground(Color.blue); //ustawienie koloru tła panelu "nagłówek" 
        naglowek.setPreferredSize(new Dimension(800, 50));         

        panel_lewy=new JPanel();   
        panel_lewy.setBackground(Color.blue);
        panel_lewy.setPreferredSize(new Dimension(200, 450));
        BoxLayout boxlayout = new BoxLayout(panel_lewy, BoxLayout.Y_AXIS); //BoxLayout - menedżer rozkładu
        panel_lewy.setLayout(boxlayout); //ustawienie manadżera rozkładu        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); //margines dla przycisku 1 - margines lewy, margines górny
        
        p1=new JButton("przycisk 1");
        p1.setBackground(Color.yellow);
        panel_lewy.add(p1);
        p1.addActionListener(this);        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); //margines dla przycisku 2 - margines lewy, margines górny
        
        p2=new JButton("przycisk 2");
        p2.setBackground(Color.yellow);
        panel_lewy.add(p2);
        p2.addActionListener(this);           
        
        panel_prawy=new JPanel(new FlowLayout(FlowLayout.LEFT));  

        JPanel stopka=new JPanel();  
        stopka.setBackground(Color.blue);
        
        JSplitPane podzial3=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel_lewy, panel_prawy);   
        podzial3.setBorder(null);  //wyłączenie obramowania dla "podział3" 
        podzial3.setDividerSize(0);
        
        JSplitPane podzial2=new JSplitPane(JSplitPane.VERTICAL_SPLIT,naglowek,podzial3);   
        podzial2.setBorder(null);
        podzial2.setDividerSize(0);
              
        JSplitPane podzial1=new JSplitPane(JSplitPane.VERTICAL_SPLIT,podzial2,stopka);
        podzial1.setBorder(null);
        podzial1.setDividerSize(0);
        
        add(podzial1);           
    }   

    public static void main(String[] args) {
        p11 okno = new p11();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e){
     
    }
    
}

