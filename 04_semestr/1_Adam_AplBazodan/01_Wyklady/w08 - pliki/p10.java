package apswing2;

/*******************************************************************
* pole typu radio - przykład 2
*******************************************************************/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class p10 extends JFrame implements ActionListener{

    JPanel panel_lewy,panel_prawy;
    int tab[]={0,0};
    JButton zapisz;
    int spr_radio=0;

    public p10(){ 
        setSize(800, 600);
        setLocation(100, 100);
        setResizable(false);
        
        JPanel naglowek=new JPanel();                
        naglowek.setPreferredSize(new Dimension(800, 50));         

        panel_lewy=new JPanel();     
        panel_lewy.setPreferredSize(new Dimension(200, 450));
        BoxLayout boxlayout = new BoxLayout(panel_lewy, BoxLayout.Y_AXIS); 
        panel_lewy.setLayout(boxlayout); //ustawienie manadżera rozkładu        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); 

        JLabel info=new JLabel("Wybierz opcję:");
        panel_lewy.add(info);   
        
        JRadioButton o1 = new JRadioButton("opcja 1");
        o1.setSelected(false); //wyłączenie "zaznaczenia" checkbox-a
        JRadioButton o2 = new JRadioButton("opcja 2");
        o2.setSelected(false); //wyłączenie "zaznaczenia" checkbox-a
        
        ButtonGroup grupa=new ButtonGroup();
        grupa.add(o1);
        grupa.add(o2);
        panel_lewy.add(o1);
        panel_lewy.add(o2);

        o1.addActionListener((ActionEvent ie) -> {
            spr_radio=1;
        });        
       
        o2.addActionListener((ActionEvent ie) -> {
            spr_radio=2;
        });     
        
        zapisz=new JButton("Sprawdź");        
        panel_lewy.add(zapisz);        
        zapisz.addActionListener(this);
                
        panel_prawy=new JPanel(new FlowLayout(FlowLayout.LEFT));  
        BoxLayout boxlayout1 = new BoxLayout(panel_prawy, BoxLayout.Y_AXIS); //BoxLayout - menedżer rozkładu
        panel_prawy.setLayout(boxlayout1); 

        JPanel stopka=new JPanel();      
        
        JSplitPane podzial3=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel_lewy, panel_prawy);        
        podzial3.setDividerSize(0);
        
        JSplitPane podzial2=new JSplitPane(JSplitPane.VERTICAL_SPLIT,naglowek,podzial3);       
        podzial2.setDividerSize(0);
              
        JSplitPane podzial1=new JSplitPane(JSplitPane.VERTICAL_SPLIT,podzial2,stopka);
        podzial1.setDividerSize(0);
        
        add(podzial1);           
    }   
    
    public static void main(String[] args) {
        p10 okno = new p10();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();
   
        if (zrodlo==zapisz)
        {
            panel_prawy.removeAll();            
            if (spr_radio==1){
                panel_prawy.removeAll();
                panel_prawy.add(Box.createRigidArea(new Dimension(30,10)));
                JLabel wynik1=new JLabel();
                wynik1.setBounds(0, 0, 300, 100);
                wynik1.setText("radio 1 - on");
                panel_prawy.add(wynik1);  
                }
            }
            if (spr_radio==2){
                panel_prawy.removeAll();
                panel_prawy.add(Box.createRigidArea(new Dimension(30,10)));
                JLabel wynik1=new JLabel();
                wynik1.setBounds(0, 0, 300, 100);
                wynik1.setText("radio 2 - on");
                panel_prawy.add(wynik1);      
            }      
            panel_prawy.doLayout(); 
    }
    
}

