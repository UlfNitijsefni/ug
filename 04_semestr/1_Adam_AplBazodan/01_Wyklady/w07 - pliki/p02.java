package apswing1;

/**********************************************
* wycentrowanie ramki
***********************************************/

import javax.swing.*; 
import java.awt.*;  //pobranie rozdzielczości ekranu

public class p02 extends JFrame{

    public p02(){  
        //pobranie rozdzielczości ekranu
        int szer=Toolkit.getDefaultToolkit().getScreenSize().width;
        int wys=Toolkit.getDefaultToolkit().getScreenSize().height;        
        setSize(800,600);       
        //pobranie wymiarów ramki
        int szer_okna=getSize().width;
        int wys_okna=getSize().height;
        //wycentrowanie ramki
        setLocation((szer-szer_okna)/2,(wys-wys_okna)/2);        
        setResizable(false);
    }    
    
    public static void main(String[] args) {
        p02 okno=new p02(); 
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true); 
    }
    
}
