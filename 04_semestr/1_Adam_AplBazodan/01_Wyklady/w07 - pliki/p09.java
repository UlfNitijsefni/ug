package apswing1;

/*******************************************************************
* wykorzystanie pola tekstowego
* wczytanie i wyświetlenie podwojonych dwóch liczb typu Int i Double
* brak walidacji
*******************************************************************/

import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

public class p09 extends JFrame implements ActionListener{

    JButton dalej; 
    JLabel info_int,wynik_int,info_double,wynik_double;
    JTextField podaj_int,podaj_double;
    Integer liczba_int;
    Double liczba_double;

    public p09(){  
        setSize(400,300);       
        setLocation(100,100);        
        setResizable(false);
        setLayout(null);
        
        info_int=new JLabel("Wprowadź liczbę typu Int");
        info_int.setBounds(10,10,300,30);
        info_int.setFont(new Font("Arial",Font.BOLD,14));
        add(info_int);        
        
        podaj_int=new JTextField("");
        podaj_int.setBounds(10,40,200,30);
        add(podaj_int);        

        info_double=new JLabel("Wprowadź liczbę typu double");
        info_double.setBounds(10,70,300,30);
        info_double.setFont(new Font("Arial",Font.BOLD,14));
        add(info_double);        
        
        podaj_double=new JTextField("");
        podaj_double.setBounds(10,100,200,30);
        add(podaj_double);     
        
        dalej=new JButton("Dalej");
        dalej.setBounds(10,140,80,30); 
        add(dalej);        
        dalej.addActionListener(this);
        
        wynik_int=new JLabel("");
        wynik_int.setBounds(10,180,200,30); 
        wynik_int.setFont(new Font("Arial",Font.BOLD,14));
        add(wynik_int);    
        
        wynik_double=new JLabel("");
        wynik_double.setBounds(10,220,200,30); 
        wynik_double.setFont(new Font("Arial",Font.BOLD,14));
        add(wynik_double);         
    }    
    
    public static void main(String[] args) {
        p09 okno=new p09(); 
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true); 
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();
        if (zrodlo==dalej)
        {         
            liczba_int=Integer.parseInt(podaj_int.getText()); //String -> Int
            liczba_int=liczba_int*2;
            wynik_int.setText(String.valueOf(liczba_int));
            
            liczba_double=Double.parseDouble(podaj_double.getText()); 
            liczba_double=liczba_double*2;
            wynik_double.setText(String.valueOf(liczba_double));
        }
    }

}

