package apswing1;

/*******************************************************************
* menu
*******************************************************************/

import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

public class p11 extends JFrame implements ActionListener{

    //kontrolki jako pola klasy
    JMenuBar menuBar;
    JMenu Plik, Pomoc, Rozwin;
    JMenuItem Otworz, Zapisz, Wyjscie, Informacja, Opcja1, Opcja2;
    
    JLabel wynik_otworz, wynik_zapisz;
    
    static JLabel l;   
    
    public p11(){
        setTitle("menu");
        setSize(500,500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        
        menuBar=new JMenuBar(); //utworzenie "paska" menu
        
        Plik=new JMenu("Plik"); //utworzenie pierwszej opcji menu
        Pomoc=new JMenu("Pomoc"); //utworzenie drugiej opcji menu
        
        //podmenu dla pierwszej opcji menu - "Plik"
        Otworz=new JMenuItem("Otwórz",'O');
        Zapisz=new JMenuItem("Zapisz");        
            //podemnu dla opcji "Rozwiń"
            Rozwin=new JMenu("Rozwiń");
            //Rozwin.setEnabled(false); //dezaktywacja menu zagnieżdżonego
            Opcja1=new JMenuItem("Opcja1");
            Opcja2=new JMenuItem("Opcja2");
            Rozwin.add(Opcja1);
            Rozwin.add(Opcja2);
        Wyjscie=new JMenuItem("Wyjście");
        Plik.add(Otworz);
        Plik.add(Zapisz);
        Plik.add(Rozwin); //menu zagnieżdżone
        Plik.addSeparator(); //dodanie separatora
        Plik.add(Wyjscie);
        
        Wyjscie.addActionListener(this); //akcja dla opcji "Wyjście"
        Wyjscie.setAccelerator(KeyStroke.getKeyStroke("ctrl X")); //przypisanie skrótu klawiszowego do opcji "Wyjście"
        
        //podmenu dla drugiej opcji menu - "Pomoc"
        Informacja=new JMenuItem("Informacja");
        Pomoc.add(Informacja);
        Informacja.addActionListener(this); //akcja dla opcji "Informacja"
        
        setJMenuBar(menuBar); //dodanie menuBar do ramki
        menuBar.add(Plik); //dodanie do menuBar pierwszej opcji "Plik"
        menuBar.add(Pomoc);
        
    }    
    

    public static void main(String[] args) {
        p11 menu=new p11();        
        menu.setVisible(true);  
    }

    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();
        if (zrodlo==Wyjscie){
            //dispose(); //natychmiastowe wyjście z programu
           int odp= JOptionPane.showConfirmDialog(null,"Czy na pewno?","Pytanie",JOptionPane.YES_NO_OPTION);
           if (odp==JOptionPane.YES_OPTION){
               dispose();
           }         
           //else if (odp==JOptionPane.CLOSED_OPTION){...} //obsługa "x" okna dialogowego           
        }
        if (zrodlo==Informacja){
            JOptionPane.showMessageDialog(this,"O programie ... \n Aplikacje bazodanowe","Tytuł",JOptionPane.WARNING_MESSAGE); //okienko dialogowe typu "Message"
            //JOptionPane.WARNING_MESSAGE - "wbudowana" ikona znajdująca się z lewej strony
        }
    }
    
}
