package apswing1;

/*******************************************************************
* wykorzystanie pola tekstowego
* wczytanie i wyświetlenie podwojonych dwóch liczb typu Int i Double
* walidacja ograniczona
*******************************************************************/

import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class p10 extends JFrame implements ActionListener{

    JButton dalej; 
    JLabel info_int,wynik_int,info_double,wynik_double;
    JTextField podaj_int,podaj_double;
    Integer liczba_int;
    Double liczba_double;
    Pattern szablon1=Pattern.compile("[0-9]*");
    Pattern szablon2=Pattern.compile("[0-9]*[.][0-9]*");    

    public p10(){  
        setSize(400,300);       
        setLocation(100,100);        
        setResizable(false);
        setLayout(null);
        
        info_int=new JLabel("Wprowadź liczbę typu Int");
        info_int.setBounds(10,10,300,30);
        info_int.setFont(new Font("Arial",Font.BOLD,14));
        add(info_int);        
        
        podaj_int=new JTextField("");
        podaj_int.setBounds(10,40,200,30);
        add(podaj_int);        

        info_double=new JLabel("Wprowadź liczbę typu double");
        info_double.setBounds(10,70,300,30);
        info_double.setFont(new Font("Arial",Font.BOLD,14));
        add(info_double);        
        
        podaj_double=new JTextField("");
        podaj_double.setBounds(10,100,200,30);
        add(podaj_double);     
        
        dalej=new JButton("Dalej");
        dalej.setBounds(10,140,80,30); 
        add(dalej);        
        dalej.addActionListener(this);
        
        wynik_int=new JLabel("");
        wynik_int.setBounds(10,180,400,30); 
        wynik_int.setFont(new Font("Arial",Font.BOLD,14));
        add(wynik_int);    
        
        wynik_double=new JLabel("");
        wynik_double.setBounds(10,220,400,30); 
        wynik_double.setFont(new Font("Arial",Font.BOLD,14));
        add(wynik_double);         
    }    
    
    public static void main(String[] args) {
        p10 okno=new p10(); 
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true); 
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();
        if (zrodlo==dalej)
        {         
            if (!podaj_int.getText().isEmpty()&&!podaj_double.getText().isEmpty()){     //instrukcja IF jest celowo ograniczona
                      
                //walidacja liczby typu Int
                Matcher test1=szablon1.matcher(podaj_int.getText());
                Boolean wynik_bool1=test1.matches();  //true lub false
                String wynik1=Boolean.toString(wynik_bool1);
                if (wynik1=="true"){
                    liczba_int=Integer.parseInt(podaj_int.getText()); //String -> Int
                    liczba_int=liczba_int*2;
                    wynik_int.setText(String.valueOf(liczba_int));                    
                }
                else {
                    wynik_int.setText("Wartość niezgodna z typem Int");
                }
                         
                //walidacja liczby typu Double
                Matcher test2=szablon2.matcher(podaj_double.getText());
                Boolean wynik_bool2=test2.matches();  //true lub false
                String wynik2=Boolean.toString(wynik_bool2);
                if (wynik2=="true"){
                    liczba_double=Double.parseDouble(podaj_double.getText()); //String -> Double
                    liczba_double=liczba_double*2;
                    wynik_double.setText(String.valueOf(liczba_double));                    
                }
                else {
                    wynik_double.setText("Wartość niezgodna z typem Double");
                }     
                
            }
        }
    }

}
