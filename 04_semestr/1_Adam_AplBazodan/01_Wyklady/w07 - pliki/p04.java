package apswing1;

/********************************************************
* utworzenie dwóch przycisków i obsługa zdarzeń w konsoli
********************************************************/

import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

public class p04 extends JFrame implements ActionListener{

    JButton start, wyjscie; //przyciski muszą być widoczne w metodzie "actionPerformed" 

    public p04(){  
        setSize(400,300);       
        setLocation(100,100);        
        setResizable(false);
        setLayout(null);
        
        start=new JButton("start");
        start.setBounds(10,10,80,30); 
        add(start);        
        start.addActionListener(this);

        wyjscie=new JButton("wyjście");
        wyjscie.setBounds(10,50,80,30); 
        add(wyjscie);        
        wyjscie.addActionListener(this);
    }    
    
    public static void main(String[] args) {
        p04 okno=new p04(); 
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true); 
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource(); //sprawdzamy co jest źródłem akcji, t.j. który z przycisków
        if (zrodlo==start)
        {
            System.out.println("Wciśnięto przycisk -> start");
        }
        else if (zrodlo==wyjscie)
        {
            System.out.println("Wciśnięto przycisk -> wyjście");
            dispose();
        }
    }

}
