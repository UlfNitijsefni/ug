--Imi� i nazwisko:
--Adam Rudnicki
--Data i godzina:
--04.05.2020

DROP TABLE IF EXISTS wypozyczenia;
DROP TABLE IF EXISTS czytelnik;
DROP TABLE IF EXISTS ksiazka;


--Zadanie 1


--Utw�rz tabele ksiazka(id_ksiazka, tytul, imie_autora, nazwisko_autora)

--id_ksiazka - automatycznie nadawany, niepusty numer ksiazki, klucz g��wny,

--tytul - niepusty �a�cuch znak�w zmiennej d�ugo�ci od 3 do 30 znak�w,

--imie_autora - niepusty �a�cuch znak�w zmiennej wielko�ci od 3 do 20 znak�w

--nazwisko_autora - niepusty �a�cuch znak�w zmiennej wielko�ci od 3 do 20 znak�w


CREATE TABLE ksiazka(
id_ksiazka INTEGER NOT NULL PRIMARY KEY IDENTITY,
tytul VARCHAR(30) NOT NULL CHECK (LEN(tytul)>=3),
imie_autora VARCHAR(20) NOT NULL CHECK (LEN(imie_autora)>=3),
nazwisko_autora VARCHAR(20) NOT NULL CHECK (LEN(nazwisko_autora)>=3)
)


-- tabel� czytelnik(id_czytelnik, imie, nazwisko, nr_telefonu)

--id_czytelnik -automatycznie nadawany, niepusty numer czytelnika, klucz g��wny,

--imie - niepusty �a�cuch znak�w zmiennej wielko�ci od 3 do 20 znak�w

--nazwisko - niepusty �a�cuch znak�w zmiennej d�ugo�ci od 3 do 20 znak�w

-- nr_telefonu - niepusty �a�cuch znak�w dok�adnie 11 znak�w

CREATE TABLE czytelnik(
id_czytelnik INTEGER NOT NULL PRIMARY KEY IDENTITY,
imie VARCHAR(20) NOT NULL CHECK (LEN(imie)>=3),
nazwisko VARCHAR(20) NOT NULL CHECK (LEN(nazwisko)>=3),
nr_telefonu VARCHAR(11) CHECK (nr_telefonu LIKE'[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') NOT NULL UNIQUE
)





--oraz tabel� wypo�yczenia ( id_wypo�yczenia, id_ksiazka,id_czytelnik,data_wypozyczenia, data_zwrotu), gdzie:

--id_wypozyczenia- niepusty klucz g��wny, warto�ci nadawane automatycznie,

--id_ksiazka - niepusty klucz obcy powi�zany z kolumn� id_ksiazka tabeli ksiazka,

--id_czytelnik - niepusty klucz obcy powi�zany z kolumn� id_czytelnik tabeli czytelnik,

--data_wypozyczenia, data_zwrotu- daty

CREATE TABLE wypozyczenia(
id_wypozyczenia INTEGER NOT NULL PRIMARY KEY IDENTITY,
id_ksiazka INTEGER NOT NULL FOREIGN KEY REFERENCES KSIAZKA(id_ksiazka),
id_czytelnik INTEGER NOT NULL FOREIGN KEY REFERENCES czytelnik(id_czytelnik),
data_wypozyczenia DATE NOT NULL,
data_zwrotu DATE NOT NULL
)


--Dodaj po 3 rekordy do ka�dej  tabel

--Rozwi�zanie [6pkt]:




--Zadanie 2

--Do ka�dej tabeli dodaj po 5 rekord�w

--Rozwi�zanie [1pkt]:

INSERT INTO ksiazka (tytul, imie_autora, nazwisko_autora) VALUES
('lorem', 'ipsum', 'dolor'),
('sit', 'amet', 'consecteur'),
('aidpiscing', 'elit', 'Fusce'),
('XXX', 'ipsum', 'dolor'),
('YYY', 'zxcv', 'qwerty')

/*INSERT INTO ksiazka VALUES
(1, 'lorem', 'ipsum', 'dolor'),
(2, 'sit', 'amet', 'consecteur'),
(3, 'aidpiscing', 'elit', 'Fusce'),
(4, 'XXX', 'ipsum', 'dolor'),
(5, 'YYY', 'zxcv', 'qwerty')*/


INSERT INTO czytelnik (imie, nazwisko, nr_telefonu) VALUES
('Adam', 'Rudnicki', '00000000000'),
('Tomasz', 'Rudnicki', '22222222222'),
('Grzegorz', 'Brz�czyszczykiewicz', '55555555555'),
('Joanna', 'Zalewska', '77777777777'),
('Anna', 'Brokowska', '99999999999')




INSERT INTO wypozyczenia (id_ksiazka, id_wypozyczenia, data_wypozyczenia, data_zwrotu) VALUES
(1, 2, '2020-12-03', '2020-12-04'),
(2, 3, '1999-03-05', '2000-04-05'),
(3, 1, '1860-11-14', '1900-12-31'),
(2, 4, '2016-02-29', '2017-03-30'),
(2, 3, '2002-04-30', '2003-04-05')




--Zadanie 3

--Dla ka�dego czytelnika oblicz ��czn� 

-- ilo�� ksi��ek wypo�yczona przez danego czytelnika (licz� si� wszystkie ksi��ki: zar�wno te zwr�cone, jak i te niezwr�cone)

--Rozwi�zanie + test[2 pkt]

SELECT wypozyczenia.id_czytelnik, ksiazka.id_ksiazka, (SELECT SUM(id_ksiazka) 
	FROM wypozyczenia 
		WHERE wypozyczenia.id_ksiazka=ksiazka.id_ksiazka ) AS 'ilosc'
			FROM wypozyczenia 
INNER JOIN ksiazka ON ksiazka.id_ksiazka=wypozyczenia.id_ksiazka
INNER JOIN czytelnik ON czytelnik.id_czytelnik=wypozyczenia.id_czytelnik





--Zadanie 4

--Do tabeli czytelnik dodaj kolumn� kara, z domy�ln� warto�ci� 0, (warto�� tej kolumny nie mo�e by� pusta)

--Rozwi�zanie +test  [1 pkt]

ALTER TABLE czytelnik ADD kara INT DEFAULT 0 with values;





--Zadanie 5

--Wypisz czytelnik�w, kt�rzy nie wypo�yczyli �adnej ksi��ki

SELECT id_czytelnik FROM czytelnik

--Rozwi�zanie [2pkt]:



--Zadanie 6

--Kt�ra ksi��ka by�a najcz�ciej wypo�yczana?

--Rozwi�zanie [2pkt]:



--Zadanie 7

--Napisz procedur� umo�liwiaj�c� dodanie rekordu do tabeli czytelnik.

--Procedura ma poprawia� wszystkie imiona w ten spos�b, aby

--aby trzy pierwsze litery by�y drukowane(du�e), a pozosta�e litery by�y ma�e.

--Rozwi�zanie [2pkt]:



--Zadanie 8

--Napisz funkcj� sprawdzaj�c�, czy jej parametr zadany jako VARCHAR(20)

--jest poprawnym kodem pocztowym zapisanym w formacie CCC-CAA, gdzie C to cyfra, a A - to du�a litera alfabetu.

--przetestuj rozwi�zanie.

--Rozwi�zanie [2pkt]:





--Zadanie 9

--Napisz funkcj� pobieraj�c� dwie daty (data_1, data_2). 

--Funkcja powinna zwraca� 1 je�li obie daty s� przed aktualn� dat�. w przeciwnym wypadku powinna zwraca� 0.

--Rozwi�zanie [2pkt]: