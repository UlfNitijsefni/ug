DROP TABLE IF EXISTS osoba

DROP FUNCTION IF EXISTS InitCap
GO

----------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[InitCap] ( @InputString VARCHAR(4000) ) 
RETURNS VARCHAR(4000)
AS
BEGIN

DECLARE @Index          INT
DECLARE @Char           CHAR(1)
DECLARE @PrevChar       CHAR(1)
DECLARE @OutputString   VARCHAR(255)

SET @OutputString = LOWER(@InputString)
SET @Index = 1

WHILE @Index <= LEN(@InputString)
BEGIN
    SET @Char     = SUBSTRING(@InputString, @Index, 1)
    SET @PrevChar = CASE WHEN @Index = 1 THEN ' '
                         ELSE SUBSTRING(@InputString, @Index - 1, 1)
                    END

    IF @PrevChar IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&', '''', '(')
    BEGIN
        IF @PrevChar != '''' OR UPPER(@Char) != 'S'
            SET @OutputString = STUFF(@OutputString, @Index, 1, UPPER(@Char))
    END

    SET @Index = @Index + 1
END

RETURN @OutputString

END
GO

----------------------------------------------------------------------------------------
CREATE TABLE osoba(
osoba_ID INTEGER primary key identity,
imie VARCHAR(20),
nazwisko VARCHAR(40),
pesel BIGINT,
data_ur DATE,
pensja INTEGER
)
GO

----------------------------------------------------------------------------------------
INSERT INTO osoba VALUES
('XYZ', 'OPR', 123456789, '2019-12-09', 123),
('ZZZ', 'PPP', 987654321, '1999-12-31', 444),
('XXX', 'OOO', 135792468, '2001-04-15', 654768),
('YYY', 'RRR', 246801357, '2008-04-10', 1)
GO

SELECT * FROM osoba
GO

/*
----------------------------------------------------------------------------------------
CREATE TRIGGER uppercases
ON osoba
FOR INSERT
--FOR INSERT
NOT FOR REPLICATION
AS
BEGIN
	--SET inserted.imie = 
	--UPDATE osoba
	--SET osoba.imie = [dbo].[InitCap](osoba.imie) FROM osoba INNER JOIN inserted ON osoba.osoba_ID = inserted.osoba_ID

	--UPDATE osoba SET imie = [dbo].[InitCap](imie);
	--UPDATE osoba SET nazwisko = [dbo].[InitCap](nazwisko);

	--UPDATE osoba
	--SET imie = (SELECT [dbo].[InitCap] (imie) FROM inserted WHERE osoba_ID = inserted.osoba_ID) --WHERE osoba_ID = inserted.osoba_ID;
END
*/
----------------------------------------------------------------------------------------
INSERT INTO osoba VALUES
('XYZ', 'OPR', 987654321, '2019-12-09', 123),
('ZZZ', 'PPP', 123456789, '1999-12-31', 444),
('XXX', 'OOO', 135792468, '2001-04-15', 654768),
('YYY', 'RRR', 246801357, '2008-04-10', 1)
GO


UPDATE osoba SET imie = [dbo].[InitCap](imie);
----------------------------------------------------------------------------------------
/*DECLARE @rows INT = 1;

WHILE @rows <= (SELECT COUNT(*) FROM osoba)
	BEGIN

UPDATE osoba
SET
	imie = (SELECT [dbo].[InitCap] (imie) FROM OSOBA WHERE osoba_ID = @rows) WHERE osoba_ID = @rows;

UPDATE osoba
SET
	nazwisko = (SELECT [dbo].[InitCap] (nazwisko) FROM OSOBA WHERE osoba_ID = @rows) WHERE osoba_ID = @rows;
	
	SET @rows = @rows + 1;
END;
*/

SELECT * FROM osoba;