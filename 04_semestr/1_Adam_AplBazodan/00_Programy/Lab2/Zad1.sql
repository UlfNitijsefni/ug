﻿DROP TABLE IF EXISTS producers
DROP TABLE IF EXISTS prices
DROP TABLE IF EXISTS models
DROP TABLE IF EXISTS clientData
DROP TABLE IF EXISTS carDetails
DROP TABLE IF EXISTS clientTransactions
DROP TABLE IF EXISTS workerData
DROP TABLE IF EXISTS carRenting

-------------------------------------------------------------------------
CREATE TABLE producers
(
lp int PRIMARY KEY IDENTITY,
nazwa varchar(15)
)


CREATE TABLE models
(
model varchar(30 )PRIMARY KEY,
producer varchar(15),
)

CREATE TABLE clientData
(
clientID int PRIMARY KEY IDENTITY,
clientName varchar(15),
clientSurname varchar(15),
clientAdress varchar(15),
clientPESEL varchar(11) check(clientPESEL like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
clientNIP int
)

CREATE TABLE prices
(
lp int PRIMARY KEY IDENTITY,
models varchar(30) FOREIGN KEY REFERENCES models(model),
price int
)

CREATE TABLE carDetails
(
carID int PRIMARY KEY IDENTITY,
carModel varchar(20),
carType varchar(20),
carFuel varchar(20),
carCondition varchar(100),
rentPrice int
)

CREATE TABLE carRenting
(
rentID int PRIMARY KEY IDENTITY,
carID int FOREIGN KEY REFERENCES carDetails(carID),
clientID int FOREIGN KEY REFERENCES clientData(clientID),
caution int,
rentDate DATE,
plannedReturn DATE,
actuqalReturn DATE
)


-------------------------------------------------------------------------
INSERT INTO producers VALUES
('A'),
('B'),
('C')

INSERT INTO models VALUES
('A', 'octavia'),
('B', 'optima'),
('C', 'zxy'),
('D', 'zxy'),
('E', 'optima'),
('F', 'zxy')

INSERT INTO clientData VALUES
('Adam','Rudnicki','Gdańsk','12345678910',21028),
('Jakub','Wyzimiński','Gdańsk','01987654321',987654),
('Olaf','Krótki','Gdynia','13579246810',7865),
('Ewa','Kowalski','Sopot','23344590102',21354),
('Kupid','Kowalski','Sopot','09876543234',78934)


-------------------------------------------------------------------------
SELECT * FROM producers
SELECT * FROM models
SELECT * FROM prices
SELECT * FROM clientData