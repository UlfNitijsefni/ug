package apswing1;

/****************************************************************
* wyświetlenie wszystkich rekordów znajdujących się w tabeli
****************************************************************/

import javax.swing.*; 

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;

public class p06{
    
    Connection con;
    static int ile_rek;
    
    JFrame f; 
    
    public p06(){ 

        f=new JFrame();  
       
        //pobranie wybranych kolumn do jednej listy; wszystkie atrybuty przyjmuję jako String
        List<String[]> lista=new ArrayList<String[]>();
        
        //tablica na potrzeby JTable
        String tab[][];
        
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection("jdbc:sqlserver://"+
                "localhost:1433;databaseName=bd_java;"+
                "user=sa;password=zaq1@WSX;");
         
            //Zapytanie SQL
            Statement zapytanie2 = con.createStatement(); 
            String sql2="select * from osoby";
            ResultSet wynik_sql_2 = zapytanie2.executeQuery(sql2);
            ResultSetMetaData wynik_kol = wynik_sql_2.getMetaData();
            int ile_kolumn = wynik_kol.getColumnCount(); 
            //pobranie wybranych kolumn do jednej listy
            while(wynik_sql_2.next()) {
                String[] t={wynik_sql_2.getString("id"),wynik_sql_2.getString("imie")};
                lista.add(t);
            }            
            //konwersja listy do tablicy na potrzeby JTable
            String array[][]=new String[lista.size()][];
            for (int i=0;i<array.length;i++){
                String[] row=lista.get(i);
                array[i]=row;
            }                   
            zapytanie2.close(); 
                    
            //"ręczne" wprowadzenie nazw kolumn
            String[] columns={"id","imie"};  
            
            //wygenerowanie tabeli
            JTable jt1=new JTable(array,columns);          
            JScrollPane sp=new JScrollPane(jt1);           
            f.add(sp); 
            f.setLocation(200,50);
            f.setSize(300,400);   
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            f.setVisible(true);      
            
            con.close();
            
        }
        catch(SQLException error_polaczenie) {
            System.out.println("Błąd połączenia z bazą danych");}
        catch(ClassNotFoundException error_sterownik) {
                       System.out.println("Brak sterownika");}  

    }    
     
    public static void main(String[] args) {
        new p06(); 
    }
    
}

