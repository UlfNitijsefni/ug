package apswing1;

/**********************************************
* utworzenie ramki
***********************************************/

import javax.swing.*; 

public class p01 extends JFrame{

    //konstruktor publiczny ramki=okna
    public p01(){    
        setSize(300,200);   //ustalenie szerokości i wysokości ramki w px
        setLocation(100, 50);   //ustalenie położenia ramki (margines lewy, margines górny); punktem odniesienia jest lewy górny róg ekranu
        setResizable(false);    //zabezpieczenie ramki przed zmianą jej rozmiaru (np. przed rozciągnięciem itp.)
        setTitle("Pierwsze okno");  //ustawienie tytułu ramki
    }    
    
    public static void main(String[] args) {
        p01 okno=new p01(); //utworzenie ramki=okna (obiektu) = wywołanie konstruktora ramki
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //obsługa zamknięcia okna
        okno.setVisible(true); //uwidocznienie okna
    }
    
}