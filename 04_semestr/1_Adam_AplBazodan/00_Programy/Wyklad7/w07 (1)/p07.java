package apswing1;

/********************************************************
* wykorzystanie pola tekstowego
* wczytanie i wyświetlenie imienia i nazwiska
********************************************************/

import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

public class p07 extends JFrame implements ActionListener{

    JButton dalej; 
    JLabel info_imie,wynik_imie,info_nazwisko,wynik_nazwisko;
    JTextField podaj_imie,podaj_nazwisko;
    String imie,nazwisko;

    public p07(){  
        setSize(400,300);       
        setLocation(100,100);        
        setResizable(false);
        setLayout(null);
        
        info_imie=new JLabel("Wprowadź imię");
        info_imie.setBounds(10,10,300,30);
        info_imie.setFont(new Font("Arial",Font.BOLD,14));
        add(info_imie);        
        
        podaj_imie=new JTextField("");
        podaj_imie.setBounds(10,40,200,30);
        add(podaj_imie);        

        info_nazwisko=new JLabel("Wprowadź nazwisko");
        info_nazwisko.setBounds(10,70,300,30);
        info_nazwisko.setFont(new Font("Arial",Font.BOLD,14));
        add(info_nazwisko);        
        
        podaj_nazwisko=new JTextField("");
        podaj_nazwisko.setBounds(10,100,200,30);
        add(podaj_nazwisko);     
        
        dalej=new JButton("Dalej");
        dalej.setBounds(10,140,80,30); 
        add(dalej);        
        dalej.addActionListener(this);
        
        wynik_imie=new JLabel("");
        wynik_imie.setBounds(10,180,200,30); 
        wynik_imie.setFont(new Font("Arial",Font.BOLD,14));
        add(wynik_imie);    
        
        wynik_nazwisko=new JLabel("");
        wynik_nazwisko.setBounds(10,220,200,30); 
        wynik_nazwisko.setFont(new Font("Arial",Font.BOLD,14));
        add(wynik_nazwisko);         
    }    
    
    public static void main(String[] args) {
        p07 okno=new p07(); 
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true); 
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();
        if (zrodlo==dalej)
        {
            imie=podaj_imie.getText(); //pobieram wartość z pola tekstowego
            wynik_imie.setText(imie);
            nazwisko=podaj_nazwisko.getText(); 
            wynik_nazwisko.setText(nazwisko); 
        }
    }

}


