package apswing1;

/************************************************************************************
* wyświetlenie wszystkich rekordów znajdujących się w tabeli po naciśnięciu przycisku
************************************************************************************/

import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class p08 extends JFrame implements ActionListener{
    
    Connection con;
    JFrame f; 
    JButton p1;
    JLabel wynik;
    
    List<String[]> lista=new ArrayList<String[]>();
    
    public p08(){ 
        setSize(400,300);       
        setLocation(100,100);        
        setResizable(false);
        setLayout(null);
        
        p1=new JButton("przycisk 1");
        p1.setBounds(10,10,100,30); 
        add(p1);        
        p1.addActionListener(this);    
    }    
     
    public static void main(String[] args) {
         p08 okno=new p08();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);         
    }

    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();
        if (zrodlo==p1)
        {            
            f=new JFrame();  
            f.setLocation(200,200);
            //pobranie wybranych kolumn do jednej listy; wszystkie atrybuty przyjmuję jako String

            //tablica na potrzeby JTable
            String tab[][];

            try{
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                con = DriverManager.getConnection("jdbc:sqlserver://"+
                    "localhost:1433;databaseName=bd_java;"+
                    "user=sa;password=zaq1@WSX;");

                //Zapytanie SQL
                Statement zapytanie2 = con.createStatement(); 
                String sql2="select * from osoby";
                ResultSet wynik_sql_2 = zapytanie2.executeQuery(sql2);
                ResultSetMetaData wynik_kol = wynik_sql_2.getMetaData();
                int ile_kolumn = wynik_kol.getColumnCount(); 
                //pobranie wybranych kolumn do jednej listy
                while(wynik_sql_2.next()) {
                    String[] t={wynik_sql_2.getString("id"),wynik_sql_2.getString("imie")};
                    lista.add(t);
                }            
                //konwersja listy do tablicy na potrzeby JTable
                String array[][]=new String[lista.size()][];
                for (int i=0;i<array.length;i++){
                    String[] row=lista.get(i);
                    array[i]=row;
                }                   
                zapytanie2.close(); 

                //"ręczne" wprowadzenie nazw kolumn
                String[] columns={"id","imie"};  

                //wygenerowanie tabeli
                JTable jt1=new JTable(array,columns);   
                JScrollPane sp=new JScrollPane(jt1);                 
                f.add(sp); 
                f.setLocation(200,50);
                f.setSize(300,400);    
                f.setVisible(true);      

                con.close();
            }
            catch(SQLException error_polaczenie) {
                System.out.println("Błąd połączenia z bazą danych");}
            catch(ClassNotFoundException error_sterownik) {
                           System.out.println("Brak sterownika");} 
        }
    }
    
}
