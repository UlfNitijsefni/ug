package apswing1;

/****************************************************************
* utworzenie dwóch przycisków i obsługa zdarzeń w etykiecie ramki
****************************************************************/

import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

public class p05 extends JFrame implements ActionListener{

    JButton p1, p2; 
    JLabel wynik;

    public p05(){  
        setSize(400,300);       
        setLocation(100,100);        
        setResizable(false);
        setLayout(null);
        
        p1=new JButton("przycisk 1");
        p1.setBounds(10,10,100,30); 
        add(p1);        
        p1.addActionListener(this);
        
        p2=new JButton("przycisk 2");
        p2.setBounds(10,50,100,30); 
        add(p2);        
        p2.addActionListener(this);   
        
        //utworzenie etykiety (label); w niej będzie wyświetlany efekt po naciśnięciu przycisków
        wynik=new JLabel("",JLabel.LEFT); //wyrównanie do lewej krawędzi etykiety (w poziomie)
        wynik.setVerticalAlignment(JLabel.TOP); //wyrónanie do górnej krawędzi etykiety (w pionie)
        wynik.setBounds(120,10,250,200); 
        wynik.setForeground(Color.WHITE);
        //wynik.setForeground(new Color(200,200,200)); //RGB
        wynik.setFont(new Font("SansSerif",Font.BOLD,15));
        wynik.setOpaque(true); //domyślnie tło etykiety jest wyłączone=false
        wynik.setBackground(Color.lightGray);
        add(wynik);  
    }    
    
    public static void main(String[] args) {
        p05 okno=new p05(); 
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true); 
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();
        if (zrodlo==p1)
        {
            wynik.setText("Wciśnięto przycisk 1");
        }
        else if (zrodlo==p2)
        {
            wynik.setText("Wciśnięto przycisk 2");
        }
    }

}
