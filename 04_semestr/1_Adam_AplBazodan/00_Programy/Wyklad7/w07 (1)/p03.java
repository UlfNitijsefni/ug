package apswing1;

/***************************************************
* utworzenie przycisku i obsługa zdarzenia w konsoli
***************************************************/

import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

public class p03 extends JFrame implements ActionListener{

    public p03(){  
        setSize(400,300);       
        setLocation(100,100);        
        setResizable(false);
        setLayout(null); //włączenie pozycjonowania absolutnego=sami ustalamy położenie komponentów
        
        JButton button=new JButton("start");
        button.setBounds(10,10,80,30); //10,10 - margines lewy i górny; 80,30 - szerokość i wysokość przycisku
        add(button); //dodanie przycisku do ramki
        
        //dodanie tzw. "słuchacza zdarzeń" = odwołujemy się do ramki (this), która jest słuchaczem
        button.addActionListener(this);
        
    }    
    
    public static void main(String[] args) {
        p03 okno=new p03(); 
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true); 
    }
    
    //obsługa zdarzenia: e - obiekt zdarzenia (nazwa dowolna), który po kliknięciu przycisku jest wysyłany do słuchacza zdarzenia
    @Override
    public void actionPerformed(ActionEvent e){
        System.out.println("Wciśnięto przycisk -> start");
    }

}
