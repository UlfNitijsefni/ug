GO
DROP TABLE IF EXISTS adres, klient
GO
create table klient
(
	id_klient INT PRIMARY KEY IDENTITY,
	imie VARCHAR(20),
	nazwisko VARCHAR(20),
	data_ur DATE
)




GO
create table adres
(
	id_adres INT PRIMARY KEY IDENTITY,
	id_klient INT FOREIGN KEY REFERENCES klient(id_klient),
	miasto VARCHAR(20),
	ulica VARCHAR(20)
)


GO
insert INTo klient values
('Dawid','Kowal','1999-02-12'),
('Ania','Radomska','1999-04-12'),
('Tymoteusz','Sikora','1998-12-12'),
('Antek','Nowy','1997-01-22')


SELECT*FROM klient

GO
insert INTo adres values
(1,'Olsztyn','PstrowskieGO'),
(2,'Gdansk','Pomorska'),
(3,'Gdansk','Kolobrzeska'),
(4,'Gdynia','Nieznana')

SELECT*FROM adres


SELECT 
	klient.nazwisko,
	DATEDIFF(YEAR,data_ur,GETDATE()) AS 'wiek',
	adres.miasto FROM
		 klient inner join adres ON klient.id_klient = adres.id_klient


SELECT 
	klient.nazwisko 
	FROM klient 
	WHERE DATEDIFF(YEAR,data_ur,GETDATE()) = (SELECT min(DATEDIFF(YEAR,data_ur,GETDATE())) FROM klient)


SELECT 
	miasto, 
	count(*) AS 'ilosc mieszkancow' FROM adres GROUP BY miasto