package apswing2;

/*******************************************************************
* pole typu checkbox - przykład 1
*******************************************************************/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class p07 extends JFrame implements ActionListener{

    JPanel panel_lewy,panel_prawy;

    public p07(){ 
        setSize(800, 600);
        setLocation(100, 100);
        setResizable(false);
        
        JPanel naglowek=new JPanel();                
        naglowek.setPreferredSize(new Dimension(800, 50));         

        panel_lewy=new JPanel();     
        panel_lewy.setPreferredSize(new Dimension(200, 450));
        BoxLayout boxlayout = new BoxLayout(panel_lewy, BoxLayout.Y_AXIS); 
        panel_lewy.setLayout(boxlayout); //ustawienie manadżera rozkładu        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); 

        JLabel info=new JLabel("Wybierz opcję:");
        panel_lewy.add(info);   
        
        JCheckBox o1=new JCheckBox("opcja 1");
        o1.setSelected(false); //wyłączenie "zaznaczenia" checkbox-a
        panel_lewy.add(o1); 
        o1.addItemListener((ItemEvent e) -> {
            if (e.getStateChange() == 1) {
                panel_prawy.removeAll();
                panel_prawy.add(Box.createRigidArea(new Dimension(50,10)));
                JLabel wynik=new JLabel();
                wynik.setBounds(0, 0, 300, 100);
                wynik.setText("checkbox 1 - on");
                panel_prawy.add(wynik);            
                panel_prawy.doLayout();
            } else {
                panel_prawy.removeAll();
                panel_prawy.add(Box.createRigidArea(new Dimension(50,10)));
                JLabel wynik=new JLabel();
                wynik.setText("checkbox 1 - off");
                wynik.setBounds(0, 0, 300, 100);
                panel_prawy.add(wynik);            
                panel_prawy.doLayout();
            }
        });        
        
        JCheckBox o2=new JCheckBox("opcja 2");
        o2.setSelected(false); 
        panel_lewy.add(o2); 
        o2.addItemListener((ItemEvent e) -> {
            if (e.getStateChange() == 1) {
                panel_prawy.removeAll();
                panel_prawy.add(Box.createRigidArea(new Dimension(50,10)));
                JLabel wynik=new JLabel();
                wynik.setBounds(0, 0, 300, 100);
                wynik.setText("checkbox 2 - on");
                panel_prawy.add(wynik);            
                panel_prawy.doLayout();
            } else {
                panel_prawy.removeAll();
                panel_prawy.add(Box.createRigidArea(new Dimension(50,10)));
                JLabel wynik=new JLabel();
                wynik.setText("checkbox 1 - off");
                panel_prawy.add(wynik);            
                panel_prawy.doLayout();
            }
        });  

        panel_prawy=new JPanel(new FlowLayout(FlowLayout.LEFT));  
        BoxLayout boxlayout1 = new BoxLayout(panel_prawy, BoxLayout.Y_AXIS); //BoxLayout - menedżer rozkładu
        panel_prawy.setLayout(boxlayout1); 

        JPanel stopka=new JPanel();      
        
        JSplitPane podzial3=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel_lewy, panel_prawy);        
        podzial3.setDividerSize(0);
        
        JSplitPane podzial2=new JSplitPane(JSplitPane.VERTICAL_SPLIT,naglowek,podzial3);       
        podzial2.setDividerSize(0);
              
        JSplitPane podzial1=new JSplitPane(JSplitPane.VERTICAL_SPLIT,podzial2,stopka);
        podzial1.setDividerSize(0);
        
        add(podzial1);           
    }   
    
    public static void main(String[] args) {
        p07 okno = new p07();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){

    }
    
}

