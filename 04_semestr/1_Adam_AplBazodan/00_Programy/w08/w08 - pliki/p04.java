package apswing2;

/*******************************************************************
* wczytanie tabeli - przykład 1
*******************************************************************/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

public class p04 extends JFrame implements ActionListener{

    JButton p1,p2;
    JPanel panel_lewy,panel_prawy;
    JLabel wynik1;
    
    Connection con;
    java.util.List<String[]> lista = new ArrayList<String[]>();
    ResultSet wynik_sql_2;

    public p04(){ 
        setSize(800, 600);
        setLocation(100, 100);
        setResizable(false);
        
        JPanel naglowek=new JPanel();                
        naglowek.setPreferredSize(new Dimension(800, 50));  

        panel_lewy=new JPanel();     
        panel_lewy.setPreferredSize(new Dimension(200, 450));
        BoxLayout boxlayout = new BoxLayout(panel_lewy, BoxLayout.Y_AXIS); //BoxLayout - menedżer rozkładu
        panel_lewy.setLayout(boxlayout); //ustawienie manadżera rozkładu        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); //margines dla przycisku 1 - margines lewy, margines górny
        
        p1=new JButton("wczytaj tabelę");
        panel_lewy.add(p1);
        p1.addActionListener(this);                 
        
        panel_prawy=new JPanel(new FlowLayout(FlowLayout.LEFT));  
        BoxLayout boxlayout1 = new BoxLayout(panel_prawy, BoxLayout.Y_AXIS); //BoxLayout - menedżer rozkładu
        panel_prawy.setLayout(boxlayout1); //ustawienie manadżera rozkładu         

        JPanel stopka=new JPanel();  
        
        JSplitPane podzial3=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel_lewy, panel_prawy);        
        podzial3.setDividerSize(0);
        
        JSplitPane podzial2=new JSplitPane(JSplitPane.VERTICAL_SPLIT,naglowek,podzial3);       
        podzial2.setDividerSize(0);
              
        JSplitPane podzial1=new JSplitPane(JSplitPane.VERTICAL_SPLIT,podzial2,stopka);
        podzial1.setDividerSize(0);
        
        add(podzial1);           
    }   

    public static void main(String[] args) {
        p04 okno = new p04();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();       
        if (zrodlo==p1)
        {
            panel_prawy.removeAll();
            
            try{
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                con = DriverManager.getConnection("jdbc:sqlserver://"+
                    "localhost:1433;databaseName=bd_java;"+
                    "user=sa;password=zaq1@WSX;");
                Statement zapytanie2 = con.createStatement(); 
                String sql2="select * from osoby";
                ResultSet wynik_sql_2 = zapytanie2.executeQuery(sql2);
                ResultSetMetaData wynik_kol = wynik_sql_2.getMetaData();
                int ile_kolumn = wynik_kol.getColumnCount(); 
                while(wynik_sql_2.next()) {
                    String[] t={wynik_sql_2.getString("id"),wynik_sql_2.getString("imie")}; //ręczne wprowadzenie nazwy kolumn, które chcemy wyświetlić - zgodne z SQL
                    lista.add(t);
                }            

                String array[][]=new String[lista.size()][];
                for (int i=0;i<array.length;i++){
                    String[] row=lista.get(i);
                    array[i]=row;
                }                   
                zapytanie2.close(); 

                String[] columns={"id","imie"};  //ręczne wprowadzenie nazwy kolumn, które chcemy wyświetlić; nazwy dowolne, ponieważ dotyczą JTable
                JTable jt1=new JTable(array,columns);  
                jt1.setRowHeight(30); //wysokość wiersza (nie dotyczy nagłówka tabeli)

                //ręczne ustawienie szerokości dla każdej kolumny
                TableColumnModel jta1kol = jt1.getColumnModel();
                jta1kol.getColumn(0).setPreferredWidth(50); //szerokość pierwszej kolumny
                jta1kol.getColumn(1).setPreferredWidth(150);   
                jt1.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 18));

                //wycentrowanie zawartości komórek w JTable
                DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
                centerRenderer.setHorizontalAlignment(JLabel.CENTER);
                jt1.getColumnModel().getColumn(0).setCellRenderer(centerRenderer); //w pierwszej kolumnie 
                jt1.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);             

                JTableHeader naglowek = jt1.getTableHeader();
                naglowek.setBackground(Color.red);     //zmiana tła nagłówka JTable       
                naglowek.setForeground(Color.yellow);  //zmiana koloru czcionki nagłówka JTable

                JScrollPane scrollpane = new JScrollPane(jt1);
                scrollpane.setPreferredSize(new Dimension(300,500));
                
                panel_prawy.add(scrollpane); 
                panel_prawy.doLayout();                
     
                con.close();
            }
            catch(SQLException error_polaczenie) {
                System.out.println("Błąd połączenia z bazą danych");}
            catch(ClassNotFoundException error_sterownik) {
                           System.out.println("Brak sterownika");} 
            
        }      
    }
    
}