package apswing2;

/*******************************************************************
* Layout aplikacji - widoczne granice sekcji
* obsługa przycisku po utworzeniu okna aplikacji 
*******************************************************************/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class p01 extends JFrame implements ActionListener{

    JButton p1,p2;
    JPanel panel_lewy,panel_prawy;
    JLabel wynik1,stopka_lab;

    public p01(){ 
        setSize(800, 500);
        setLocation(100, 100);
        setResizable(false);

        panel_lewy=new JPanel();
        panel_lewy.setPreferredSize(new Dimension(200, 400));
        
        p1=new JButton("przycisk 1");
        panel_lewy.add(p1);
        p1.addActionListener(this);    
        
        panel_prawy=new JPanel(new FlowLayout(FlowLayout.LEFT)); //wyjustowanie elementów do lewej strony   

        JPanel stopka=new JPanel();  
        
        stopka_lab=new JLabel("stopka");
        stopka.add(stopka_lab);

        //najpierw tworzę podział poziomy 
        JSplitPane podzial2=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel_lewy, panel_prawy);        
              
        //tworzę podział główny (ostateczny) -> podział2 + stopka
        JSplitPane podzial1=new JSplitPane(JSplitPane.VERTICAL_SPLIT,podzial2,stopka);
        
        add(podzial1);
    }   

    public static void main(String[] args) {
        p01 okno = new p01();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
    }
      
    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();       
        if (zrodlo==p1)
        {
            panel_prawy.removeAll(); //czyszczenie całej zawartości panelu prawego 
            wynik1=new JLabel("Wciśnięto przycisk 1");
            panel_prawy.add(wynik1);            
            panel_prawy.doLayout();
        }
    }

}