package apswing2;

/*******************************************************************
* wczytanie tabeli - przykład 2
*******************************************************************/

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

public class p05 extends JFrame implements ActionListener{

    JButton p1,p2;
    JPanel panel_lewy,panel_prawy;
    JLabel wynik1;
    
    Connection con;
    java.util.List<String[]> lista = new ArrayList<String[]>();
    ResultSet wynik_sql_2;
    int licznik=1;

    public p05(){ 
        setSize(800, 600);
        setLocation(100, 100);
        setResizable(false);
        
        JPanel naglowek=new JPanel();                
        naglowek.setPreferredSize(new Dimension(800, 50));  

        panel_lewy=new JPanel();     
        panel_lewy.setPreferredSize(new Dimension(200, 450));
        BoxLayout boxlayout = new BoxLayout(panel_lewy, BoxLayout.Y_AXIS); //BoxLayout - menedżer rozkładu
        panel_lewy.setLayout(boxlayout); //ustawienie manadżera rozkładu        
        panel_lewy.add(Box.createRigidArea(new Dimension(30, 10))); //margines dla przycisku 1 - margines lewy, margines górny
        
        p1=new JButton("wczytaj tabelę");
        panel_lewy.add(p1);
        p1.addActionListener(this);                 
        
        panel_prawy=new JPanel(new FlowLayout(FlowLayout.LEFT));  
        BoxLayout boxlayout1 = new BoxLayout(panel_prawy, BoxLayout.Y_AXIS); //BoxLayout - menedżer rozkładu
        panel_prawy.setLayout(boxlayout1); //ustawienie manadżera rozkładu         

        JPanel stopka=new JPanel();  
        
        JSplitPane podzial3=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, panel_lewy, panel_prawy);        
        podzial3.setDividerSize(0);
        
        JSplitPane podzial2=new JSplitPane(JSplitPane.VERTICAL_SPLIT,naglowek,podzial3);       
        podzial2.setDividerSize(0);
              
        JSplitPane podzial1=new JSplitPane(JSplitPane.VERTICAL_SPLIT,podzial2,stopka);
        podzial1.setDividerSize(0);
        
        add(podzial1);           
    }   

    public static void main(String[] args) {
        p05 okno = new p05();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e){
        Object zrodlo=e.getSource();       
        if (zrodlo==p1)
        {
            panel_prawy.removeAll();
            
            try{
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                con = DriverManager.getConnection("jdbc:sqlserver://"+
                    "localhost:1433;databaseName=bd_java;"+
                    "user=sa;password=zaq1@WSX;");

                Statement zapytanie1 = con.createStatement();
                String sql1="select count(*) from osoby";
                ResultSet wynik_zapytania1 = zapytanie1.executeQuery(sql1);     
                wynik_zapytania1.next();
                int ile_rek = wynik_zapytania1.getInt(1);
                System.out.println("ilość rekordów = "+ile_rek);          

                Statement zapytanie = con.createStatement(); 
                String sql="select * from osoby";
                ResultSet wynik = zapytanie.executeQuery(sql);          
                ResultSetMetaData wynik_kol = wynik.getMetaData();
                int ile_kolumn = wynik_kol.getColumnCount();
                System.out.println("ilość kolumn = "+ile_kolumn+"\n"); 

                /*
                while(wynik.next()) //pętla wykonuje się tyle razy, ile jest rekordów
                {
                    System.out.println("rekord nr:"+licznik);
                    System.out.println(wynik.getString(1)); //pierwsza wartość rekordu
                    System.out.println(wynik.getString(2)); //druga wartość rekordu
                    System.out.println(wynik.getString(3)); //trzecia wartość rekordu
                    System.out.println(wynik.getString(4)); 
                    System.out.println();
                    licznik++;
                }        
                */
                while(wynik.next()) //pętla wykonuje się tyle razy, ile jest rekordów
                {   
                    System.out.print("rekord nr (tablica) = "+licznik+" :");
                    String[] tab=new String[ile_kolumn]; //do zapisu wszystkich wartości każdego rekordu

                    //zapis wartości rekordów do tablicy
                    //tablica -> numeracja zaczyna się od 0
                    //numeracja wartości zaczyna się od 1
                    for (int i=0;i<ile_kolumn;i++){
                        tab[i]=wynik.getString(i+1); 
                    }
                    //zapis tablicy do listy
                    lista.add(tab);
                    //wyświetlenie każdego rekordu w formie tablicy
                    for (int i=0;i<ile_kolumn;i++){
                        System.out.print(tab[i]+" ");
                    }                
                    licznik++;
                    System.out.println();                 
                }      

                //przekształcenie listy w tablicę na potrzeby JTable
                String array[][]=new String[lista.size()][];
                for (int i=0;i<array.length;i++){
                    String[] row=lista.get(i);
                    array[i]=row;
                }         

                //Utworzenie tabeli JTable
                String[] columns={"id","imie","nazwisko","waga"};  
                JTable jt1=new JTable(array,columns);         

                JScrollPane scrollpane = new JScrollPane(jt1);
                scrollpane.setPreferredSize(new Dimension(300,500));

                panel_prawy.add(scrollpane); 
                panel_prawy.doLayout();         

                zapytanie1.close();
                zapytanie.close();

                con.close();
            }
            catch(SQLException error_polaczenie) {
                System.out.println("Błąd połączenia z bazą danych");}
            catch(ClassNotFoundException error_sterownik) {
                           System.out.println("Brak sterownika");} 
        }      
    }
    
}
