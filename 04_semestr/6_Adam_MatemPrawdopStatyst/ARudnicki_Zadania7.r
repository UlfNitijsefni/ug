---
title: "Zadania 7 - Estymacja przedziałowa, dowody"
author: "AR"
date: "28 kwietnia 2020"
output: pdf_document
---

#Zadanie 1.(Model 2)   
#========
#Dla roznych mu i sigma, wygeneruj N=1000 prob licznosci n=100: x1,x2,...,xn  z rozkładu N(mu,sigma^2)
#i korzystajac z nich, wyznacz N: t1,t2,...,tN  mozliwych wartości zmiennej U. Korzystajac z testow diagnostycznych sprawdz teze Faktu 2. 
N=1000; n=100
m=3; sigma=2

licz.t <- function(dane) {(mean(dane)-m)/ (sd(dane)/sqrt(n-1))}

t <- replicate(N, licz.t(rnorm(n,m,sigma)))

hist(t,prob=T)
curve(dt(x,n-1), add=T,col=2)

p <- ppoints(100); p
t.teor <- qt(p,n-1)  #kwantyle z rozkladu teoretycznego

qqplot(t,t.teor)
abline(a=0,b=1,col=2)


#Zadanie 2. (EPU dla wariancji)   
#========
#Dla roznych mu i sigma, wygeneruj N=1000 prob licznosci n=100: x1,x2,...,xn  z rozkładu N(mu,sigma^2)
#i korzystajac z nich, wyznacz N: c1,c2,...,cn  mozliwych wartosci zmiennej chi^2. 
#Korzystajac z testow diagnostycznych sprawdz teze Faktu 3. 
N=1000; n=100;
m=3; sigma=2;

licz.chi <- function(dane){(n*sd(dane)^2)/(sigma^2)}

chi <- replicate(N, licz.chi(rnorm(n,m,sigma)))

hist(chi,prob=T)
curve(dchisq(x,n-1), add=T,col=2)

p <- ppoints(100); p
chi.teor <- qchisq(p,n-1)

qqplot(chi.teor, chi)
abline(a=0,b=1,col=2)


#Zadanie 3. (Model 3)   
#========
#Wybierz trzy rozne rozklady (rozne tez od rozkladu normalnego) i
#wygeneruj z nich proby licznosci n=150: x1,x2,...,xn. Wyznacz przedzial ufnosci dla sredniej, 
#korzystajac z Modelu 3. Sprawdz, czy wartosc oczekiwana rozkladu z ktorego generowana byla proba
#nalezy do wyliczonego przedzialu.


f.exp = function(N,n,alpha) {
  X = rexp(n,2)
  l=mean(X)-qnorm(1-alpha)*sd(X)/sqrt(n)
  p=mean(X)+qnorm(1-alpha)*sd(X)/sqrt(n)
  f.granicePrzedzialow(rexp(n,2), alpha, n)
  return(c(p, l))
}
f.exp(1000, 150, 0.05)
mean.teor = 1/2
mean.teor
# jest w przedziale


f.tStudentsentsentsents = function(N,n,alpha) {
  X = rt(n,2)
  l=mean(X)-qnorm(1-alpha)*sd(X)/sqrt(n)
  p=mean(X)+qnorm(1-alpha)*sd(X)/sqrt(n)
  return(c(p, l))
}
f.tStudentsentsents(1000,150,0.05)
stud.teor=0
stud.teor
#miesci sie w przedziale


f.logarythymNormal = function(N,n,m,sigma,alpha) {
  X = rlnorm(n, m, sigma)
  l=mean(X)-qnorm(1-alpha)*sd(X)/sqrt(n)
  p=mean(X)+qnorm(1-alpha)*sd(X)/sqrt(n)
  return(c(p, l))
}
f.logarythymNormal(1000,150,5,2,0.05)
log.teor=exp(m+sigma^2/2)
log.teor
#roznie, czasami sie miesci, czasami sie nie miesci

#Zad3
n<-750
p0=0.06
K=37
alpha=0.01
#l.obs<-c(10,30,45,15)

#funkcja obliczajaca wartosc  statystyki testowej, 
licz.U <- function(n,K,p0){(K/n-p0)/sqrt(p0*(1-p0)/n)}
#Hipoteza zerowa: p=0.70(70%), Kontrhipoteza: p neq 0.70
U <- licz.U(n,K,p0); U
q1<-qnorm(alpha);q1



data <- c(4.9,6,9,7.2,8.9,5.1,5,8.8,7.7,7.5)

mu = mean(data)

alpha <- 0.05/2
n <- length(data)
?qchisq

qn <- qnorm(1-alpha)
sdx = 0.4

left <- mu - qn*sdx/sqrt(n)
right <- mu + qn*sdx/sqrt(n)

left; right;
right-left



n<-550
p0=0.05
K=22
alpha=0.05
#l.obs<-c(10,30,45,15)

#funkcja obliczajaca wartosc  statystyki testowej, 
licz.U <- function(n,K,p0){(K/n-p0)/sqrt(p0*(1-p0)/n)}
#Hipoteza zerowa: p=0.70(70%), Kontrhipoteza: p neq 0.70
U <- licz.U(n,K,p0); U
q1<-qnorm(alpha);q1



X <- c(14.3,9.6,4.4,12.8,12.4,11.1,12.2,9.5,10.7,22.4)

m <- mean(X)
m
#Ko�ce przedzia�u dla �redniej - model 2
CI.mean2 <- function(data,alpha=0.05) {
  mu = mean(data)
  n = length(data)
  sdx = sd(data)/sqrt(n-1)
  alph = (1-alpha/2)
  student = qt(alph, n-1)
  l = mu - student * sdx
  p = mu + student * sdx
  return (c(l,p))
}

CI.mean2(X,0.05)






library(MASS)
library('nortest')



X=c(14,5,19,1,4)
mu <- mean(X);mu

#Estymator nieobciazony
var1 <- sum((X-mu)^2)/4
#Estymator obciazony
var2 <- sum((X-mu)^2)/5

sqrt(var1)
sqrt(var2)


