package objects;

public class Node {
    private Node parent;
    private int maxKeys;
    private boolean isLeaf;
    private int keyAmount;
    private int[] keys;
    private Node[] sons;

    public Node(int maxKeys, boolean isLeaf, int[] keys, Node parent) {
        this.maxKeys = maxKeys;
        this.isLeaf = isLeaf;
        this.keys = keys;
        this.keyAmount = keys.length;
        Node[] tempSons = {};
        this.sons = tempSons;
        this.parent = parent;
    }

    public void setParent(Node newParent){
        this.parent = newParent;
    }

    public int getGetSonsSize() {
        this.keyAmount = keys.length;
        return sons.length;
    }

    public Node[] getSons() {
        this.keyAmount = keys.length;
        return this.sons;
    }

    public void setSons(Node[] newSons) {
        this.keyAmount = keys.length;
        this.sons = newSons;
    }

    public void changeLeaf() {
        this.keyAmount = keys.length;
        this.isLeaf = !this.isLeaf;
    }

    public boolean isLeaf() {
        this.keyAmount = keys.length;
        return isLeaf;
    }

    public int getKeyAmount() {
        this.keyAmount = keys.length;
        return keys.length;
    }

    public int[] getKeys() {
        this.keyAmount = keys.length;
        return keys;
    }

    public void setKeys(int[] keysToSet) {
        this.keys = keysToSet;
        this.keyAmount = keys.length;
//        while(keys.length > 2*maxKeys - 1){
//
//        }
    }

    public int getMaxKeys() {
        return maxKeys;
    }

    public Node getSon(int index) {
        if (index < sons.length) {
            return sons[index];
        } else
            return null;
    }
}
