package objects;

import ops.Operations;

public class Main {

    public static void main(String[] args) {
        int[] tempKeys = {};
        Node root = new Node(2, true, tempKeys, null);
        Operations operations = new Operations();
        root = operations.insert(root, 1);
        root = operations.insert(root, 2);
        root = operations.insert(root, 3);
        root = operations.insert(root, 4);
        root = operations.insert(root, 5);
        root = operations.insert(root, 6);

        System.out.println("Szukanie 6: " + operations.searchBTree(root, 6));
//        root = operations.insert(root, 7);
//        root = operations.insert(root, 8);
//        root = operations.insert(root, 9);
//        root = operations.insert(root, 10);
//        root = operations.insert(root, 11);
//        root = operations.insert(root, 12);
//        root = operations.insert(root, 13);
        operations.printArr(root, 0);
    }
}