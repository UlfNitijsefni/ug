package objects;

public class SearchResult {
    private Node parent;
    private int index;

    public SearchResult(Node parent, int index){
        this.parent = parent;
        this.index = index;
    }

    public int getIndex(){
        return this.index;
    }

    public Node getParent(){
        return this.parent;
    }
}
