package ops;

import objects.Node;
import objects.SearchResult;

import java.util.Arrays;

public class Operations {

    //-------------------------------------------------------------------------------------------
    public SearchResult searchBTree(Node root, int searched) {
        int i = 0;
        while (i < root.getKeyAmount() && searched > root.getKeys()[i]) {
            i++;
        }
        if (i < root.getKeyAmount() && searched == root.getKeys()[i]) {
            SearchResult result = new SearchResult(root, i);
            return result;
        }
        if (root.isLeaf())
            return null;
        else if (root.getSons().length > 0)
            return searchBTree(root.getSon(i), searched);
        else return null;
    }

    //-------------------------------------------------------------------------------------------
    public void splitSon(Node root, int i, Node split) {
        int t = split.getMaxKeys() * 2 - 1;
        Node temp1 = new Node(t, split.isLeaf(), Arrays.copyOfRange(split.getKeys(), 0, t / 2 - 1), root);
        Node temp2 = new Node(t, split.isLeaf(), Arrays.copyOfRange(split.getKeys(), t / 2, split.getKeyAmount() - 1), root);

        Node[] tempSmallSons = {};

        if (split.getGetSonsSize() > 0) {
            if(split.getGetSonsSize() > 1) {
                temp1.setSons(Arrays.copyOfRange(split.getSons(), 0, split.getGetSonsSize() / 2 - 1));
                temp2.setSons(Arrays.copyOfRange(split.getSons(), split.getGetSonsSize() / 2, split.getGetSonsSize() - 1));
            }
            else{
                temp1.setSons(split.getSons());
                Node[] tempEmptySons = {};
                temp2.setSons(tempEmptySons);
            }
        } else {
            temp1.setSons(tempSmallSons);
            temp2.setSons(tempSmallSons);
        }

        Node[] tempSons = new Node[root.getGetSonsSize()];
        for (int q = 0; q < root.getGetSonsSize() - 1; q++) {
            if (q == i + 1) {
                ++q;
            }
            tempSons[q] = root.getSons()[q];
        }
        tempSons[i] = temp1;
        if (tempSons.length > 1) {
            tempSons[i + 1] = temp2;
        }
//        else{
////            tempSon
//        }

        root.setSons(tempSons);

        int[] tempKeys = new int[root.getKeyAmount() + 1];

        for (int j = 0; j < root.getKeyAmount() - 1; j++) {
            tempKeys[j] = root.getKeys()[j];
        }
        tempKeys[root.getKeyAmount()] = split.getKeys()[t / 2];

        root.setKeys(tempKeys);
    }

    //-------------------------------------------------------------------------------------------
    public void insertNotFull(Node root, int key) {
        if (root == null) {
            System.out.println("Root jest nullem");
            return;
        }
        else if(root.getKeyAmount() == 2 * root.getMaxKeys() - 1){
            System.out.println("Użyłeś złej metody; drzewo pełne");
            return;
        }
        int i = root.getKeyAmount();

//        int[] tempKeys = new int[i + 1];
//        for(int j = 0; j < i; j++){
//            tempKeys[j] = root.getKeys()[j];
//        }
//        tempKeys[i] = key;
//        root.setKeys(tempKeys);

        if (root.isLeaf()) {
//            root.setKeys();
            int[] temp = new int[i + 1];
            while (i >= 1 && key < root.getKeys()[i - 1]) {
                //watch out for cutting edges
                temp[i] = root.getKeys()[i - 1];
                --i;
            }
            temp[i] = key;
            i--;
            while (i >= 0) {
                temp[i] = root.getKeys()[i];
                i--;
            }
            root.setKeys(temp);
        } else {
            while (i >= 0 && root.getKeyAmount() - 1 > i && key < root.getKeys()[i]) {
//            while (i >= 1 && key < root.getKeys()[i]) {
                i--;
            }
//            i++;
            if (root.getSon(i - 1).getKeyAmount() == 2 * root.getMaxKeys() - 1) {
                splitSon(root, i - 1, root.getSon(i - 1));
                if (key > root.getKeys()[i - 1])
                    i++;
            }
            insertNotFull(root.getSon(i - 1), key);
        }
    }


    //-------------------------------------------------------------------------------------------
    public Node insert(Node root, int key) {
        int[] tempEmpty = new int[0];
        int currKeys = root.getKeyAmount();
        int maxKeys = root.getMaxKeys();
        if (currKeys == 2 * maxKeys - 1) {
//            if (root.isLeaf())
//                root.changeLeaf();
            Node newRoot = new Node(root.getMaxKeys(), false, tempEmpty, null);
            Node[] tempSons = {root};
            newRoot.setSons(tempSons);
            splitSon(newRoot, 0, root);
            insertNotFull(newRoot, key);
            root.setParent(newRoot);
            return newRoot;
        } else {
            insertNotFull(root, key);
            return root;
        }
    }

    //-------------------------------------------------------------------------------------------
    public void printArr(Node root, int indentaion) {
        String newline = System.getProperty("line.separator"); //kod działa na windowsie i linuxie
        int sonsSize = root.getGetSonsSize();
        int keysSize = root.getKeyAmount();
        if (!root.isLeaf()) {
            for (int x = 0; x < sonsSize / 2; x++) {
                printArr(root.getSon(x), indentaion + 4);
            }
        }
        for (int i = 0; i < keysSize / 2; i++) {
            for (int j = 0; j < indentaion; j++)
                System.out.print(" ");
            System.out.print(root.getKeys()[i] + newline);
        }
    }

}