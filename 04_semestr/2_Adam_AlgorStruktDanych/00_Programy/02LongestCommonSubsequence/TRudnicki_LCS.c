#include <stdio.h>
#include <stdlib.h>

typedef struct countLCS
{
    int counter;
    char pointer;
} count_LCS;

count_LCS **createTable(int, int);
void printTable(count_LCS **, int, int);
void fillTable(count_LCS **, int, int);
int findLength(char *);
void compareStrings(count_LCS **, int, int, char *, char *);
void giveResults(count_LCS **, int, int, char *);
void freeTable(count_LCS **, int);

void test(char *, char *);

//==================================

void main()
{
    test("kwzzzrzeslo", "ytykrzabcespuilo");
    test("alf", "");
    test("", "a");
    test("", "");
    test("kwiatek", "a");
    test("arf", "bef");
}

//----------------------------------------------

count_LCS **createTable(int height, int width)
{
    count_LCS **dataTable = malloc(sizeof(count_LCS *) * (height));
    for (int y = 0; y < height; y++)
        dataTable[y] = malloc(sizeof(count_LCS) * (width));
    return dataTable;
}
//----------------------------------------------

void fillTable(count_LCS **dataTable, int height, int width)
{
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            dataTable[y][x].counter = 0;
        }
    }
}

//----------------------------------------------

void printTable(count_LCS **dataTable, int height, int width)
{
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            printf("|%c %d\t", dataTable[y][x].pointer, dataTable[y][x].counter);
        }
        printf("\n");
    }
}

//----------------------------------------------

int findLength(char *word)
{
    int size = 0;
    while (word[size] != 0)
        size++;
    return size;
}

//----------------------------------------------

void compareStrings(count_LCS **dataTable, int width, int height, char *firstWord, char *secWord)
{ //pierwsze słowo poziomo, drugie pionowo
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            if (firstWord[x] == secWord[y])
            {
                dataTable[y + 1][x + 1].counter = (dataTable[y][x].counter + 1);
                dataTable[y + 1][x + 1].pointer = '\\';
            }
            else if (dataTable[y + 1][x].counter < dataTable[y][x + 1].counter)
            {
                dataTable[y + 1][x + 1].counter = dataTable[y][x + 1].counter;
                dataTable[y + 1][x + 1].pointer = '|';
            }
            else
            {
                dataTable[y + 1][x + 1].counter = dataTable[y + 1][x].counter;
                dataTable[y + 1][x + 1].pointer = '-';
            }
        }
    }
}

//----------------------------------------------

void giveResults(count_LCS **dataTable, int height, int width, char *horizonWord)
{
    int x = width - 1;
    int y = height - 1;
    while (dataTable[y][x].counter != 0)
    {
        if (dataTable[y][x].pointer == '\\')
        {
            printf("%c", horizonWord[x-1]);
            x--;
            y--;
        }
        else if (dataTable[y][x].pointer == '|')
        {
            y--;
        }
        else
            x--;
    }
    printf("\n\n");
}

//----------------------------------------------

void test(char *string1, char *string2) {

    int firstLength = findLength(string1);
    int secLength = findLength(string2);
    int width = firstLength + 1;
    int height = secLength + 1;

    count_LCS **dataTable = createTable(height, width);
    fillTable(dataTable, height, width);
    printf("\n\n");
    compareStrings(dataTable, firstLength, secLength, string1, string2);
    printTable(dataTable, height, width);
    giveResults(dataTable, height, width, string1);
    freeTable(dataTable, height);
}

//----------------------------------------------

void freeTable(count_LCS **dataTable, int height)
{
    for (int i = 0; i < height; i++)
    {
        free(dataTable[i]);
    }
    free(dataTable);
}