#include <stdio.h>
#include <stdlib.h>

typedef struct arrCell
{
    int value;
    char direction;
} arr_Cell;

arr_Cell **createTable(int length, int width);
void printArr(arr_Cell **arrPrinted, int length, int width);
int findLength(char *word);
void LCSFinding(arr_Cell **arrLCS, int lengthA, int lengthB, char *wordA, char *wordB);
void printLCS(arr_Cell **arrLCS, int length, int width, char *word);
void freeTable(arr_Cell **arrLCS, int size);
void printString(char *word, int length);

void test(char *firstWord, char *secondWord);

//==================================

void main()
{
    test("", "");
    test("temporary", "test");
    test("kwiatek", "a");
    test("arf", "bef");
}

//----------------------------------------------
arr_Cell **createTable(int height, int width)
{
    arr_Cell **dataTable = malloc(sizeof(arr_Cell *) * (height));
    for (int y = 0; y < height; y++)
        dataTable[y] = malloc(sizeof(arr_Cell) * (width));
    return dataTable;
}

//----------------------------------------------
void printArr(arr_Cell **dataTable, int height, int width)
{
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
            printf("|%c %d", dataTable[y][x].direction, dataTable[y][x].value);
        printf("\n");
    }
}

//----------------------------------------------

int findLength(char *word)
{
    int size = 0;
    while (word[size] != 0)
        size++;
    return size;
}

//----------------------------------------------

void LCSFinding(arr_Cell **dataTable, int width, int height, char *firstWord, char *secWord)
{ //pierwsze słowo poziomo, drugie pionowo
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            if (firstWord[x] == secWord[y])
            {
                dataTable[y + 1][x + 1].value = (dataTable[y][x].value + 1);
                dataTable[y + 1][x + 1].direction = '\\';
            }
            else if (dataTable[y + 1][x].value < dataTable[y][x + 1].value)
            {
                dataTable[y + 1][x + 1].value = dataTable[y][x + 1].value;
                dataTable[y + 1][x + 1].direction = '|';
            }
            else
            {
                dataTable[y + 1][x + 1].value = dataTable[y + 1][x].value;
                dataTable[y + 1][x + 1].direction = '-';
            }
        }
    }
}

//----------------------------------------------

void printLCS(arr_Cell **dataTable, int height, int width, char *horizonWord)
{
    int x = width - 1;
    int y = height - 1;
    while (dataTable[y][x].value != 0)
    {
        if (dataTable[y][x].direction == '\\')
        {
            printf("%c", horizonWord[x - 1]);
            x--;
            y--;
        }
        else if (dataTable[y][x].direction == '|')
            y--;
        else
            x--;
    }
    printf("\n\n");
}

//----------------------------------------------

void test(char *string1, char *string2)
{

    int firstLength = findLength(string1);
    int secLength = findLength(string2);
    int width = firstLength + 1;
    int height = secLength + 1;

    printf("\n\n");
    printf("first word: ");
    printString(string1, firstLength);
    printf("second word: ");
    printString(string2, secLength);

    arr_Cell **dataTable = createTable(height, width);
    LCSFinding(dataTable, firstLength, secLength, string1, string2);
    printArr(dataTable, height, width);
    printLCS(dataTable, height, width, string1);
    freeTable(dataTable, height);
}

//----------------------------------------------
void printString(char *word, int length)
{
    for (int i = 0; i < length; i++)
    {
        printf("%c", word[i]);
    }
    printf("\n");
}

//----------------------------------------------

void freeTable(arr_Cell **dataTable, int height)
{
    for (int i = 0; i < height; i++)
    {
        free(dataTable[i]);
    }
    free(dataTable);
}