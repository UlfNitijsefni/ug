#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

// #define LEFT -3

typedef struct arrCell
{
    int value;
    char direction;
} arr_Cell;

void printString(char *toPrint, int length);
void LCSnumbersArrGenerator(arr_Cell **algArr, char *wordA, char *wordB, int lengthA, int lengthB);
int findStringLength(char *toCount);
void print2DArr(arr_Cell **arr, int width, int length);
void printLCS(arr_Cell **algArr, char *word, int x, int y);
arr_Cell **createTable(int heigth, int width);
//============================================================================================
int main()
{
    char *word1 = "abc";
    char *word2 = "dcabcz"; //pionowe
    int length1 = findStringLength(word1);
    int length2 = findStringLength(word2);

    arr_Cell **testArr = createTable(length1 + 1, length2 + 1);

    LCSnumbersArrGenerator(testArr, word1, word2, length1, length2);
    printLCS(testArr, word2, length2 + 1, length1 + 1);
    printf("//--------------------------\n");
    print2DArr(testArr, length1 + 1, length2 + 1);

    // for(int i = 0; i < l)
}

//--------------------------------------------------------------------------------------------
void print2DArr(arr_Cell **printedArr, int heigth, int width)
{
    for (int y = 0; y < heigth; y++)
    {
        for (int x = 0; x < width; x++)
        {
            printf("| %d", printedArr[y][x].value);
        }

        printf("\t\t");

        for (int x = 0; x < heigth; x++)
        {
            printf(" %c", printedArr[x][y].direction);
        }
        printf("\n");
    }
}

//--------------------------------------------------------------------------------------------
void LCSnumbersArrGenerator(arr_Cell **algArr, char *wordA, char *wordB, int width, int height)
{
    for (int y = 0; y < height; y++)
        for (int x = 0; x < width; x++)
        {
            if (wordA[x] == wordB[y])
            {
                algArr[y + 1][x + 1].direction = '\\';
                algArr[y + 1][x + 1].value = algArr[y][x].value + 1;
            }
            else if (algArr[y + 1][x].value < algArr[y][x + 1].value)
            {
                algArr[y + 1][x + 1].direction = '|';
                algArr[y + 1][x + 1].value = algArr[y][x + 1].value;
            }
            else
            {
                algArr[y + 1][x + 1].direction = '-';
                algArr[y + 1][x + 1].value = algArr[y + 1][x].value;
            }
        }
}

//--------------------------------------------------------------------------------------------
void printLCS(arr_Cell **algArr, char *word, int height, int width)
{
    int x = width - 1;
    int y = height - 1;
    while (algArr[y][x].value != 0)
    {
        if (algArr[y][x].direction == '\\')
        {
            printf("%c ", word[x - 1]);
            x--;
            y--;
        }
        else if (algArr[y][x].direction == '|')
        {
            y--;
        }
        else
            x--;
    }
    printf("\n\n");
    // if (height == 0 || width == 0)
    // {
    //     printf("%c |", word[0]);
    //     return;
    // }
    // else if (algArr[height][width].direction == '\\')
    // {
    //     printLCS(algArr, word, height - 1, width - 1);
    //     printf("%c |", word[height]);
    // }
    // else if (algArr[height][width].direction == '-')
    // {
    //     printLCS(algArr, word, height - 1, width);
    //     // printf("%c", algArr[height][width].value);
    // }
    // else
    // {
    //     printLCS(algArr, word, height, width - 1);
    // }
}

//--------------------------------------------------------------------------------------------
void printString(char *toPrint, int length)
{
    for (int i = 0; i < length; i++)
    {
        printf("%c", toPrint[i]);
    }

    printf("\n");
}

//--------------------------------------------------------------------------------------------
int findStringLength(char *toCount)
{
    int x = 0;
    while (toCount[x] != 0)
    {
        x++;
    }
    return x;
}

//--------------------------------------------------------------------------------------------
arr_Cell **createTable(int heigth, int width)
{
    arr_Cell **testArr2D = malloc(sizeof(arr_Cell *) * heigth + 1);
    for (int i = 0; i < width + 1; i++)
        testArr2D[i] = malloc(sizeof(arr_Cell) * width + 1);
    return testArr2D;
}