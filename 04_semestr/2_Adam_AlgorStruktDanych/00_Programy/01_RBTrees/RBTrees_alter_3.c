#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#define RED 1
#define BLACK 0
#define RIGHT -2
#define LEFT -3

typedef struct treeNode
{
    int color;
    int value;
    struct treeNode *parent;
    struct treeNode *left;
    struct treeNode *right;
} tree_node;

void restoreRedBlack(struct treeNode *node);
void insert(tree_node *tree, int value);
int getSide(struct treeNode *node);
void moveUpRED(tree_node *node, tree_node *uncle);
void putInLine(struct treeNode *node);
void rotation(struct treeNode *node, int);
tree_node *getRoot(tree_node *searchStart);

void printTree(tree_node *treePrinted);
tree_node *newNode(tree_node *root, int value);

void test(int[], int n);

//============================================================================================
int main()
{
    int arr_3[] = {1, 2, 3};
    test(arr_3, 3);

    int arr_4[] = {1, 2, 2, 5};
    test(arr_4, 4);

    int arr_5[] = {8, 3, 3, 5, 1};
    test(arr_5, 5);

    int arr_6[] = {9, 2, 8, 2, 7, 2};
    test(arr_6, 6);

    int arr_7[] = {3, 4, 5, 6, 5, 4, 3};
    test(arr_7, 7);
}

//--------------------------------------------------------------------------------------------
void restoreRedBlack(struct treeNode *node)
{
    tree_node *uncle;

    while (node->parent->color == RED)
    {
        int parentSide = getSide(node->parent);
        int nodeSide = getSide(node);

        if (parentSide == LEFT)
        {
            uncle = node->parent->parent->right;

            // przypadek 1
            if (uncle->color == RED)
            {
                moveUpRED(node->parent, uncle);
                node = node->parent->parent;
            }

            else
            {
                // przypadek 2
                if (nodeSide == RIGHT)
                {
                    node = node->parent;
                    rotation(node, LEFT);
                }

                //przypadek 3
                node = node->parent->parent;
                rotation(node, RIGHT);
            }
        }
        else
        {
            uncle = node->parent->parent->left;

            // przypadek 1
            if (uncle->color == RED)
            {
                moveUpRED(node->parent, uncle);
                node = node->parent->parent;
            }

            else
            {
                // przypadek 2
                if (nodeSide == LEFT)
                {
                    node = node->parent;
                    rotation(node, RIGHT);
                }

                //przypadek 3
                node = node->parent->parent;
                rotation(node, LEFT);
            }
        }
        getRoot(node)->color = BLACK;
    }
}

//--------------------------------------------------------------------------------------------
void insert(tree_node *root, int value)
{
    if (root->value > value)
    {
        if (root->left == NULL)
            root->left = newNode(root, value);
        else
            insert(root->left, value);
    }
    else
    {
        if (root->right == NULL)
            root->right = newNode(root, value);
        else
            insert(root->right, value);
    }
    restoreRedBlack(root);
}

//--------------------------------------------------------------------------------------------
void moveUpRED(tree_node *parent, tree_node *uncle)
{
    parent->color == BLACK;
    uncle->color == BLACK;
    parent->parent->color == RED;
    // getRoot(parent)->color = BLACK;
}

//--------------------------------------------------------------------------------------------
void putInLine(struct treeNode *node)
{
    int parentSide = getSide(node->parent);
    int nodeSide = getSide(node);

    // struct treeNode *forSwaps = node->parent;

    if (nodeSide == RIGHT)
        node->parent->right = node->left;
    else
        node->parent->left = node->right;

    if (nodeSide == RIGHT)
        node->left = node->parent;
    else
        node->right = node->parent;

    node->parent = node->parent->parent;

    if (parentSide == RIGHT)
        node->parent->right = node;
    else
        node->parent->left = node;

    getRoot(node)->color = BLACK;
}

//--------------------------------------------------------------------------------------------
int getSide(struct treeNode *node)
{
    if (node == NULL || node->parent->left == node)
        return LEFT;
    else
        return RIGHT;
}

//--------------------------------------------------------------------------------------------
void rotation(struct treeNode *node, int direction)
{
    tree_node *B, *C, *D, *P = NULL, *N = NULL;

    if (direction == RIGHT)
    {
        C = node;
        B = C->left;
        D = C->right;
        P = C->parent;

        if (B != NULL)
        {
            N = B->right;

            B->parent = P;
            if (P != NULL)
            {
                if (C == P->left)
                    P->left = B;
                else
                    P->right = B;
            }
        }

        C->left = N;
        if (N != NULL)
            N->parent = C;

        if (B != NULL)
            B->right = C;
        C->parent = B;
    }
    else
    {
        C = node;
        B = C->right;
        D = C->left;
        P = C->parent;
        if (B != NULL)
        {
            N = B->left;

            B->parent = P;
            if (P != NULL)
            {
                if (C == P->right)
                    P->right = B;
                else
                    P->left = B;
            }
        }

        C->right = N;
        if (N != NULL)
            N->parent = C;

        if (B != NULL)
            B->left = C;
        C->parent = B;
    }
    getRoot(node)->color = BLACK;
}

//--------------------------------------------------------------------------------------------
void test(int valueArr[], int n)
{
    tree_node *tree = (tree_node *)malloc(sizeof(tree_node));
    tree = newNode(NULL, valueArr[0]);
    tree->color = BLACK;

    for (int i = 1; i < n; i++)
        insert(tree, valueArr[i]);

    printf("\n\n-----------------------------------\n");
    printTree(tree);
}

//--------------------------------------------------------------------------------------------
void printTree(tree_node *treePrinted)
{
    if (treePrinted->left != NULL)
    {
        printTree(treePrinted->left);
    }
    if (treePrinted->right != NULL)
    {
        printTree(treePrinted->right);
    }
    if (treePrinted->color == BLACK)
        printf("%d  BLACK\n", treePrinted->value);
    else
        printf("%d  RED\n", treePrinted->value);
}

//--------------------------------------------------------------------------------------------
tree_node *newNode(tree_node *parent, int value)
{
    tree_node *newNode = (tree_node *)malloc(sizeof(tree_node));

    newNode->value = value;
    newNode->color = RED;
    newNode->parent = parent;
    newNode->left = NULL;
    newNode->right = NULL;

    return newNode;
}

//--------------------------------------------------------------------------------------------
tree_node *getRoot(tree_node *searchStart)
{
    while (searchStart->parent != NULL)
        searchStart = searchStart->parent;
    return searchStart;
}