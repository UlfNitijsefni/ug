#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define RED 1
#define BLACK 0
#define RIGHT -2
#define LEFT -3

typedef struct treeNode
{
    int color;
    int value;
    struct treeNode *parent;
    struct treeNode *left;
    struct treeNode *right;
} tree_node;

void insert(tree_node *tree, int value);
int getSide(struct treeNode *node);
void printTree(tree_node *treePrinted);
void checkInsertion(struct treeNode *node);
void moveUpRED(tree_node *node, tree_node *uncle);
void putInLine(struct treeNode *node);
void rotateRIGHT(struct treeNode *node);
void rotateLEFT(struct treeNode *node);
void test(int[], int n);

tree_node *newNode(tree_node *root, int value);

//============================================================================================
int main()
{
    int arr_01[] = {1, 2, 3};
    test(arr_01, 3);
}

//--------------------------------------------------------------------------------------------
void insert(tree_node *root, int value)
{
    if (root->value > value)
    {
        if (root->left == NULL)
            root->left = newNode(root, value);
        else
            insert(root->left, value);
    }
    else
    {
        if (root->right == NULL)
            root->right = newNode(root, value);
        else
            insert(root->right, value);
    }
}
//--------------------------------------------------------------------------------------------
tree_node *newNode(tree_node *parent, int value)
{
    tree_node *newNode = (tree_node *)malloc(sizeof(tree_node));

    newNode->value = value;
    newNode->color = RED;
    newNode->parent = parent;
    newNode->left = NULL;
    newNode->right = NULL;

    return newNode;
}
//--------------------------------------------------------------------------------------------
void printTree(tree_node *treePrinted)
{
    if (treePrinted->left != NULL)
    {
        printTree(treePrinted->left);
    }
    if (treePrinted->right != NULL)
    {
        printTree(treePrinted->right);
    }
    if (treePrinted->value != -1)
    {
        if (treePrinted->color == BLACK)
            printf("%d  BLACK\n", treePrinted->value);
        else
            printf("%d  RED\n", treePrinted->value);
    }
}

//--------------------------------------------------------------------------------------------
void checkInsertion(struct treeNode *node)
{
    tree_node *uncle;

    while (node->parent->color == RED)
    {
        //Finds uncle
        if (node->parent->parent->left == node->parent)
            uncle = node->parent->parent->right;
        else
            uncle = node->parent->parent->left;

        int parentSide = getSide(node->parent);
        int nodeSide = getSide(node);

        if (uncle->color == RED)
        {
            moveUpRED(node, uncle);
            node = node->parent->parent;
        }

        if (parentSide != nodeSide)
        {
            putInLine(node);

            if (getSide(node) == LEFT)
                rotateRIGHT(node);
            else
                rotateLEFT(node);
        }
    }

    // if (node->parent->color == BLACK)
    //     return;

    // struct treeNode *uncle;

    // //Checks if element "node" is the root
    // if (node->parent == NULL)
    //     return;

    // if (node->parent->color == RED && uncle->color == RED)
    // {
    //     changeParentUncleColor(node, uncle);
    // }
    // else if (getDirection(node) != getDirection(node->parent))
    // {
    //     putInLine(node);
    // }
}
//--------------------------------------------------------------------------------------------
void moveUpRED(tree_node *node, tree_node *uncle)
{
    node->parent->color == BLACK;
    uncle->color == BLACK;
    node->parent->parent->color == RED;
}

//--------------------------------------------------------------------------------------------
void putInLine(struct treeNode *node)
{
    int parentSide = getSide(node->parent);
    int nodeSide = getSide(node);

    // struct treeNode *forSwaps = node->parent;

    if (nodeSide == RIGHT)
        node->parent->right = node->left;
    else
        node->parent->left = node->right;

    if (nodeSide == RIGHT)
        node->left = node->parent;
    else
        node->right = node->parent;

    node->parent = node->parent->parent;

    if (parentSide == RIGHT)
        node->parent->right = node;
    else
        node->parent->left = node;
}

//--------------------------------------------------------------------------------------------
int getSide(struct treeNode *node)
{
    if (node == NULL || node->parent->left == node)
        return LEFT;
    else
        return RIGHT;
}

//--------------------------------------------------------------------------------------------
void rotateRIGHT(struct treeNode *node)
{
    struct treeNode *temp;

    node->parent->parent->left = node->parent->right;
    node->parent->parent->color = RED;
    temp = node->parent->parent->parent;
    node->parent->parent = node->parent;
    node->parent->right = node->parent->parent;
    node->parent->color = BLACK;
}

//--------------------------------------------------------------------------------------------
void rotateLEFT(struct treeNode *node)
{
    struct treeNode *temp;

    node->parent->parent->right = node->parent->left;
    node->parent->parent->color = RED;
    temp = node->parent->parent->parent;
    node->parent->parent = node->parent;
    node->parent->left = node->parent->parent;
    node->parent->color = BLACK;
}

//--------------------------------------------------------------------------------------------
void test(int valueArr[], int n)
{
    tree_node *tree = (tree_node *)malloc(sizeof(tree_node));
    tree = newNode(NULL, valueArr[0]);
    tree->color = BLACK;

    for (int i = 1; i < n; i++)
        insert(tree, valueArr[i]);
    printTree(tree);
}

//--------------------------------------------------------------------------------------------
// void delete (tree_node *root, int value)
// {
//     if (root == NULL)
//         return;
//     else if (root->value == value)
//         root->value = findBiggestInLeft(root->left, NULL);
//     else if (root->value > value)
//         delete (root->left, value);
//     else
//         delete (root->right, value);
// }

// //--------------------------------------------------------------------------------------------
// int findBiggestInLeft(tree_node *root, int currValue)
// {
//     if ((root->value) == NULL)
//     {
//         root->parent = NULL;           //Now roots parent is a NULL
//         // (tree_node)(root->parent)->right = NULL;

//         // (tree_node)(root->parent)->value = NULL;
//         // (tree_node)(root->parent)->right = NULL;
//         free(root);                                 //This frees memeory taken by root
//         return currValue;
//     }
//     else
//         return findBiggestInLeft(root->right, root->value);
// }