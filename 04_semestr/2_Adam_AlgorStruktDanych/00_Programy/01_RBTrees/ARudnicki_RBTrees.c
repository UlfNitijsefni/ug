#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#define RED 1
#define BLACK 0
#define RIGHT -2
#define LEFT -3

typedef struct treeNode
{
    int color;
    int value;
    struct treeNode *parent;
    struct treeNode *left;
    struct treeNode *right;
} tree_node;

void restoreRedBlack(struct treeNode *node);
void insert(tree_node *tree, tree_node *node);
void insertBinary(tree_node *tree, tree_node *node);
int getSide(struct treeNode *node);
void putInLine(struct treeNode *node);
void rotation(struct treeNode *node, int);
tree_node *getRoot(tree_node *node);

void printTree(tree_node *treePrinted);
tree_node *newNode(tree_node *root, int value);

void test(int[], int n);

//============================================================================================
int main()
{
    // int arr_3[] = {1, 2, 3};
    // test(arr_3, 3);

    // int arr_4[] = {1, 2, 2, 5};
    // test(arr_4, 4);

    // int arr_5[] = {8, 3, 3, 5, 1};
    // test(arr_5, 5);

    // int arr_6[] = {9, 2, 8, 2, 7, 2};
    // test(arr_6, 6);

    // int arr_7[] = {3, 4, 5, 6, 5, 4, 3};
    // test(arr_7, 7);

    // int arr_12[] = {
    //     2, 5, 2, 9,
    //     6, 3, 4, 4,
    //     4, 1, 9, 7};
    // test(arr_12, 12);

    // int arr_16[] = {
    //     3, 4, 5, 6,
    //     5, 4, 3, 5,
    //     8, 8, 1, 2,
    //     3, 1, 9, 9};
    // test(arr_16, 16);

    int arr_TEST[] = {
        5, 3, 2};
    test(arr_TEST, 3);
}

//--------------------------------------------------------------------------------------------
void restoreRedBlack(struct treeNode *node)
{
    tree_node *uncle;

    while (node->parent != NULL && node->parent->color == RED)
    {
        int parentSide = getSide(node->parent);
        int nodeSide = getSide(node);

        if (parentSide == LEFT)
        {
            uncle = node->parent->parent->right;

            // przypadek 1
            if (uncle != NULL && uncle->color == RED)
            {
                node->parent->color = BLACK;
                uncle->color = BLACK;
                node->parent->parent->color = RED;
                node = node->parent->parent;
            }

            else
            {
                // przypadek 2
                if (nodeSide == RIGHT)
                {
                    node = node->parent;
                    rotation(node, LEFT);
                }

                //przypadek 3
                node->parent->color = BLACK;
                node->parent->parent->color = RED;
                rotation(node->parent->parent, RIGHT);
            }
        }
        else
        {
            uncle = node->parent->parent->left;

            // przypadek 1
            if (uncle != NULL && uncle->color == RED)
            {
                node->parent->color = BLACK;
                uncle->color = BLACK;
                node->parent->parent->color = RED;
                node = node->parent->parent;
            }

            else
            {
                // przypadek 2
                if (nodeSide == LEFT)
                {
                    node = node->parent;
                    rotation(node, RIGHT);
                }

                //przypadek 3
                node->parent->color = BLACK;
                node->parent->parent->color = RED;
                rotation(node->parent->parent, LEFT);
            }
        }
    }
    getRoot(node)->color = BLACK;
}

//--------------------------------------------------------------------------------------------
void insert(tree_node *root, tree_node *node)
{
    insertBinary(root, node);
    restoreRedBlack(node);
}

//--------------------------------------------------------------------------------------------
void insertBinary(tree_node *newParent, tree_node *node)
{
    if (newParent->value > node->value)
    {
        if (newParent->left == NULL)
        {
            newParent->left = node;
            node->parent = newParent;
        }
        else
            insertBinary(newParent->left, node);
    }
    else
    {
        if (newParent->right == NULL)
        {
            newParent->right = node;
            node->parent = newParent;
        }
        else
            insertBinary(newParent->right, node);
    }
}

//--------------------------------------------------------------------------------------------
void rotation(struct treeNode *node, int direction)
{
    tree_node *B, *C, *D, *P = NULL, *N = NULL;

    if (direction == RIGHT)
    {
        C = node;
        B = C->left;
        D = C->right;
        P = C->parent;

        if (B != NULL)
        {
            N = B->right;

            B->parent = P;
            if (P != NULL)
            {
                if (C == P->left)
                    P->left = B;
                else
                    P->right = B;
            }
        }

        C->left = N;
        if (N != NULL)
            N->parent = C;

        if (B != NULL)
            B->right = C;
        C->parent = B;
    }
    else
    {
        C = node;
        B = C->right;
        D = C->left;
        P = C->parent;
        if (B != NULL)
        {
            N = B->left;

            B->parent = P;
            if (P != NULL)
            {
                if (C == P->right)
                    P->right = B;
                else
                    P->left = B;
            }
        }

        C->right = N;
        if (N != NULL)
            N->parent = C;

        if (B != NULL)
            B->left = C;
        C->parent = B;
    }
}

//--------------------------------------------------------------------------------------------
int getSide(struct treeNode *node)
{
    if (node->parent == NULL || node->parent->left == node)
        return LEFT;
    else
        return RIGHT;
}

//--------------------------------------------------------------------------------------------
tree_node *getRoot(tree_node *node)
{
    while (node->parent != NULL)
        node = node->parent;
    return node;
}

//--------------------------------------------------------------------------------------------
void test(int valueArr[], int n)
{
    tree_node *root = (tree_node *)malloc(sizeof(tree_node));
    root = newNode(NULL, valueArr[0]);
    root->color = BLACK;
    tree_node *node;

    for (int i = 1; i < n; i++)
    {
        node = newNode(root, valueArr[i]);
        insert(root, node);
        root = getRoot(root);
    }

    char *title = "TEST\n";
    printf("\n%s\n-----------------------------------\n", title);
    printTree(getRoot(root));
}

//--------------------------------------------------------------------------------------------
void printTree(tree_node *node)
{
    if (node->left != NULL)
        printTree(node->left);

    if (node->right != NULL)
        printTree(node->right);

    printf("%d  %s\n", node->value, (node->color == BLACK) ? "BLACK" : "RED");
}

//--------------------------------------------------------------------------------------------
tree_node *newNode(tree_node *parent, int value)
{
    tree_node *newNode = (tree_node *)malloc(sizeof(tree_node));

    newNode->value = value;
    newNode->color = RED;
    newNode->parent = parent;
    newNode->left = NULL;
    newNode->right = NULL;

    return newNode;
}
