#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define RED 1
#define BLACK 0
#define RIGHT -2
#define LEFT -3
#define NULL -4

typedef struct treeNode
{
    int color;
    int valueInside;
    struct treeNode *parent;
    struct treeNode *left;
    struct treeNode *right;
} tree_node;

// tree_node *NULL;

void insert(tree_node *tree, int value);
tree_node *generateEmptyNode(tree_node *root);
//============================================================================================
int main()
{

    tree_node *exampleTree = (tree_node *)malloc(sizeof(tree_node));
    exampleTree = generateEmptyNode(NULL);
    exampleTree->color = BLACK;

    insert(exampleTree, 1);
    insert(exampleTree, 2);
    insert(exampleTree, 2);
    insert(exampleTree, 1);
    insert(exampleTree, 2156);
    insert(exampleTree, 12);
    insert(exampleTree, 8);

    printTree(exampleTree);
}

//--------------------------------------------------------------------------------------------
void insert(tree_node *root, int value)
{
    // tree_node *currLocation = root;

    if (root->valueInside == -1)
    {
        root->valueInside = value;
        root->left = generateEmptyNode(root);
        root->right = generateEmptyNode(root);
        checkInsertion(root);
        // #define NULL -4;
    }
    else if (root->valueInside > value)
    {
        insert(root->left, value);
    }
    else
        insert(root->right, value);
}

//--------------------------------------------------------------------------------------------
tree_node *generateEmptyNode(tree_node *parent)
{
    tree_node *exampleTree = (tree_node *)malloc(sizeof(tree_node));

    exampleTree->valueInside = -1;
    exampleTree->color = RED;
    exampleTree->parent = parent;
    exampleTree->left = NULL;
    exampleTree->right = NULL;

    return exampleTree;
}
//--------------------------------------------------------------------------------------------
void printTree(tree_node *treePrinted)
{
    if (treePrinted->left != NULL)
    {
        printTree(treePrinted->left);
    }
    if (treePrinted->right != NULL)
    {
        printTree(treePrinted->right);
    }
    if (treePrinted->valueInside != -1 && treePrinted->valueInside != NULL)
    {
        if (treePrinted->color == BLACK)
            printf("%d  BLACK\n", treePrinted->valueInside);
        else
            printf("%d  RED\n", treePrinted->valueInside);
    }
}

//--------------------------------------------------------------------------------------------
void checkInsertion(struct treeNode *inserted)
{
    tree_node *uncle;
    tree_node *currentlyProcessed = inserted;

    while (currentlyProcessed->parent->color != BLACK)
    {
        //Finds uncle
        if (currentlyProcessed->parent->parent->left == currentlyProcessed->parent)
            uncle = currentlyProcessed->parent->parent->right;
        else
            uncle = currentlyProcessed->parent->parent->left;

        int parentDirection = getDirection(currentlyProcessed->parent);
        int insertedDirection = getDirection(currentlyProcessed);

        if (uncle->color == currentlyProcessed->parent->color)
        {
            changeParentUncleColor(currentlyProcessed, uncle);
            currentlyProcessed = currentlyProcessed->parent->parent;
        }

        if (parentDirection != insertedDirection)
        {
            putInLine(currentlyProcessed);

            if (getDirection(currentlyProcessed) == LEFT)
                rotateRIGHT(currentlyProcessed);
            else
                rotateLEFT(currentlyProcessed);
        }
    }

    // if (inserted->parent->color == BLACK)
    //     return;

    // struct treeNode *uncle;

    // //Checks if element "inserted" is the root
    // if (inserted->parent == NULL)
    //     return;

    // if (inserted->parent->color == RED && uncle->color == RED)
    // {
    //     changeParentUncleColor(inserted, uncle);
    // }
    // else if (getDirection(inserted) != getDirection(inserted->parent))
    // {
    //     putInLine(inserted);
    // }
}
//--------------------------------------------------------------------------------------------
void changeParentUncleColor(struct treeNode *inserted, struct treeNode *uncle)
{
    inserted->parent->color == BLACK;
    uncle->color == BLACK;
    inserted->parent->parent->color == RED;
}

//--------------------------------------------------------------------------------------------
void putInLine(struct treeNode *inserted)
{
    int parentDirection = getDirection(inserted->parent);
    int insertedDirection = getDirection(inserted);

    // struct treeNode *forSwaps = inserted->parent;

    if (insertedDirection == RIGHT)
        inserted->parent->right = inserted->left;
    else
        inserted->parent->left = inserted->right;

    if (insertedDirection == RIGHT)
        inserted->left = inserted->parent;
    else
        inserted->right = inserted->parent;

    inserted->parent = inserted->parent->parent;

    if (parentDirection == RIGHT)
        inserted->parent->right = inserted;
    else
        inserted->parent->left = inserted;
}

//--------------------------------------------------------------------------------------------
int getDirection(struct treeNode *inserted)
{
    if (inserted == NULL || inserted->parent->left == inserted)
        return LEFT;
    else
        return RIGHT;
}

//--------------------------------------------------------------------------------------------
rotateRIGHT(struct treeNode *inserted)
{
    struct treeNode *temp;

    inserted->parent->parent->left = inserted->parent->right;
    inserted->parent->parent->color = RED;
    temp = inserted->parent->parent->parent;
    inserted->parent->parent = inserted->parent;
    inserted->parent->right = inserted->parent->parent;
    inserted->parent->color = BLACK;
}

//--------------------------------------------------------------------------------------------
rotateLEFT(struct treeNode *inserted)
{
    struct treeNode *temp;

    inserted->parent->parent->right = inserted->parent->left;
    inserted->parent->parent->color = RED;
    temp = inserted->parent->parent->parent;
    inserted->parent->parent = inserted->parent;
    inserted->parent->left = inserted->parent->parent;
    inserted->parent->color = BLACK;
}

//--------------------------------------------------------------------------------------------
// void delete (tree_node *root, int value)
// {
//     if (root == NULL)
//         return;
//     else if (root->valueInside == value)
//         root->valueInside = findBiggestInLeft(root->left, NULL);
//     else if (root->valueInside > value)
//         delete (root->left, value);
//     else
//         delete (root->right, value);
// }

// //--------------------------------------------------------------------------------------------
// int findBiggestInLeft(tree_node *root, int currValue)
// {
//     if ((root->valueInside) == NULL)
//     {
//         root->parent = NULL;           //Now roots parent is a NULL
//         // (tree_node)(root->parent)->right = NULL;

//         // (tree_node)(root->parent)->valueInside = NULL;
//         // (tree_node)(root->parent)->right = NULL;
//         free(root);                                 //This frees memeory taken by root
//         return currValue;
//     }
//     else
//         return findBiggestInLeft(root->right, root->valueInside);
// }