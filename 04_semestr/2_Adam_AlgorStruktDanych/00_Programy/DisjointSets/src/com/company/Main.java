package com.company;

import ops.Operations;
import ops.SetElement;

public class Main {

    static Operations operations = new Operations();

    public static void main(String[] args) {

        SetElement[] sets = new SetElement[10];

        sets[0] = operations.makeSet(1);
        sets[1] = operations.makeSet(2);
        sets[2] = operations.makeSet(3);
        sets[3] = operations.makeSet(4);
        sets[4] = operations.makeSet(5);
        sets[5] = operations.makeSet(6);
        sets[6] = operations.makeSet(7);
        sets[7] = operations.makeSet(8);
        sets[8] = operations.makeSet(9);

        operations.union(operations.findSet(sets[0]), operations.findSet(sets[1]));
        operations.union(operations.findSet(sets[2]), operations.findSet(sets[3]));
        operations.union(operations.findSet(sets[1]), operations.findSet(sets[2]));
        operations.union(operations.findSet(sets[5]), operations.findSet(sets[6]));
        operations.union(operations.findSet(sets[7]), operations.findSet(sets[8]));
        operations.union(operations.findSet(sets[3]), operations.findSet(sets[5]));
        operations.union(operations.findSet(sets[0]), operations.findSet(sets[7]));

        for(int i = 0; i < 9; i++){
            operations.printToRoot(sets[i]);
        }

    }
}
