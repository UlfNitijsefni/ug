package ops;

public class Edge {
    private SetElement start;
    private SetElement end;
    private int cost;

    public SetElement getStart(){
        return this.start;
    }

    public SetElement getEnd(){
        return this.end;
    }

    public int getCost(){
        return this.cost;
    }
}
