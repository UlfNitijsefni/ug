package ops;

public class Operations {

    //    SetElement temp = new SetElement();
    public SetElement makeSet(int key) {
        SetElement temp = new SetElement();
        temp.setKey(key);
        temp.setParent(temp);
        temp.setRank(0);
        return temp;
    }


    public SetElement findSet(SetElement searched) {
        if (searched != searched.getParent())
            searched.setParent(findSet(searched.getParent()));
        return searched.getParent();
    }


    public void union(SetElement firstSet, SetElement secondSet) {

        if (firstSet.getRank() > secondSet.getRank())
            firstSet.setParent(secondSet);
        else {
            secondSet.setParent(firstSet);
            if (firstSet.getRank() == secondSet.getRank())
                secondSet.setRank(secondSet.getRank() + 1);
        }
    }

    public void printToRoot(SetElement printed) {
        while (printed.getParent() != printed) {
            System.out.print(printed.getKey() + " -> ");
            printed = printed.getParent();
        }
        System.out.print(printed.getKey());
        System.out.println("");
    }

    //    public void Kruskal(int[] keys, ){
//    public void Kruskal(int nodeAmount, int edgeAmount, Edge[] edges, int[] keys) {
//        SetElement[] allSets = new SetElement[nodeAmount];
//        for (int i = 0; i < nodeAmount - 1; i++) {
//            allSets[i] = makeSet(keys[i]);
//        }
//
//        sortEdges(edges);
//        for (int i = 0; i < edgeAmount - 1; i++) {
//            SetElement tempStart = findSet(allSets[edges[i].getStart().getKey()]);
//            SetElement tempEnd = findSet(allSets[edges[i].getEnd().getKey()]);
//            if(tempEnd != tempStart){
//                System.out.println("Nowa krawedz: " + tempStart.getKey() + " -> " + tempEnd.getKey());
//            }
//        }
//    }
//
//    public Edge[] makeEdges(SetElement[] nodes){
//        Edge[] edges = new Edge[nodes.length*nodes.length];
//        for(int i = 0; i < nodes.length - 1; i++){
//            for(int j = 0; j < nodes.length; j++){
//                edges
//            }
//        }
//        return  edges;
//    }

    public void sortEdges(Edge[] edges) {
        for (int i = 0; i < edges.length - 1; i++) {
            for (int j = 0; j < edges.length - 1; j++) {
                if (edges[i].getCost() > edges[i + 1].getCost()) {
                    Edge temp = edges[i];
                    edges[i] = edges[i + 1];
                    edges[i + 1] = temp;
                }
            }
        }
    }
}