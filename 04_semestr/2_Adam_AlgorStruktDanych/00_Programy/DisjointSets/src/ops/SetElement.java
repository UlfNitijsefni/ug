package ops;

public class SetElement {
    private int key;
    private SetElement parent;
    private int rank;


    public SetElement getParent() {
        return parent;
    }

    public void setParent(SetElement parent) {
        this.parent = parent;
    }

    public int getRank() {
        return this.rank;
    }

    public int getKey() {
        return this.key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public void setRank(int newRank) {
        this.rank = newRank;
    }
}
