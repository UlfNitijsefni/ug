package Ops;

public class Ops {
    public void DumbAlgorithm(String lookedFor, String searchedString) {
        int appearances = 1;
        for (int i = 0; i < searchedString.length() - lookedFor.length(); i++) {
            if (lookedFor.equals(searchedString.substring(i, i + lookedFor.length()))) {
                System.out.println("Wystąpnienie " + appearances + " jest na indeksie: " + i);
                appearances++;
            }
        }
    }

//    public void Rabin_Karp(String lookedFor, String searchedString, int alphabetSize, int hashNumber){
//        int hash = 1;
//        for(int i = 0; i < searchedString.length() - 1; i++){
//            hash = (hash*alphabetSize)%hashNumber;
//        }
//
//        int lookedForHash = 0;
//        int searchedHash = 0;
//
//        for(int i = 0; i < searchedString.length(); i++){
//            lookedForHash = (alphabetSize*lookedForHash + (int)lookedFor.charAt(i))%hashNumber;
//            searchedHash = (alphabetSize*searchedHash + (int)searchedString.charAt(i))%hashNumber;
//        }
//    }

    public void Knuth_Morris_Pratt(String lookedFor, String searchedString) {
        int[] pi = prefixFunction(searchedString);
        int q = 0;

        for(int i = 0; i < lookedFor.length(); i++){
            while(q>0 && searchedString.charAt(q+1) != lookedFor.charAt(i)){
                q = pi[q];
            }
            if(searchedString.charAt(q+1) == lookedFor.charAt(i)){
                q++;
            }
            if(q == searchedString.length()){
                System.out.println("Wzorzec znaleziony z poczatkiem na pozycji " + (i - searchedString.length() - 1));
                q = pi[q];
            }
        }
    }

    public int[] prefixFunction(String provided) {
        int[] prefixes = new int[provided.length()];
        prefixes[0] = 0;
        int k = 0;

        for(int i = 1; i < provided.length() - 1; i++){
            while (k>0 && provided.charAt(k+1) != provided.charAt(i)){
                k = prefixes[k];
            }

            if(provided.charAt(k+1) == provided.charAt(i)){
                k++;
            }
            prefixes[i] = k;
        }
        return prefixes;
    }
}