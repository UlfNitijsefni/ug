public class SetElement {
    private int key;
    private SetElement next;
    private SetElement head;
    private SetElement last;
    private int length;



    public boolean isLast(){
        return(this.next == null);
    }

    public boolean isHead(){
        return(this.last == null);
    }

    public SetElement getNext(){
        return this.next;
    }

    public SetElement getLast(){
        return this.last;
    }

    public SetElement getHead(){
        return this.head;
    }

    public int getLength(){
        return this.length;
    }

    public int getKey(){
        return this.key;
    }



    public void setNext(SetElement nextToSet){
        this.next = nextToSet;
    }

    public void setLast(SetElement lastToSet){
        this.last = lastToSet;
    }

    public void setHead(SetElement headToSet){
        this.head = headToSet;
    }

    public void setLength(int newLength){
        this.length = newLength;
    }

    public void setKey(int key){
        this.key = key;
    }
}
