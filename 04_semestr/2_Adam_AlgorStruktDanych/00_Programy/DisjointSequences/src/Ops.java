public class Ops {
    //    SetElement temp = new SetElement();
    public SetElement makeSet(int key) {
        SetElement temp = new SetElement();
        temp.setKey(key);
        temp.setHead(temp);
        temp.setLast(temp);
        temp.setNext(null);
        return temp;
    }


    public SetElement findSet(SetElement searched) {
        return searched.getHead();
    }


    public void union(SetElement firstSet, SetElement secondSet) {
        firstSet.getLast().setNext(secondSet);
        firstSet.setLast(secondSet.getLast());

        while (secondSet.getNext() != null) {
            secondSet.setHead(firstSet);
            secondSet = secondSet.getNext();
        }
    }
}