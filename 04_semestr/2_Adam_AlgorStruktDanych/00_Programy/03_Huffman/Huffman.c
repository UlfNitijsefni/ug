#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#define RED 1

typedef struct huffNode
{
    int occurences;
    struct huffNode *parent;
    struct huffNode *left;
    struct huffNode *right;
    int ASCII;
} huff_node;

typedef struct basicData
{
    int occurences;
    int ASCII;
} basic_data;

huff_node *findMinimum(huff_node *root);
basic_data *createTable(int size);
fillTable(char *inputValues, int size, basic_data *toFill);
printTable(basic_data *printed, int size);
//============================================================================================
int main()
{
    basic_data *values = createTable(256);
    fillTable("sadlz,xmm/dsmx ds vxv a'ds ;z", 29, values);
    printTable(values, 256);
}

//--------------------------------------------------------------------------------------------
huff_node *findMinimum(huff_node *root)
{
}

//--------------------------------------------------------------------------------------------
basic_data *createTable(int size)
{
    basic_data *dataTable = malloc(sizeof(basic_data) * (size));
}

//--------------------------------------------------------------------------------------------
fillTable(char *inputValues, int size, basic_data *toFill)
{
    for (int i = 0; i < size; i++)
    {
        toFill[inputValues[i]].occurences++;
    }
}

//--------------------------------------------------------------------------------------------
printTable(basic_data *printed, int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("%c: %d",size, printed->occurences);
    }
}