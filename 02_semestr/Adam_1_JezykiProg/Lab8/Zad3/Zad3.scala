object Zad3 {
  def main(args: Array[String]) = {
    def swap[A](seq: Seq[A]): Seq[A] = {
      seq.zipWithIndex
        .map(x =>
          x._2 % 2 match {
            case 1 => (x._1, x._2)
            case 0 => (x._1, x._2 + 2)
        })
        .sortBy(x => x._2)
        .map(x => x._1)
    }

    var doTestow = Seq(1, 2, 3, 4, 5, 6, 7, 8, 9)

    println("TEST " + swap(doTestow))
  }
}
