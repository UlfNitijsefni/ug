object Zad2 {
  def main(args: Array[String]) {

    def position[A](seq: Seq[A], el: A): Option[Int] = {
      //seq.zipWithIndex.filter(_._1 == el).map(x => x._2).
      seq.zipWithIndex
        .filter(_._1 == el)
        .find(x => x._1 == el)
        .map(x => x._2)
    }

    var doTestow = Seq(1, 2, 3, 4, 1, 2, 3)
    println("dla 3: " + position(doTestow, 5))
  }
}
