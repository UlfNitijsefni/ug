object Gucio {
  def main(args: Array[String]) {

    def pairPosNeg(seq: Seq[Double]): (Seq[Double], Seq[Double]) = {
      seq.filter(_ != 0.0).partition(_ > 0)
    }

    var doTestow = Seq(-2, 0.0, 9.1, 12, -0.1, 1.23)

    println("Wynik to: " + pairPosNeg(doTestow))
  }
}
