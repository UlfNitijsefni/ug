import akka.actor.{ActorSystem, Actor, ActorRef, Props, Terminated, Stash}

case class Odbierz(message: String)
case object Wyswietl
case class Odbieranie(message: String)

class Nadzorca extends Actor with Stash {
  def receive: Receive = {
    case Odbierz(message) =>
      stash()

    case Wyswietl =>
      context.become(printer)
      unstashAll()
  }

  def printer: Receive = {
    case Odbierz(message) =>
      println("X")
      processMessage(message)
  }

  def processMessage(x: String) = {
    println(x)
  }
}

object Main extends App {

  val system = ActorSystem("sys")
  val Nadz = system.actorOf(Props[Nadzorca], "Nadzorca")

  Nadz ! Odbierz("Dzialam1")
  Nadz ! Odbierz("Dzialam2")
  Nadz ! Odbierz("Dzialam3")
  Nadz ! Wyswietl
}
