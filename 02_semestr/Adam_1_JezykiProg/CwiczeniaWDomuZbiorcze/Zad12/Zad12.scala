object OneForTheDarkLordOnHisDarkThrone {
  def main(args: Array[String]) {

    def position[A](seq: Seq[A], el: A): Option[Int] = {
      seq.zipWithIndex.filter(_._1 == el).map(k => k._2).headOption
    }
    var xyz = Seq(1, 2, 3, 4, 5, 3, 2, 1)
    println("Wynik to: " + position(xyz, 1))
  }
}
