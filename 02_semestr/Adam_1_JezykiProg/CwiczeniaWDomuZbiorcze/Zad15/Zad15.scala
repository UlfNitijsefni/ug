object WhyPleaseWhy {
  def main(args: Array[String]) {

    def sum(seq: Seq[Option[Double]]): Double = {
      seq.foldLeft(0.0)((total: Double, current: Option[Double]) =>
        current match {
          case Some(x) => total + x
          case None    => total
      })
      //total + current.getOrElse(0))
    }

    var doTestow =
      Seq[Option[Double]](Some(0.2), Some(1.1), None, Some(12.3), Some(1.4))
    println("Wynik to: " + sum(doTestow))
  }
}
