object SevenForTheDwarfLordsInTheirHallsOfStone {
  def main(args: Array[String]) {

    def deStutter[A](seq: Seq[A]): Seq[A] = {
      seq.foldLeft(Seq[A]())(
        (result, next) =>
          if (result.isEmpty) Seq(next)
          else if (result.last == next) result
          else result :+ next)
    }
    var doTestow = Seq(1, 1, 1, 2, 3, 4, 4, 5, 1, 1, 2)

    println("Wynik to: " + deStutter(doTestow))
  }
}
