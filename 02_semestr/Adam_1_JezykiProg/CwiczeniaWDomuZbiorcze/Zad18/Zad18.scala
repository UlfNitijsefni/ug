object Zad18 {
  def main(args: Array[String]) {

    def mastermind(guess: Seq[Char], answer: Seq[Char]): (Int, Int) = {
      (guess
         .zip(answer)
         .filter(k => k._1 == k._2)
         .size,
         
       guess.size -
         guess
           .zip(answer)
           .filter(k => k._1 == k._2)
           .size
         - guess.diff(answer).size)
    }

    var guess = io.StdIn.readLine
    var secret = io.StdIn.readLine

    println(
      "Czarne: " + mastermind(secret, guess)._1 + " Białe: " + mastermind(
        secret,
        guess)._2)
  }
}
