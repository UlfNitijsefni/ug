object TheBoldOne {
  def main(args: Array[String]) {

    def deStutter[A](seq: Seq[A]): Seq[A] = {
      seq.foldLeft(Seq[A]())(
        (result, next) =>
          if (result.isEmpty) Seq(next)
          else if (result.last == next) result
          else result :+ next)
    }
    var doTestow = Seq(1, 2, 3, 3, 3, 2, 2, 1, 4)
    println("Wynik to: " + deStutter(doTestow))
  }
}
