object PleaseLetItEnd {
  def main(args: Array[String]) {
    def remElems[A](seq: Seq[A], k: Int): Seq[A] = {
      seq.zipWithIndex.filter(_._2 != k).map(k => k._1)
    }
    var doTestow = Seq(1, 2, 3, 4, 5, 6, 7)
    println("Wynik to: " + remElems(doTestow, 4))
  }
}
