object SooooLong {
  def main(args: Array[String]) {

    def freq[A](seq: Seq[A]): Map[A, Int] = {
      seq.groupBy(k => k).mapValues(k => k.length)
    }

    var doTestow = Seq(1, 1, 1, 2, 3, 4, 5, 1, 2, 3, 1, 1, 4, 1)
    println("Wynik to: " + freq(doTestow))
    println("Test: " + doTestow.groupBy(k => k))
  }
}
