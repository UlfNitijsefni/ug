object WelcomeAgainReader {
  def main(args: Array[String]) {

    def divide[A](seq: Seq[A]): (Seq[A], Seq[A]) = {
      def helper[A](seq: Seq[A],
                    result1: Seq[A],
                    result2: Seq[A]): (Seq[A], Seq[A]) = {
        seq match {
          case Seq()             => (result1, result2)
          case (a1 +: Seq())     => helper(Seq(), result1 :+ a1, result2)
          case (a1 +: a2 +: seq) => helper(seq, result1 :+ a1, result2 :+ a2)
        }
      }
      helper(seq, Seq(), Seq())
    }

    var doTestow1 = Seq(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    println("Wynik to: " + divide(doTestow1))
  }
}
