object ReadersWelcome {
  def main(args: Array[String]) {

    def compute[A, B](seq: Seq[A])(init: B)(op: (A, B) => B): B = {
      def helper[A, B](seq: Seq[A], total: B)(init: B)(op: (A, B) => B): B = {
        seq match {
          case Seq()     => total
          case a1 +: seq => helper(seq, op(a1, total))(init)(op)
        }
      }
      helper(seq, init)(init)(op)
    }
    var doTestow = Seq('a', 'b', 'c', 'd')

    println("Wynik to: " + compute(doTestow)("")(_.toString ++ _.toString))
  }
}
