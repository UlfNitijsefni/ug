object GodThisIsHard {
  def main(args: Array[String]) {

    def Strefer(): Unit = {

      java.util.TimeZone.getAvailableIDs.toSeq
        .filter(_.slice(0, 6) == "Europe")
        .map(k => (k, k.length))
        .sortBy(k => k._2)
        .map(k => k._1.stripPrefix("Europe/"))
        .sortWith(_ < _)
    }

    println("Test: " + Strefer())
  }
}
