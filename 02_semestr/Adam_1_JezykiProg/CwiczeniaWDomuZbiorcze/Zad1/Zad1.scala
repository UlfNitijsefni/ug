object WhoReadsThisAnyway {
  def main(args: Array[String]) {

    def powerFinder(curr: Int): Int = {
      def helper(actual: Int, total: Int): Int = {
        curr match {
          case 0 => total
          case _ => helper(actual - 1, total * actual)
        }
      }
      helper(curr, 1)
    }
    println("Wynik to: " + powerFinder(6))
  }
}
