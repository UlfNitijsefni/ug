import scala.io.Source

object Zad20 {
  def main(args: Array[String]) {

    def countAscending(): Int = {
//Seq[Seq[String]]
      Source
        .fromFile("cyfry.txt")
        .getLines
        .toList
        .map(k => k.init.zip(k.tail))
        .filter(x => x.forall(y => y._1 < y._2))
        .length
    }
    println("Wynik: " + countAscending())

  }
}
