import scala.io.Source

object Zad22 {
  def main(args: Array[String]) {

    def partitioner(): Unit = {
      println(
        Source
          .fromFile("liczby.txt")
          .getLines
          .toList
          .map(k => k.partition(_.asDigit % 2 == 0)))
    }

    partitioner()

  }
}
