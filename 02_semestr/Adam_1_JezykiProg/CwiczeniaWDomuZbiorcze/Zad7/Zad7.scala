object MayItWork {
  def main(args: Array[String]) {

    def subseq[A](seq: Seq[A], begIdx: Int, endIdx: Int): Seq[A] = {
      seq.take(endIdx).drop(begIdx)
    }

    var doTestow = Seq(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)

    println("Wynik to: " + subseq(doTestow, 2, 7))
  }
}
