object Main {
  def main(args: Array[String]) {

    println("Podaj liczbe zeby sprawdzic czy jest pierwsza:")
    var wczytana = io.StdIn.readInt()
    var pierwsza = true
    var iterator = wczytana - 1

    while (iterator > 1) {
      if (wczytana % iterator == 0) {
        pierwsza = false
      }
      iterator -= 1
    }

    if (pierwsza == true) {
      println("Podana liczba jest liczba pierwsza.")
    } else {
      println("Podana liczba nie jest pierwsza.")
    }
  }
}
