object Main {

  //--------------------------------------------------------------------------------
  def main(args: Array[String]) {

    println("Podaj parzystą liczbę całkowitą większą od 2:")

    var doTestu = io.StdIn.readInt
    var pierwsza1 = 1
    var pierwsza2 = 0
    var ready = false

    while (ready == false) {

      pierwsza2 = doTestu - pierwsza1

      if (czyPierwsza(pierwsza2)) {
        println("Podana liczba: " + doTestu)
        println("Liczba podana jest sumą: " + pierwsza1 + " i " + pierwsza2)
        ready = true
      } else {
        do {
          pierwsza1 += 2
        } while (!czyPierwsza(pierwsza1))
      }
    }
  }

  //--------------------------------------------------------------------------------
  def czyPierwsza(x: Int): Boolean = {
    var wczytana = x
    var pierwsza = true
    var iterator = wczytana - 1

    while (iterator > 1) {
      if (wczytana % iterator == 0) {
        pierwsza = false
      }
      iterator -= 1
    }
    pierwsza
  }
}
