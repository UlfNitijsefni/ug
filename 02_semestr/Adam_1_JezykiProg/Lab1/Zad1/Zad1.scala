object Main {

  var dod = 0
  var uj = 0
  var wczyt = 1

  def main(args: Array[String]) {
    println("Podawaj kolejne liczby, gdy chcesz przestać wpisz 0.")

    while (wczyt != 0) {
      wczyt = io.StdIn.readInt()

      if (wczyt == 0) {
        println("Jest " + dod + " liczb dodatnich i " + uj + " liczb ujemnych.")
      } else if (wczyt > 0) {
        dod += 1
      } else {
        uj += 1
      }
    }
  }
}
