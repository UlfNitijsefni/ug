object Main {
  def main(args: Array[String]) {}
  println("Podaj dwie liczby których NWD chcesz poznać:")

  var liczba1 = 0
  var liczba2 = 0
  var NWD = 0
  var mniejsza = 0
  liczba1 = io.StdIn.readInt()
  liczba2 = io.StdIn.readInt()

  if (liczba1 > liczba2) {
    mniejsza = liczba2
  } else {
    mniejsza = liczba1
  }

  while (mniejsza > 0) {
    if (liczba1 % mniejsza == 0 && liczba2 % mniejsza == 0) {
      println("NWD = " + mniejsza)
      mniejsza = 0
    } else {
      mniejsza -= 1
    }
  }
}
