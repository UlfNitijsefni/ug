object Masterming {
  def main(args: Array[String]) = {

    def Mastermind(guess: Seq[Int], answer: Seq[Int]): Unit = {
      println("czarne: " + guess.zip(answer).filter(x => x._2 == x._1).size)
      println(
        "biale: " + (answer.size - guess
          .zip(answer)
          .filter(x => x._2 == x._1)
          .size - answer.diff(guess).size))
    }

    Mastermind(Seq(1, 2, 3, 4, 7, 2), Seq(2, 1, 3, 4, 8, 0))
  }
}
