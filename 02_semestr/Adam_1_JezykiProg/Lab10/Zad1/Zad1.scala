object Zad1 {
  def main(args: Array[String]) {

    def mastermind[A](guess: Seq[A], code: Seq[A]): (Int, Int) = {
      (guess.zip(code).filter(k => k._1 == k._2).size,
       guess.size - guess.zip(code).filter(k => k._1 == k._2).size - guess
         .diff(code)
         .size)

      //(guess.intersect(code).size)
    }
    var guess = Seq(1, 2, 3, 4, 5)
    var code = Seq(2, 3, 6, 1, 5)

    println("Wynik to: " + mastermind(guess, code))
  }
}
