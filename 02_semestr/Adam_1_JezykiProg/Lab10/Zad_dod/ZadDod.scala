import scala.io.Source

object ZadDod {
  def main(args: Array[String]) {

    def historgam(): Unit = {
      println(
        "Wynik to: " + Source
          .fromFile("Test.txt")
          .getLines
          .toSeq
          .groupBy(k => k)
          .filter(_ != ",")
          .map(k => k)
          .mapValues(k => k.size)
      )
    }
    historgam()
  }
}
