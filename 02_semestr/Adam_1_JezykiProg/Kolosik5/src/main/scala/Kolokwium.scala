import akka.actor.{
  Actor,
  ActorRef,
  ActorSystem,
  Props,
  PoisonPill,
  Stash,
  ActorLogging,
  Terminated
}

//========================================================
object Nadzorca {
  case class Init(liczbaPracownikow: Int)
  case class Wynik(n: Int)
  case class Zlecenie(tekst: Seq[String])
}

//--------------------------------------------------------
class Nadzorca extends Actor with Stash with ActorLogging {
  import Nadzorca._

//--------------------------------------------------------
  def receive: Receive = {
    case Init(n) =>
      val aktorzy = makeActors(n, Seq.empty[ActorRef])
      aktorzy.foreach(context.watch(_))
      context.become(waitForText(aktorzy, 0))
  }

//--------------------------------------------------------
  def waitForText(aktorzy: Seq[ActorRef], wynik: Int): Receive = {
    case Zlecenie(tekst) =>
      additioner(Seq.empty[ActorRef], aktorzy, tekst)
      context.become(waitingForResult(aktorzy, wynik))
      unstashAll()
      aktorzy.foreach(_ ! PoisonPill)
    case Wynik(n) => stash()
  }

//--------------------------------------------------------
  def waitingForResult(aktorzy: Seq[ActorRef], wynik: Int): Receive = {
    case Wynik(n) => context.become(waitingForResult(aktorzy, wynik + n))
    case Terminated(actor) =>
      aktorzy.filter(_ != actor).size match {
        case 0 =>
          println("Wynik: " + wynik)
          context.become(receive)
        case _ =>
          context.become(waitingForResult(aktorzy.filter(_ != actor), wynik))
      }
  }
//--------------------------------------------------------
  def additioner(used: Seq[ActorRef],
                 toUse: Seq[ActorRef],
                 linesToDo: Seq[String]): Unit = {
    (toUse, linesToDo) match {
      case (Seq(), _) => additioner(toUse, used, linesToDo)
      case (_, Seq()) => return
      case (nowWorking +: actorsToUse, line +: unusedLines) =>
        nowWorking ! Pracownik.Wykonaj(line)
        additioner(used :+ nowWorking, actorsToUse, unusedLines)
    }
  }

//--------------------------------------------------------
  def makeActors(n: Int, actors: Seq[ActorRef]): Seq[ActorRef] = {
    n match {
      case 0 => actors
      case _ => makeActors(n - 1, actors :+ context.actorOf(Props[Pracownik]))
    }
  }
}

//========================================================
object Pracownik {
  case class Wykonaj(toWork: String)
}

class Pracownik extends Actor with ActorLogging {
  import Pracownik._

//--------------------------------------------------------
  def receive: Receive = {
    case Wykonaj(line: String) =>
      sender ! Nadzorca.Wynik(line.trim.split(" ").size)
  }
}

//========================================================
object Main extends App {
  val system = ActorSystem("sys")
  val szef = system.actorOf(Props[Nadzorca], "Nadzorca")

//--------------------------------------------------------
  def dane(): Seq[String] = {
    scala.io.Source.fromResource("ogniem_i_mieczem.txt").getLines.toSeq
  }

//--------------------------------------------------------
  szef ! Nadzorca.Init(12)
  szef ! Nadzorca.Zlecenie(dane())
}
