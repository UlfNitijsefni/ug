object Zad1 {
  def main(args: Array[String]) {

    val doTestow = Seq("xyz", "zzz", "abcde", "z", "cs", "cs", "ca")

    val wynik = Seq()

    def involves(tested: Seq[String],
                 isIn: Int,
                 missing: Seq[(Int, Int)]): Seq[(Int, Int)] = {
      tested match {
        case Seq() => missing :+ (isIn, 1)
        case a1 +: seq =>
          a1._1 match {
            case isIn => missing ++ ((a1._1, a1._2 + 1) +: seq)
            case _    => involves(seq, isIn, missing :+ a1)
          }
      }
    }

    def repeater(iterator: Int,
                 given: Seq[String],
                 wynik: Seq[(Int, Int)]): Unit = {
      iterator match {
        case 0 => return
        case _ => println("TEST" + involves(given, given.head.size, seq()))
      }
    }

    repeater(doTestow.size, doTestow, wynik)

  }
}
/*
    def repeater(iterator: Int): Unit = {
      iterator match {
        case 0 => return
        case _ => //toDo
      }
    }
 */
