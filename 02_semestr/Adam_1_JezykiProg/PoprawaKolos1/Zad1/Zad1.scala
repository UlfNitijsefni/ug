object Zad1 {
  def main(args: Array[String]) {

    var doTestow = Seq(1, 2, 3)
    var doTestow2 = Seq('a', 'b', 'c', 'd')

    def multiplier[A](seq: Seq[A], ind: Int): Seq[A] = {
      def helper[A](seq: Seq[A], ind: Int, wynik: Seq[A]): Seq[A] = {
        def additioner[A](toAdd: A, ind: Int, added: Seq[A]): Seq[A] = {
          ind match {
            case 0 => added
            case _ => additioner(toAdd, ind - 1, added :+ toAdd)
          }
        }
        seq match {
          case Seq() => wynik
          case a1 +: remaining =>
            helper(remaining, ind, wynik ++ additioner(a1, ind, Seq()))
        }
      }
      helper[A](seq, ind, Seq())
    }

    println("Wynik testu z Intami: " + multiplier(doTestow, 3))
    println("Wynik testu z Charami: " + multiplier(doTestow2, 2))
  }
}
