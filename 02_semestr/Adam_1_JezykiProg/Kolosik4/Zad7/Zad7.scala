object Zad7 {
  def main(args: Array[String]) {

    println("Podaj sekwencję a potem wyraz którego indeksy będziesz chciał: ")

    var givenseq = io.StdIn.readLine
    var searched = io.StdIn.readChar

    def indices[A](seq: Seq[A], el: A): Set[Int] = {
      seq.zipWithIndex.filter(k => k._1 == el).map(k => k._2).toSet
    }
    println("Wynik to: " + indices(givenseq, searched))
  }
}
