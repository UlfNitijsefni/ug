object Zad1 {
  def main(args: Array[String]) {

    def sum(seq: Seq[Option[Double]]): Double = {
      seq.foldLeft(0.0)(_ + _.value)
    }
    var doTestow: Seq[Option[Double]] =
      Seq(Some(1), None, Some(2), None, Some(3.3), Some(4.5), Some(5.1))
    println("Wynik to: " + sum(doTestow))
  }
}
