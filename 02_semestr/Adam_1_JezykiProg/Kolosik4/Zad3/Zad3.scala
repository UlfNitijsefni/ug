object Zad2 {
  def main(args: Array[String]) {

    println("Podaj sekwencję do zamiany:")
    var given = io.StdIn.readLine

    def swap[A](seq: Seq[A]): Seq[A] = {
      seq.zipWithIndex
        .map(k =>
          k._2 % 2 match {
            case 1 => (k._1, k._2)
            case 0 => (k._1, k._2 + 2)
        })
        .sortBy(k => k._2)
        .map(k => k._1)
    }
    println("Wynik to" + swap(given))

    // println("Wynik to" + swap(swap(given)))
    // println("Wynik to" + swap(swap(swap(given))))
    // println("Wynik to" + swap(swap(swap(swap(given)))))
  }
}
