import scala.io.Source

object Zad3 {
  def main(args: Array[String]) {

    var doTestow = Source.fromFile("osoby.txt").getLines.toList

    println(
      "Wynik to: " + doTestow
        .map(k => k.length)
        .groupBy(k => k)
        .mapValues(k => k.size))
  }
}
