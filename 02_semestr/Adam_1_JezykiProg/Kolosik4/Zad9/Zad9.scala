object Zad7 {
  def main(args: Array[String]) {

    println("Podaj ostateczny ciąg, a potem próbę:")
    var secret = io.StdIn.readLine
    var guess = io.StdIn.readLine

    def mastermind(secret: Seq[Char], guess: Seq[Char]): (Int, Int) = {
      (secret
         .zip(guess)
         .filter(k => k._1 == k._2)
         .size,
       secret.size - secret
         .zip(guess)
         .filter(k => k._1 == k._2)
         .size - secret.diff(guess).size)
    }

    println(
      "Czarne: " + mastermind(secret, guess)._1 + " i białe: " + mastermind(
        secret,
        guess)._2)
  }
}
