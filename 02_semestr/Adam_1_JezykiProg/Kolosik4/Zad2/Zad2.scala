object Zad2 {
  def main(args: Array[String]) {

    println("Podaj sekwencję, potem element szukany.")
    var given = io.StdIn.readLine
    var searched = io.StdIn.readChar

    def position[A](seq: Seq[A], el: A): Option[Int] = {
      seq.zipWithIndex.filter(k => k._1 == el).map(k => k._2).toSet.headOption
    }
    println("Wynik to: " + position(given, searched))
  }
}
