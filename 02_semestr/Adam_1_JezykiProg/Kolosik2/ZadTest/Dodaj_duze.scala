object Main {

    def main (args: Array[String]) {

        println("Podaj pierwsza liczbe")
        var a: Seq[Char] = io.StdIn.readLine

        println("Podaj druga liczbe")
        var b: Seq[Char] = io.StdIn.readLine

        def dodaj (a: Seq[Char], b: Seq[Char]): String = {
            
            def helper (aa: Seq[Char], bb: Seq[Char], extra: Int, total: String): String = {
                
                (aa, bb, extra) match {
                    case (Seq(), Seq(), 0)            => total
                    case (Seq(), Seq(), 1)            => "1" + total
                    case (xxx :+ x, Seq(), ex)        => helper (xxx, Seq(), (x.asDigit + ex) / 10, (x.asDigit + ex) % 10 + total)
                    case (Seq(), xxx :+ x, ex)        => helper (xxx, Seq(), (x.asDigit + ex) / 10, (x.asDigit + ex) % 10 + total)
                    case (aaa :+ a, bbb :+ b, ex)     => 
                        helper (aaa, bbb, (a.asDigit + b.asDigit + ex) / 10, (a.asDigit + b.asDigit + ex) % 10  + total)
                }
            }

            helper (a, b, 0, "")
        }

        println (dodaj(a, b))
    }
}