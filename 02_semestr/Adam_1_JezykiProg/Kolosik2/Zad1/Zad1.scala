object Zad1 {
  def main(args: Array[String]) {

    var doTestow1: Seq[Char] = io.StdIn.readLine
    var doTestow2: Seq[Char] = io.StdIn.readLine

    def additioner(liczba1: Seq[Char], liczba2: Seq[Char]): Seq[Int] = {
      def helper(worked: (Seq[Char], Seq[Char]),
                 total: Seq[Int],
                 rest: Int): Seq[Int] = {
        worked match {
          case (Seq(), seq :+ a1) =>
            rest match {
              case 0 => helper((Seq(), seq), a1.asDigit +: total, 0)
              case 1 => helper((Seq(), seq), (a1.asDigit + rest) +: total, 0)
            }
          case (seq :+ a1, Seq()) =>
            helper((seq, Seq()), a1.asDigit +: total, 0)
          case (Seq(), Seq()) if (rest == 0) => total
          case (Seq(), Seq()) if (rest != 0) => rest +: total
          case (seq1 :+ a1, seq2 :+ a2) =>
            helper((seq1, seq2), //worked
                   ((a1.asDigit + a2.asDigit + rest) % 10) +: total, //total
                   (a1.asDigit + a2.asDigit + rest) / 10) //rest
        }
      }
      helper((liczba1, liczba2), Seq(), 0)
    }
    println("Wynik to: " + additioner(doTestow1, doTestow2))
  }
}
