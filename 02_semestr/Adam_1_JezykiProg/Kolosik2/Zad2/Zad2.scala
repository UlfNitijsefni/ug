object Zad2 {
  def main(args: Array[String]) {

    println(
      "Podaj indeks poszukiwany. Dla niedozwolonej wartości wynik będzie równał się 0:")
    var given = io.StdIn.readInt

    def whatAtN(n: Int): Int = {
      n match {
        case 1            => 1
        case 0            => 1
        case _ if (n < 0) => 0
        case _ if (n > 0) => whatAtN(n - 2) + whatAtN(n - 1)
      }
    }
    println("Wynik to: " + whatAtN(given))
  }
}
