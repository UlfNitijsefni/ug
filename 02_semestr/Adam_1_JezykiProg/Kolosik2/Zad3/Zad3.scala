object Zad3 {
  def main(args: Array[String]) {

    def compress[A](seq: Seq[A]): Seq[(A, Int)] = {
      def helper[A](seq: Seq[A],
                    last: A,
                    current: (A, Int),
                    total: Seq[(A, Int)]): Seq[(A, Int)] = {
        seq match {
          case Seq() => total :+ current
          case a1 +: seq if (a1 == last) =>
            helper(seq, last, (current._1, current._2 + 1), total)
          case a1 +: seq if (a1 != last) =>
            helper(seq, a1, (a1, 1), total :+ current)
        }
      }
      seq match {
        case Seq()     => Seq()
        case a1 +: seq => helper(seq, a1, (a1, 1), Seq())
      }
    }

    var doTestow: Seq[Char] = io.StdIn.readLine
    println("Wynik to :" + compress(doTestow))
  }
}
