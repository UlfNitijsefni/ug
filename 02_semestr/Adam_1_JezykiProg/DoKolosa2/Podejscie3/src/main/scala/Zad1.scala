import akka.actor.{
  Actor,
  ActorRef,
  ActorSystem,
  Stash,
  Props,
  Terminated,
  ActorLogging,
  PoisonPill
}
import scala.util.Random
//====================================================
object mainLinie extends App {

  val system = ActorSystem("systemik")
  val wychowawca = system.actorOf(Props[Wychowawca], "nadzorca")

  wychowawca ! Wychowawca.Tworz(10, 5)
}
