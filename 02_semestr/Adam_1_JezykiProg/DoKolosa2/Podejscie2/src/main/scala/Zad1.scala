import akka.actor.{
  Actor,
  ActorRef,
  ActorSystem,
  Stash,
  Props,
  Terminated,
  ActorLogging,
  PoisonPill
}
import scala.util.Random

//====================================================
object Wychowawca {
  case class Tworz(iloscUczniow: Int, iloscNauczycieli: Int)
  case class WynikKlasy(n: Float)
}

//----------------------------------------------------
class Wychowawca extends Actor with Stash {
  import Wychowawca._

  def receive: Receive = {
    case Tworz(n, m) =>
      val uczniowie = makeStudents(n, Seq.empty[ActorRef])
      pytanko(m, uczniowie)
      context.become(teachersWorking(m, n, 0))
      unstashAll()
    case Terminated(teacher) => stash()
  }

  def teachersWorking(nauczyciele: Int,
                      ileUczniow: Int,
                      wynik: Float): Receive = {
    case Terminated(teacher) =>
      println("Terminated")
      nauczyciele match {
        case 1 => println("Wynik to: " + (wynik / nauczyciele))
        case _ =>
          context.become(teachersWorking(nauczyciele - 1, ileUczniow, wynik))
      }
    case WynikKlasy(n) =>
      context.become(teachersWorking(nauczyciele, ileUczniow, wynik + n))
  }

//----------------------------------------------------
  def makeStudents(n: Int, actors: Seq[ActorRef]): Seq[ActorRef] = {
    n match {
      case 0 => actors
      case _ => makeStudents(n - 1, actors :+ context.actorOf(Props[Uczen]))
    }
  }

//----------------------------------------------------
  def pytanko(n: Int, students: Seq[ActorRef]): Unit = {
    n match {
      case 0 => return
      case _ =>
        val nauczyciel = context.actorOf(Props[Nauczyciel])
        context.watch(nauczyciel)
        nauczyciel ! Nauczyciel.Pytaj(students)
        pytanko(n - 1, students)
    }
  }
}

//====================================================
object Nauczyciel {
  case class Odpowiedzi(odp: Seq[Boolean])
  case class Pytaj(students: Seq[ActorRef])
}

class Nauczyciel extends Actor with Stash {
  import Nauczyciel._

//----------------------------------------------------
  def receive: Receive = {
    case Odpowiedzi(odp) => stash()
    case Pytaj(students) =>
      students.foreach(_ ! Uczen.Pytanie)
      context.become(
        waitingForStudents(students, 0, students.size, students.size))
      unstashAll()
  }

//----------------------------------------------------
  def waitingForStudents(students: Seq[ActorRef],
                         total: Int,
                         classSize: Int,
                         studentsLeft: Int): Receive = {
    case Odpowiedzi(odp) =>
      studentsLeft match {
        case 1 =>
          println("Srednia: " + total.toFloat / classSize)
          context.parent ! Wychowawca.WynikKlasy(total.toFloat / classSize)
          self ! PoisonPill
        case _ =>
          context.become(
            waitingForStudents(students,
                               total + odp.filter(_ == true).size + 1,
                               classSize,
                               studentsLeft - 1))
      }
  }
}

//====================================================
object Uczen {
  case object Pytanie
}

//----------------------------------------------------
class Uczen extends Actor with Stash {
  import Uczen._

//----------------------------------------------------
  def receive: Receive = {
    case Pytanie =>
      sender ! Nauczyciel.Odpowiedzi(sequencer( /*Random.nextInt(5)*/ 5, Seq()))
  }

  def sequencer(n: Int, current: Seq[Boolean]): Seq[Boolean] = {
    n match {
      case 0 => current.padTo(5, false)
      case _ => sequencer(n - 1, current :+ true)
    }
  }
}

//====================================================
object mainLinie extends App {

  val system = ActorSystem("systemik")
  val wychowawca = system.actorOf(Props[Wychowawca], "nadzorca")

  wychowawca ! Wychowawca.Tworz(10, 5)
}
