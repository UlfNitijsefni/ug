import akka.actor.{Actor, ActorRef, ActorSystem, Stash, Props, ActorLogging}
import scala.util.Random

//====================================================
object Wychowawca {
  case class Tworz(ileNau: Int, ileUcz: Int)
  case class Wynik(n: Float)
}

class Wychowawca extends Actor with Stash with ActorLogging {
  import Wychowawca._

  def receive: Receive = {
    case Tworz(nau, ucz) =>
      val students = makeStudents(ucz, Seq.empty[ActorRef])
      questioning(nau, students)
      context.become(waitingForResults(0, nau, nau))
      unstashAll()
    case Wynik(n) => stash()
  }

//----------------------------------------------------
  def questioning(teachers: Int, students: Seq[ActorRef]): Unit = {
    teachers match {
      case 0 => return
      case _ =>
        context.actorOf(Props[Nauczyciel]) ! Nauczyciel.Pytaj(students)
        questioning(teachers - 1, students)
    }
  }

//----------------------------------------------------
  def waitingForResults(result: Float, teachers: Int, teachLeft: Int): Receive = {
    case Wynik(n) =>
      teachLeft match {
        case 1 => println("Wynik klasy: " + result / teachers)
        case _ =>
          context.become(waitingForResults(result + n, teachers, teachLeft - 1))
      }
    //println("Otrzymałem: " + n)
  }

//----------------------------------------------------
  def makeStudents(amount: Int, studs: Seq[ActorRef]): Seq[ActorRef] = {
    amount match {
      case 0 => studs
      case _ => makeStudents(amount - 1, studs :+ context.actorOf(Props[Uczen]))
    }
  }
}

//====================================================
object Nauczyciel {
  case class Pytaj(students: Seq[ActorRef])
  case class Boole(results: Seq[Boolean])
}

class Nauczyciel extends Actor with Stash {
  import Nauczyciel._

//----------------------------------------------------
  def receive: Receive = {
    case Pytaj(studs) =>
      studs.foreach(_ ! Uczen.Pytanie)
      context.become(waitingForStuds(studs.size, 0, studs.size))
      unstashAll()
    case Boole(n) => stash
  }

  def waitingForStuds(students: Int, result: Double, studsLeft: Int): Receive = {
    case Boole(n) =>
      studsLeft match {
        case 1 =>
          context.parent ! Wychowawca.Wynik(result.toFloat / students)
        case _ =>
          context.become(
            waitingForStuds(students,
                            result + n.filter(_ == true).size + 1.25,
                            studsLeft - 1))
      }
  }
}

//====================================================
object Uczen {
  case object Pytanie
}

class Uczen extends Actor with Stash {
  import Uczen._

//----------------------------------------------------
  def receive: Receive = {
    case Pytanie =>
      sender ! Nauczyciel.Boole(boolGenerator(Random.nextInt(5), Seq()))
  }

//----------------------------------------------------
  def boolGenerator(n: Int, result: Seq[Boolean]): Seq[Boolean] = {
    n match {
      case 0 => result.padTo(5, false)
      case _ => boolGenerator(n - 1, result :+ true)
    }
  }
}

//====================================================
object mainLinie extends App {

  val system = ActorSystem("systemik")
  val wychowawca = system.actorOf(Props[Wychowawca], "nadzorca")

  wychowawca ! Wychowawca.Tworz(10, 5)
}
