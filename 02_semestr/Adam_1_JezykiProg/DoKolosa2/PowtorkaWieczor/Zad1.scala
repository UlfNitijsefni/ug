import scala.io.Source

object Xyz {
  def main(args: Array[String]) {

    var doTestow = Source.fromFile("Przylkadowy.txt").getLines.toList

    println("Zad1: " + doTestow.filter(_.size % 2 == 0))
    println("Zad2: " + doTestow.diff(doTestow.filter(_.contains(0))))
    println("Zad3: " + doTestow.)

  }
}
