import akka.actor.{Actor, ActorRef, ActorSystem, Stash, Props}
import scala.util.Random

//----------------------------------------------------
object Wychowawca {
  case class Tworz(n: Int, m: Int)
  case class Srednia(n: Float)
}

//----------------------------------------------------
class Wychowawca extends Actor with Stash {
  import Wychowawca._

//----------------------------------------------------
  def receive: Receive = {
    case Tworz(students, teachers) =>
      val uczniowie = makeStudents(students, Seq.empty[ActorRef])
      pytanko(teachers, uczniowie)
      context.become(waitForAvg(0, uczniowie.size, teachers, teachers))
      unstashAll()
    case Srednia(n) => stash()
  }

//----------------------------------------------------
  def waitForAvg(total: Float,
                 studs: Int,
                 teachersLeft: Int,
                 teachersMax: Int): Receive = {
    case Srednia(n) =>
      teachersLeft match {
        case 1 =>
          println(
            "Średnia klasy ze wszystkich przedmiotów razem: " + total / teachersMax)
        case _ =>
          context.become(
            waitForAvg(total + n, studs, teachersLeft - 1, teachersMax))
      }

  }

//----------------------------------------------------
  def pytanko(n: Int, students: Seq[ActorRef]): Unit = {
    n match {
      case 0 => return
      case _ =>
        context.actorOf(Props[Nauczyciel]) ! Nauczyciel.Pytaj(students)
        pytanko(n - 1, students)
    }
  }

//----------------------------------------------------
  def makeStudents(n: Int, actors: Seq[ActorRef]): Seq[ActorRef] = {
    n match {
      case 0 => actors
      case _ => makeStudents(n - 1, actors :+ context.actorOf(Props[Uczen]))
    }
  }
}

//====================================================
object Nauczyciel {
  case class Odpowiedz(odp: Seq[Boolean])
  case class Pytaj(students: Seq[ActorRef])
  case class DajSrednia(srednia: Float)
}

//----------------------------------------------------
class Nauczyciel extends Actor with Stash {
  import Nauczyciel._

//----------------------------------------------------
  def receive: Receive = {
    case Odpowiedz(odp: Seq[Boolean]) =>
      stash()
    case Pytaj(students) =>
      students.foreach(_ ! Uczen.Pytanie)
      context become waitForAnswers(students.size, 0, students.size)
      unstashAll()
    case DajSrednia(n) => println("Średnia klasy: " + n)
  }

//----------------------------------------------------
  def waitForAnswers(totalActors: Int, wynik: Int, actorsLeft: Int): Receive = {
    case Odpowiedz(odp: Seq[Boolean]) =>
      actorsLeft match {
        case 1 =>
          context.parent ! Wychowawca.Srednia(wynik.toFloat / totalActors)
          context.become(receive)
        case _ =>
          context.become(
            waitForAnswers(totalActors,
                           wynik + odp.filter(_ == true).size + 2,
                           actorsLeft - 1))
      }
  }
}

//====================================================
object Uczen {
  case object Pytanie
}

//----------------------------------------------------
class Uczen extends Actor {
  import Uczen._

//----------------------------------------------------
  def receive: Receive = {
    case Pytanie =>
      sender ! Nauczyciel.Odpowiedz(
        (generateOdp(Random.nextInt(5), Seq())).padTo(5, false))
  }

//----------------------------------------------------
  def generateOdp(n: Int, odp: Seq[Boolean]): Seq[Boolean] = {
    n match {
      case 0 => odp
      case _ => generateOdp(n - 1, odp :+ true)
    }
  }
}

//====================================================
object mainLinie extends App {

  val system = ActorSystem("systemik")
  val wychowawca = system.actorOf(Props[Wychowawca], "nadzorca")

  wychowawca ! Wychowawca.Tworz(10, 5)
}
