object UwU {
  def main(args: Array[String]) {

    var doTestow: Seq[Int] = Seq(1, 2, 3, 4, 5, 6, 7)

    println("Wynik to: " + initPairMap[Int, Int, Int](doTestow)(_ * 2)(_ * 3))

    def initPairMap[A, B, C](seq: Seq[A])(op1: (A) => B)(
        op2: (B) => C): Seq[(B, C)] = {
      var Temp
        : Seq[(B, C)] = Seq((op1(seq.head), op2(op1(seq.head)))) +: Seq.empty
      Temp +: initPairMap(seq.tail)(op1)(op2)
    }
  }
}
