object counter {
  def main(args: Array[String]) {
    def testSeq1: Seq[String] = Seq("abba", "a", "alfal", "a", "B")
    def testSeq2: Seq[String] = Seq("aba", "a", "alfal", "a", "a")
    println(checker(testSeq1, testSeq2, 0))
  }

  def checker(seq1: Seq[String], seq2: Seq[String], howManyMatch: Int): Int = {
    if (seq1.isEmpty || seq2.isEmpty) {
      howManyMatch
    } else {
      if ((seq1.head).length == (seq2.head).length) {
        checker(seq1.tail, seq2.tail, (howManyMatch + 1))
      } else {
        checker(seq1.tail, seq2.tail, howManyMatch)
      }
    }
  }

}
