object XYZ {
  def main(args: Array[String]) {

    var doTestow1: Seq[String] = Seq("xxx", "zzzz", "a", "zxf")
    var doTestow2: Seq[String] = Seq("xz", "ssss", "d", "uxf", "asdf")

    println("Wynik to: " + lengthStrings(doTestow1, doTestow2))

    def lengthStrings(seq1: Seq[String], seq2: Seq[String]): Int = {
      if (seq1.size == 0 || seq2.size == 0) {
        0
      } else if (seq1.head.size == seq2.head.size) {
        1 + lengthStrings(seq1.tail, seq2.tail)
      } else {
        lengthStrings(seq1.tail, seq2.tail)
      }
    }
  }
}
