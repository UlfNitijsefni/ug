object ZZZYZ {
  def main(args: Array[String]) {

    var doTestow: Seq[Int] = Seq(1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5)

    println(
      "Czy warunek jest spelniony: " + checkElement[Int](doTestow, 5)(
        _ % 2 == 0))

    def checkElement[A](seq: Seq[A], ind: Int)(leq: (A) => Boolean): Boolean = {

      ind match {
        case ((seq.size + 1) || (-1)) => {
          println("Podany indeks jest poza sekwencją.")
          false
        }
        case _ => {
          helper[A](seq, ind, 0)(leq)
        }
      }

    }
  }
  def helper[A](seq: Seq[A], ind: Int, curr: Int)(
      leq: (A) => Boolean): Boolean = {

    curr match {
      case (ind) => {
        leq(seq.head)
      }
      case _ => {
        helper[A](seq, ind, curr)(leq)
      }
    }
  }
}
