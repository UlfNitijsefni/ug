object ZOINK {
  def main(args: Array[String]) {

    var doTestow: Seq[(Char, Int)] = Seq(('a', 2), ('b', 3), ('z', 1), ('x', 0))

    println("Wynik to: " + inverseCompress[Char](doTestow))

    def inverseCompress[A](seq: Seq[(A, Int)]): Seq[A] = {

      if (seq.isEmpty) {
        return Seq.empty
      } else {
        return writeXAmount[A](seq.head) ++ inverseCompress[A](seq.tail)
      }

      def writeXAmount[A](x: (A, Int)): Seq[A] = {

        var Temp: Seq[A] = Seq.empty

        for (i <- 0 to (x._2) - 1) {
          Temp :+ (" " + x._1)
        }

        Temp
      }
    }
  }
}
