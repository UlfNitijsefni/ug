object WorkingAsIntended {
  def main(args: Array[String]) {

    def checkIndex[A](seq: Seq[A], ind: Int)(leq: (A) => Boolean): Boolean = {
      def helper[A](seq: Seq[A], ind: Int, actualInd: Int)(
          leq: (A) => Boolean): Boolean = {
        seq match {
          case Seq() => false
          case (a1 +: seq) if (ind == actualInd) => {
            leq(a1)
          }
          case (a1 +: seq) if (ind != actualInd) =>
            helper(seq, ind, actualInd + 1)(leq)
        }
      }
      helper(seq, ind, 0)(leq)
    }

    var doTestow = Seq(1, 3, 4, 56, 7)
    println("Wynik to: " + checkIndex[Int](doTestow, 3)(_ > 4))
  }
}
