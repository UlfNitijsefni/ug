object PreciseGermanEngineering {
  def main(args: Array[String]) {

    def decompress[A](seq: Seq[(A, Int)]): Seq[A] = {
      def helper[A](printable: A, times: Int), acc:Seq[A]): Seq[A] = {
        times match {
          case 0 => Seq()
          case _ => printable +: helper(printable, times - 1)
        }
      }

      seq match {
        case Seq()         => Seq()
        case (a1 +: Seq()) => Seq()
        case (a1 +: seq)   => helper(a1._1, a1._2, acc++decompress(seq))  //<= rekurencja ogonowa
      }
    helper(seq, 0, Seq()) //??
    }

    var doTestow = Seq(('a', 1), ('b', 2), ('c', 3), ('d', 4), ('e', 5))
    println("Wynik to: " + decompress(doTestow))
  }
}
