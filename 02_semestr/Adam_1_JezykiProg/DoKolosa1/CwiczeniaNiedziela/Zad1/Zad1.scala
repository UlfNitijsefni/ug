object WorkPls {
  def main(args: Array[String]) {

    def counter(seq: (Seq[String], Seq[String])): Int = {
      def helper(seq: (Seq[String], Seq[String]), acc: Int): Int = {
        seq match {
          case (Seq(), Seq()) => acc
          case (Seq(), seq)   => acc
          case (seq, Seq())   => acc
          case (a1 +: seq1, a2 +: seq2) if (a1.size == a2.size) =>
            helper((seq1, seq2), acc + 1)
          case (a1 +: seq1, a2 +: seq2) if (a1.size != a2.size) =>
            helper((seq1, seq2), acc)
        }
      }
      helper(seq, 0)
    }
    var doTestow1: Seq[String] = Seq("abba", "bab", "xyzsa", "ababab", "adssa")
    var doTestow2: Seq[String] = Seq("baab", "xz", "ababab", "ba")

    println("Wynik to: " + counter(doTestow1, doTestow2))

  }
}
