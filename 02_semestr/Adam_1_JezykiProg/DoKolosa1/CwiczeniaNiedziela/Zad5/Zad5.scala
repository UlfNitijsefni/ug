object Ameng {
  def main(args: Array[String]) {

    def joiner[A](seq: (Seq[A], Seq[A])): Seq[A] = {
      seq match {
        case (Seq(), Seq())     => Seq()
        case (seq, Seq())       => seq
        case (Seq(), seq)       => seq
        case (a1 +: seq1, seq2) => a1 +: joiner(seq2, seq1)
      }
    }
    var doTestow1 = Seq('a', 'a', 'a', 'a', 'a', 'a', 'a')
    var doTestow2 = Seq('b', 'b', 'b', 'b')

    println("Wynik to: " + joiner(doTestow1, doTestow2))
  }
}
