object WorkPls {
  def main(args: Array[String]) {

    var doTestow1: Seq[String] = Seq("xcvz", "zdfads", "zdfaefewafd", "abba")
    var doTestow2: Seq[String] = Seq("lux", "arthas", "x", "abba")

    println("Wynik to: " + countEqual(doTestow1, doTestow2))

    def countEqual(first: Seq[String], second: Seq[String]): Int = {

      def helper(total: (Seq[String], Seq[String]), acc: Int): Int = {
        (total) match {
          case (Seq(), Seq())                                   => acc
          case (seq1 :+ a1, seq2 :+ a2) if (a1.size == a2.size) =>
            //case (seq1, seq2) if ((seq1.head).size == (seq2.head).size) =>
            println("a1: " + seq1.head)
            helper((seq1.tail, seq2.tail), acc + 1)
          case (a1 +: seq1, a2 +: seq2) => helper((seq1, seq2), acc)
        }
      }
      helper((first, second), 0)
    }
  }
}
