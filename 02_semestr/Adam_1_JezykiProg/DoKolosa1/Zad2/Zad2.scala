object XYZ {
  def main(args: Array[String]) {

    println("Podaj indeks wyrazu ktory chcesz uzyskac:")
    var n = io.StdIn.readInt()

    println("Wynik to: " + ciag(n))

  }
  def ciag(n: Int): Int = {
    if (n == 0 || n == 1) {
      return 1
    } else {
      return (ciag(n - 1) + ciag(n - 2))
    }
  }
}