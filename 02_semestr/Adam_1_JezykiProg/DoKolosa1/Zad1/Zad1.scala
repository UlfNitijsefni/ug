object main {
  def main(args: Array[String]) {

    var do_testu = "abcdef"
    var wynik = ""

    println("Wynik to: " + swapper(do_testu))

    def swapper(to_swap: String): String = {

      if (to_swap.isEmpty) {
        ""
      } else {
          swapper(to_swap.tail) + to_swap.head
      }
    }
  }
}
