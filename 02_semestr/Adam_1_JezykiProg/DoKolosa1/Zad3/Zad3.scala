object XYZ {
  def main(args: Array[String]) {

    println("Podaj wymagana dlugosc tablicy: ")
    var n = io.StdIn.readInt

    println("Podawaj po jednym napisy ktore maja sie znalezc w tablicy: ")

    var to_be_read = new Array[String](n)

    read_sort(to_be_read, n)
    print_this(to_be_read)

//------------------------------------------------------------------------
    def print_this(to_be_read: Array[String]) {
      for (i <- 0 to (to_be_read.size) - 1) {
        println("Wyraz " + i + " : " + to_be_read(i))
      }
    }

//------------------------------------------------------------------------
    def read_sort(to_be_read: Array[String], n: Int) {
      var temp = "temp"
      for (i <- 0 to n - 1) {
        to_be_read(i) = io.StdIn.readLine()
      }

      for (i <- 0 to n - 1) {
        for (j <- 0 to n - 2) {
          if (to_be_read(j).size > to_be_read(j + 1).size) {
            temp = to_be_read(j)
            to_be_read(j) = to_be_read(j + 1)
            to_be_read(j + 1) = temp
          }
          if ((to_be_read(j)
                .charAt(0))
                .toInt > (to_be_read(j + 1).charAt(0)).toInt) {
            temp = to_be_read(j)
            to_be_read(j) = to_be_read(j + 1)
            to_be_read(j + 1) = temp
          }
        }
      }
    }
  }
}
