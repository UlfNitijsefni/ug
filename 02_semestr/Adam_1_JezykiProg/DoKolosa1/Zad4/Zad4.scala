object XYZ {
  def main(args: Array[String]) {

    println("Podaj liczbe ktorej pierwszosc chcesz sprawdzic: ")
    var n = io.StdIn.readInt

    if (prime(n)) {
      println("Liczba jest pierwsza.")
    } else {
      println("Liczba ma inne dzielniki poza 1.")
    }

//------------------------------------------------------------------------
    def prime(n: Int): Boolean = {
      def helper(n: Int, divider: Int): Boolean = {
        if (divider == 1) {
          true
        } else if (n % divider == 0) {
          false
        } else {
          helper(n, divider - 1)
        }
      }
      helper(n, n / 2)
    }
  }
}
