object Xyz {
  def main(args: Array[String]) {

    println("Number of ones: " + binaryOnes(12123456))

    def binaryOnes(a: Int): Int = {
      var Temp = a.toBinaryString
      var ones = 0

      for (i <- 0 to Temp.length - 1) {
        if (Temp.charAt(i) == '1') {
          ones = ones + 1
        }
      }
      ones
    }
  }
}
