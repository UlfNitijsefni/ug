object Lynx {
  def main(args: Array[String]) {

    var doTestow: Seq[Int] = Seq(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)

    println("Wynik to: " + halverAt2[Int](doTestow))
    println("Wynik to: " + halverAt1[Int](doTestow))

    def halverAt2[A](seq: Seq[A]): Seq[A] = {
      if (seq.size > 1) {
        seq.tail.head +: halverAt2(seq.tail.tail)
      } else {
        Seq.empty
      }
    }

    def halverAt1[A](seq: Seq[A]): Seq[A] = {
      if (seq.tail.size > 1) {
        seq.head +: halverAt1(seq.tail.tail)
      } else if (seq.tail.size == 0) {
        seq.head +: Seq.empty
      } else {
        Seq.empty
      }
    }

  }
}
