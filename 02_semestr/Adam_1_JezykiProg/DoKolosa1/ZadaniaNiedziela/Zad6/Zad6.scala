object EEwEE {
  def main(args: Array[String]) {

    var doTestow: Seq[Int] = Seq(1, 2, 3, 4, 5, 6)

    println(compute[Int, Int](doTestow)(0)(_ + _))

    def compute[A, B](seq: Seq[A])(init: B)(op: (A, B) => B): B = {
      if (seq.size != 0) {
        op(seq.head, compute(seq.tail)(init)(op))
      } else {
        init
      }
    }
  }
}
