object Kek {
  def main(args: Array[String]) {

    println("Podaj wyraz do zakodowania:")
    var original = io.StdIn.readLine
    println("Podaj klucz kodujacy:")
    var keyMain = io.StdIn.readLine

    var theCode = Array.ofDim[Char](27, 27)

    for (j <- 0 to 26) {
      for (i <- 0 to 26) {
        var temp = 97 + ((i + j) % 26)
        theCode(j)(i) = temp.toChar
      }
    }

    /*for (j <- 0 to 26) {
      for (i <- 0 to 26) {
        print(theCode(j)(i) + " ")
      }
      println("")
    }*/

    println("Przed kodowaniem: " + original)
    println("Po kodowaniu: " + cypher(original, keyMain, theCode))

    def cypher(toCode: String,
               key: String,
               theCode: Array[Array[Char]]): String = {
      var Wynik = ""
      var toMod = key.length
      for (i <- 0 to ((toCode.size) - 1)) {
        Wynik = Wynik + theCode(i)(i % toMod)
      }
      Wynik
    }
  }
}
