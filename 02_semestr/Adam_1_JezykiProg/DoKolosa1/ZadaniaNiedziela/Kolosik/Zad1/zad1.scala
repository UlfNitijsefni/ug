object Workingpls {
  def main(args: Array[String]) {

    def checkAll[A](a: Seq[A])(pred: (A) => Boolean): Boolean = {
      a match {
        case Seq()                      => true
        case (a1 +: seq) if (pred(a1))  => checkAll(seq)(pred)
        case (a1 +: seq) if (!pred(a1)) => false
      }
    }
    var doTestow = Seq(3, 145678, 5, 6, 6, 8)

    println("Wynik to: " + checkAll(doTestow)(_ > 2))
  }
}
