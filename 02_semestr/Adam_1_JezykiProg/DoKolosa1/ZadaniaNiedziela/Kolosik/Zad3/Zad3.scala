object Free10$ {
  def main(args: Array[String]) {

    def merge[A](a: Seq[A], b: Seq[A])(leq: (A, A) => Boolean): Seq[A] = {
      def helper[A](pair: (Seq[A], Seq[A]))(leq: (A, A) => Boolean): Seq[A] = {
        pair match {
          case (Seq(), Seq())      => Seq()
          case (a1 +: seq1, Seq()) => a1 +: seq1
          case (Seq(), a2 +: seq2) => a2 +: seq2
          case (a1 +: seq1, a2 +: seq2) if (leq(a1, a2)) =>
            a1 +: helper((seq1, a2 +: seq2))(leq)
          case (a1 +: seq1, a2 +: seq2) if (!leq(a1, a2)) =>
            a2 +: helper((a1 +: seq1, seq2))(leq)
        }
      }
      helper((a, b))(leq)
    }
    var doTestow1 = Seq(1, 2, 3, 4, 4, 5, 6, 7)
    var doTestow2 = Seq(-5, 0, 1, 4, 4, 6, 9, 150, 200)

    println("Wynik to: " + merge(doTestow1, doTestow2)(_ < _))
  }
}
