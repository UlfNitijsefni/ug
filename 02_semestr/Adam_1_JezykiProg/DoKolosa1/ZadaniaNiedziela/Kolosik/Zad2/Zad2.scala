object WorkingAsIntended {
  def main(args: Array[String]) {

    def insertInto[A](a: A, seq: Seq[A])(leq: (A, A) => Boolean): Seq[A] = {
      def helper[A](a: A, seq: Seq[A], isLong: Boolean)(
          leq: (A, A) => Boolean): Seq[A] = {
        seq match {
          case Seq() if (isLong == false)   => a +: Seq()
          case Seq() if (isLong == true)    => Seq()
          case (a1 +: seq1) if (leq(a1, a)) => a +: seq
          case (a1 +: seq1) if (!leq(a1, a)) =>
            a1 +: helper(a, seq1, isLong)(leq)
        }
      }
      seq.size match {
        case 0 => helper(a, seq, false)(leq)
        case _ => helper(a, seq, true)(leq)
      }
    }
    var doTestow = Seq(1, 2, 3, 4, 5, 6)
    var doTestow2 = Seq('a', 'b', 'c', 'd', 'e')

    println("Wynik to: " + insertInto(3, doTestow)(_ > _))
    println("Wynik to: " + insertInto('c', doTestow2)(_ == _))

    var cc = Seq(1, 2, 5, 6)
    var dd = Seq('d', 'e')

    println("Wynik to: " + insertInto(3, cc)(_ > _))
    println("Wynik to: " + insertInto('c', dd)(_ == _))
  }
}
