object XYZ {
  def main(args: Array[String]) {

    var doTestow: Seq[Int] = Seq(1, 2, 3, 4, 5)
    var doTestow2: Seq[Char] = Seq('a', 'b', 'c')

    println("Rekurencja ogonowa: " + sizeFront[Int](doTestow))

//--------------------------------------------------------
    def sizeFront[A](a: Seq[A]): Int = {
      if (a.size != 0) {
        sizeFront(a.tail) + 1
      } else {
        0
      }
    }

//--------------------------------------------------------
    def sizeTail[A](a: Seq[A]): Int = {
      if (a.size == 0) {
        0
      } else {
        1 + sizeTail(a.tail)
      }
    }
  }
}
