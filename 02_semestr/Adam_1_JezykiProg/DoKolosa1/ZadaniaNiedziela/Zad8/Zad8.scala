object Twix {
  def main(args: Array[String]) {

    def longestRising[Int](tested: Seq[Int],
                           longest: Int,
                           current: Int,
                           start: Int): Seq[Int] = {
      if (tested.size == 1) {
        return 1
      }
      if (tested.head < tested.tail.head) {
        if (current + 1 > longest) {
          longestRising(tested.tail, current + 1, current + 1, )
        }
      }
    }
  }
}
