object UwU {
  def main(args: Array[String]) {

    var doTestow: Seq[Int] = Seq(1, 2, 3, 4)

    if (checkAll[Int](doTestow)(x => x > 0)) {
      println("Predykat spełniony.")
    } else {
      println("Predykat nie jest spełniony")
    }

    def checkAll[A](a: Seq[A])(pred: (A) => Boolean): Boolean = {
      if (a.length == 0) {
        true
      } else if (pred(a.head)) {
        checkAll(a.tail)(pred)
      } else {
        false
      }
    }
  }
}
