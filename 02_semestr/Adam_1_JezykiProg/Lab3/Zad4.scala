object Main {
  def main(args: Array[String]) {

    println("Podaj liczbę do sprawdzenia na bycie pierwszą:")
    var podana = io.StdIn.readInt

    if (podana < 2) {
      println("Liczba nie spełnia warunków bycia pierwszą.")
    } else if (pierwsza(podana, podana - 1) == true) {
      println("Liczba jest pierwsza.")
    } else {
      println("Liczba nie spełnia warunków bycia pierwszą.")
    }

    def pierwsza(n: Int, dziel: Int): Boolean = {
      if (dziel == 1) {
        true
      } else if (n % dziel != 0) {
        pierwsza(n, dziel - 1)
      } else {
        false
      }

    }
  }
}
