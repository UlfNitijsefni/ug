object Main {
  def main(args: Array[String]) {

    println("Który wyraz ciągu chcesz uzyskać?")
    var wyraz = io.StdIn.readInt

    println("Ten wyraz ma wartosc " + ciag(wyraz))

//---------------------------------------------------
    def ciag(n: Int): Int = {
      if (n == 1 || n == 0) {
        1
      } else {
        var wynik = 1
        wynik = ciag(n - 1) + ciag(n - 2)
        wynik
      }
    }
  }
}
