object Main {
  def main(args: Array[String]) {

    var doTestow: Seq[Int] = Seq(1, 3, 5, 7, 9)
    var Wynik: Seq[Int] = Seq.empty
    Wynik ++ insertInto[Int](5, doTestow)(x, y => x > y)

    println("doTestow: " + doTestow.mkString("[", ", ", "]"))

    //var doTestow2 = seq.Empty
    def insertInto[A](a: A, seq: Seq[A])(leq: (A, A) => Boolean): Seq[A] = {
      if (leq(seq.head, a)) {
        a +: seq
      } else {
        insertInto[A](a, seq.tail)(leq)
      }
    }
  }
}
