object Main {
  def main(args: Array[String]) {

    var doTestow: Seq[Int] = Seq(1, 3, 3, 7, 7, 9)

    var Wynik: Seq[Int] = Seq.empty
    Wynik = Wynik ++ deStutter[Int](doTestow)

    println("Wynik : " + Wynik.mkString("[", ", ", "]"))

    def deStutter[A](seq: Seq[A]): Seq[A] = {
      if ((seq.tail).isEmpty) {
        seq
      } else if (seq.head == (seq.tail).head) {
        seq.head +: deStutter((seq.tail).tail)
      } else {
        seq.head +: deStutter(seq.tail)
      }
    }
  }
}
