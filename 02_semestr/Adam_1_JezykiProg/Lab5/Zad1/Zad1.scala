object Main {
  def main(args: Array[String]) {

    var doTestow: Seq[Int] = Seq(1, 3, 7, 9)
    var doTestow2: Seq[Char] = Seq(a, b, c, d)
    var Wynik: Seq[Int] = Seq.empty
    var Wynik2: Seq[Char] = Seq.empty

    Wynik2 = Wynik2 ++ insertInto[Int](5, doTestow2)((x, y) => x = y)

    println("Wynik : " + Wynik.mkString("[", ", ", "]"))

    def insertInto[A](a: A, wewSeq: Seq[A])(leq: (A, A) => Boolean): Seq[A] = {

      def Inserter[A](a: A, wewSeq: Seq[A], i: Int)(
          leq: (A, A) => Boolean): Seq[A] = {
        if (leq(wewSeq.head, a)) {
          a +: wewSeq
        } else {
          wewSeq.head +: Inserter[A](a, wewSeq.tail, i + 1)(leq)
        }
      }
      Inserter[A](a, wewSeq, 0)(leq)
    }
  }
}
