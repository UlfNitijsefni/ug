object Zad1 {
  def main(args: Array[String]) {

    def pairPosNeg(seq: Seq[Double]): (Seq[Double], Seq[Double]) = {
      seq.filter(_ != 0).partition(_ > 0)
    }
    var doTestow = Seq(1, -2, 0.0, -9.13234, 12.31, 0, 0.001)
    println("Wynik to: " + pairPosNeg(doTestow))
  }
}
