object Zad2 {
  def main(args: Array[String]) {

    def deStutter[A](seq: Seq[A]): Seq[A] = {

      seq.foldLeft(Seq[A]())(
        (curr: Seq[A], next: A) =>
          if (curr == Seq()) Seq(next)
          else if (curr.last == next) curr
          else curr :+ next
      )
    }

    var doTestow = io.StdIn.readLine

    //var doTestow = Seq(1, 1, 1, 2, 3, 4, 4, 3, 3, 1, 1, 2, 2, 2)
    println("Wynik to: " + deStutter(doTestow))
  }
}
