object Zad3 {
  def main(args: Array[String]) {

    println("Podaj string:")
    var doTestow = io.StdIn.readLine

    println("Podaj indeks usuwanego elementu (indeksując od 0)")
    var k = io.StdIn.readInt

    def remElems[A](seq: Seq[A], k: Int): Seq[A] = {
      seq.zipWithIndex.filter(_._2 != k).map(k => k._1)
    }

    if (k < 0) println("Podałeś niemożliwy indeks")
    else println("Wynik to: " + remElems(doTestow, k))
  }
}
