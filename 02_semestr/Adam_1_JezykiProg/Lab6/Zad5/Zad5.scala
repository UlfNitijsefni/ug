object Main {
  def main(args: Array[String]) {

    //var doTestow: Seq[Char] = Seq('a', 'b', 'c', 'b', 'a', 'a', 'd')
    var doTestow: Seq[Int] = Seq(1, 2, 1, 3, 3, 3, 2, 2, 2)

    println("Wynik to: " + freq[Int](doTestow))

    def freq[A](seq: Seq[A]): Map[A, Int] = {
      (seq
        .groupBy(x => x))
        .map((n: (A, Seq[A])) => (n._1, (n._2).size))
    }
  }
}
