object Main {
  def main(args: Array[String]) {

    // solution
    // --------------------------------------------------------------------------
    def remElems[A](seq: Seq[A], k: Int): Seq[A] = {
      var indexed = seq.zipWithIndex
      var afterFilter = indexed.filter((n: (A, Int)) => n._2 != k)
      afterFilter.map((n: (A, Int)) => n._1)
    }

    // tests
    // --------------------------------------------------------------------------
    var doTestow: Seq[Double] = Seq(0.1, -2, 0, 7, 9, -1.1, -3, 15)
    var doTestow2: Seq[Char] = Seq('a', 'v', 'b', 'c', 'd', 'e', 'f', 'g', 'h')
    var doTestow3: Seq[String] = Seq("xyz", "zzz", "asdz")

    var Wynik: Seq[Double] = Seq.empty

    println("Test1: " + doTestow + " 5")
    println("Test1: " + remElems[Double](doTestow, 5) + "\n")

    println("Test1: " + doTestow + " 1")
    println("Test2: " + remElems[Double](doTestow, 1) + "\n")

    println("Test1: " + doTestow2 + " 5")
    println("Test3: " + remElems[Char](doTestow2, 5))

    println("Test1: " + doTestow2 + " 1")
    println("Test4: " + remElems[Char](doTestow2, 1))

    println("Test1: " + doTestow3 + " 5")
    println("Test3: " + remElems[String](doTestow3, 5))

    println("Test1: " + doTestow3 + " 1")
    println("Test4: " + remElems[String](doTestow3, 1))

  }
}
