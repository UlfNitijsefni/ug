object Main {
    def main(args: Array[String]) {

        var doTestow: Seq[Int] = Seq(1, 3, 5, 7, 9, 11, 13, 15)
        var Wynik: Seq[Int] = Seq.empty

        Wynik = subseq[Int](doTestow, 2, 5)

        println("Wynik to: " + Wynik.mkString("[", ", ", "]"))

        def subseq[A](seq: Seq[A], begIdx: Int, endIdx: Int): Seq[A] = {
            seq.take(endIdx).drop(begIdx)
        }
    }
}