object Main {
  def main(args: Array[String]) {

    var doTestow: Seq[Double] = Seq(0.1, -2, 0, 7, 9, -1.1, -3, 15)
    var Wynik: Seq[Double] = Seq.empty

    println("Wynik to: " + pairPosNeg(doTestow))

    def pairPosNeg(seq: Seq[Double]): (Seq[Double], Seq[Double]) = {
      seq.filter(n => n != 0).partition(n => n>0)
    }
  }
}
