object Main {
  def main(args: Array[String]) {

    //type Pred[A] = A => Boolean
    type MSet[A] = A => Int
    //var x = 12

    // Przez6(x)

    var a: MSet[Int] = (n) => n % 3
    var b: MSet[Int] = (n) => n % 2

    // plus[Int](a, b)

    def plus[A](s1: MSet[A], s2: MSet[A]): MSet[A] = {
      s1
    }

    // def imp[A](p: Pred[A], q: Pred[A]): Pred[A] = { m =>
    //   !(p(m) && !q(m))
    // }

    // def Przez6(n: Int): Unit = {
    //   if (imp[Int](n => (n % 2 != 0), n => (n % 4 == 0))(x)) {
    //     println("Liczba nie dzieli sie przez 2")
    //   }
    // }
  }
}
