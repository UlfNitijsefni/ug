object Main {
  def main(args: Array[String]) {

    var doTestow: Seq[Int] = Seq(1, 2, 3, 4, 5)
    var i = 0
    var dlugosc = 0
    var powiekszana: Seq[Int] = doTestow

    println("Dlugosc podanej sekwencji = " + size2[Int](doTestow, dlugosc))

//------------------------------------------------------------
    def size1[A](a: Seq[A], dlugosc: Int): Int = {
      if (!a.isEmpty) {
        size1[A](a.tail, dlugosc + 1)
      } else {
        dlugosc
      }
    }

//------------------------------------------------------------
    def size2[A](a: Seq[A], dlugosc: Int): Int = {
      if (a.isEmpty) {
        dlugosc
      } else {
        size2[A](a.tail, dlugosc + 1)
      }
    }
  }
}
