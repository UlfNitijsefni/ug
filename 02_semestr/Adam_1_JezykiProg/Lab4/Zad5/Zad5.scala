object Main {
  def main(args: Array[String]) {

    var doTestow: Seq[Int] = Seq(1, 3, 5, 7, 9)
    var doTestow2: Seq[Int] = Seq(1, 2, 5, 7, 9)

    if (forall[Int](doTestow)(n => n % 2 != 0)) {
      println("Wszystkie elementy zbioru spelniaja warunek.")
    } else {
      println("Nie wszystkie elementy zbioru spelniaja warunek.")
    }

//---------------------------------------------------------------------
    def forall[A](a: Seq[A])(pred: (A) => Boolean): Boolean = {
      if (a.isEmpty) {
        a.isEmpty
      } else {
        if (pred(a.head)) {
          forall[A](a.tail)(pred)
        } else {
          a.isEmpty
        }
      }
    }

  }
}
