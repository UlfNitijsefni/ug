object Main {
  def main(args: Array[String]) {

    var doTestow: Seq[Int] = Seq(1, 3, 5, 7, 9)
    var doTestow2: Seq[Int] = Seq(2, 4, 6, 8, 10)

    type Pred[A] = (A, A) => Boolean

    var TestPred: Pred[Int] = (x, y) => x > y
    var Koniec: Seq[Int] = Seq()

    Koniec = merge[Int](doTestow, doTestow2)(TestPred)

    if (Koniec.isEmpty) {
      println("Jestem pusta.")
    } else {
      Koniec.mkString("[", ", ", "]")
      println(Koniec)
    }

//---------------------------------------------------------------
    def merge[A](a: Seq[A], b: Seq[A])(leq: (A, A) => Boolean): Seq[A] = {
      var Temp: Seq[A] = Seq.empty

      if (a.isEmpty) {
        b
      } else if (b.isEmpty) {
        a
      } else if (leq(a.head, b.head)) {
        (Temp :+ a.head) ++ merge(a.tail, b)(leq)
      } else {
        (Temp :+ b.head) ++ merge(a, b.tail)(leq)
      }
    }
  }
}
