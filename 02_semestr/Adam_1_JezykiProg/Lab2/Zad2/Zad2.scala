object Main {
  def main(args: Array[String]) {

    var i = 0
    var dlugosc = 0
    var dluzsza = 0
    var roznica = 0
    var temp = " "
    println("Podaj pierwsza liczbe do dodania:")
    var liczba1 = io.StdIn.readLine
    println("Podaj druga liczbe do dodania:")
    var liczba2 = io.StdIn.readLine

    if (liczba1.length < liczba2.length) {
      dlugosc = liczba1.length
      dluzsza = liczba2.length

      temp = liczba2
      liczba2 = liczba1
      liczba1 = temp
    } else {
      dlugosc = liczba2.length
      dluzsza = liczba2.length
    }
    roznica = dluzsza - dlugosc

    var wynik = new Array[Int](dluzsza + 1)
    var dziesiatki: Int = 0
    var suma: Int = 0

    // -------------------------------------------------------------------------
    i = dlugosc - 1
    while (i >= 0) {
        println("i + roznica = " + (i + roznica) + " i = " + (i))
      suma = (liczba1.charAt(i + roznica).asDigit + liczba2.charAt(i).asDigit + dziesiatki)
      wynik(i + roznica) = suma % 10
      dziesiatki = suma / 10
      i -= 1
    }

    i = roznica - 1

    while (i >= 0) {
      suma = ((liczba1.charAt(i + 1).asDigit) + dziesiatki)
      wynik(i + 1) = suma % 10
      dziesiatki = suma / 10
      i -= 1
    }

    // -------------------------------------------------------------------------
    print("\nWynik to: ")
    for (i <- 0 to dluzsza) {
      print(wynik(i))
    }
    print("\n")
  }
}
