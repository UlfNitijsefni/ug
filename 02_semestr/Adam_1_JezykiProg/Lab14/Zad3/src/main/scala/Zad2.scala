import akka.actor.{ActorSystem, Actor, ActorRef, Props, Terminated, Stash}

case class Wynik(liczba1: Double, liczba2: Double)
case object Zmien

class Pracownik extends Actor {
  def receive: Receive = {
    case Wynik(l1, l2) => println("suma=" + (l1 + l2))
    case Zmien         => context.become(multiplier)
  }
  def multiplier: Receive = {
    case Wynik(l1, l2) => println("iloraz=" + (l1 * l2))
    case Zmien         => context.become(receive)
  }
}

object Main extends App {
  val system = ActorSystem("sys")

  val pracownik = system.actorOf(Props[Pracownik], "pracownik")
  pracownik ! Wynik(1, 2)
  pracownik ! Zmien
  pracownik ! Wynik(2, 3)
  pracownik ! Zmien
  pracownik ! Wynik(3, 4)
}
