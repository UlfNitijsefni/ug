import akka.actor.{ActorSystem, Actor, ActorRef, Props, Terminated, Stash}

class Nadzorca extends Actor with Stash {
  def receive: Receive = {
    case Odbierz(message) =>
      if (message == "zmien") {
        context.become(printer)
        unstashAll()
      } else stash()
  }

  def printer: Receive = {
    case Odbierz(message) =>
      println("Wczesniej bylo: " + message)
  }
}

object Main extends App {

  val system = ActorSystem("sys")
  val Nadz = system.actorOf(Props[Nadzorca], "Nadzorca")

  Nadz ! Odbierz("Dzialam1")
  Nadz ! Odbierz("Dzialam2")
  Nadz ! Odbierz("Dzialam3")
  Nadz ! Odbierz("zmien")
}
