import akka.actor.{ActorSystem, Actor, ActorRef, Props, Terminated}

object MyNadz {
  case class Init(liczbaPracownikow: Int)
  case class Zlecenie(tekst: List[String]) 
  case class Wynik(count: Int)
}

object MyWork {
    case class Wykonaj(tekst: String)
}

class Nadzorca extends Actor {
  import MyNadz._

  def receive: Receive = {

    def createWork(ile: Int, acc: Set[ActorRef]) = {
      ile match {
        case _ if ile <= 0 => acc
        case _ =>
          val work = context.actorOf(Props[Pracownik], s"pracownik${acc.size}")
          context.watch(work)
          createWork(ile - 1, acc + work)
      }
    }
    case Init(liczbaPracownikow: Int) =>
        val works = context.become(getOrder(createWork(liczbaPracownikow, Set())))
  }
  def getOrder(works: Set[ActorRef]): Receive = {
      def sendOrders(tekst: Seq[String], works Seq[ActorRef]): List[String] = {
          (tekst, works) match {
              case (lista, Seq()) =>
                lista
            case (t1 +: tekst, w1 :+ wTail) =>
                w1 ! Wykonaj(t1)
                sendOrders(tTail, wTail)
          }
      }

      case Zlecenie(tekst: List[String]) =>
        val lista = sendOrders(tekst, works.toSeq)
        context.become(getResult(lista, 0))
            val suma2 = suma + count
  }
    def getResult(works: Set[Actorref], listaL List[String], suma: Int) =>

        def checkList(lista: List[String], wynik: Int): Unit =>
            lista match = {
                case Seq() => 
                case (l1 +: tail, _)=>
                    sender() ! Wykonaj(l1)
                    context.become(checkList(lTail, wynik))
            }

            case Wynik(count: Int) =>
                val suma2 = suma + count
                checkList(lista)

            case Terminated(work) =>
                val ws = works - work
                context.become(getResult(ws, lista, wynik))
                if(ws.isEmpty()) {
                    println("Suma słów wynosi: " + wynik)
                    
                }
    //case wynik(count: Int) =>
    //    suma += count
}



class Pracownik extends Actor {

    import Mywork._

    def receive: Receive = {
        case Wykonaj(tekst: String) =>
            val count = tekst.split(" ").filter(_ != "").size
            sender() ! Wynik(count)
    }
}

object Main extends App {
  def dane(): List[String] = {
    var tekst = scala.io.Source.fromResource("ogniem_i_mieczem.txt").getLines.toList
  }

  val system = Actorsystem("sys")
  val szef = actorOf.system(Props[MyNadz], "Nadzorca")

  //val
  //val
}
