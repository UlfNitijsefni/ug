import akka.actor.{Actor, ActorRef, ActorSystem, Stash, Props}

//----------------------------------------------------
case class Tworz(n: Int)
case class Work(dane: Seq[String])
case class Opracuj(a: String)
case object DajWynik
case class Wynik(n: Int)

//----------------------------------------------------
class Nadzorca extends Actor with Stash {
  var wynik = 0
  var nLinii: Int = 0

  def receive: Receive = {
    case Tworz(n) =>
      val actors = helper(Seq.empty[ActorRef], n)
      val daneSeq = dane()
      nLinii = daneSeq.length
      sumator(Seq(), actors, daneSeq)
      context.become(obliczWynik)
      unstashAll()
    case Wynik(n) =>
      stash()
  }

  def obliczWynik: Receive = {
    case Wynik(n) =>
      println(".. aktualny wynik = " + (wynik + n))
      wynik = wynik + n
      nLinii = nLinii - 1
      if (nLinii == 0)
        println("\nLiczba wszystkich slow to: " + wynik)

  }

  def helper(actorSeq: Seq[ActorRef], n: Int): Seq[ActorRef] = {
    n match {
      case 0 => actorSeq
      case _ => helper(actorSeq :+ context.actorOf(Props[Pracownik]), n - 1)
    }
  }

  def sumator(used: Seq[ActorRef],
              toUse: Seq[ActorRef],
              lines: Seq[String]): Unit = {
    (lines, toUse) match {
      case (Seq(), _) => return
      case (_, Seq()) => sumator(toUse, used, lines)
      case (x1 +: tail, a1 +: rest) =>
        a1 ! Opracuj(x1)
        sumator(used :+ a1, rest, tail)
    }
  }

  def dane(): Seq[String] = {
    scala.io.Source
      .fromResource("ogniem_i_mieczem.txt")
      .getLines
      .toList
      .toSeq
  }
}

//----------------------------------------------------
class Pracownik extends Actor {
  def receive: Receive = {
    case Opracuj(toGo) =>
      context.parent ! Wynik(toGo.trim.split(" ").length)
  }
}

//----------------------------------------------------
object mainLinie extends App {

  val system = ActorSystem("systemik")
  val nadzorca = system.actorOf(Props[Nadzorca], "nadzorca")

  nadzorca ! Tworz(3)
}
