package jp.lab13

sealed trait Płeć
case object M extends Płeć
case object K extends Płeć

object Gen {
  import scala.io.Source
  import scala.util.Random
  private val rand = new Random

  def osoba: Osoba = ???
  def osoba(płeć: Płeć): Osoba = ???
  def ocena: Ocena = ???
}
