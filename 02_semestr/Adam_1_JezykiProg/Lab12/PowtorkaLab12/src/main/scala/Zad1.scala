//package jp.aktorzy.monitoring

import akka.actor.{
  ActorSystem,
  Actor,
  ActorRef,
  Props,
  Terminated,
  ActorLogging,
  PoisonPill,
  Stash
}

import scala.util.Random

object Bibliotekarz {
  case class Tworz(n: Int)
  case class Terminated(actor: ActorRef)
  case object Start
  case object Lubie
  case object NieLubie
}

//----------------------------------------------------
class Bibliotekarz extends Actor /*with ActorLogging*/ with Stash {
  import Bibliotekarz._

//----------------------------------------------------
  def receive: Receive = {
    case Tworz(n) =>
      val czytelnicy = makeActors(n, Seq.empty[ActorRef])
      val ksiazki = makeBooks(n, Seq.empty[ActorRef])
      context.become(dawajKsiazki(czytelnicy, ksiazki, 995))
      unstashAll()
      self ! Bibliotekarz.Start
    case Terminated(actor) => stash()
  }

//----------------------------------------------------
  def dawajKsiazki(readers: Seq[ActorRef],
                   books: Seq[ActorRef],
                   left: Int): Receive = {
    case Start =>
      (left, readers, books) match {
        case (0, _, Seq()) => println("Skończyły się książki.")
        case (_, Seq(), _) => println("Koniec czytelników.")
        case (_, a1 +: seq, b1 +: beq) =>
          a1 ! Czytelnik.Ksiazka(1)
          context.become(dawajKsiazki(readers, books.filter(_ != b1), left - 1))
      }
    case Terminated(actor) =>
      readers.size match {
        case 1 => println("Koniec czytelnikow")
        case _ =>
          context.become(dawajKsiazki(readers.filter(_ != actor), books, left))
      }
  }

//----------------------------------------------------
  def makeActors(n: Int, actors: Seq[ActorRef]): Seq[ActorRef] = {
    n match {
      case 0 => actors
      case _ =>
        val newActor = context.actorOf(Props[Czytelnik])
        context.watch(newActor)
        makeActors(n - 1, actors :+ newActor)
    }
  }

//----------------------------------------------------
  def makeBooks(n: Int, actors: Seq[ActorRef]): Seq[ActorRef] = {
    n match {
      case 0 => actors
      case _ => makeBooks(n - 1, actors :+ context.actorOf(Props[Ksiazka]))
    }
  }
}

//----------------------------------------------------
object Ksiazka {}

class Ksiazka extends Actor {
  def receive: Receive = {
    case _ => println("Dzialam")
  }
}

//----------------------------------------------------
object Czytelnik {
  case class Ksiazka(n: Int)
}

//----------------------------------------------------
class Czytelnik extends Actor /*with ActorLogging*/ {
  import Czytelnik._

//----------------------------------------------------
  def receive: Receive = {
    case Ksiazka(n) =>
      val lubie = Random.nextInt(1)
      lubie match {
        case 1 => sender ! Bibliotekarz.Lubie
        case 0 =>
          sender ! Bibliotekarz.NieLubie
          self ! PoisonPill
      }
  }
}

//----------------------------------------------------

object main extends App {
  val system = ActorSystem("sys")

  val bibliotekarz = system.actorOf(Props[Bibliotekarz], "szef")

  bibliotekarz ! Bibliotekarz.Tworz(12)
}
/*
object Nadzorca {
  case class Init(n: Int)
  case class Wynik(n: Int)
  case object Pokaz
}
//----------------------------------------------------
class Nadzorca extends Actor with Stash with ActorLogging {
  import Nadzorca._

  def receive: Receive = {
    case Init(n) =>
      val aktorzy = makeActors(n, Seq.empty[ActorRef])
      val dane = readFile()
      counter(Seq.empty[ActorRef], aktorzy, dane)
      aktorzy.foreach(_ ! PoisonPill)
      context.become(waitingForResult(0, aktorzy))
      unstashAll()
    case Wynik(n: Int) => stash()
  }

  def waitingForResult(wynik: Int, aktorzy: Seq[ActorRef]): Receive = {
    case Terminated(actor) =>
      aktorzy.size match {
        case 1 =>
          self ! Nadzorca.Pokaz
        case _ =>
          context.become(waitingForResult(wynik, aktorzy.filter(_ != actor)))
      }
    case Wynik(n) =>
      context.become(waitingForResult(wynik + n, aktorzy))
    case Pokaz => println("Wynik końcowy: " + wynik)
  }

//----------------------------------------------------
  def counter(used: Seq[ActorRef],
              toUse: Seq[ActorRef],
              linesToGo: Seq[String]): Unit = {
    (toUse, linesToGo) match {
      case (_, Seq()) => return
      case (Seq(), _) => counter(toUse, used, linesToGo)
      case (worker +: usable, line +: toCountLines) =>
        context.watch(worker)
        worker ! Pracownik.Work(line)
        counter(used :+ worker, usable, toCountLines)
    }
  }

//----------------------------------------------------
  def makeActors(n: Int, actors: Seq[ActorRef]): Seq[ActorRef] = {
    n match {
      case 0 => actors
      case _ => makeActors(n - 1, actors :+ context.actorOf(Props[Pracownik]))
    }
  }

  //----------------------------------------------------
  def readFile(): Seq[String] = {
    scala.io.Source
      .fromResource("ogniem_i_mieczem.txt")
      .getLines
      .toList
      .toSeq
  }
}

object Pracownik {
  case class Work(line: String)
}

class Pracownik extends Actor with ActorLogging {
  import Pracownik._

  def receive: Receive = {
    case Work(line) => sender ! Nadzorca.Wynik(line.trim.split(" ").size)
  }
}*/
