//package jp.aktorzy.monitoring

import akka.actor.{
  ActorSystem,
  Actor,
  ActorRef,
  Props,
  Terminated,
  ActorLogging,
  PoisonPill,
  Stash
}

import scala.util.Random

object Bibliotekarz {
  case class Init(n: Int)
  case object Recenzja
  case object Dawaj
}

//----------------------------------------------------
class Bibliotekarz extends Actor with Stash with ActorLogging {
  import Bibliotekarz._

  def receive: Receive = {
    case Init(n) =>
      val readers = makeActors(n, Seq.empty[ActorRef])
      val books = makeBooks(n, Seq.empty[ActorRef])
      context.become(giveBooks(Seq(), readers, books, 995))
      self ! Dawaj
      unstashAll()
    case Terminated(n) =>
      stash()
      println("Terminuję")
    case Recenzja => stash()
  }

//----------------------------------------------------
  def giveBooks(readersUsed: Seq[ActorRef],
                readersLeft: Seq[ActorRef],
                books: Seq[ActorRef],
                left: Int): Receive = {
    case Dawaj =>
      left match {
        case 0 => println("Koniec ksiazek")
        case _ =>
          readersLeft match {
            case Seq() => println("Koniec czytelnikow")
            case a1 +: seq =>
              a1 ! Czytelnik.Czytaj
              context.become(
                giveBooks(readersUsed :+ a1,
                          readersLeft.tail,
                          books :+ context.actorOf(Props[Ksiazka]),
                          left - 1))
          }
      }
    case Terminated(n) =>
      readersLeft match {
        case Seq() => println("Ostatni czytelnik umarł")
        case _ =>
          context.become(
            giveBooks(readersUsed :+ readersLeft.head,
                      readersLeft.tail /*filter(_ != n)*/,
                      books,
                      left))
      }
    case Recenzja =>
      readersLeft match {
        case Seq() =>
          println("Koniec czytleników recenzujących")
        case _ =>
          context.become(
            giveBooks(readersUsed :+ readersLeft.head,
                      readersLeft.tail,
                      books.tail,
                      left - 1))
          self ! Dawaj
      }
  }

//----------------------------------------------------
  def makeActors(n: Int, actors: Seq[ActorRef]): Seq[ActorRef] = {
    n match {
      case 0 => actors
      case _ =>
        val actor = context.actorOf(Props[Czytelnik])
        context.watch(actor)
        makeActors(n - 1, actors :+ actor)
    }
  }

//----------------------------------------------------
  def makeBooks(n: Int, actors: Seq[ActorRef]): Seq[ActorRef] = {
    n match {
      case 0 => actors
      case _ => makeActors(n - 1, actors :+ context.actorOf(Props[Ksiazka]))
    }
  }

}
//----------------------------------------------------

//====================================================
object Czytelnik {
  case object Czytaj
}

class Czytelnik extends Actor with ActorLogging {
  import Czytelnik._

  def receive: Receive = {
    case Czytaj =>
      val opinia = Random.nextInt(2)
      opinia match {
        case 1 =>
          self ! PoisonPill
          sender ! Bibliotekarz.Recenzja
        case 0 =>
          sender ! Bibliotekarz.Recenzja
      }
  }
}

//====================================================
object Ksiazka {
  case object lubie
}

class Ksiazka extends Actor {
  def receive: Receive = {
    case lubie => println("Lubie")
  }
}

//====================================================
object main extends App {
  val system = ActorSystem("sys")
  val bibliotekarz = system.actorOf(Props[Bibliotekarz], "szef")

  bibliotekarz ! Bibliotekarz.Init(10)
}
