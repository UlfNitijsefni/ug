package jp.aktorzy.monitoring

import akka.actor.{
  ActorSystem,
  Actor,
  ActorRef,
  Props,
  Terminated,
  ActorLogging,
  PoisonPill,
  Stash
}

object Overseer {
  case class Init(n: Int)
  case class Result(n: Int)
  case object Print
}

class Overseer extends Actor with Stash with ActorLogging {
  import Overseer._

  def receive: Receive = {
    case Init(n) =>
      val actors = makeActors(n, Seq.empty[ActorRef])
      val given = readFile()
      additioner(Seq.empty[ActorRef], actors, given)
      context.become(waitingForResult(0, actors))
      unstashAll()
      actors.foreach(_ ! PoisonPill)
    case Result(n) => stash()
  }

//----------------------------------------------------
  def waitingForResult(result: Int, actors: Seq[ActorRef]): Receive = {
    case Result(n) => context.become(waitingForResult(result + n, actors))
    case Print     => println("Final result: " + result)
    case Terminated(actor) =>
      actors.size match {
        case 1 => self ! Print
        case _ =>
          context.become(waitingForResult(result, actors.filter(_ != actor)))
      }
  }

//----------------------------------------------------
  def additioner(used: Seq[ActorRef],
                 toUse: Seq[ActorRef],
                 linesToGo: Seq[String]): Unit = {
    (toUse, linesToGo) match {
      case (Seq(), _) => additioner(toUse, used, linesToGo)
      case (_, Seq()) => return
      case (actor +: leftoverActors, line +: linesLeft) =>
        context.watch(actor)
        actor ! Worker.Work(line)
    }
  }

//----------------------------------------------------
  def readFile(): Seq[String] = {
    scala.io.Source
      .fromResource("ogniem_i_mieczem.txt")
      .getLines
      .toList
      .toSeq
  }

//----------------------------------------------------
  def makeActors(n: Int, actors: Seq[ActorRef]): Seq[ActorRef] = {
    n match {
      case 0 => actors
      case _ => makeActors(n - 1, actors :+ context.actorOf(Props[Pracownik]))
    }
  }
}

//----------------------------------------------------
object Worker {
  case class Work(toProcess: String)
}

class Worker extends Actor with ActorLogging {
  import Worker._

  def receive: Receive = {
    case Work(toProcess) =>
      sender ! Overseer.Result(toProcess.trim.split(" ").size)
  }
}
//----------------------------------------------------

object main extends App {
  val system = ActorSystem("sys")

  val szef = system.actorOf(Props[Overseer], "szef")

  szef ! Overseer.Init(12)
}
