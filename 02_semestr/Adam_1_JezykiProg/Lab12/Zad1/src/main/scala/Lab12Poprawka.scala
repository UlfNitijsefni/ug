import akka.actor.{
  Actor,
  ActorRef,
  ActorSystem,
  Stash,
  Props,
  PoisonPill,
  Terminated,
  ActorLogging
}

//====================================================
object Nadzorca {
  case class Init(n: Int)
  case class Wynik(n: Int)
  case object Wypisz
}

//----------------------------------------------------
class Nadzorca extends Actor with Stash with ActorLogging {
  val dane: Seq[String] = readFile()

  import Nadzorca._

//----------------------------------------------------
  def receive: Receive = {
    case Init(n) =>
      val pracownicy = makeActors(n, Seq.empty[ActorRef])
      sumator(Seq.empty[ActorRef], pracownicy, dane)
      pracownicy.foreach(_ ! PoisonPill)
      context.become(waitForEffects(0, pracownicy))
      unstashAll()
    case Wynik(n: Int) => stash()
  }

//----------------------------------------------------
  def waitForEffects(wynik: Int, pracownicy: Seq[ActorRef]): Receive = {
    case Wynik(n: Int) =>
      context.become(waitForEffects(wynik + n, pracownicy))
    case Wypisz => println("Wynik końcowy: " + wynik)
    case Terminated(aktor) =>
      pracownicy.size match {
        case 1 =>
          self ! Wypisz
        case _ =>
          context.become(waitForEffects(wynik, pracownicy.filter(_ != aktor)))
      }
  }

//----------------------------------------------------
  def makeActors(n: Int, workers: Seq[ActorRef]): Seq[ActorRef] = {
    n match {
      case 0 => workers
      case _ =>
        makeActors(n - 1, workers :+ context.actorOf(Props[Pracownik]))
    }
  }

//----------------------------------------------------
  def readFile(): Seq[String] = {
    scala.io.Source
      .fromResource("ogniem_i_mieczem.txt")
      .getLines
      .toList
      .toSeq
  }

//----------------------------------------------------
  def sumator(used: Seq[ActorRef],
              toUse: Seq[ActorRef],
              lines: Seq[String]): Unit = {
    (lines, toUse) match {
      case (Seq(), _) => return
      case (_, Seq()) => sumator(toUse, used, lines)
      case (x1 +: linesRest, a1 +: actorsRest) =>
        context.watch(a1)
        a1 ! Pracownik.Opracuj(x1)
        sumator(used :+ a1, actorsRest, linesRest)
    }
  }
}

//====================================================
object Pracownik {
  case class Opracuj(toGo: String)
}

//----------------------------------------------------
class Pracownik extends Actor with ActorLogging {
  import Pracownik._

//----------------------------------------------------
  def receive: Receive = {
    case Opracuj(toGo) =>
      //println("Pracuje nad: " + toGo)
      context.parent ! Nadzorca.Wynik(toGo.trim.split(" ").length)
  }

}

//====================================================
object main extends App {

  val system = ActorSystem("systemik")
  val nadzorca = system.actorOf(Props[Nadzorca], "nadzorca")

  nadzorca ! Nadzorca.Init(3)
}
