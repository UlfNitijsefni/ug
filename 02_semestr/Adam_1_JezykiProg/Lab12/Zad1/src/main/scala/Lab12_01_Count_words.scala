// import akka.actor.{Actor, ActorRef, ActorSystem, Stash, Props}

// //----------------------------------------------------
// case class Tworz(n: Int)
// case class Work(dane: Seq[String])
// case class Opracuj(a: String)
// case object DajWynik
// case class Wynik(n: Int)

// //----------------------------------------------------
// class Nadzorca extends Actor {
//   var wynik = 0
//   def receive: Receive = {
//     case Tworz(n) =>
//       val actors = helper(Seq.empty[ActorRef], n)
//       sumator(Seq(), actors, dane())
//     case Wynik(n) =>
//       println("Akturalny wynik: " + (wynik + n))
//       wynik = wynik + n
//     case DajWynik => println("Suma to: " + wynik)
//   }

//   def helper(actorSeq: Seq[ActorRef], n: Int): Seq[ActorRef] = {
//     n match {
//       case 0 => actorSeq
//       case _ => helper(actorSeq :+ context.actorOf(Props[Pracownik]), n - 1)
//     }
//   }

//   def sumator(used: Seq[ActorRef],
//               toUse: Seq[ActorRef],
//               lines: Seq[String]): Unit = {
//     (lines, toUse) match {
//       case (Seq(), _) => return
//       case (_, Seq()) => sumator(toUse, used, lines)
//       case (x1 +: tail, a1 +: rest) =>
//         a1 ! Opracuj(x1)
//         sumator(used :+ a1, rest, tail)
//     }
//   }

//   def dane(): Seq[String] = {
//     scala.io.Source
//       .fromResource("ogniem_i_mieczem.txt")
//       .getLines
//       .toList
//       .toSeq
//   }
// }

// //----------------------------------------------------
// class Pracownik extends Actor {
//   def receive: Receive = {
//     case Opracuj(toGo) =>
//       context.parent ! Wynik(toGo.trim.split(" ").length)
//   }
// }

// //----------------------------------------------------
// object main extends App {

//   val system = ActorSystem("systemik")
//   val nadzorca = system.actorOf(Props[Nadzorca], "nadzorca")

//   nadzorca ! Tworz(3)
//   Thread.sleep(1000)
//   nadzorca ! DajWynik
// }
