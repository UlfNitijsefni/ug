import akka.actor.{ActorSystem, Actor, ActorRef, Props}

case object Ping
case object Pong

class Boss extends Actor {
  def receive: Receive = {
    case Pong =>
      Thread.sleep(1000)
      println("Pong")
      self ! Ping
    case Ping =>
      Thread.sleep(1000)
      println("Ping")
      self ! Pong
  }
}

class Test extends Actor {
  def receive: Receive = {
    case (1, a: ActorRef) =>
      println(1)
      a ! Ping
    case 1 =>
      println(1 + " bez ponga.")
  }
}

object Main extends App {
  val system = ActorSystem("PongPing")
  val boss = system.actorOf(Props[Boss]) //, "BossName")
  val test1 = system.actorOf(Props[Test]) //, "Test1")

  test1 ! 1
  test1 ! (1, boss)
}
