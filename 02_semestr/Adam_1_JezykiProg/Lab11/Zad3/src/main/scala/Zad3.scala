import akka.actor.{ActorSystem, Actor, ActorRef, Props}
import scala.io.Source

//case class Policz(a: List[String])

class Pracownik extends Actor {
  def receive: Receive = {
    case a: String => println("Wynik")
  }
}

object Main extends App {
  def histogram(max: Int): Unit = {
    val system = ActorSystem("system")
    val given = Source
      .fromFile("ogniem_i_mieczem.txt")
      .getLines
      .toList
      .mkString
      .toLowerCase
      .replaceAll("""[\p{Punct} ]""", "")
      .take(max)

    //println(given)
    def helper(a: Char): Unit = {
      a match {
        case 'z' => println(a)
        case _ =>
          println(a)
          helper((a.toInt + 1).toChar)
      }
    }
    helper('a')
  }
  histogram(10)
}
