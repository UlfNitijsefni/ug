import akka.actor.{Actor, ActorRef, ActorSystem, Props}

case class Start(odbicia: Int, second: ActorRef, finish: ActorRef)
case class Kolejne(odbicia: Int, whatNow: String, finish: ActorRef)
case class Stop()

class Pinger extends Actor {
  def receive: Receive = {
    case Start(odbicia, nextOne, finish) =>
      if (odbicia > 1) {
        Thread.sleep(1000)
        println("Ping")
        nextOne ! Kolejne(odbicia - 1, "Ping", finish)
      } else
        finish ! Stop()
    case Kolejne(odbicia, "Ping", finish) =>
      if (odbicia > 0) {
        Thread.sleep(1000)
        println("Pong")
        sender ! Kolejne(odbicia - 1, "Pong", finish)
      } else
        finish ! Stop()
    case Kolejne(odbicia, "Pong", finish) =>
      if (odbicia > 0) {
        Thread.sleep(1000)
        println("Ping")
        sender ! Kolejne(odbicia - 1, "Ping", finish)
      } else
        finish ! Stop()
  }
}

class Ender extends Actor {
  def receive: Receive = {
    case Stop() => println("Koniec")
  }
}

object Main extends App {
  println("Podaj ilość odbić:")
  var odbicia = io.StdIn.readInt

  val system = ActorSystem("PiongPoing")
  val player1 = system.actorOf(Props[Pinger], "player1")
  val player2 = system.actorOf(Props[Pinger], "player2")
  val ender = system.actorOf(Props[Ender], "ender")

  if (odbicia >= 0 && odbicia < 10)
    player1 ! Start(odbicia, player2, ender)
  else
    println("Niedozwolona ilość odbić")
}
