import akka.actor.{ActorSystem, Actor, ActorRef, Props}

case class Lista(a: String, b: String, c: String, budget: Int, sklep: ActorRef)
case class Zamowienie(a: String, b: String, c: String, budget: Int)
case class Koszt(koszt: Int, budget: Int)

case object Start

class Klient extends Actor {
  def receive: Receive = {
    case Koszt(koszt, budget) =>
      if (koszt > budget) println("Nie stać mnie")
      else println("Stać mnie")
    case Lista(a, b, c, budget, sklep) => sklep ! Zamowienie(a, b, c, budget)
  }
}

class Sklep extends Actor {
  def receive: Receive = {
    case Zamowienie(a, b, c, budget) => sender ! Koszt(12, budget)
  }
}

object Main extends App {

  val system = ActorSystem("System")
  val klient1 = system.actorOf(Props[Klient], "klient1")
  val klient2 = system.actorOf(Props[Klient], "klient2")
  val sklep = system.actorOf(Props[Sklep], "sklep")

  klient1 ! Lista("xz", "xzcf", "jaaj", 13, sklep)
  klient2 ! Lista("xz", "xzcf", "jaaj", 11, sklep)
}
