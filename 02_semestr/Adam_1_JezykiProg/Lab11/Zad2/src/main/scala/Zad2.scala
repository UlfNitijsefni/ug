import akka.actor.{ActorSystem, Actor, ActorRef, Props}

case class Wyslij(a: Double, b: Double, c: Double, serwer: ActorRef)
case class Sprawdz(a: Double, b: Double, c: Double)

case object Start

class Klient extends Actor {
  def receive: Receive = {
    case Wyslij(a, b, c, serwer) =>
      println("Pytam")
      serwer ! Sprawdz(a, b, c)
    case true  => println("Da się zbudowac trójkąt o bokach podanej długości.")
    case false => println("Niemożliwe jest zbudowanie trójkąta o bokach.")
  }
}

class Serwer extends Actor {
  def receive: Receive = {
    case Sprawdz(a, b, c) =>
      if (a + b > c && a + c > b && b + c > a) sender ! true
      else sender ! false
  }
}

object Main extends App {

  val system = ActorSystem("system")
  val serwer = system.actorOf(Props[Serwer])
  val klient = system.actorOf(Props[Klient])

  klient ! Wyslij(3, 4, 5, serwer)
}
