import akka.actor.{ActorSystem, Actor, ActorRef, Props, Revolver}

case object Ping
case object Pong

class Boss extends Actor {
  def Receive = {
    case Pong =>
      Thread.sleep(1000)
      println("Pong")
      self ! Ping
    case Ping =>
      Thread.sleep(1000)
      println("Ping")
      self ! Pong
  }
}

object Main extends App {
  val system = ActorSystem("PongPing")
  val boss = system.actorOf(Props[Boss], "pong")
  boss ! Ping
}
